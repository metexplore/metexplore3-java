### MetExplore 3 Java

#### Launch unit tests

```bash
mvn clean test
```

#### Launch integration tests

First, you have to create a .env file in the directory src/integration_test/resources with this format:

```config
MYSQL_DATABASE = 'metexplore_test'
MYSQL_HOST = 'localhost'
MYSQL_USER = 'metexplore'
MYSQL_PASS = '******'
```

Then : 

```bash
mvn clean verify
```

##### Creation of the jar with all dependencies for MetExplore

```bash
mvn clean package -DskipTests
```

##### Creation of the npm package

First, think to change the version in package.json according to the version in the pom.xml file.

```bash
mvn clean package -DskipTests
cd target
cp metexploreJava-0.3.9.jar metexplore3Java.jar
cd ..
export CI_JOB_TOKEN=cBZySZdsF3??????
npm publish
```

##### Update of the valid identifiers of identifiers.org

```
gron https://registry.api.identifiers.org/resolutionApi/getResolverDataset | fgrep .prefix |  sed -re 's/.*\"(.*)\";/\1/g' | sort > identifiersDbs.txt
```

