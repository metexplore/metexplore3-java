/**
 * Copyright INRA
 * <p>
 * Contact: ludovic.cottret@toulouse.inra.fr
 * <p>
 * <p>
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * <p>
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
*/

/**
 Test the class {@link BioNetworkToMetExplore}. The organism, collection and Network created during the process
 are removed after the tests.

 Runs the tests in the alphabetical order of their names (@FixMethodOrder(MethodSorters.NAME_ASCENDING))
 * 10 déc. 2013
 */
package fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.*;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.collection.BioCollection;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.network.NetworkAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.reaction.ReactionAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.BioUnitDefinitionCollection;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.Globals;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.BioNetworkToMetExploreDataBase;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.MetExploreDataBaseToBioNetwork;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.*;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreStringArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.comparison.CompareNetworks;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.database.MetExploreConnection;
import org.junit.*;
import org.junit.runners.MethodSorters;
import org.sbml.jsbml.text.parser.ParseException;

import java.sql.SQLException;
import java.util.*;
import java.util.stream.Collectors;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class BioNetworkToMetExploreIT {
    public static String dbConf = IntegrationTestsUtils.dbConf;
    private static String sourceId = "TEST_ECOLI";
    private static String sourceName = "test_ecol";
    private static String strain = "Test";
    private static String tissue = "";
    private static String cellType = "";
    private static String comments = "";
    private static int userId = -1;
    private static String dbVersion = "Test";
    private static BioNetwork network = null;
    private static MetExploreConnection connection = null;
    private static BioNetworkToMetExploreDataBase bntm;

    public static int dbId = 0;

    /**
     * Actions performed before all the tests
     *
     * @throws SQLException
     */
    @BeforeClass
    public static void beforeTests() throws SQLException, ParseException {


        try {
            connection = IntegrationTestsUtils.createConnection();
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Problem while accessing the metexplore database");
        }

        if (connection == null) {
            Assert.fail("Connection impossible to the metexplore database");
        }

        try {
            network = IntegrationTestsUtils.readMetExploreXmlFile("src/integration_test/resources/BioCyc18.0/ECOLI/completeNetwork.xml");
            bntm = IntegrationTestsUtils.createNetworkUploader(network, connection);
            dbId = bntm.dbId;
        } catch (Exception e) {
            Assert.fail("Network uploading failed");
        }
    }

    /**
     * Actions performed after all the tests
     *
     * @throws SQLException
     */
    @AfterClass
    public static void afterTests() throws SQLException {

        if(dbId != -1) {
            // Remove the collection and so the Network
            System.err.println("Remove collection " + dbId);
            MetExploreResult resRemoveDb = CollectionQueries.removecollection(
                    dbId, connection.getConnection());

            if (resRemoveDb.getIntError() == 1) {
                Assert.fail("Error while removing the collection " + dbId);
            }
            // Close the connection
//        try {
//            connection.close();
//        } catch (SQLException e) {
//            // TODO Auto-generated catch block
//            e.printStackTrace();
//            Assert.fail("Close the connection");
//        }

            try {
                bntm.closeConnection();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Assert.fail("Close the connection of bntm");
            }
        }

    }

    @Test
    public void test1_SetRefStatus() {

        System.out
                .println("Test setRefStatus");

        MetExploreResult resLoad = null;
        try {
            resLoad = bntm.setRefStatus();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.fail("Can't set ref status");
        }

        if (resLoad.getIntError() == 1) {
            Assert.fail(resLoad.getMessage());
        }

        System.out
                .println("Success Test setRefStatus");

    }

    @Test
    public void test2_SetRefScores() {

        System.out
                .println("Test setRefScores");

        MetExploreResult resLoad = null;
        try {
            resLoad = bntm.setRefScores();
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.fail("Impossible to set ref scores");
        }

        if (resLoad.getIntError() == 1) {
            Assert.fail(resLoad.getMessage());
        }

        System.out
                .println("Success Test setRefScores");
    }

    /**
     * test for
     * {@link BioNetworkToMetExploreDataBase#loadUnitDefinitions()}
     *
     * @throws InterruptedException
     * @throws SQLException
     */
    @Test
    public void test3_LoadUnitDefinitions() throws InterruptedException,
            SQLException {

        System.out
                .println("Test loadUnitDefinitions");

        MetExploreResult resLoad = bntm.loadUnitDefinitions();

        if (resLoad.getIntError() == 1) {
            Assert.fail(resLoad.getMessage());
        }

        // get the unit definitions
        BioUnitDefinitionCollection unitDefinitionsRef = NetworkAttributes.getUnitDefinitions(network);
        MetExploreIntegerArrayResult resUnitDefinitions = NetworkQueries
                .getUnitDefinitionSqlIds(bntm.id_network,
                        connection.getConnection());
        if (resUnitDefinitions.getIntError() == 1) {
            Assert.fail(resUnitDefinitions.getMessage());
        }

        // Compare the number of unit definitions
        Boolean flag = compareNumberObjects(unitDefinitionsRef,
                resUnitDefinitions, "unit definitions");

        MetExploreStringArrayResult resIdentifiers = UnitDefinitionQueries
                .getIdentifiersFromIdArray(resUnitDefinitions.results,
                        connection.getConnection());

        HashSet<String> udIdsTest = new HashSet<String>(resIdentifiers.results);

        Assert.assertEquals("not the same unit def", udIdsTest,
                unitDefinitionsRef.getIds());

        if (flag == false) {

            Assert.fail("Bad number of unit definitions : actual : "
                    + resUnitDefinitions.count() + " ; expected : "
                    + unitDefinitionsRef.size());
        }



        System.out
                .println(" Success Test loadUnitDefinitions");

    }

    /**
     * test for
     * {@link BioNetworkToMetExploreDataBase#loadCompartments()}
     *
     * @throws InterruptedException
     * @throws SQLException
     */
    @Test
    public void test4_LoadCompartments() throws InterruptedException,
            SQLException {

        System.out
                .println("Test loadCompartments");


        MetExploreResult resLoad = bntm.loadCompartments();

        if (resLoad.getIntError() == 1) {
            Assert.fail(resLoad.getMessage());
        }


        // get the compartments
        BioCollection<BioCompartment> compartmentsRef = network
                .getCompartmentsView();
        MetExploreIntegerArrayResult resCompartments = NetworkQueries
                .getCompartmentSqlIds(bntm.id_network,
                        connection.getConnection());
        if (resCompartments.getIntError() == 1) {
            Assert.fail(resCompartments.getMessage());
        }

        // Compare the number of compartments
        Boolean flag = compareNumberObjects(compartmentsRef, resCompartments,
                "compartments");

        if (flag == false) {

            Assert.fail("Bad number of compartments : actual : "
                    + resCompartments.count() + " ; expected : "
                    + compartmentsRef.size());
        }

        System.err.println("id compartments : "+resCompartments.results);


        MetExploreStringArrayResult resDbIds = CompartmentQueries
                .getIdentifiersFromid_compartment_inArray(
                        resCompartments.results, connection.getConnection());

        System.err.println("id compartments : "+resDbIds.results);

        HashSet<String> cptIdsTest = new HashSet<>(resDbIds.results.stream().map(v -> Globals.unformatId(v)).collect(Collectors.toSet()));

        Assert.assertEquals("Not the same compartment ids", cptIdsTest, compartmentsRef.getIds());


        System.out
                .println("Success Test loadCompartments");

    }

    /**
     * test for
     * {@link BioNetworkToMetExploreDataBase#loadMetabolites()}
     *
     * @throws InterruptedException
     * @throws SQLException
     */
    @Test
    public void test5_LoadMetabolites() throws InterruptedException,
            SQLException {

        System.out
                .println("Test loadMetabolites");

        // It's obligatory to load compartments before metabolites
        bntm.loadCompartments();

        MetExploreResult resLoad = bntm.loadMetabolites();

        if (resLoad.getIntError() == 1) {
            Assert.fail(resLoad.getMessage());
        }

        // get the metabolites
        BioCollection<BioMetabolite> metabolitesRef = network
                .getMetabolitesView();
        MetExploreIntegerArrayResult resMetabolites = NetworkQueries
                .getMetaboliteSqlIds(bntm.id_network,
                        connection.getConnection());
        if (resMetabolites.getIntError() == 1) {
            Assert.fail(resMetabolites.getMessage());
        }

        // Compare the number of metabolites
        Boolean flag = compareNumberObjects(metabolitesRef, resMetabolites,
                "metabolites");

        MetExploreStringArrayResult resDbIds = MetaboliteQueries
                .getdb_identifiersFromIdArray(resMetabolites.results,
                        connection.getConnection());

        HashSet<String> cpdIdsRef = new HashSet<String>(resDbIds.results.stream().map(id -> Globals.unformatId(id)).collect(Collectors.toSet()));

        Assert.assertEquals("Not the same metabolite ids", cpdIdsRef,
                metabolitesRef.getIds());

        if (flag == false) {

            Assert.fail("Bad number of metabolites : actual : "
                    + resMetabolites.count() + " ; expected : "
                    + metabolitesRef.size());
        }

        for (int id_metabolite : resMetabolites.results) {
            MetExploreStringArrayResult reqdb_identifier = MetaboliteQueries
                    .getdb_identifier(id_metabolite, connection.getConnection());
            if (reqdb_identifier.getIntError() == 1) {
                Assert.fail("Impossible to get the db identifier of the metabolite id="
                        + id_metabolite);
            }

            String metabolitedb_identifier = Globals.unformatId(reqdb_identifier.get(0));

            BioMetabolite metaboliteRef = network.getMetabolitesView()
                    .get(metabolitedb_identifier);

            if (metaboliteRef == null) {
                Assert.fail("The metabolite " + metabolitedb_identifier
                        + " does not exist in the reference network");
            }

            // test name
            String metaboliteNameRef = metaboliteRef.getName();

            MetExploreStringArrayResult reqName = MetaboliteQueries.getName(
                    id_metabolite, connection.getConnection());
            if (reqName.getIntError() == 1) {
                Assert.fail("Impossible to get the name of the metabolite id="
                        + id_metabolite);
            }

            String metaboliteNameTest = reqName.get(0);

            Assert.assertEquals("Test name of " + metabolitedb_identifier,
                    metaboliteNameRef, metaboliteNameTest);

            // Test Formula
            String formulaRef = metaboliteRef.getChemicalFormula();
            MetExploreStringArrayResult reqFormula = MetaboliteQueries
                    .getFormula(id_metabolite, connection.getConnection());

            if (reqFormula.getIntError() == 1) {
                Assert.fail("Impossible to get the formula of the metabolite id="
                        + id_metabolite);
            }

            String formulaTest = reqFormula.get(0);

            Assert.assertEquals("Test formula of " + metabolitedb_identifier,
                    formulaRef, formulaTest);

            // Test weight
            Double weightRef = metaboliteRef.getMolecularWeight();
            ;
            MetExploreStringArrayResult reqWeight = MetaboliteQueries
                    .getMolecularWeight(id_metabolite,
                            connection.getConnection());

            if (reqWeight.getIntError() == 1) {
                Assert.fail("Impossible to get the weight of the metabolite id="
                        + id_metabolite);
            }

            String weightTest = reqWeight.get(0) == null ? null : reqWeight.get(0).replaceAll("[^0-9\\.]", "");

            Double weightFloatTest = null;
            if (weightTest != null) {
                try {
                    weightFloatTest = Double.parseDouble(weightTest);
                } catch (NumberFormatException e) {
                    System.err
                            .println("[Test Warning] The weight for the metabolite "
                                    + metabolitedb_identifier
                                    + "can't be transformed in float (ref : "
                                    + weightRef + ", test : " + weightTest);
                }
            }

            Assert.assertEquals("Test weight (float) of the metabolite "
                            + metabolitedb_identifier, weightRef,
                    weightFloatTest);

            // test charge
            int chargeRef = metaboliteRef.getCharge();
            MetExploreIntegerArrayResult reqCharge = MetaboliteQueries
                    .getCharge(id_metabolite, connection.getConnection());

            if (reqCharge.getIntError() == 1) {
                Assert.fail("Impossible to get the weight of the charge id="
                        + id_metabolite);
            }

            int chargeTest = reqCharge.get(0);

            Assert.assertEquals("Test charge of " + metabolitedb_identifier,
                    chargeRef, chargeTest);

        }

        System.out
                .println("Success Test loadMetabolites");

    }

    /**
     * test for {@link BioNetworkToMetExploreDataBase#loadGenes()}
     *
     * @throws InterruptedException
     * @throws SQLException
     */
    @Test
    public void test6_LoadGenes() throws InterruptedException, SQLException {

        bntm.getCon().setAutoCommit(false);
        MetExploreResult resLoad = bntm.loadGenes();

        if (resLoad.getIntError() == 1) {
            Assert.fail(resLoad.getMessage());
        }

        // get the genes
        BioCollection<BioGene> genesRef = network.getGenesView();
        MetExploreIntegerArrayResult resGenes = NetworkQueries.getGenes(
                bntm.id_network, connection.getConnection());
        if (resGenes.getIntError() == 1) {
            Assert.fail(resGenes.getMessage());
        }

        // Compare the number of genes
        Boolean flag = compareNumberObjects(genesRef, resGenes, "genes");

        MetExploreStringArrayResult resDbIds = GeneQueries
                .getdb_identifiersFromIdArray(resGenes.results,
                        connection.getConnection());

        HashSet<String> geneIdsRef = new HashSet<String>(resDbIds.results.stream().map(v -> Globals.unformatId(v)).collect(Collectors.toSet()));

        Assert.assertEquals("Not the same gene ids", geneIdsRef,
                genesRef.getIds());

        if (flag == false) {
            Assert.fail("Bad number of genes : actual : " + resGenes.count()
                    + " ; expected : " + genesRef.size());
        }

        // Comparison of gene attributes
        for (int id_gene : resGenes.results) {
            MetExploreStringArrayResult reqGeneId = GeneQueries
                    .getdb_identifier(id_gene, connection.getConnection());
            if (reqGeneId.getIntError() == 1) {
                Assert.fail("Impossible to get the db_identifier of the gene id="
                        + id_gene);
            }

            String geneId = reqGeneId.get(0);

            BioGene geneRef = network.getGene(Globals.unformatId(geneId));
            if (geneRef == null) {
                Assert.fail("The gene " + geneId
                        + " does not exist in the reference network");
            }

            // Compare the name of the gene
            String nameRef = geneRef.getName();

            MetExploreStringArrayResult reqGeneName = GeneQueries.getName(
                    id_gene, connection.getConnection());

            if (reqGeneName.getIntError() == 1) {
                Assert.fail("Impossible to get the name of the gene id="
                        + id_gene);
            }

            String geneNameTest = reqGeneName.get(0);

            Assert.assertEquals("Test name of " + geneId, nameRef, geneNameTest);

        }

        System.out
                .println("Success Test loadGenes");

    }

    /**
     * test for {@link BioNetworkToMetExploreDataBase#loadProteins()}
     *
     * @throws InterruptedException
     * @throws SQLException
     */
    @Test
    public void test7_LoadProteins() throws InterruptedException, SQLException {
        System.out
                .println("Test loadProteins");
        MetExploreResult resLoad = bntm.loadProteins();

        if (resLoad.getIntError() == 1) {
            Assert.fail(resLoad.getMessage());
        }


        // get the proteins
        BioCollection<BioProtein> refs = network.getProteinsView();
        MetExploreIntegerArrayResult resGet = NetworkQueries.getProteins(
                bntm.id_network, connection.getConnection());
        if (resGet.getIntError() == 1) {
            Assert.fail(resGet.getMessage());
        }

        // Compare the number of proteins
        Boolean flag = compareNumberObjects(refs, resGet, "proteins");

        MetExploreStringArrayResult resDbIds = ProteinQueries
                .getdb_identifiersFromIdArray(resGet.results,
                        connection.getConnection());

        HashSet<String> idsRef = new HashSet<String>(resDbIds.results.stream().map(id -> Globals.unformatId(id)).collect(Collectors.toSet()));

        Assert.assertEquals("Not the same protein identifiers", idsRef, refs.getIds());

        if (flag == false) {
            Assert.fail("Bad number of proteins : actual : " + resGet.count()
                    + " ; expected : " + refs.size());
        }

        // Comparison of protein attributes
        for (int id : resGet.results) {

            MetExploreStringArrayResult reqIdentifier = ProteinQueries
                    .getdb_identifier(id, connection.getConnection());
            if (reqIdentifier.getIntError() == 1) {
                Assert.fail("Impossible to get the db_identifier of the protein id="
                        + id);
            }

            String identifier = reqIdentifier.get(0);

            BioProtein ref = network.getProtein(Globals.unformatId(identifier));
            if (ref == null) {
                Assert.fail("The protein " + identifier
                        + " does not exist in the reference network");
            }

            // Compare the name of the protein
            String nameRef = ref.getName();

            MetExploreStringArrayResult reqName = ProteinQueries.getName(id,
                    connection.getConnection());

            if (reqName.getIntError() == 1) {
                Assert.fail("Impossible to get the name of the protein id="
                        + id);
            }

            String nameTest = reqName.get(0);

            Assert.assertEquals("Test name of " + identifier, nameRef, nameTest);

            // Checks the genes
            Set<String> genesRef = new HashSet<String>();

            if (ref.getGene() != null) {
                genesRef.add(ref.getGene().getId());
            }

            MetExploreStringArrayResult reqGenes = ProteinQueries
                    .getGenedb_identifiers(id, bntm.id_network,
                            connection.getConnection());

            if (reqGenes.getIntError() == 1) {
                Assert.fail(reqGenes.message);
            }
            Set<String> genesTest = new HashSet<String>(reqGenes.results.stream().map(v -> Globals.unformatId(v)).collect(Collectors.toSet()));
            Assert.assertEquals("Test genes of " + identifier, genesRef,
                    genesTest);

        }

        System.out
                .println("Success Test loadProteins");

    }

    /**
     * test for {@link BioNetworkToMetExploreDataBase#loadEnzymes()}
     *
     * @throws InterruptedException
     * @throws SQLException
     */
    @Test
    public void test8_LoadEnzymes() throws InterruptedException, SQLException {
        System.out
                .println("Test loadEnzymes ");


        MetExploreResult resLoad = bntm.loadEnzymes();

        if (resLoad.getIntError() == 1) {
            Assert.fail(resLoad.getMessage());
        }


        // get the enzymes
        BioCollection<BioEnzyme> refs = network.getEnzymesView();
        MetExploreIntegerArrayResult resGet = NetworkQueries.getEnzymes(
                bntm.id_network, connection.getConnection());
        if (resGet.getIntError() == 1) {
            Assert.fail(resGet.getMessage());
        }

        // Compare the number of enzymes
        Boolean flag = compareNumberObjects(refs, resGet, "enzymes");

        MetExploreStringArrayResult resDbIds = EnzymeQueries
                .getdb_identifiersFromIdArray(resGet.results,
                        connection.getConnection());

        HashSet<String> idsRef = new HashSet<String>(resDbIds.results.stream().map(id -> Globals.unformatId(id)).collect(Collectors.toSet()));

        Assert.assertEquals("Not the same enzyme identifiers", idsRef, refs.getIds());

        if (flag == false) {
            Assert.fail("Bad number of enzymes : actual : " + resGet.count()
                    + " ; expected : " + refs.size());
        }

        // Comparison of enzyme attributes
        for (int id : resGet.results) {

            MetExploreStringArrayResult reqIdentifier = EnzymeQueries
                    .getdb_identifier(id, connection.getConnection());
            if (reqIdentifier.getIntError() == 1) {
                Assert.fail("Impossible to get the db_identifier of the enzyme id="
                        + id);
            }

            String identifier = Globals.unformatId(reqIdentifier.get(0));

            BioEnzyme enzymeRef = network.getEnzyme(identifier);
            if (enzymeRef == null) {
                Assert.fail("The enzyme " + identifier
                        + " does not exist in the reference network");
            }

            // Compare the name of the enzyme
            String nameRef = enzymeRef.getName();

            MetExploreStringArrayResult reqName = EnzymeQueries.getName(id,
                    connection.getConnection());

            if (reqName.getIntError() == 1) {
                Assert.fail("Impossible to get the name of the enzyme id=" + id);
            }

            String nameTest = reqName.get(0);

            Assert.assertEquals("Test name of " + identifier, nameRef, nameTest);

            // Checks the proteins involved in the enzymes
            Set<String> proteinsRef = enzymeRef.getParticipantsView().stream().map(e -> e.getPhysicalEntity().getId()).collect(Collectors.toSet());

            MetExploreStringArrayResult reqProteins = EnzymeQueries
                    .getProteindb_identifiers(id, bntm.id_network,
                            connection.getConnection());

            if (reqProteins.getIntError() == 1) {
                Assert.fail(reqProteins.message);
            }
            Set<String> proteinsTest = new HashSet<String>(reqProteins.results.stream().map(id_protein -> Globals.unformatId(id_protein)).collect(Collectors.toSet()));
            Assert.assertEquals("Test proteins of " + identifier, proteinsRef,
                    proteinsTest);

        }

        System.out
                .println("Success Test loadEnzymes");

    }

    /**
     * test for {@link BioNetworkToMetExploreDataBase#loadPathways()}
     *
     * @throws InterruptedException
     * @throws SQLException
     */
    @Test
    public void test9_LoadPathways() throws InterruptedException, SQLException {
        System.out
                .println("Test loadPathways ");


        MetExploreResult resLoad = bntm.loadPathways();

        if (resLoad.getIntError() == 1) {
            Assert.fail(resLoad.getMessage());
        }

        System.out
                .println("Success Test loadPathways");
        // TODO : compare pathways


    }

    /**
     * test for
     * {@link BioNetworkToMetExploreDataBase#loadReactions()}
     *
     * @throws InterruptedException
     * @throws SQLException
     */
    @Test
    public void testA_LoadReactions() throws InterruptedException, SQLException {


        bntm.loadCompartments();
        bntm.loadMetabolites();
        bntm.loadGenes();
        bntm.loadProteins();
        bntm.loadEnzymes();

        MetExploreResult resLoad = bntm.loadReactions();

        if (resLoad.getIntError() == 1) {
            Assert.fail(resLoad.getMessage());
        }

        // get the reactions
        BioCollection<BioReaction> reactionsRef = network.getReactionsView();
        MetExploreIntegerArrayResult resReactions = NetworkQueries
                .getReactions(bntm.id_network, connection.getConnection());
        if (resReactions.getIntError() == 1) {
            Assert.fail(resReactions.getMessage());
        }

        // Compare the number of reactions
        Boolean flag = compareNumberObjects(reactionsRef, resReactions,
                "reactions");

        MetExploreStringArrayResult resDbIds = ReactionQueries
                .getdb_identifiersFromIdArray(resReactions.results,
                        connection.getConnection());

        HashSet<String> reactionIdsRef = new HashSet<>(resDbIds.results.stream().map(v -> Globals.unformatId(v)).collect(Collectors.toSet()));

        Assert.assertEquals("Not the same reaction identifiers", reactionIdsRef, reactionsRef.getIds());

        if (flag == false) {
            Assert.fail("Bad number of reactions : actual : "
                    + resReactions.count() + " ; expected : "
                    + reactionsRef.size());
        }

        // Comparison of reaction attributes
        for (int id_reaction : resReactions.results) {
            MetExploreStringArrayResult reqRxnId = ReactionQueries
                    .getdb_identifier(id_reaction, connection.getConnection());
            if (reqRxnId.getIntError() == 1) {
                Assert.fail("Impossible to get the db_identifier of the reaction id="
                        + id_reaction);
            }

            String rxnId = Globals.unformatId(reqRxnId.get(0));

            BioReaction reactionRef = network
                    .getReaction(rxnId);
            if (reactionRef == null) {
                Assert.fail("The reaction " + rxnId
                        + " does not exist in the reference network");
            }

            // compare the name of the reactions
            String rxnNameRef = reactionRef.getName();
            MetExploreStringArrayResult reqRxnName = ReactionQueries.getName(
                    id_reaction, connection.getConnection());

            if (reqRxnName.getIntError() == 1) {
                Assert.fail("Impossible to get the name of the reaction id="
                        + id_reaction);
            }

            String rxnNameTest = reqRxnName.get(0);

            Assert.assertEquals("Test name of " + rxnId, rxnNameRef,
                    rxnNameTest);

            // compare the ec of the reactions
            String ecNumberRef = reactionRef.getEcNumber();
            MetExploreStringArrayResult reqRxnEc = ReactionQueries.getEc(
                    id_reaction, connection.getConnection());

            if (reqRxnEc.getIntError() == 1) {
                Assert.fail("Impossible to get the ec of the reaction id="
                        + id_reaction);
            }

            String ecNumberTest = reqRxnEc.get(0);

            Assert.assertEquals("Test ec of " + rxnId, ecNumberRef,
                    ecNumberTest);

            // Compare the type
            String typeRef = ReactionAttributes.getType(reactionRef);
            MetExploreStringArrayResult reqType = ReactionQueries.getType(
                    id_reaction, connection.getConnection());

            if (reqType.getIntError() == 1) {
                Assert.fail("Impossible to get the type of the reaction id="
                        + id_reaction);
            }

            String typeTest = reqType.get(0);

            Assert.assertEquals("Test type of " + rxnId, typeRef, typeTest);

            // Compare the generic flag
            Boolean genericRef = ReactionAttributes.getGeneric(reactionRef);
            MetExploreIntegerArrayResult reqGeneric = ReactionQueries
                    .getGeneric(id_reaction, connection.getConnection());

            if (reqGeneric.getIntError() == 1) {
                Assert.fail("Impossible to get the generic flag of the reaction id="
                        + id_reaction);
            }

            int genericAsInteger = reqGeneric.get(0);
            Boolean genericTest = false;

            if (genericAsInteger == 1) {
                genericTest = true;
            }

            Assert.assertEquals("Test generic flag of " + rxnId, genericRef,
                    genericTest);

            // Compare the lefts
            Set<String> leftsRef = reactionRef.getLeftsView().getIds();
            MetExploreStringArrayResult reqLeft = ReactionQueries
                    .getLeftdb_identifiers(id_reaction,
                            connection.getConnection());

            if (reqLeft.getIntError() == 1) {
                Assert.fail("Impossible to get the lefts of the reaction id="
                        + id_reaction);
            }
            Set<String> leftsTest = new HashSet<String>(reqLeft.results.stream().map(Globals::unformatId).collect(Collectors.toSet()));
            Assert.assertEquals("Test lefts of " + rxnId, leftsRef, leftsTest);

            // Compare the rights
            Set<String> rightsRef = reactionRef.getRightsView().getIds();
            MetExploreStringArrayResult reqRight = ReactionQueries
                    .getRightdb_identifiers(id_reaction,
                            connection.getConnection());

            if (reqRight.getIntError() == 1) {
                Assert.fail("Impossible to get the rights of the reaction id="
                        + id_reaction);
            }
            Set<String> rightsTest = reqRight.results.stream().map(Globals::unformatId).collect(Collectors.toSet());
            Assert.assertEquals("Test rights of " + rxnId, rightsRef,
                    rightsTest);

            // Compare the sides
            MetExploreStringArrayResult reqSides = ReactionQueries
                    .getSidedb_identifiers(id_reaction,
                            connection.getConnection());

            if (reqSides.getIntError() == 1) {
                Assert.fail("Impossible to get the sides of the reaction id="
                        + id_reaction);
            }
        }

        System.out
                .println("Success Test loadReactions");

    }

    /**
     * Use MetExploreToBioNetwork and compare the two networks Be careful, this
     * means that MetExploreToBioNetwork works well !
     *
     * Test method for
     * {@link BioNetworkToMetExploreDataBase#toMetExplore()} .
     *
     * @throws InterruptedException
     */
    @Test
    public void testB_ToMetExplore() throws Exception {

        System.out
                .println("Test toMetExplore");

        try {
            bntm = new BioNetworkToMetExploreDataBase(network,
                    connection.getConnection(), dbId, sourceName, sourceId, 1289,
                    strain, tissue, cellType,
                    dbVersion, "NA", userId, comments, "NA", "NA");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            Assert.fail("Sql Problem in Instanciation of BioNetworkToMetExplore from source as string");
        }


        try {
            bntm.toMetExplore();
        } catch (SQLException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
            Assert.fail("Sql problem in toMetExplore");
        }

        MetExploreResult result = bntm.getResult();

        if (result.getIntError() == 1) {
            Assert.fail(result.getMessage());
        }

        int id_network = bntm.id_network;

        System.err.println("id_network : " + id_network);

        MetExploreDataBaseToBioNetwork mtbn = new MetExploreDataBaseToBioNetwork();

        try {
            mtbn.Connection(dbConf);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Impossible to create the connection in MetExploreToBioNetwork");
        }

        mtbn.setid_network(id_network);

        mtbn.convert();

        BioNetwork networkTest = mtbn.getNetwork();

        CompareNetworks test = new CompareNetworks(network,
                networkTest);

        test.testAll();

        System.out
                .println("Success Test toMetExplore");

    }

    /**
     * Compare number of objects in a HashMap from a BioNetwork and a
     * MetExploreIntegerArrayResult
     *
     * @param refObjects
     * @param queryResult
     * @param objectName
     */
    private Boolean compareNumberObjects(HashMap<String, ?> refObjects,
                                         MetExploreIntegerArrayResult queryResult, String objectName) {

        int nRef = refObjects.size();
        int nTest = queryResult.count();

        if (nRef != nTest) {
            return false;
        } else {
            return true;
        }

    }

    private Boolean compareNumberObjects(BioCollection<?> refObjects,
                                         MetExploreIntegerArrayResult queryResult, String objectName) {

        int nRef = refObjects.size();
        int nTest = queryResult.count();

        if (nRef != nTest) {
            return false;
        } else {
            return true;
        }

    }

}
