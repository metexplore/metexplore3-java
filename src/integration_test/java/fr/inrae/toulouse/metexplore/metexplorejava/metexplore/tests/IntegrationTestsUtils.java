package fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.reader.JsbmlReader;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.reader.Met4jSbmlReaderException;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.BioNetworkToMetExploreDataBase;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.CollectionQueries;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.UserQueries;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.database.MetExploreConnection;
import fr.inrae.toulouse.metexplore.metexplorexml.reader.MetexploreXmlReader;
import org.junit.Assert;
import org.sbml.jsbml.text.parser.ParseException;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;


public class IntegrationTestsUtils {
    public static String dbConf = "src/integration_test/resources/.env";
    public static String userName = "usertest1";




    /**
     * Creates a new collection in the database with the given description.
     *
     * @param description the description of the collection to be created
     * @return the ID of the created collection
     * @throws SQLException if a database access error occurs
     * @throws IOException if an I/O error occurs
     * @throws ClassNotFoundException if the JDBC class is not found
     */
    public static int createCollection(String description) throws SQLException, IOException, ClassNotFoundException {
        MetExploreConnection connection = createConnection();

        MetExploreIntegerResult res = CollectionQueries.createCollection("test", description,
                getUserId(connection), false, connection.getConnection());

        if(res.getIntError() == 1) {
            throw new Error("Error while creating collection");
        }

        return res.get();

    }

    /**
     * Remove a collection
     *
     * @param idDb
     * @param con
     * @throws SQLException
     */
    public static void removecollection(int idDb, Connection con) throws SQLException {
        MetExploreResult resRemoveDb = CollectionQueries.removecollection(
                idDb, con);

        if (resRemoveDb.getIntError() == 1) {
            Assert.fail("Error while removing the collection " + idDb);
        }
    }

    /**
     * Create a MetExploreConnection
     *
     * @return a {@link MetExploreConnection}
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static MetExploreConnection createConnection() throws SQLException, IOException, ClassNotFoundException {
        MetExploreConnection connection = null;

        int dbId = -1;

        connection = new MetExploreConnection(dbConf);
        return connection;
    }

    /**
     * Load a test network in the database
     *
     * @param network
     * @return the id of the created collection
     * @throws Met4jSbmlReaderException
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    public static int loadNetworkInDatabase(BioNetwork network, MetExploreConnection connection) throws Met4jSbmlReaderException, SQLException, IOException, ClassNotFoundException, InterruptedException {

        int dbId = -1;
        BioNetworkToMetExploreDataBase bntm = null;

        try {
            bntm = createNetworkUploader(network, connection);
            bntm.toMetExplore();
            dbId = bntm.dbId;
        } catch (Exception e) {
            System.err.println("Problem while importing the network");
            if (dbId != -1)
                removecollection(dbId, connection.getConnection());
        }
        return dbId;
    }

    /**
     * Load the network in a MetExploreXml file in the database
     *
     * @param xmlFile
     * @param connection
     * @return
     * @throws ParseException
     * @throws Met4jSbmlReaderException
     * @throws SQLException
     * @throws IOException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    public static int loadMetExploreXmlFileInDatabase(String xmlFile, MetExploreConnection connection) throws ParseException, Met4jSbmlReaderException, SQLException, IOException, ClassNotFoundException, InterruptedException {
        BioNetwork network = readMetExploreXmlFile(xmlFile);
        return loadNetworkInDatabase(network, connection);
    }

    /**
     * Read a MetExplore Xml file
     *
     * @param xmlFile
     * @return a {@link BioNetwork}
     * @throws ParseException
     */
    public static BioNetwork readMetExploreXmlFile(String xmlFile) throws ParseException {
        MetexploreXmlReader reader = new MetexploreXmlReader(xmlFile);
        reader.read();
        return reader.getNetwork();
    }

    /**
     * Read a Sbml file
     *
     * @param xmlFile
     * @return a {@link BioNetwork}
     * @throws ParseException
     */
    public static BioNetwork readSbmlFile(String xmlFile) throws Met4jSbmlReaderException {
        JsbmlReader reader = new JsbmlReader(xmlFile);
        return reader.read();
    }

    /**
     * @param network
     * @param connection
     * @return
     * @throws SQLException
     */
    public static BioNetworkToMetExploreDataBase createNetworkUploader(BioNetwork network, MetExploreConnection connection) throws SQLException {

        BioNetworkToMetExploreDataBase bntm = null;
        int dbId = -1;

        try {
            Connection con = connection.getConnection();

            // Create collection
            MetExploreIntegerResult collectionRes = CollectionQueries.createCollection("testMetexploreJava", "desc", -1, false, con);

            if (collectionRes.getIntError() == 1) {
                Assert.fail("Problem while creating collection");
            }

            dbId = collectionRes.get();

            // Get user id
            int userId = getUserId(connection);

            bntm = new BioNetworkToMetExploreDataBase(network,
                    connection.getConnection(), dbId, "test", "test",
                    1289, "", "" , "",
                    "", "NA", userId, "", "", "");

        } catch (Exception e) {
            System.err.println("Problem while importing the network : "+e.getMessage());
            e.printStackTrace();
            if (dbId != -1) {
                removecollection(dbId, connection.getConnection());
            }
        }
        return bntm;
    }

    private static int getUserId(MetExploreConnection connection) throws SQLException {
        MetExploreIntegerArrayResult resUser = UserQueries.getUserIdByUserName(
                userName, connection.getConnection());

        if (resUser.count() == 0) {
            throw new Error("No user " + userName + " in the database");
        }

        int userId = resUser.get(0);
        return userId;
    }

}
