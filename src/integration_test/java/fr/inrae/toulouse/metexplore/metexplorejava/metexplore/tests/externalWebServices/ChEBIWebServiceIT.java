package fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.externalWebServices;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import fr.inrae.toulouse.metexplore.metexplorejava.webServices.ChEBIWebService;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ChEBIWebServiceIT {


    public static ChEBIWebService chEBIWS;
    public static HashMap<String, ArrayList<String>> dataForJSon;


    @BeforeClass
    public static void setParam() {
        chEBIWS = new ChEBIWebService("https://www.ebi.ac.uk/webservices/chebi/2.0/test/");
        chEBIWS.setChebi("29985");
    }


    @Test
    public void A_DataRetrieval() {

        dataForJSon = chEBIWS.getData();

        assertFalse("Could not find metabolite data, hashMap is empty.", dataForJSon.isEmpty());
    }


    @Test
    public void B_testData() {
        assertTrue("No mass in Hash map.", dataForJSon.containsKey("mass"));
        ArrayList<String> expectedValues = new ArrayList<String>(Arrays.asList("146.12136"));
        assertEquals("Invalid mass.", expectedValues, dataForJSon.get("mass"));

        assertTrue("No charge in Hash map.", dataForJSon.containsKey("charge"));
        expectedValues = new ArrayList<String>(Arrays.asList("-1"));
        assertEquals("Invalid charge.", expectedValues, dataForJSon.get("charge"));

        assertTrue("No name in Hash map.", dataForJSon.containsKey("chebiAsciiName"));
        expectedValues = new ArrayList<String>(Arrays.asList("L-glutamate(1-)"));
        assertEquals("Invalid Name", expectedValues, dataForJSon.get("chebiAsciiName"));

        assertTrue("No Formulae in Hash map.", dataForJSon.containsKey("Formulae"));
        expectedValues = new ArrayList<String>(Arrays.asList("C5H8NO4"));
        assertEquals("Invalid Formulae", expectedValues, dataForJSon.get("Formulae"));

        assertTrue("No inchi in Hash map.", dataForJSon.containsKey("inchi"));
        expectedValues = new ArrayList<String>(Arrays.asList("InChI=1S/C5H9NO4/c6-3(5(9)10)1-2-4(7)8/h3H,1-2,6H2,(H,7,8)(H,9,10)/p-1/t3-/m0/s1"));
        assertEquals("Invalid inchi", expectedValues, dataForJSon.get("inchi"));

        assertTrue("No inchiKey in Hash map.", dataForJSon.containsKey("inchiKey"));
        expectedValues = new ArrayList<String>(Arrays.asList("WHUUTDBJXJRKMK-VKHMYHEASA-M"));
        assertEquals("Invalid inchiKey", expectedValues, dataForJSon.get("inchiKey"));

        assertTrue("No smiles in Hash map.", dataForJSon.containsKey("smiles"));
        expectedValues = new ArrayList<String>(Arrays.asList("[NH3+][C@@H](CCC([O-])=O)C([O-])=O"));
        assertEquals("Invalid smiles", expectedValues, dataForJSon.get("smiles"));
    }

}
