package fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.externalWebServices;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import fr.inrae.toulouse.metexplore.metexplorejava.webServices.PubMebWebservice;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class PubMedWebServiceIT {

    public static PubMebWebservice pubMebWS;
    public static HashMap<String, ArrayList<String>> dataForJSon;


    @BeforeClass
    public static void setParam() {
        pubMebWS = new PubMebWebservice();
        pubMebWS.setPMID("1");
    }


    @Test
    public void A_DataRetrieval() {

        dataForJSon = pubMebWS.getData();

        assertFalse("Could not find article data, hashMap is empty.", dataForJSon.isEmpty());
    }

    @Test
    public void B_testData() {
        assertTrue("No Author in Hash map.", dataForJSon.containsKey("Author"));
        ArrayList<String> expectedValues = new ArrayList<String>(Arrays.asList("Makar AB", "McMartin KE", "Palese M", "Tephly TR"));
        assertEquals("Invalid Author list", expectedValues, dataForJSon.get("Author"));

        assertTrue("No Title in Hash map.", dataForJSon.containsKey("Title"));
        expectedValues = new ArrayList<String>(Arrays.asList("Formate assay in body fluids: application in methanol poisoning."));
        assertEquals("Invalid Publication title", expectedValues, dataForJSon.get("Title"));

        assertTrue("No journal name in Hash map.", dataForJSon.containsKey("FulljournalName"));
        expectedValues = new ArrayList<String>(Arrays.asList("Biochemical medicine"));
        assertEquals("Invalid journal Name", expectedValues, dataForJSon.get("FulljournalName"));

        assertTrue("No Publication date in Hash map.", dataForJSon.containsKey("PubDate"));
        expectedValues = new ArrayList<String>(Arrays.asList("1975"));
        assertEquals("Invalid Publication date", expectedValues, dataForJSon.get("PubDate"));
    }
}
