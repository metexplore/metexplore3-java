package fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.gui.Export;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;

import fr.inrae.toulouse.metexplore.met4j_io.jsbml.reader.Met4jSbmlReaderException;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.BioNetworkToMetExploreDataBase;
import fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.IntegrationTestsUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.CollectionQueries;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.database.MetExploreConnection;
import fr.inrae.toulouse.metexplore.metexplorexml.reader.MetexploreXmlReader;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.comparison.CompareNetworks;
import org.apache.commons.io.FilenameUtils;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;

import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Export.ExportAsExtendedSbml;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import org.sbml.jsbml.text.parser.ParseException;

import static org.junit.Assert.*;

public class ExportAsExtendedSbmlIT {

    static String testFile = "src/integration_test/resources/BioCyc18.0/ECOLI/completeNetwork.xml";
    static int id_network = -1;
    static int dbId = -1;
    private static MetExploreConnection connection;
    public static String hostTmpPath = "/tmp";
    public static String hostTmpUrl = "fakeUrl";

    public static BioNetwork network = null;

    private static File tempSbmlFileTest = null;

    /**
     * Actions performed before all the tests
     */
    @BeforeClass
    public static void beforeTests() throws SQLException, IOException, ClassNotFoundException, Met4jSbmlReaderException, ParseException, InterruptedException {

        // Load MetExploreXml in the database
        connection = IntegrationTestsUtils.createConnection();
        network = IntegrationTestsUtils.readMetExploreXmlFile(testFile);
        BioNetworkToMetExploreDataBase networkUploader = IntegrationTestsUtils.createNetworkUploader(network, connection);
        networkUploader.toMetExplore();

        dbId = networkUploader.dbId;
        id_network = networkUploader.id_network;

        java.nio.file.Path tmpPath = null;
        try {
            tmpPath = java.nio.file.Files.createTempFile(
                    "test_metexploreJava", ".tmp");
        } catch (IOException e1) {
            Assert.fail("Creation of the temporary files");
        }

        tempSbmlFileTest = tmpPath.toFile();
    }

    @AfterClass
    public static void afterTests() throws SQLException {
        if (tempSbmlFileTest != null)
            tempSbmlFileTest.delete();

        if (dbId != -1) {
            MetExploreResult resRemoveDb = CollectionQueries.removecollection(
                    dbId, connection.getConnection());

            if (resRemoveDb.getIntError() == 1) {
                Assert.fail("Error while removing the collection " + dbId);
            }
        }

        // Close the connection
        if(connection != null) {
            try {
                connection.close();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Assert.fail("Close the connection");
            }
        }

    }

    @Test
    public void test() throws Exception {

        ExportAsExtendedSbml f = new ExportAsExtendedSbml();

        f.setDbConf(IntegrationTestsUtils.dbConf);

        f.setid_network(id_network);
        f.setTmpPath(hostTmpPath);
        f.setUrlTmpPath(hostTmpUrl);

        File tempFile = File.createTempFile("export", ".json");
        f.jsonFile = tempFile.getAbsolutePath();

        Boolean flag = f.compute();

        if (!flag) {
            Assert.fail("Problem while exporting the Network");
        }

        String path = hostTmpPath + "/" + FilenameUtils.getName(f.url);
        MetexploreXmlReader reader = new MetexploreXmlReader(path);

        reader.read();

        BioNetwork networkTest = reader.getNetwork();

        assertNotNull(networkTest);

        CompareNetworks test = new CompareNetworks(network,
                networkTest);

        test.testNumberEnzymes();
        test.testNumberGenes();
        test.testNumberPathways();
        test.testNumberMetabolites();
        test.testNumberReactions();
        test.testNumberProteins();
        test.testNumberCompartments();
        test.testNumberUnitDefinitions();

    }

}
