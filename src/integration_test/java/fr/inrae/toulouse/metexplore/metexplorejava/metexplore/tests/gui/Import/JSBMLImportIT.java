package fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.gui.Import;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.reader.Met4jSbmlReaderException;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Import.JSBMLImport;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.BioNetworkToMetExploreDataBase;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.MetExploreDataBaseToBioNetwork;
import fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.IntegrationTestsUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.CollectionQueries;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.comparison.CompareNetworks;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.database.MetExploreConnection;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.sbml.jsbml.text.parser.ParseException;

import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class JSBMLImportIT {

    static String testFile = "src/integration_test/resources/sbml/iAf1260.xml";
    static int id_network = -1;
    static int dbId = -1;
    public static String dbconf = IntegrationTestsUtils.dbConf;

    public static int id_user = 120;
    public static int idOrg = 42; // Cystobacter
    public static BioNetworkToMetExploreDataBase networkUploader = null;
    public static BioNetwork network = null;

    /**
     * Actions performed before all the tests
     */
    @BeforeClass
    public static void beforeTests() throws IOException, SQLException, ClassNotFoundException, Met4jSbmlReaderException {
        dbId = IntegrationTestsUtils.createCollection("bigg");
        network = IntegrationTestsUtils.readSbmlFile(testFile);

        JSBMLImport f = new JSBMLImport();

        f.setDbConf(IntegrationTestsUtils.dbConf);

        f.useFBC2Plugin = true;
        f.useGroupPlugin = true;
        f.useAnnotPlugin = true;
        f.sendMail = false;
        File tempFile = File.createTempFile("import", ".json");
        f.jsonFile = tempFile.getAbsolutePath();

        f.networkFile = testFile;
        f.ncbiId = idOrg;
        f.idUser = id_user;
        f.dbId= dbId;

        Boolean flag = f.compute();

        if (!flag) {
            Assert.fail("Problem while uploading the Network");
        }

        networkUploader = f.bntm;

        if (networkUploader == null) {
            Assert.fail("Not possible to upload the Network");
        }

        id_network = networkUploader.id_network;

        MetExploreResult result = networkUploader.getResult();

        if (result.getIntError() == 1) {
            Assert.fail(result.getMessage());
        }
    }

    /**
     * Actions performed after all the tests
     */
    @AfterClass
    public static void afterTests()  {

        if (dbId != -1) {
            try {
                MetExploreConnection connection = new MetExploreConnection(dbconf);

                // Remove the collection and so the Network
                MetExploreResult resRemoveDb = CollectionQueries
                        .removecollection(dbId, connection.getConnection());

                if (resRemoveDb.getIntError() == 1) {
                    Assert.fail("Error while removing the collection " + dbId);
                }

                connection.close();

            } catch (Exception e) {
                e.printStackTrace();
                Assert.fail("Problem while accessing the metexplore database");
            }
        }

    }

    @Test
    public void testOrganismName() {
        String query = "SELECT organism_name FROM Network WHERE id = " + id_network;

        try {
            MetExploreConnection connection = new MetExploreConnection(dbconf);
            ResultSet rs = connection.getConnection().createStatement().executeQuery(query);
            String orgName = null;
            if (rs.next()) {
                orgName = rs.getString("organism_name");
            }
            connection.close();
            Assert.assertEquals("Cystobacter", orgName);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Problem while accessing the metexplore database");
        }

    }

    @Test
    public void reciprocalTest() throws Exception {
        int id_network = networkUploader.id_network;

        MetExploreDataBaseToBioNetwork mtbn = new MetExploreDataBaseToBioNetwork();

        try {
            mtbn.Connection(dbconf);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("Impossible to create the connection in MetExploreDataBaseToBioNetwork");
        }

        mtbn.setid_network(id_network);

        mtbn.convert();

        BioNetwork networkTest = mtbn.getNetwork();

        mtbn.getCon().close();

        CompareNetworks test = new CompareNetworks(network,
                networkTest);

        test.testAll();
    }
}
