package fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.gui.flux;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.IntegrationTestsUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Flux.DeadOrAlive;


public class FluxDrIT extends GenericFluxTest {

    @Test
    public void testDrMode0() throws Exception {

        DeadOrAlive f = new DeadOrAlive();

        f.setDbConf(IntegrationTestsUtils.dbConf);

        f.id_network = id_network;

        String nb_expected = "991";

        JSONObject json = f.run();

        assertEquals("False number of dead reactions", nb_expected, json.get("nb_dead").toString());

        if (!json.containsKey("reactions")) {
            Assert.fail("No key 'reactions' in the json");
        }


        JSONArray reactions = (JSONArray) json.get("reactions");

        for (Object object : reactions) {
            @SuppressWarnings("unchecked")
            HashMap<String, String> reaction = (HashMap<String, String>) object;

            if (reaction.get("db_identifier").compareTo("R_BTS4") == 0) {
                assertEquals("Bad dead identification for R_BTS4", "1", reaction.get("dead"));
            }
        }
    }

    @Test
    public void testDrMode1() throws Exception {

        DeadOrAlive f = new DeadOrAlive();

        f.setDbConf(IntegrationTestsUtils.dbConf);

        f.id_network = id_network;

        f.mode = true;

        String nb_expected = "215";

        JSONObject json = f.run();

        assertEquals("False number of dead reactions (mode = true)", nb_expected, json.get("nb_dead").toString());

    }


}
