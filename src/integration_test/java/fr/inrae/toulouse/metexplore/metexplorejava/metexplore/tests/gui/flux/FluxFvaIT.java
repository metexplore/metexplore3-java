package fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.gui.flux;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;

import fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.IntegrationTestsUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.FluxUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Flux.FVA;

public class FluxFvaIT extends GenericFluxTest {

    String objectiveReaction = "R_Ec_biomass_iAF1260_core_59p81M";
    String objectiveReaction2 = "FluxSum";

    @Test
    public void testSimpleFva() throws Exception {

        FVA f = new FVA();

        f.objectiveReactions = objectiveReaction;
        f.id_network = id_network;
        f.setDbConf(IntegrationTestsUtils.dbConf);

        JSONObject json = f.run();
        assertEquals("Bad optimal objective function value",
                "Optimal value of the objective function: 0.917\n"
                        + "Results of FVA have been added in the reaction grid.",
                json.get("message"));

        if (!json.containsKey("reactions")) {
            Assert.fail("No key 'reactions' in the json");
        }

        JSONArray reactions = (JSONArray) json.get("reactions");
        Double min = 6.484;
        Double max = 8.324;

        Boolean flag = false;

        for (Object object : reactions) {
            HashMap<String, String> reaction = (HashMap<String, String>) object;

            if (reaction.get("db_identifier").compareTo("R_PFK") == 0) {
                flag = true;
                assertEquals("Bad min for R_PFK", min, Double.parseDouble(reaction.get("min")), 0.001);
                assertEquals("Bad max for R_PFK", max, Double.parseDouble(reaction.get("max")), 0.001);
            }
        }

        if (flag == false) {
            Assert.fail("R_PFK not found");
        }

    }

    @Test
    public void testFvaWithGeneKo() throws Exception {

        FVA f = new FVA();

        f.objectiveReactions = objectiveReaction;
        f.id_network = id_network;
        f.ko_genes = "b3959";
        f.setDbConf(IntegrationTestsUtils.dbConf);

        JSONObject json = f.run();
        assertEquals("Bad optimal objective function value",
                "Optimal value of the objective function: 0.0\n"
                        + "Results of FVA have been added in the reaction grid.",
                json.get("message"));

        if (!json.containsKey("reactions")) {
            Assert.fail("No key 'reactions' in the json");
        }

    }

    @Test
    public void testFvaWithReactionKo() throws Exception {

        FVA f = new FVA();

        f.objectiveReactions = objectiveReaction;
        f.id_network = id_network;
        f.ko_reactions = "R_MOBDabcpp";
        f.setDbConf(IntegrationTestsUtils.dbConf);

        JSONObject json = f.run();
        assertEquals("Bad optimal objective function value",
                "Optimal value of the objective function: 0.0\n"
                        + "Results of FVA have been added in the reaction grid.",
                json.get("message"));

        if (!json.containsKey("reactions")) {
            Assert.fail("No key 'reactions' in the json");
        }

    }

    @Test
    public void testFvaWithNestedObjective() throws Exception {

        FVA f = new FVA();

        f.objectiveReactions = objectiveReaction;
        f.secondObjectiveReactions = objectiveReaction2;
        f.secondObjectiveSense = FluxUtils.Senses.MIN;
        f.id_network = id_network;
        f.setDbConf(IntegrationTestsUtils.dbConf);

        JSONObject json = f.run();
        assertEquals("Bad optimal objective function value",
                "Optimal value of the first objective function: 0.917<br />"
                        + "Optimal value of the second objective function: 739.129<br />"
                        + "Results of FVA have been added in the reaction grid.",
                json.get("message"));

        if (!json.containsKey("reactions")) {
            Assert.fail("No key 'reactions' in the json");
        }

        JSONArray reactions = (JSONArray) json.get("reactions");
        Double min = 6.484;
        Double max = 6.484;

        Boolean flag = false;

        for (Object object : reactions) {
            HashMap<String, String> reaction = (HashMap<String, String>) object;

            if (reaction.get("db_identifier").compareTo("R_PFK") == 0) {
                flag = true;
                assertEquals("Bad min for R_PFK", min, Double.parseDouble(reaction.get("min")), 0.001);
                assertEquals("Bad max for R_PFK", max, Double.parseDouble(reaction.get("max")), 0.001);
            }
        }

        if (flag == false) {
            Assert.fail("R_PFK not found");
        }

        f.libertyPercentage = 50.0;
        json = f.run();
        assertEquals("Bad optimal objective function value",
                "Optimal value of the first objective function: 0.459<br />"
                        + "Optimal value of the second objective function: 393.551<br />"
                        + "Results of FVA have been added in the reaction grid.",
                json.get("message"));

        if (!json.containsKey("reactions")) {
            Assert.fail("No key 'reactions' in the json");
        }

        reactions = (JSONArray) json.get("reactions");
        min = 0.0;
        max = 31.549;

        flag = false;

        for (Object object : reactions) {
            HashMap<String, String> reaction = (HashMap<String, String>) object;

            if (reaction.get("db_identifier").compareTo("R_PFK") == 0) {
                flag = true;
                assertEquals("Bad min for R_PFK", min, Double.parseDouble(reaction.get("min")), 0.001);
                assertEquals("Bad max for R_PFK", max, Double.parseDouble(reaction.get("max")), 0.001);
            }
        }

        if (flag == false) {
            Assert.fail("R_PFK not found when we test liberty of percentage");
        }

    }

}
