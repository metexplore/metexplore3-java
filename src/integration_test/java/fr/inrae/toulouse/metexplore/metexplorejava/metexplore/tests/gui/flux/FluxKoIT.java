package fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.gui.flux;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;

import fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.IntegrationTestsUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Flux.Ko;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Flux.Ko.KoTypes;

public class FluxKoIT extends GenericFluxTest {

    String objectiveReaction = "R_Ec_biomass_iAF1260_core_59p81M";

    @Test
    public void testGeneKo() throws Exception {

        Ko f = new Ko();

        f.objectiveReactions = objectiveReaction;
        f.id_network = id_network;
        f.ko_type = KoTypes.genes;
        f.setDbConf(IntegrationTestsUtils.dbConf);

        JSONObject json = f.run();

        if (!json.containsKey("genes")) {
            Assert.fail("No key 'genes' in the json");
        }

        JSONArray genes = (JSONArray) json.get("genes");
        Double res1 = 0.0;        //b4040
        Double res2 = 0.917;    //b1006
        Double res3 = 0.676;    //b2287

        Boolean flag1 = false;
        Boolean flag2 = false;
        Boolean flag3 = false;

        for (Object object : genes) {
            HashMap<String, String> gene = (HashMap<String, String>) object;

            if (gene.get("db_identifier").compareTo("b4040") == 0) {
                flag1 = true;
                assertEquals("Bad value for b4040", res1, Double.parseDouble(gene.get("objective_value")), 0.001);
            } else if (gene.get("db_identifier").compareTo("b1006") == 0) {
                flag2 = true;
                assertEquals("Bad value for b1006", res2, Double.parseDouble(gene.get("objective_value")), 0.001);
            } else if (gene.get("db_identifier").compareTo("b2287") == 0) {
                flag3 = true;
                assertEquals("Bad value for b2287", res3, Double.parseDouble(gene.get("objective_value")), 0.001);
            }
        }

        if (flag1 == false) {
            Assert.fail("b4040 not found");
        }

        if (flag2 == false) {
            Assert.fail("b1006 not found");
        }

        if (flag3 == false) {
            Assert.fail("b2287 not found");
        }


    }

}
