package fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.gui.flux;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.IntegrationTestsUtils;
import org.junit.Test;

import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.FluxUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Flux.Optimal;

public class FluxOptimalIT extends GenericFluxTest {

    String objectiveReaction = "R_Ec_biomass_iAF1260_core_59p81M";
    String objectiveReaction2 = "FluxSum";

    @Test
    public void testSimpleFba() throws Exception {

        Optimal f = new Optimal();

        f.objectiveReactions = objectiveReaction;
        f.id_network = id_network;
        f.setDbConf(IntegrationTestsUtils.dbConf);

        String message = f.run();

        assertTrue("Simple FBA failed", message.contains("Optimal value of the objective function: 0.917"));

    }

    @Test
    public void testFbaWithKoGene() throws Exception {

        String messageToTest = "{\"success\":\"true\", \"message\":\"Optimal value of the objective function: 0.0\"}";
        Optimal f = new Optimal();

        f.objectiveReactions = objectiveReaction;
        f.id_network = id_network;
        f.ko_genes = "b3959";
        f.setDbConf(IntegrationTestsUtils.dbConf);

        String message = f.run();

        assertTrue(message.contains("Optimal value of the objective function: 0.0"));

    }

    @Test
    public void testFbaWithKoReaction() throws Exception {

        Optimal f = new Optimal();

        f.objectiveReactions = objectiveReaction;
        f.id_network = id_network;
        f.ko_reactions = "R_MOBDabcpp";
        f.setDbConf(IntegrationTestsUtils.dbConf);

        String message = f.run();

        assertTrue(message.contains("Optimal value of the objective function: 0.0"));

    }

    @Test
    public void testFbaWithNestedObjective() throws Exception {

        Optimal f = new Optimal();

        f.objectiveReactions = objectiveReaction;
        f.secondObjectiveReactions = objectiveReaction2;
        f.secondObjectiveSense = FluxUtils.Senses.MIN;
        f.id_network = id_network;
        f.setDbConf(IntegrationTestsUtils.dbConf);

        String message = f.run();

        assertTrue(message.contains("Optimal value of the first objective function: 0.917<br \\/>Optimal value of the second objective function: 739.129"));

        f.libertyPercentage = 50.0;
        message = f.run();

        assertTrue(message.contains("Optimal value of the first objective function: 0.459<br \\/>Optimal value of the second objective function: 393.551"));
    }


}
