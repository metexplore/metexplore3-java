package fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.gui.flux;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;

import fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.IntegrationTestsUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Flux.Orphan;


public class FluxOrphanIT extends GenericFluxTest {

    @Test
    public void testOrphan() throws Exception {

        Orphan f = new Orphan();

        f.id_network = id_network;
        f.setDbConf(IntegrationTestsUtils.dbConf);

        String nb_expected = "118";

        JSONObject json = f.run();

        assertEquals("False number of orphan", nb_expected, json.get("nb_orphans").toString());

        if (!json.containsKey("metabolites")) {
            Assert.fail("No key 'metabolites' in the json");
        }

        JSONArray metabolites = (JSONArray) json.get("metabolites");

        for (Object object : metabolites) {
            @SuppressWarnings("unchecked")
            HashMap<String, String> metabolite = (HashMap<String, String>) object;

            if (metabolite.get("db_identifier").compareTo("M_lpp_p") == 0) {
                assertEquals("Bad dead identification for M_lpp_p", "1", metabolite.get("orphan"));
            } else if (metabolite.get("db_identifier").compareTo("M_hco3_c") == 0) {
                assertEquals("Bad dead identification for M_hco3_c", "0", metabolite.get("orphan"));
            }

        }

    }


}
