package fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.gui.flux;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.reader.Met4jSbmlReaderException;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.BioNetworkToMetExploreDataBase;
import fr.inrae.toulouse.metexplore.metexplorejava.metexplore.tests.IntegrationTestsUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.CollectionQueries;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.database.MetExploreConnection;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;

import java.io.IOException;
import java.sql.SQLException;

public abstract class GenericFluxTest {
    static int id_network = -1;
    static int dbId = -1;
    private static MetExploreConnection connection;
    private static BioNetworkToMetExploreDataBase networkUploader;

    @BeforeClass
    public static void beforeTests() throws Met4jSbmlReaderException, SQLException, IOException, ClassNotFoundException, InterruptedException {

        BioNetwork network = IntegrationTestsUtils.readSbmlFile("src/integration_test/resources/sbml/iAf1260.xml");

        connection = IntegrationTestsUtils.createConnection();

        networkUploader = IntegrationTestsUtils.createNetworkUploader(network, connection);

        networkUploader.toMetExplore();

        id_network = networkUploader.id_network;
        dbId = networkUploader.dbId;

    }

    @AfterClass
    public static void afterTests() throws SQLException {
        if (dbId != -1) {
            MetExploreResult resRemoveDb = CollectionQueries.removecollection(
                    dbId, connection.getConnection());

            if (resRemoveDb.getIntError() == 1) {
                Assert.fail("Error while removing the collection " + dbId);
            }
        }
        // Close the connection
        if(networkUploader != null) {
            try {
                networkUploader.closeConnection();
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                Assert.fail("Close the connection of bntm");
            }
        }
    }
}

