/**
 * Copyright INRA
 * <p>
 * Contact: ludovic.cottret@toulouse.inra.fr
 * <p>
 * <p>
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * <p>
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * <p>
 * 23 mai 2011
 * <p>
 * 23 mai 2011
 * <p>
 * 23 mai 2011
 * <p>
 * 23 mai 2011
 * <p>
 * 23 mai 2011
 * <p>
 * 23 mai 2011
 * <p>
 * 23 mai 2011
 * <p>
 * 23 mai 2011
 * <p>
 * 23 mai 2011
 * <p>
 * 23 mai 2011
 * <p>
 * 23 mai 2011
 * <p>
 * 23 mai 2011
 * <p>
 * 23 mai 2011
 * <p>
 * 23 mai 2011
 */
package fr.inrae.toulouse.metexplore.metexplorejava.apps;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.BioNetworkToMetExploreDataBase;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.MetExploreDataBaseToBioNetwork;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.NetworkQueries;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.CollectionQueries;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreStringResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.comparison.CompareNetworks;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.database.MetExploreConnection;
import fr.inrae.toulouse.metexplore.metexplorexml.reader.MetexploreXmlReader;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.sbml.jsbml.text.parser.ParseException;

import java.io.*;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.Set;

/**
 * @author ludo 23 mai 2011
 *
 */
public class BatchMetExploreLoader {


    private final String message = "Loads a set of networks SBML files in the metexplore database.\n";

    @Option(name = "-dbConf", usage = "Metexplore conf path (e.g. .env)")
    public String dbConf = ".env";
    @Option(name = "-f", usage = "[] Tabulated file where are listed the networks.\n"
            + "1st column : Network identifier (must correspond to the Network directory name\n"
            + "2nd column : Network Name\n"
            + "3rd column : NCBI taxon id\n"
            + "4th column : Strain name\n"
            + "5th column : Version\n"
            + "6th column : url\n"
            + "5th column : Mysql id of the user who will own the Network\n"
            + "6th column : Mysql id of the collection\n")
    private final String fileIn = "";

    @Option(name = "-dir", usage = "[] Directory where sub-directories correspond to the network identifiers.\n"
            + "Each subdirectory must contain a file called completeNetwork.xml")
    private final String dir = "";

    @Option(name = "-test", usage = "[deactivated] If activated, test without doing anything")
    private final Boolean test = false;

    @Option(name = "-check", usage = "[deactivated] If activated, don't load the networks but checks if they are well loaded")
    private final Boolean check = false;

    @Option(name = "-checkFile", usage = "[check.txt] File to write the result of checking")
    private final String checkFile = "check.txt";

    @Option(name="-identifier_origin", usage = "[] Identifier origin (e.g. KEGG, MetaCyc)")
    private final String identifier_origin = "";

    @Option(name="-source", usage = "[] Original database (e.g. KEGG, MetaCyc)")
    private final String source = "";

    @Option(name = "-h", usage = "[deactivated] Prints this help")
    private final Boolean h = false;

    /**
     * @param args
     * @throws IOException
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws SQLException
     * @throws ClassNotFoundException
     * @throws InterruptedException
     */
    public static void main(String[] args) throws IOException,
            ClassNotFoundException, SQLException, InstantiationException,
            IllegalAccessException, InterruptedException, ParseException {

        BatchMetExploreLoader bmel = new BatchMetExploreLoader();

        CmdLineParser parser = new CmdLineParser(bmel);

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println(bmel.getMessage());
            parser.printUsage(System.err);
            System.exit(0);
        }

        if (bmel.getH()) {
            System.err.println(bmel.getMessage());
            parser.printUsage(System.err);
            System.exit(1);
        }

        if (!(new File(bmel.getFileIn())).exists()) {
            System.err.println("The file " + bmel.getFileIn()
                    + " does not exist");
            System.err.println(bmel.getMessage());
            parser.printUsage(System.err);
            System.exit(0);
        }

        if (!(new File(bmel.getDir())).exists()) {
            System.err.println("The directory " + bmel.getDir()
                    + " does not exist");
            System.err.println(bmel.getMessage());
            parser.printUsage(System.err);
            System.exit(0);
        }

        String fileIn = bmel.getFileIn();
        String dir = bmel.getDir();

        if (bmel.getTest()) {
            bmel.testFileIn();
        } else {
            if (bmel.testFileIn()) {

                MetExploreConnection connection = new MetExploreConnection(bmel.dbConf);

                FileWriter fw = null;
                if (bmel.getCheck()) {

                    fw = new FileWriter(bmel.getCheckFile(), false);

                    System.err.println("Check results in the file " + bmel.getCheckFile());

                }

                FileInputStream in = new FileInputStream(fileIn);
                InputStreamReader ipsr = new InputStreamReader(in);
                BufferedReader br = new BufferedReader(ipsr);
                String ligne;

                int n = 0;

                while ((ligne = br.readLine()) != null) {

                    n++;
                    if (!ligne.matches("^#.*")) {
                        String[] tab = ligne.split("\\t");

                        if (tab.length < 7) {
                            System.err
                                    .println("The line "
                                            + n
                                            + " does not have the good number of columns");
                        }

                        String NetworkId = tab[0];
                        String NetworkName = tab[1];

                        int ncbiId = Integer.parseInt(tab[2]);

                        String strain = "";

                        strain = tab[3];

                        String tissue = "";

                        String cellType = "";

                        String version = tab[4];
                        String url = tab[5];
                        int userId = Integer.parseInt(tab[6]);
                        int dbId = Integer.parseInt(tab[7]);



                        String xmlFileName = dir + "/" + NetworkId
                                + "/completeNetwork.xml";

                        if (!new File(xmlFileName).exists()) {
                            System.err.println("Warning : the file "
                                    + xmlFileName + " does not exist !");
                        }

                        MetexploreXmlReader reader = new MetexploreXmlReader(
                                xmlFileName);

                        reader.read();

                        BioNetwork bn = reader.getNetwork();

                        if (!bmel.getCheck()) {

                            // load the bioNetwork in MetExplore
                            BioNetworkToMetExploreDataBase bnm = new BioNetworkToMetExploreDataBase(
                                    bn, connection.getConnection(), dbId, NetworkName, NetworkId, ncbiId,
                                    strain, tissue, cellType,
                                    version, url, userId, "Imported with BatchMetExploreLoader",
                                    bmel.getIdentifierOrigin(), bmel.getSource());

                            bnm.toMetExplore();

                            bnm.closeConnection();

                            MetExploreResult err = bnm.getResult();

                            System.err.println(err.warnings);

                            System.err.println(err.message);
                        } else {
                            // Checks that the network has been well uploaded

                            String message = "";

                            // Get the Network id
                            MetExploreIntegerArrayResult reqBs = NetworkQueries
                                    .getNetwork(NetworkId,
                                            NetworkName, strain, tissue,
                                            cellType, dbId, connection.getConnection());

                            if (reqBs.getIntError() == 1) {
                                message = reqBs.message;
                            } else if (reqBs.count() == 0) {
                                message = "No instance of the Network";
                            } else if (reqBs.count() > 1) {
                                message = "Multiple instances of the Network";
                            } else {
                                int idSqlNetwork = reqBs.get(0);

                                MetExploreDataBaseToBioNetwork mtbn = new MetExploreDataBaseToBioNetwork();

                                int nError = 0;

                                try {
                                    mtbn.Connection(bmel.dbConf);

                                    mtbn.setid_network(idSqlNetwork);

                                    mtbn.convert();

                                    BioNetwork networkTest = mtbn.getNetwork();

                                    CompareNetworks test = new CompareNetworks(bn,
                                            networkTest);

                                    // We don't test the number of compartments because of the fake compartments

                                    test.testNumberEnzymes();
                                    test.testNumberGenes();
                                    test.testNumberMetabolites();
                                    test.testNumberReactions();
                                    test.testNumberPathways();
                                    test.testNumberProteins();
                                    test.testNumberUnitDefinitions();

                                    test.testAttributeEnzymes();
                                    test.testAttributesGenes();
                                    test.testAttributesMetabolites();
                                    test.testAttributesProteins();
                                    test.testAttributesReactions();
                                    test.testAttributesUnitDefinitions();


                                } catch (Exception e) {
                                    e.printStackTrace();
                                    message = "Impossible to create the connection in MetExploreToBioNetwork";
                                } catch (AssertionError e) {
                                    nError++;
                                    System.err.println("Error " + nError);
                                    if (nError == 1)
                                        message = e.getMessage();
                                }
                            }


                            if (!message.equalsIgnoreCase("ok")) {
                                fw.write("****************************************\n");
                                fw.write(NetworkId);
                                fw.write("\n****************************************\n");
                                fw.write(message);
                                fw.write("\n****************************************\n");
                            }


                        }

                    }
                }

                br.close();

                if (bmel.getCheck()) {
                    fw.close();
                    System.err.println("Check results in the file " + bmel.getCheckFile());
                }

            }


        }

    }

    /**
     * Test the input file
     *
     * @return true if the input file exists and has the correct syntax
     * @throws IOException
     */
    public Boolean testFileIn() throws IOException {

        Boolean flag = true;

        String fileIn = this.getFileIn();

        if (!new File(fileIn).exists()) {
            System.err.println("The file " + fileIn + " does not exist");
            return false;
        }

        FileInputStream in = new FileInputStream(fileIn);

        InputStreamReader ipsr = new InputStreamReader(in);
        BufferedReader br = new BufferedReader(ipsr);
        String ligne;

        int n = 0;

        Set<String> Networks = new HashSet<>();

        while ((ligne = br.readLine()) != null) {

            n++;
            if (!ligne.matches("^#.*")) {
                String[] tab = ligne.split("\\t");

                if (tab.length < 10) {
                    System.err.println("[FILE ERROR] The line " + n
                            + " does not have the good number of columns");
                    flag = false;
                }

                // Check that the user id is a number
                try {
                    Integer.parseInt(tab[8]);
                } catch (NumberFormatException e) {
                    // TODO Auto-generated catch block
                    System.err.println("[FILE ERROR] The mysql user id line "
                            + n + " is not a number");
                    flag = false;
                }

                // Check that the species name has two terms
                String orgName = tab[2];
                String[] tabOrg = orgName.split(" ");
                if (tabOrg.length != 2) {
                    System.err
                            .println("[FILE WARNING] The name of the organism has a number of terms different than two (line "
                                    + n + ")");
                }

                // Checks that the xml file exists
                String NetworkId = tab[0];

                if (Networks.contains(NetworkId)) {
                    System.err.println("[FILE ERROR] The Network " + NetworkId + " line "
                            + n + " is duplicated");
                    flag = false;
                } else {
                    Networks.add(NetworkId);
                }

                String xmlFileName = dir + "/" + NetworkId
                        + "/completeNetwork.xml";

                if (!new File(xmlFileName).exists()) {
                    System.err.println("[FILE ERROR] : the file " + xmlFileName
                            + " does not exist ! (line " + n + ")");
                    flag = false;
                }
            }
        }

        br.close();

        if (flag) {
            System.err.println("Input File Ok");
        }

        return flag;

    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return the fileIn
     */
    public String getFileIn() {
        return fileIn;
    }

    /**
     * @return the dir
     */
    public String getDir() {
        return dir;
    }

    /**
     * @return the h
     */
    public Boolean getH() {
        return h;
    }

    /**
     * @return the test
     */
    public Boolean getTest() {
        return test;
    }

    /**
     * @return the check
     */
    public Boolean getCheck() {
        return check;
    }

    /**
     * @return the checkFile
     */
    public String getCheckFile() {
        return checkFile;
    }

    public String getIdentifierOrigin() {
        return identifier_origin;
    }

    public String getSource() {
        return source;
    }

}
