package fr.inrae.toulouse.metexplore.metexplorejava.apps;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import fr.inrae.toulouse.metexplore.met4j_io.kegg.Kegg2BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_io.kegg.KeggServices;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;


/**
 * This class retrieves compound data from KEGG
 *
 * @author bmerlet
 */
public class GetKeggAsJson {

    @Option(name = "-KeggID", usage = "Kegg identifier of the Compound", required = true)
    private static String KeggID = "-1";

    /**
     * Main method. used in the Metexplore Interface:
     * <br>MetExplore.controller.C_AddMetaboliteForm class
     * <br>AutoCompleteKegg() method
     *
     * @param args
     */
    public static void main(String[] args) throws Exception {
//		long startTime = System.nanoTime();
        GetKeggAsJson KtoJson = new GetKeggAsJson();

        CmdLineParser parser = new CmdLineParser(KtoJson);

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            parser.printUsage(System.err);
            System.exit(0);
        }

        Kegg2BioNetwork KToBN = new Kegg2BioNetwork("hsa");

        KeggServices ks = new KeggServices();

        List<String> list = new ArrayList<>();
        list.add(KtoJson.KeggID);

        HashMap<String, HashMap<String, ArrayList<String>>> data = KToBN.getEntitiesData(list);

        JsonObject myObj = new JsonObject();

        if (data.isEmpty()) {
            myObj.addProperty("success", false);
            myObj.addProperty("message", "Invalid Kegg ID");
        } else {
            Gson gson = new Gson();
            JsonElement jsonData = gson.toJsonTree(data.get(KeggID));
            myObj.addProperty("success", true);
            myObj.add("data", jsonData);
        }

//		long elapsedTime = System.nanoTime() - startTime;

//		if (elapsedTime/1000000000==0){
//			System.out.println("Time to retrieve references: "
//					+ elapsedTime/1000000+ " milliseconds");
//		}
//		else{
//			System.out.println("Time to retrieve references: "
//					+ elapsedTime/1000000000+ " seconds");
//		}


        String output = myObj.toString();

        System.out.println(output);

    }

}
