package fr.inrae.toulouse.metexplore.metexplorejava.apps;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import fr.inrae.toulouse.metexplore.metexplorejava.utils.mail.MetExploreMail;
import org.apache.commons.lang.time.DateUtils;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import fr.inrae.toulouse.metexplore.metexplorejava.sql.ProjectQueries;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.database.MetExploreConnection;

/**
 * @author lcottret
 */
public class SendHistoryToProjectMembers {

    public String description = "Send an email with the history of a project to all the members";
    @Option(name = "-id_project", usage = "Sql id of the Project", required = true)
    public int id_project = -1;
    @Option(name = "-nb", usage = "Number of days of the history", required = false)
    public Integer nb = 30;
    @Option(name = "-smtp", usage = "[smtp.inrae.fr] SMTP")
    String smtp = "smtp.inrae.fr";
    @Option(name = "-mailFrom", usage = "[contact-metexplore@inrae.fr ] Mail from")
    String mailFrom = "contact-metexplore@inrae.fr";

    @Option(name = "-dbConf", usage = "Path of the .env file containing db connection information")
    String dbConf = ".env";
    private Connection con;
    @Option(name = "-h", usage = "[deactivated] Prints this help")
    private Boolean h = false;

    /**
     * Main function
     *
     * @param args
     */
    public static void main(String[] args) {

        SendHistoryToProjectMembers f = new SendHistoryToProjectMembers();

        CmdLineParser parser = new CmdLineParser(f);

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            parser.printUsage(System.err);
            System.exit(0);
        }

        if (f.h) {
            System.err.println(f.description);
            parser.printUsage(System.err);
            System.exit(1);
        }

        try {
            f.Connection();
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error while accessing the database");
            System.exit(0);
        }

        Date today = new Date();
        Date firstDay = DateUtils.addDays(today, -f.nb);

        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String from = df.format(firstDay);
        String to = df.format(today);
        MetExploreResult res = null;

        try {
            res = ProjectQueries.getProjectName(f.id_project, f.con);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.err.println("Error while getting the name of the project");
            System.exit(0);
        }

        if (res.getIntError() == 1) {
            System.err.println("Error while getting the name of the project : " + res.getMessage());
            System.exit(0);
        }

        String projectName = res.getMessage();

        res = null;
        try {
            res = ProjectQueries.getProjectMails(f.id_project, f.con);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.err.println("Error while getting project mails");
            System.exit(0);
        }

        if (res.getIntError() == 1 || res.getMessage() == "") {
            System.err.println("Error while getting project mails : " + res.getMessage());
            System.exit(0);
        }

        String mails = res.getMessage();

        String[] a_mails = mails.split(",");

        res = null;
        try {
            res = ProjectQueries.getResumeHistoryByUser(f.id_project, from, to, true, f.con);
        } catch (SQLException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            System.err.println("Error while getting the history");
            System.exit(0);
        }

        if (res.getIntError() == 1) {
            System.err.println("Error while getting the history : " + res.getMessage());
            System.exit(0);
        }

        String message = "";

        message += "<html><head>";

        message += "<style>";
        message += "table {"
                + "border-width:1px;border-collapse:collapse;border-style:solid;border-color:black;text-align:left;}"
                + "td {" + "border-width:1px;border-style:solid;padding:5px;}" + "th {"
                + "border-width:1px;border-style:solid;}";
        message += "</style>";

        message += "</head><body><h1>" + projectName + " report</h1><br />From " + from + " to " + to + "<br /><br />";

        String historyByUser = res.getMessage();

        if (historyByUser.compareTo("") == 0) {
            message += "No activity during this period on this project! Come on, let's annotate!";
        } else {
            message += "<h2>History by user</h2>";

            message += res.getMessage();

            res = null;

            try {
                res = ProjectQueries.getHistory(f.id_project, from, to, true, f.con);
            } catch (SQLException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
                System.err.println("Error while getting the history");
                System.exit(0);
            }

            if (res.getIntError() == 1) {
                System.err.println("Error while getting the history : " + res.getMessage());
                System.exit(0);
            }

            message += "<br /><br />" + res.getMessage();

            message += "</body></html>";

            System.out.println(message);

        }

        for (int i = 0; i < a_mails.length; i++) {

            String mail = a_mails[i];

            Boolean flag = MetExploreMail.sendMail(f.mailFrom, mail,
                    "[" + projectName + "] Automatic report from " + from + " to " + to, message, f.smtp);

            if (!flag) {
                System.err.println("Unable to send email to " + mail);
            }
        }

        System.exit(1);

    }

    /**
     * Connection to the database
     *
     * @throws ClassNotFoundException
     * @throws InstantiationException
     * @throws IllegalAccessException
     * @throws SQLException
     */
    public void Connection()
            throws ClassNotFoundException, InstantiationException, IllegalAccessException, SQLException, IOException {

        MetExploreConnection connection = new MetExploreConnection(dbConf);
        this.con = connection.getConnection();

    }

}
