package fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Admin;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.BioNetworkToMetExploreDataBase;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.MetExploreDataBaseToBioNetwork;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.NetworkQueries;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.CollectionQueries;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.GenericUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.database.MetExploreConnection;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.FileWriter;

/**
 * @author bmerlet
 */
public class CopyNetwork extends MetExploreGuiFunction {
    /**
     * The label of this {@link MetExploreGuiFunction}
     */
    public String label = "Copy a Network";
    /**
     * The description of the {@link MetExploreGuiFunction}
     */
    public String description = "Creates a complete copy of the selected Network. "
            + "If a userId is passed as argument the copie is set as private.";
    /**
     * The 'requireLogin' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when user is not connected
     */
    public Boolean requireLogin = true;
    /**
     * The 'requireNetwork' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when no Network is
     * selected
     */
    public Boolean requireNetwork = true;
    /**
     * The 'requireAdmin' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function's UI when user is not an
     * admin user
     */
    public Boolean requireAdmin = true;
    /**
     * The 'sendMail' attributes of the {@link MetExploreGuiFunction}. Setting
     * this to true enables the method to send an email when the job is done
     */
    public Boolean sendMail = false;
    /**
     * The type of result expected by the interface
     *
     * @see MetExploreGuiFunction#resultType
     */
    public String resultType = "Network";

    /**
     * A notification e-mail will be sent to this address when the job will end.
     * the default value is only used when this is used in the admin interface
     */
    @Option(name = "-mail", usage = "Email address of the user")
    public String mail = "contact-metexplore@inrae.fr";

    // @Option(name="-dbConf", usage="Database Config file")
    // public String dbConf = "";

    /**
     * Mysql id of the Network
     */
    @Option(name = "-id_network", usage = "Sql id of the Network to copy", required = true)
    public int id_network;

    /**
     * Mysql id of the collection where copy the new Network
     */
    @Option(name = "-id_collection", usage = "Mysql id of the collection where copy the new Network", required = true)
    public int id_collection;

    /**
     * The user id that will be the owner of the copied Network
     */
    @Option(name = "-id_user", usage = "Sql id of the new user of this Network", required = true)
    public int id_user;

    /**
     * We use {@link #setLongJob(Boolean)} in the constructor to override the
     * default value set in {@link MetExploreGuiFunction}
     */
    public CopyNetwork() {
        super();
        this.setLongJob(true);
    }

    /**
     * @param args
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws java.sql.SQLException
     * @throws InterruptedException
     */
    public static void main(String[] args)
            throws Exception {

        CopyNetwork copyBS = new CopyNetwork();

        String mailBody, mailSubject;

        CmdLineParser parser = new CmdLineParser(copyBS);

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            if (copyBS.getPrintJson()) {
                copyBS.printJson();
                System.exit(1);
            } else {
                e.printStackTrace();
                GenericUtils.MetExploreError("Problem in the arguments of the function " + copyBS.getLabel());
                System.exit(0);
            }
        }

        if (copyBS.getPrintJson()) {
            copyBS.printJson();
            System.exit(1);
        }

        FileWriter fwJson = new FileWriter(new File(copyBS.jsonFile));

        MetExploreDataBaseToBioNetwork toBioNet = new MetExploreDataBaseToBioNetwork();

        try {
            System.err.println("Connection to the database..");
            MetExploreConnection metCon;

            metCon = new MetExploreConnection(copyBS.getDbConf());
            toBioNet.setCon(metCon.getConnection());

            toBioNet.getCon().setAutoCommit(false);
        } catch (Exception e) {
            e.printStackTrace();
            GenericUtils
                    .MetExploreError("Problem while connecting to the database in the function " + copyBS.getLabel());
            System.exit(0);
        }

        toBioNet.setid_network(copyBS.getid_network());

        toBioNet.convert();
        System.err.println("Network copied in a Java object.");

        BioNetwork newNetwork = toBioNet.getNetwork();

        String strain = NetworkQueries.getNetworkStrain(copyBS.getid_network(), toBioNet.getCon()).get();
        String tissue = NetworkQueries.getNetworkTissue(copyBS.getid_network(), toBioNet.getCon()).get();
        String cellType = NetworkQueries.getNetworkCellType(copyBS.getid_network(), toBioNet.getCon()).get();
        String description = NetworkQueries.getNetworkDescription(copyBS.getid_network(), toBioNet.getCon()).get();
        String NetworkName = NetworkQueries.getNetworkName(copyBS.getid_network(), toBioNet.getCon()).get();
        String url = NetworkQueries.getNetworkUrl(copyBS.getid_network(), toBioNet.getCon()).get();
        String identifierOrigin = NetworkQueries.getNetworkIdentifierOrigin(copyBS.getid_network(), toBioNet.getCon()).get();
        String version = NetworkQueries.getNetworkVersion(copyBS.getid_network(), toBioNet.getCon()).get();
        String source = NetworkQueries.getNetworkSource(copyBS.getid_network(), toBioNet.getCon()).get();

        MetExploreIntegerResult resOrgId = NetworkQueries.getOrgId(copyBS.getid_network(), toBioNet.getCon());
        if(resOrgId.getIntError() == 1 )
        {
            System.err.println(resOrgId.message);
            toBioNet.getCon().close();
            System.exit(0);
        }

        int orgId = resOrgId.get();

        MetExploreIntegerResult resDbId = NetworkQueries.getDbId(copyBS.getid_network(), toBioNet.getCon());
        if(resDbId.getIntError() == 1 )
        {
            System.err.println(resOrgId.message);
            toBioNet.getCon().close();
            System.exit(0);
        }

        int originalDbId = resDbId.get();
        String dbName = CollectionQueries.getDbName(originalDbId, toBioNet.getCon()).get();

        BioNetworkToMetExploreDataBase bioNetToMet = new BioNetworkToMetExploreDataBase(newNetwork, toBioNet.getCon(), copyBS.id_collection, NetworkName, dbName, orgId,
                strain, tissue, cellType, url, version, copyBS.getIdBdUser(), description, identifierOrigin, source );

        try {
            bioNetToMet.toMetExplore();
        } catch (Exception e) {

            mailBody = "Problem while importing the copied Network in the database.\nStack Trace first entry:\n\t";
            mailBody += e.getStackTrace()[0].toString();

            mailSubject = "Copy error";
            copyBS.sendMail(copyBS.getMail(), mailSubject, mailBody);
            toBioNet.getCon().close();
            System.exit(0);
        }
        MetExploreResult result = bioNetToMet.getResult();

        if (result.getIntError() == 1) {
            System.err.println(result.message);
            System.err.println("Rollback: No changes committed to the database.");
            mailBody = "Problem while importing the copied Network in the database.";
            mailSubject = "Copy error";
            copyBS.sendMail(copyBS.getMail(), mailSubject, mailBody);
            toBioNet.getCon().close();
            System.exit(0);
        }

        int newBSid = bioNetToMet.id_network;

        System.err.println("Network copied");
        mailBody = "The Network " + copyBS.getid_network()
                + " has successfully been copied. The id of the copied Network " + "is " + newBSid + ". ";
        mailSubject = "Copy Successful";
        copyBS.sendMail(copyBS.getMail(), mailSubject, mailBody);
        toBioNet.getCon().close();

        fwJson.write("{\"success\":\"true\", \"id_network\":\"" + newBSid + "\"}");

        fwJson.close();

    }

    @Override
    public String getLabel() {
        return this.label;
    }

    @Override
    public String getDescription() {
        return this.description;
    }

    @Override
    public Boolean getRequireNetwork() {
        return this.requireNetwork;
    }

    @Override
    public Boolean getRequireLogin() {
        return this.requireLogin;
    }

    @Override
    public Boolean getSendMail() {
        return this.sendMail;
    }

    /**
     * @return the id_network
     */
    public int getid_network() {
        return id_network;
    }

    /**
     * @return the user id
     */
    public int getIdBdUser() {
        return id_user;
    }

    /**
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    /**
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @param requireLogin
     */
    public void setRequireLogin(Boolean requireLogin) {
        this.requireLogin = requireLogin;
    }

    /**
     * @param requireNetwork
     */
    public void setRequireNetwork(Boolean requireNetwork) {
        this.requireNetwork = requireNetwork;
    }

    /**
     * @param sendMail
     */
    public void setSendMail(Boolean sendMail) {
        this.sendMail = sendMail;
    }

    /**
     * @param id_network
     */
    public void setid_network(int id_network) {
        this.id_network = id_network;
    }

    /**
     * @param idBdUser
     */
    public void setIdBdUser(int idBdUser) {
        this.id_user = idBdUser;
    }

    @Override
    public Boolean getRequireAdmin() {
        return requireAdmin;
    }

    @Override
    public void setRequireAdmin(Boolean requireAdmin) {
        this.requireAdmin = requireAdmin;
    }

    /**
     * @return the email of the user
     */
    public String getMail() {
        return mail;
    }

    /**
     * @param mail
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

}
