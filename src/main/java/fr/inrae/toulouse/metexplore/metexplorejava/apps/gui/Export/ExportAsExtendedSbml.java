/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Export;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import fr.inrae.toulouse.metexplore.metexplorexml.writer.BioNetworkToMetexploreXml;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import fr.inrae.toulouse.metexplore.metexplorejava.convert.MetExploreDataBaseToBioNetwork;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.GenericUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction;

/**
 * Export a Network in a MetExplore XML file (see the description <a
 * target='_blank'
 * href=http://metexplore.toulouse.inra.fr/joomla3/index.php/documentation/technicaldoc/docv1#extSbml>here</a>)
 *
 * @author lcottret
 */
public class ExportAsExtendedSbml extends MetExploreGuiFunction {

    /**
     * The label of this {@link MetExploreGuiFunction}
     */
    public String label = "Export as MetExplore XML";
    /**
     * The description of the {@link MetExploreGuiFunction}
     */
    public String description = "Export the current Network in a MetExplore XML file (see the description <a target='_blank' href=http://metexplore.toulouse.inra.fr/joomla3/index.php/documentation/technicaldoc/docv1#extSbml>here</a>)";
    /**
     * The 'requireLogin' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when user is not connected
     */
    public Boolean requireLogin = false;
    /**
     * The 'requireNetwork' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when no Network is
     * selected
     */
    public Boolean requireNetwork = true;
    /**
     * The 'sendMail' attributes of the {@link MetExploreGuiFunction}. Setting
     * this to true enables the method to send an email when the job is done
     */
    public Boolean sendMail = false;
    /**
     * The type of result expected by the interface
     *
     * @see MetExploreGuiFunction#resultType
     */
    public String resultType = "file";

    /**
     * The download URL
     */
    public String url = "";

    /**
     * This object convert the database network into a Java object
     */
    public MetExploreDataBaseToBioNetwork mtb;

    /**
     * Mysql id of the Network
     */
    @Option(name = "-id_network", usage = "Sql id of the Network", metaVar = "id_network", required = true)
    public int id_network;

    /**
     * We use {@link #setLongJob(Boolean)} in the constructor to override the
     * default value set in {@link MetExploreGuiFunction}
     */
    public ExportAsExtendedSbml() {
        super();
        this.setLongJob(true);
    }

    /**
     * Main
     *
     * @param args
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    public static void main(String[] args) throws Exception {

        ExportAsExtendedSbml f = new ExportAsExtendedSbml();

        CmdLineParser parser = new CmdLineParser(f);

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            if (f.getPrintJson()) {
                f.printJson();
                System.exit(1);
            } else {
                e.printStackTrace();
                GenericUtils.MetExploreError("Problem in the arguments of the function " + f.getLabel());
                System.exit(0);
            }
        }

        if (f.getPrintJson()) {
            f.printJson();
            System.exit(1);
        }

        Boolean flag = f.compute();

        if (flag == false) {
            System.exit(0);
        }

        System.exit(1);

    }

    /**
     * @return true if the computation was successful, false otherwise
     */
    public Boolean compute() throws Exception {
        mtb = new MetExploreDataBaseToBioNetwork();

        try {
            mtb.Connection(this.getDbConf());

        } catch (Exception e) {
            e.printStackTrace();
            GenericUtils.MetExploreError("Problem while connecting to the database in the function " + this.getLabel());
            return false;
        }

        mtb.setid_network(this.getid_network());

        mtb.convert();

        File tmpFile = null;

        try {
            tmpFile = File.createTempFile("ExportAsExtendedSbml", ".xml", new File(this.getTmpPath()));
        } catch (IOException e) {
            e.printStackTrace();
            GenericUtils.MetExploreError("Problem while creating the new file in the function " + this.getLabel());
            return false;
        }

        String path = tmpFile.getPath();

        try {

            BioNetworkToMetexploreXml writer = new BioNetworkToMetexploreXml(mtb.getNetwork(), path);

            writer.write();

        } catch (IOException e) {
            e.printStackTrace();
            GenericUtils.MetExploreError("Problem in saving the sbml file");
            return false;
        }

        url = this.getUrlTmpPath() + "/" + tmpFile.getName();

        FileWriter fw = new FileWriter(new File(this.jsonFile));
        fw.write("{\"success\":\"true\", \"download\":\"" + url + "\"}");
        fw.close();

        return true;

    }

    /**
     * @return the label
     */
    @Override
    public String getLabel() {
        return label;
    }

    /**
     * @return the description
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getRequireLogin()
     */
    @Override
    public Boolean getRequireLogin() {
        // TODO Auto-generated method stub
        return requireLogin;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getRequireNetwork()
     */
    @Override
    public Boolean getRequireNetwork() {
        return requireNetwork;
    }

    /**
     * @return the id_network
     */
    public int getid_network() {
        return id_network;
    }

    /**
     * @param id
     */
    public void setid_network(int id) {
        this.id_network = id;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getSendMail()
     */
    @Override
    public Boolean getSendMail() {
        return sendMail;
    }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

}
