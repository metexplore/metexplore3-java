/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Export;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.writer.JsbmlWriter;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.writer.plugin.*;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.database.MetExploreConnection;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import fr.inrae.toulouse.metexplore.metexplorejava.convert.MetExploreDataBaseToBioNetwork;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.GenericUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction;

/**
 * This class is used to export a Network in a SBML file. This is used in the
 * metexplore UI. This class, however does not use the automatic UI generation.
 * <p>
 * See MetExplore.controller.C_LaunchJavaApplication
 *
 * @author bmerlet
 */
public class JSBMLExport extends MetExploreGuiFunction {
    /**
     * The label of this {@link MetExploreGuiFunction}
     */
    public String label = "Export as SBML";
    /**
     * The description of the {@link MetExploreGuiFunction}
     */
    public String description = "Export the current Network in a SBML file.";
    /**
     * The 'requireLogin' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when user is not connected
     */
    public Boolean requireLogin = false;
    /**
     * The 'requireNetwork' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when no Network is
     * selected
     */
    public Boolean requireNetwork = true;
    /**
     * The 'sendMail' attributes of the {@link MetExploreGuiFunction}. Setting
     * this to true enables the method to send an email when the job is done
     */
    public Boolean sendMail = false;
    /**
     * The type of result expected by the interface
     *
     * @see MetExploreGuiFunction#resultType
     */
    public String resultType = "file";

    /**
     * Mysql id of the Network
     */
    @Option(name = "-id_network", usage = "Sql id of the Network", metaVar = "id_network", required = true)
    public int id_network;

    /**
     * True to use the default parameters for this class and all it's plugin
     */
    @Option(name = "-useDefaultPluginParams", usage = "Check to use the default values for all the writing Plugins")
    public boolean useDefaultPluginParams = false;

    /**
     * true to use FBC
     */
    @Option(name = "-useFbcPlugin", usage = "Uses the FBC Writer Plugin", metaVar = "advanced")
    public boolean useFbcPlugin = false;

    /**
     * true to use FBC
     */
    @Option(name = "-useGroupPlugin", usage = "Use Group plugin for pathways", metaVar = "advanced")
    public boolean useGroupPlugin = false;

    /**
     * true to export Annotation element
     */
    @Option(name = "-useAnnotPlugin", usage = "Enable the Annotation Writer Plugin", metaVar = "advanced")
    public boolean useAnnotPlugin = false;

    /**
     * true to export Notes element
     */
    @Option(name = "-useNotesPlugin", usage = "Enable the Notes Writer Plugin", metaVar = "advanced")
    public boolean useNotesPlugin = false;

    /**
     * True to update notes saved in the database upon export with values that
     * might have changed in the different entities
     */
    @Option(name = "-updateExistingNoteValues", usage = "When notes already exists for an entity, update the values with the current state. (Notes are imported when using the SBMLImport feature)", metaVar = "advanced")
    public boolean updateExistingNoteValues = false;

    @Option(name = "-jobsUrl", usage ="Url of the jobs directory", metaVar = "url", required = false)
    public String jobsUrl = "http://localhost/jobs";

    /**
     * We use {@link #setLongJob(Boolean)} in the constructor to override the
     * default value set in {@link MetExploreGuiFunction#longJob}</br>
     * The same is done for {@link #setAutoUI(Boolean)}
     */
    public JSBMLExport() {
        super();
        this.setLongJob(true);
        this.setAutoUI(false);
    }

    /**
     * Main method.
     * Used in MetExplore.controller.C_LaunchJavaApplication
     *
     * @param args arguments passed in the command line.
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    public static void main(String[] args) throws Exception {

        JSBMLExport exportSBML = new JSBMLExport();

        CmdLineParser parser = new CmdLineParser(exportSBML);

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            if (exportSBML.getPrintJson()) {
                exportSBML.printJson();
                System.exit(1);
            } else {
                e.printStackTrace();
                GenericUtils.MetExploreError("Problem in the arguments of the function " + exportSBML.getLabel());
                System.exit(0);
            }
        }

        if (exportSBML.getPrintJson()) {
            exportSBML.printJson();
            System.exit(1);
        }

        System.err.println("Initializing Export...");

        MetExploreDataBaseToBioNetwork toBioNet = new MetExploreDataBaseToBioNetwork();

        try {
            MetExploreConnection metCon;

            metCon = new MetExploreConnection(exportSBML.getDbConf());
            toBioNet.setCon(metCon.getConnection());
        } catch (Exception e) {
            e.printStackTrace();
            GenericUtils.MetExploreError(
                    "Problem while connecting to the database in the function " + exportSBML.getLabel());
            System.exit(0);
        }

        System.err.println("Converting Network " + exportSBML.getid_network() + " to Java object...");
        toBioNet.setid_network(exportSBML.getid_network());
        toBioNet.convert();
        System.err.println("Network converted to BioNetWork object...");

        try {
            toBioNet.getCon().close();
        } catch (SQLException e1) {
            e1.printStackTrace();
            GenericUtils.MetExploreError(
                    "Problem while closing the connection to the database. Please contact contact-metexplore@inrae.fr ");
        }

        BioNetwork exportedBN = toBioNet.getNetwork();

        // Get the directory where there is the jsonFile (corresponding to the
        String jobDirectory = new File(exportSBML.jsonFile).getParent();
        // Get the basename of the job directory
        String jobName = new File(jobDirectory).getName();
        // Get the user directory
        String userDirectory = new File(jobDirectory).getParent();
        // Get the name of the user directory
        String userMd5 = new File(userDirectory).getName();
        // Get the jobs directory
        String jobsDirectory = new File(userDirectory).getParent();
        // Get the basename of the jobs directory
        String jobsDirectoryName = new File(jobsDirectory).getName();

        // Be careful, the directory of the jobs file must be at the root of the
        // server

        File tmpFile = null;
        try {
            tmpFile = File.createTempFile("ExportAsSbml_" + exportedBN.getId(), ".xml",
                    new File(jobDirectory));
        } catch (IOException e) {
            e.printStackTrace();
            GenericUtils
                    .MetExploreError("Problem while creating the new file in the function " + exportSBML.getLabel());
            System.exit(0);
        }

        String urlSbmlFile = exportSBML.jobsUrl+"/"+userMd5+"/"+jobName+"/"+tmpFile.getName();

        JsbmlWriter sbmlWriter = new JsbmlWriter(jobDirectory+"/"+tmpFile.getName(), exportedBN);

        HashSet<PackageWriter> pkgs = new HashSet();

        if (exportSBML.isUseDefaultPluginParams()) {
            System.err.println("Using Default Writer plugins and parameters");
            pkgs.add(new AnnotationWriter());
            pkgs.add(new FBCWriter());
            pkgs.add(new GroupPathwayWriter());
        } else {

            if (exportSBML.isUseAnnotPlugin()) {
                pkgs.add(new AnnotationWriter());
            }
            if (exportSBML.isUseNotesPlugin()) {
                pkgs.add(new NotesWriter(false));
            }
            if (exportSBML.isUseFbcPlugin()) {
                pkgs.add(new FBCWriter());
            }

            if (exportSBML.isUseGroupPlugin()) {
                pkgs.add(new GroupPathwayWriter());
            }

        }

        sbmlWriter.write(pkgs);

        System.err.println("File successfully written.");
        System.err.println("Network export finished at : " + new Date());

        FileWriter fw = new FileWriter(new File(exportSBML.jsonFile));
        fw.write("{\"success\":\"true\", \"download\":\"" + urlSbmlFile + "\"}");
        fw.close();

    }

    /**
     * @return the label
     */
    @Override
    public String getLabel() {
        return label;
    }

    /**
     * @return the description
     */
    @Override
    public String getDescription() {
        return description;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getRequireLogin()
     */
    @Override
    public Boolean getRequireLogin() {
        return requireLogin;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getRequireNetwork()
     */
    @Override
    public Boolean getRequireNetwork() {
        return requireNetwork;
    }

    /**
     * @return the id_network
     */
    public int getid_network() {
        return id_network;
    }

    /**
     * @return {@link #useDefaultPluginParams}
     */
    public boolean isUseDefaultPluginParams() {
        return useDefaultPluginParams;
    }

    /**
     * @param useDefaultPluginParams
     */
    public void setUseDefaultPluginParams(boolean useDefaultPluginParams) {
        this.useDefaultPluginParams = useDefaultPluginParams;
    }

    /**
     * @return {@link #useAnnotPlugin}
     */
    public boolean isUseAnnotPlugin() {
        return useAnnotPlugin;
    }

    /**
     * @param useAnnotPlugin
     */
    public void setUseAnnotPlugin(boolean useAnnotPlugin) {
        this.useAnnotPlugin = useAnnotPlugin;
    }

    public boolean isUseFbcPlugin() {
        return useFbcPlugin;
    }

    public boolean isUseGroupPlugin() {
        return useGroupPlugin;
    }

    /**
     * @return {@link #useNotesPlugin}
     */
    public boolean isUseNotesPlugin() {
        return useNotesPlugin;
    }

    /**
     * @param useNotesPlugin
     */
    public void setUseNotesPlugin(boolean useNotesPlugin) {
        this.useNotesPlugin = useNotesPlugin;
    }

    /**
     * @return {@link #updateExistingNoteValues}
     */
    public boolean isUpdateExistingNoteValues() {
        return updateExistingNoteValues;
    }

    /**
     * @param updateExistingNoteValues
     */
    public void setUpdateExistingNoteValues(boolean updateExistingNoteValues) {
        this.updateExistingNoteValues = updateExistingNoteValues;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getSendMail()
     */
    @Override
    public Boolean getSendMail() {
        return sendMail;
    }

    // public int getLevel() {
    // return level;
    // }
    //
    // public int getVersion() {
    // return version;
    // }

    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    // public void setLevel(int level) {
    // this.level = level;
    // }
    //
    // public void setVersion(int version) {
    // this.version = version;
    // }
    //
    // public Boolean getConvert() {
    // return convert;
    // }
    //
    // public void setConvert(Boolean convert) {
    // this.convert = convert;
    // }

}
