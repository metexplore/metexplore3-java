/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/

package fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Flux;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioEntity;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioReaction;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.collection.BioCollection;
import fr.inrae.toulouse.metexplore.met4j_flux.analyses.DRAnalysis;
import fr.inrae.toulouse.metexplore.met4j_flux.analyses.result.DRResult;
import fr.inrae.toulouse.metexplore.met4j_flux.general.Bind;
import fr.inrae.toulouse.metexplore.met4j_flux.general.GLPKBind;
import fr.inrae.toulouse.metexplore.met4j_flux.general.Vars;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.network.NetworkAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.reaction.Flux;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.reaction.ReactionAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.BioUnitDefinition;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.GenericUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

/**
 * @author lcottret
 */
public class DeadOrAlive extends MetExploreGuiFunction {
    /**
     * The label of this {@link MetExploreGuiFunction}
     */
    public String label = "Dead reactions";
    /**
     * The description of the {@link MetExploreGuiFunction}
     */
    public String description = "Identification of dead reactions, i.e those that are unable to carry a steady state flux.";
    /**
     * The 'requireLogin' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when user is not connected
     */
    public Boolean requireLogin = false;
    /**
     * The 'requireNetwork' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when no Network is
     * selected
     */
    public Boolean requireNetwork = true;
    /**
     * The 'sendMail' attributes of the {@link MetExploreGuiFunction}. Setting
     * this to true enables the method to send an email when the job is done
     */
    public Boolean sendMail = false;

    /**
     * Mysql id of the Network
     */
    @Option(name = "-id_network", usage = "Sql id of the Network", metaVar = "id_network", required = true)
    public int id_network = -1;

    @Option(name = "-mode", usage = "Dead reactions mode : if not checked, reactions flux bounds are not changed, if checked,"
            + "all reactions flux bounds are set to a maximum value.")
    public Boolean mode = false;

    /**
     * Constructor
     */
    public DeadOrAlive() {
        super();
        this.setLongJob(true);
    }

    /**
     * @param args
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static void main(String[] args) throws Exception {
        DeadOrAlive f = new DeadOrAlive();

        CmdLineParser parser = new CmdLineParser(f);

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            if (f.getPrintJson()) {
                f.printJson();
                System.exit(1);
            } else {
                e.printStackTrace();
                GenericUtils
                        .MetExploreError("Problem in the arguments of the function: "
                                + f.getLabel()
                                + ". . Please contact contact-metexplore@inrae.fr ");
                System.exit(0);
            }
        }

        JSONObject jsonObject = f.run();

        String json = jsonObject.toJSONString();

        FileWriter fw = new FileWriter(new File(f.jsonFile));
        fw.write(json);
        fw.close();

    }

    public JSONObject run() throws Exception {
        if (this.getPrintJson()) {
            this.printJson();
            System.exit(1);
        }

        this.initNetwork(this.getid_network());

        BioNetwork network = this.getNetwork();

        if (network == null) {
            GenericUtils
                    .MetExploreError("Problem while getting the network in the function "
                            + this.getLabel());
            System.exit(0);
        }

        Set<String> originalReactionIds = new HashSet<String>(network.getReactionsView().getIds());
        Bind bind = new GLPKBind();
        Vars.decimalPrecision = 4;

        double nTotalReactions = network.getReactionsView().size();

        BioCollection<BioUnitDefinition> unitDefinitions = NetworkAttributes.getUnitDefinitions(network);

        BioUnitDefinition ud;

        if (unitDefinitions.size() == 1) {
            ud = unitDefinitions.iterator().next();
        } else {
            ud = new BioUnitDefinition();
        }

        if (this.getMode()) {

            // Weird behaviour : if we put 99999.0 instead of 1000.0, it finds
            // bad results...
            Flux fluxMax = new Flux("fluxmax", 1000.0, ud);
            Flux fluxMin = new Flux("fluxmin", -1000.0, ud);
            Flux fluxZero = new Flux("fluxzero", 0.0, ud);

            for (BioReaction reaction : network.getReactionsView()) {

                if (reaction.isReversible()) {
                    ReactionAttributes.setLowerBound(reaction, fluxMin);
                } else {
                    ReactionAttributes.setLowerBound(reaction, fluxZero);
                }
                ReactionAttributes.setUpperBound(reaction, fluxMax);
            }
        }

        bind.setNetworkAndConstraints(network);

        bind.prepareSolver();

        DRAnalysis analysis = new DRAnalysis(bind, 0.000001);
        DRResult result = analysis.runAnalysis();

        Set<String> resultIds = new HashSet<String>();

        for (BioEntity ent : result.getDeadReactions()) {
            resultIds.add(ent.getId());
        }

        ArrayList<HashMap<String, String>> results = new ArrayList<HashMap<String, String>>();

        int nDeads = result.getDeadReactions().size();

        for (String reactionId : originalReactionIds) {
            HashMap<String, String> map = new HashMap<String, String>();

            map.put("db_identifier", reactionId);
            if (resultIds.contains(reactionId)) {
                map.put("dead", "1");
            } else {
                map.put("dead", "0");
            }
            results.add(map);
        }

        Double per = (double) (Math.round(nDeads / nTotalReactions * 10000)) / 100;

        JSONArray json_results = new JSONArray();
        json_results.addAll(results);
        JSONObject jsonObject = new JSONObject();
        jsonObject
                .put("message",
                        nDeads
                                + " dead reactions ("
                                + per
                                + "%). These"
                                + " have been flagged in the grid of reactions (0:live,1:dead)");

        jsonObject.put("success", "true");

        jsonObject.put("reactions", json_results);

        jsonObject.put("nb_dead", Math.round(nDeads));

        bind.end();
        return jsonObject;

    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getLabel()
     */
    @Override
    public String getLabel() {
        return this.label;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getDescription()
     */
    @Override
    public String getDescription() {
        return this.description;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getRequireLogin()
     */
    @Override
    public Boolean getRequireLogin() {
        return this.requireLogin;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getRequireNetwork()
     */
    @Override
    public Boolean getRequireNetwork() {
        return this.requireNetwork;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getSendMail()
     */
    @Override
    public Boolean getSendMail() {
        return this.sendMail;
    }

    /**
     * @return the id_network
     */
    public int getid_network() {
        return id_network;
    }

    public Boolean getMode() {
        return mode;
    }

}
