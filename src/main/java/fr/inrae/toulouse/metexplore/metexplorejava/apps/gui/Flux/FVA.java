/**
 * Copyright INRA
 * <p>
 * Contact: ludovic.cottret@toulouse.inra.fr
 * <p>
 * <p>
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * <p>
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 */

/**
 * Computes the optimal objective function
 */
package fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Flux;

import java.io.File;
import java.io.FileWriter;
import java.util.*;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioEntity;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.collection.BioCollection;
import fr.inrae.toulouse.metexplore.met4j_flux.analyses.FVAAnalysis;
import fr.inrae.toulouse.metexplore.met4j_flux.analyses.result.FVAResult;
import fr.inrae.toulouse.metexplore.met4j_flux.general.Bind;
import fr.inrae.toulouse.metexplore.met4j_flux.general.GLPKBind;
import fr.inrae.toulouse.metexplore.met4j_flux.general.Vars;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import org.hibernate.validator.constraints.Range;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;


import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.FluxUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.GenericUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.FluxUtils.Senses;


/**
 *
 * @author lcottret
 *
 */
public class FVA extends MetExploreGuiFunction {
    /**
     * The label of this {@link MetExploreGuiFunction}
     */
    public String label = "Flux Variability Analysis";
    /**
     * The description of the {@link MetExploreGuiFunction}
     */
    public String description = "Computes the minimum and the maximum flux for each reaction "
            + "that allows the objective function to be optimal";
    /**
     * The 'requireLogin' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when user is not connected
     */
    public Boolean requireLogin = false;
    /**
     * The 'requireNetwork' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when no Network is
     * selected
     */
    public Boolean requireNetwork = true;
    /**
     * The 'sendMail' attributes of the {@link MetExploreGuiFunction}. Setting
     * this to true enables the method to send an email when the job is done
     */
    public Boolean sendMail = false;

    /**
     * Mysql id of the Network
     */
    @Option(name = "-id_network", usage = "Sql id of the Network", metaVar = "id_network", required = true)
    public int id_network = -1;

    /**
     * List of the objective reactions
     */
    @Option(name = "-objectiveReactions", usage = "Select reactions to put them as objective function.", metaVar = "reactions", required = true)
    public String objectiveReactions = "";

    @Option(name = "-objectiveSense", usage = "Minimize or Maximize the objective function", required = true)
    public Senses objectiveSense = Senses.MAX;

    @Option(name = "-reactionSet", usage = "Select reactions to compute FVA only on these ones. If no selection, all the reactions are taken into account", metaVar = "reactions", required = false)
    public String reactionSet = "";

    @Option(name = "-secondObjectiveReactions", usage = "Select a second set of reactions to put them as second objective function."
            + "If you select a second objective function, the two objective functions are sequentially considered. "
            + "The optimal value of the first one becomes a new constraint for the second run", metaVar = "reactions", required = false)
    public String secondObjectiveReactions = null;

    @Option(name = "-secondObjectiveSense", usage = "Minimize or Maximize the second objective function", required = false)
    public Senses secondObjectiveSense = Senses.MAX;

    @Range(min = 0, max = 100)
    @Option(name = "-libertyPercentage", usage = "Liberty of percentage in the constraints set for the first optimal value found "
            + "(used if a second objective function is set", required = false)
    public Double libertyPercentage = 0.0;

    @Option(name = "-ko_genes", usage = "Genes to knock out", metaVar = "genes", required = false)
    public String ko_genes = "";

    @Option(name = "-ko_reactions", usage = "Reactions to knock out", metaVar = "reactions", required = false)
    public String ko_reactions = "";

    // @Option(name = "-regFile", usage = "Regulation file path (<a
    // href=\"http://lipm-bioinfo.toulouse.inra.fr/flexflux/documentation.html#reg\"
    // target=_blank>sbml-qual format</a>)", metaVar = "file", required=false)
    // public String regFile = "";

    /**
     * Constructor
     */
    public FVA() {
        super();
        this.setLongJob(true);
    }

    public static void main(String[] args) throws Exception {
        FVA f = new FVA();

        CmdLineParser parser = new CmdLineParser(f);

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            if (f.getPrintJson()) {
                f.printJson();
                System.exit(1);
            } else {
                e.printStackTrace();
                GenericUtils.MetExploreError("Problem in the arguments of the function: " + f.getLabel()
                        + ". Please contact contact-metexplore@inrae.fr ");
                System.exit(0);
            }
        }

        JSONObject jsonObject = f.run();

        String json = jsonObject.toString();

        Gson gson = new GsonBuilder().setPrettyPrinting().create();

        JsonParser jp = new JsonParser();
        JsonElement je = jp.parse(json);
        String prettyJsonString = gson.toJson(je);

        FileWriter fw = new FileWriter(new File(f.jsonFile));
        fw.write(prettyJsonString);
        fw.close();

    }

    @SuppressWarnings("unchecked")
    public JSONObject run() throws Exception {
        if (this.getPrintJson()) {
            this.printJson();
            System.exit(1);
        }

        this.initNetwork(this.getid_network());

        BioNetwork network = this.getNetwork();

        if (network == null) {
            GenericUtils.MetExploreError("Problem while getting the network in the function " + this.getLabel());
            System.exit(0);
        }

        // Sets the solver
        Vars.libertyPercentage = this.getLibertyPercentage();
        Vars.decimalPrecision = 4;

        Bind bind = new GLPKBind();

        bind.setNetworkAndConstraints(network);

        Boolean nested = false;

        // adds the nested objective functions
        if (this.getSecondObjectiveReactions() != null && !this.getSecondObjectiveReactions().equals("")) {

            FluxUtils.addNestedObjectiveFunction(this.getObjectiveReactions(), this.getObjectiveSense(), bind);
            FluxUtils.addObjectiveFunction(this.getSecondObjectiveReactions(), this.getSecondObjectiveSense(), bind);

            nested = true;
        } else {
            FluxUtils.addObjectiveFunction(this.getObjectiveReactions(), this.getObjectiveSense(), bind);
        }

        // adds the ko as constraints
        FluxUtils.addKoConstraints(this.getKo_genes(), bind);
        FluxUtils.addKoConstraints(this.getKo_reactions(), bind);

        // select reactions to apply FVA

        BioCollection<BioEntity> entitiesMap = new BioCollection<BioEntity>();

        Set<String> firstObj = new HashSet<String>();
        for (String r : this.getObjectiveReactions().split(",")) {
            firstObj.add(r);
        }

        Set<String> secondObj = new HashSet<String>();
        if (nested) {
            for (String r : this.getSecondObjectiveReactions().split(",")) {
                secondObj.add(r);
            }
        }

        if (!this.getReactionSet().equals("")) {
            ArrayList<String> entitiesArray = new ArrayList<String>(Arrays.asList(this.getReactionSet().split(",")));
            entitiesArray.addAll(firstObj);
            entitiesArray.addAll(secondObj);

            for (String id : entitiesArray) {

                BioEntity b = bind.getInteractionNetwork().getEntity(id);
                if (b == null) {
                    System.err.println("Unknown entity " + id);
                } else {

                    entitiesMap.add(b);
                }
            }
        }

        // if (this.regFile != "") {
        // bind.loadRegulationFile(this.regFile);
        // }

        bind.prepareSolver();

        FVAAnalysis analysis;

        if (entitiesMap.size() == 0) {
            analysis = new FVAAnalysis(bind, null, null);
        } else {
            analysis = new FVAAnalysis(bind, entitiesMap, null);
        }

        FVAResult result = analysis.runAnalysis();

        ArrayList<HashMap<String, String>> results = new ArrayList<HashMap<String, String>>();

        Double valFirstObj = 0.0;

        for (BioEntity entity : result.getMap().keySet()) {

            HashMap<String, String> map = new HashMap<String, String>();

            map.put("db_identifier", entity.getId());
            map.put("min", Double.toString(Vars.round(result.getMap().get(entity)[0])));
            map.put("max", Double.toString(Vars.round(result.getMap().get(entity)[1])));

            results.add(map);

            if (nested && firstObj.contains(entity.getId())) {
                valFirstObj += Vars.round(result.getMap().get(entity)[0]);
            }

        }

        JSONArray json_results = new JSONArray();
        json_results.addAll(results);
        JSONObject jsonObject = new JSONObject();

        if (!nested) {
            jsonObject.put("message",
                    new String("Optimal value of the objective function: "
                            + Math.round(result.getObjValue() * 1000.0) / 1000.0 + "\n"
                            + "Results of FVA have been added in the reaction grid."));
        } else {
            jsonObject.put("message",
                    new String("Optimal value of the first objective function: "
                            + Math.round(valFirstObj * 1000.0) / 1000.0 + "<br />"
                            + "Optimal value of the second objective function: "
                            + Math.round(result.getObjValue() * 1000.0) / 1000.0 + "<br />"
                            + "Results of FVA have been added in the reaction grid."));
        }
        jsonObject.put("success", "true");

        jsonObject.put("reactions", json_results);

        bind.end();

        return jsonObject;

    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getLabel()
     */
    @Override
    public String getLabel() {
        return this.label;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getDescription()
     */
    @Override
    public String getDescription() {
        return this.description;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getRequireLogin()
     */
    @Override
    public Boolean getRequireLogin() {
        return this.requireLogin;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getRequireNetwork()
     */
    @Override
    public Boolean getRequireNetwork() {
        return this.requireNetwork;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getSendMail()
     */
    @Override
    public Boolean getSendMail() {
        return this.sendMail;
    }

    /**
     * @return the id_network
     */
    public int getid_network() {
        return id_network;
    }

    /**
     * @return the objectiveReactions
     */
    public String getObjectiveReactions() {
        return objectiveReactions;
    }

    public Senses getObjectiveSense() {
        return objectiveSense;
    }

    public String getSecondObjectiveReactions() {
        return secondObjectiveReactions;
    }

    public Senses getSecondObjectiveSense() {
        return secondObjectiveSense;
    }

    public Double getLibertyPercentage() {
        return libertyPercentage;
    }

    public String getKo_genes() {
        return ko_genes;
    }

    public String getKo_reactions() {
        return ko_reactions;
    }

    public String getReactionSet() {
        return reactionSet;
    }

}
