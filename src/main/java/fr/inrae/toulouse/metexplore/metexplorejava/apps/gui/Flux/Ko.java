/**
 * Copyright INRA
 * <p>
 * Contact: ludovic.cottret@toulouse.inra.fr
 * <p>
 * <p>
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * <p>
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 */

/**
 * Computes the optimal objective function
 */
package fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Flux;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioEntity;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_flux.analyses.KOAnalysis;
import fr.inrae.toulouse.metexplore.met4j_flux.analyses.result.KOResult;
import fr.inrae.toulouse.metexplore.met4j_flux.general.Bind;
import fr.inrae.toulouse.metexplore.met4j_flux.general.GLPKBind;
import fr.inrae.toulouse.metexplore.met4j_flux.general.Vars;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.FluxUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.FluxUtils.Senses;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.GenericUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;


public class Ko extends MetExploreGuiFunction {

    public String label = "Knock out Analysis";
    public String description = "Performs a ko in each reaction or each gene and computes the optimal objective function value";
    public Boolean requireLogin = false;
    public Boolean requireNetwork = true;
    public Boolean sendMail = false;

    /**
     * Mysql id of the Network
     */
    @Option(name = "-id_network", usage = "Sql id of the Network", metaVar = "id_network", required = true)
    public int id_network = -1;

    /**
     * List of the objective reactions
     */
    @Option(name = "-objectiveReactions", usage = "Select reactions to put them as objective function.", metaVar = "reactions", required = true)
    public String objectiveReactions = "";

    @Option(name = "-objectiveSense", usage = "Minimize or Maximize the objective function", required = true)
    public Senses objectiveSense = Senses.MAX;

    @Option(name = "-secondObjectiveReactions", usage = "Select a second set of reactions to put them as second objective function."
            + "If you select a second objective function, the two objective functions are sequentially considered. "
            + "The optimal value of the first one becomes a new constraint for the second run", metaVar = "reactions", required = false)
    public String secondObjectiveReactions = null;

    @Option(name = "-secondObjectiveSense", usage = "Minimize or Maximize the second objective function", required = false)
    public Senses secondObjectiveSense = Senses.MAX;
    @Option(name = "-ko_type", usage = "Select the entities that you want to knock out: genes or reactions", required = false)
    public KoTypes ko_type = KoTypes.genes;

    ;
    @Option(name = "-ko_genes", usage = "Genes to knock out (in addition to each gene or reaction)",
            metaVar = "genes", required = false)
    public String ko_genes = "";
    @Option(name = "-ko_reactions", usage = "Reactions to knock out (in addition to each gene or reaction)", metaVar = "reactions", required = false)
    public String ko_reactions = "";

    /**
     * Constructor
     */
    public Ko() {
        super();
        this.setLongJob(true);
    }

    /**
     * Main
     * @param args
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static void main(String[] args) throws Exception {

        Ko f = new Ko();

        CmdLineParser parser = new CmdLineParser(f);

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            if (f.getPrintJson()) {
                f.printJson();
                System.exit(1);
            } else {
                e.printStackTrace();
                GenericUtils
                        .MetExploreError("Problem in the arguments of the function: "
                                + f.getLabel()
                                + ". . Please contact contact-metexplore@inrae.fr ");
                System.exit(0);
            }
        }


        JSONObject jsonObject = f.run();

        String json = jsonObject.toJSONString();

        FileWriter fw = new FileWriter(new File(f.jsonFile));
        fw.write(json);
        fw.close();

    }

    public JSONObject run() throws Exception {
        if (this.getPrintJson()) {
            this.printJson();
            System.exit(1);
        }

        this.initNetwork(this.getid_network());

        BioNetwork network = this.getNetwork();

        if (network == null) {
            GenericUtils
                    .MetExploreError("Problem while getting the network in the function "
                            + this.getLabel());
            System.exit(0);
        }

        // Sets the solver
        Vars.decimalPrecision = 6;

        Bind bind = new GLPKBind();

        bind.setNetworkAndConstraints(network);

        // adds the nested objective functions

        if (this.getSecondObjectiveReactions() != null
                && !this.getSecondObjectiveReactions().equals("")) {
            FluxUtils.addNestedObjectiveFunction(this.getObjectiveReactions(),
                    this.getObjectiveSense(), bind);
            FluxUtils.addObjectiveFunction(this.getSecondObjectiveReactions(),
                    this.getSecondObjectiveSense(), bind);
        } else {
            FluxUtils.addObjectiveFunction(this.getObjectiveReactions(),
                    this.getObjectiveSense(), bind);
        }


        // adds the ko as constraints
        FluxUtils.addKoConstraints(this.getKo_genes(), bind);
        FluxUtils.addKoConstraints(this.getKo_reactions(), bind);

        bind.prepareSolver();

        int mode = 1;
        if (this.getKoType().equals(KoTypes.reactions)) {
            mode = 0;
        }

        KOAnalysis analysis = new KOAnalysis(bind, mode, null);

        KOResult result = analysis.runAnalysis();

        ArrayList<HashMap<String, String>> results = new ArrayList<HashMap<String, String>>();

        for (BioEntity entity : result.getMap().keySet()) {

            HashMap<String, String> map = new HashMap<String, String>();

            map.put("db_identifier", entity.getId());
            map.put("objective_value",
                    Double.toString(Vars.round(result.getMap().get(entity))));
            results.add(map);
        }

        JSONArray json_results = new JSONArray();
        json_results.addAll(results);
        JSONObject jsonObject = new JSONObject();
        jsonObject
                .put("message",
                        new String(
                                "Results of Ko have been added in the grid of " + this.getKoType()));

        jsonObject.put("success", "true");

        jsonObject.put(this.getKoType().toString(), json_results);

        bind.end();
        return jsonObject;


    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getLabel()
     */
    @Override
    public String getLabel() {
        return this.label;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getDescription()
     */
    @Override
    public String getDescription() {
        return this.description;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getRequireLogin()
     */
    @Override
    public Boolean getRequireLogin() {
        return this.requireLogin;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getRequireNetwork()
     */
    @Override
    public Boolean getRequireNetwork() {
        return this.requireNetwork;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getSendMail()
     */
    @Override
    public Boolean getSendMail() {
        return this.sendMail;
    }

    /**
     * @return the id_network
     */
    public int getid_network() {
        return id_network;
    }

    /**
     * @return the objectiveReactions
     */
    public String getObjectiveReactions() {
        return objectiveReactions;
    }

    public Senses getObjectiveSense() {
        return objectiveSense;
    }

    public String getSecondObjectiveReactions() {
        return secondObjectiveReactions;
    }

    public Senses getSecondObjectiveSense() {
        return secondObjectiveSense;
    }

    public String getKo_genes() {
        return ko_genes;
    }

    public String getKo_reactions() {
        return ko_reactions;
    }

    public KoTypes getKoType() {
        return ko_type;
    }

    public enum KoTypes {
        reactions, genes
    }

}
