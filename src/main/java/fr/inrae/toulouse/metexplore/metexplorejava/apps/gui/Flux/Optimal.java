/**
 * Copyright INRA
 * <p>
 * Contact: ludovic.cottret@toulouse.inra.fr
 * <p>
 * <p>
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * <p>
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 */

/**
 * Computes the optimal objective function
 */
package fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Flux;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.hibernate.validator.constraints.Range;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import fr.inrae.toulouse.metexplore.met4j_flux.analyses.FBAAnalysis;
import fr.inrae.toulouse.metexplore.met4j_flux.analyses.result.FBAResult;
import fr.inrae.toulouse.metexplore.met4j_flux.general.Bind;
import fr.inrae.toulouse.metexplore.met4j_flux.general.GLPKBind;
import fr.inrae.toulouse.metexplore.met4j_flux.general.Vars;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.FluxUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.GenericUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.FluxUtils.Senses;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;

/**
 *
 * @author lcottret
 *
 */
public class Optimal extends MetExploreGuiFunction {
    /**
     * The label of this {@link MetExploreGuiFunction}
     */
    public String label = "Flux Balance Analysis";
    /**
     * The description of the {@link MetExploreGuiFunction}
     */
    public String description = "Computes the optimal value of an objective reaction";
    /**
     * The 'requireLogin' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when user is not connected
     */
    public Boolean requireLogin = false;
    /**
     * The 'requireNetwork' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when no Network is
     * selected
     */
    public Boolean requireNetwork = true;
    /**
     * The 'sendMail' attributes of the {@link MetExploreGuiFunction}. Setting
     * this to true enables the method to send an email when the job is done
     */
    public Boolean sendMail = false;

    /**
     * Mysql id of the Network
     */
    @Option(name = "-id_network", usage = "Sql id of the Network", metaVar = "id_network", required = true)
    public int id_network = -1;

    /**
     * List of the objective reactions
     */
    @Option(name = "-objectiveReactions", usage = "Select reactions to put them as objective function.", metaVar = "reactions", required = true)
    public String objectiveReactions = "";

    @Option(name = "-objectiveSense", usage = "Minimize or Maximize the objective function", required = true)
    public Senses objectiveSense = Senses.MAX;

    @Option(name = "-secondObjectiveReactions", usage = "Select a second set of reactions to put them as second objective function."
            + "If you select a second objective function, the two objective functions are sequentially considered. "
            + "The optimal value of the first one becomes a new constraint for the second run", metaVar = "reactions", required = false)
    public String secondObjectiveReactions = null;

    @Option(name = "-secondObjectiveSense", usage = "Minimize or Maximize the second objective function", required = false)
    public Senses secondObjectiveSense = Senses.MAX;

    @Range(min = 0, max = 100)
    @Option(name = "-libertyPercentage", usage = "Liberty of percentage in the constraints set for the first optimal value found "
            + "(used if a second objective function is set", required = false)
    public Double libertyPercentage = 0.0;

    @Option(name = "-ko_genes", usage = "Genes to knock out", metaVar = "genes", required = false)
    public String ko_genes = "";

    @Option(name = "-ko_reactions", usage = "Reactions to knock out", metaVar = "reactions", required = false)
    public String ko_reactions = "";

    public Optimal() {
        super();
        this.setLongJob(true);
    }

    public static void main(String[] args) throws Exception {
        Optimal f = new Optimal();

        CmdLineParser parser = new CmdLineParser(f);

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            if (f.getPrintJson()) {
                f.printJson();
                System.exit(1);
            } else {
                e.printStackTrace();
                GenericUtils.MetExploreError("Problem in the arguments of the function: " + f.getLabel()
                        + ". . Please contact contact-metexplore@inrae.fr ");
                System.exit(0);
            }
        }

        String jsonStr = f.run();

        FileWriter fw = new FileWriter(new File(f.jsonFile));
        fw.write(jsonStr);
        fw.close();
    }


    public String run() throws Exception {

        System.err.println("run FBA");
        System.err.println(new java.util.Date());
        int id_network = this.getid_network();

        if (this.getPrintJson()) {
            this.printJson();
            System.exit(1);
        }

        this.initNetwork(id_network);

        BioNetwork network = this.getNetwork();

        if (network == null) {
            GenericUtils.MetExploreError("Problem while getting the network in the function " + this.getLabel());
            System.exit(0);
        }

        // Sets the solver
        Vars.libertyPercentage = this.getLibertyPercentage();
        Vars.decimalPrecision = 4;

        Bind bind = new GLPKBind();

        bind.setNetworkAndConstraints(network);

        // adds the nested objective functions

        if (this.getSecondObjectiveReactions() != null && !this.getSecondObjectiveReactions().equals("")) {
            FluxUtils.addNestedObjectiveFunction(this.getObjectiveReactions(), this.getObjectiveSense(), bind);
            FluxUtils.addObjectiveFunction(this.getSecondObjectiveReactions(), this.getSecondObjectiveSense(), bind);
        } else {
            FluxUtils.addObjectiveFunction(this.getObjectiveReactions(), this.getObjectiveSense(), bind);
        }

        // adds the ko as constraints
        FluxUtils.addKoConstraints(this.getKo_genes(), bind);
        FluxUtils.addKoConstraints(this.getKo_reactions(), bind);

        bind.prepareSolver();
        // DoubleResult value = bind.FBA(constraintsToAdd, false, true);

        FBAAnalysis analysis = new FBAAnalysis(bind);
        FBAResult result = analysis.runAnalysis();
        System.err.println("End analysis");
        System.err.println(new java.util.Date());


        String message = "";

        Map<String, String> res = result.getEntToResult();

        if (this.getSecondObjectiveReactions() == null || this.getSecondObjectiveReactions().equals("")) {
            message = "Optimal value of the objective function: " + Math.round(result.getObjValue() * 1000.0) / 1000.0 + "<br />";
        } else {

            Double valFirstObj = 0.0;

            for (String r : this.getObjectiveReactions().split(",")) {
                Double val;

                try {
                    val = Double.parseDouble(res.get(r));
                } catch (Exception e) {
                    val = 0.0;
                }

                valFirstObj = valFirstObj + val;
            }

            message = "Optimal value of the first objective function: "
                    + Math.round(valFirstObj * 1000.0) / 1000.0 + "<br />"
                    + "Optimal value of the second objective function: "
                    + Math.round(result.getObjValue() * 1000.0) / 1000.0 + "<br />"
            ;

        }

        bind.end();

        ArrayList<HashMap<String, String>> results = new ArrayList<HashMap<String, String>>();

        for (String reactionId : network.getReactionsView().getIds()) {
            HashMap<String, String> map = new HashMap<String, String>();

            map.put("db_identifier", reactionId);
            if (res.containsKey(reactionId)) {
                map.put("fba", res.get(reactionId));
            } else {
                map.put("fba", "0");
            }
            results.add(map);
        }

        message += "Flux distribution are displayed in the reaction grid.";

        JSONArray json_results = new JSONArray();
        json_results.addAll(results);
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("message",message);
        jsonObject.put("reactions", json_results);

        jsonObject.put("success", "true");
        return (jsonObject.toJSONString());

    }

    /**
     *
     * @param objectiveReactionsStr
     * @param objectiveSense
     * @param bind
     * @return
     */

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getLabel()
     */
    @Override
    public String getLabel() {
        return this.label;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getDescription()
     */
    @Override
    public String getDescription() {
        return this.description;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getRequireLogin()
     */
    @Override
    public Boolean getRequireLogin() {
        return this.requireLogin;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getRequireNetwork()
     */
    @Override
    public Boolean getRequireNetwork() {
        return this.requireNetwork;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getSendMail()
     */
    @Override
    public Boolean getSendMail() {
        return this.sendMail;
    }

    /**
     * @return the id_network
     */
    public int getid_network() {
        return id_network;
    }

    /**
     * @return the objectiveReactions
     */
    public String getObjectiveReactions() {
        return objectiveReactions;
    }

    public Senses getObjectiveSense() {
        return objectiveSense;
    }

    public String getSecondObjectiveReactions() {
        return secondObjectiveReactions;
    }

    public Senses getSecondObjectiveSense() {
        return secondObjectiveSense;
    }

    public Double getLibertyPercentage() {
        return libertyPercentage;
    }

    public String getKo_genes() {
        return ko_genes;
    }

    public String getKo_reactions() {
        return ko_reactions;
    }

}
