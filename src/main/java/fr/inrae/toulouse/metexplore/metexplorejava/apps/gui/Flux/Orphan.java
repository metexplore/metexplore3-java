/**
 * Copyright INRA
 * <p>
 * Contact: ludovic.cottret@toulouse.inra.fr
 * <p>
 * <p>
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * <p>
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 * <p>
 * Computes the optimal objective function
 */

/**
 * Computes the optimal objective function
 */
package fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Flux;

import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.collection.BioCollection;
import fr.inrae.toulouse.metexplore.met4j_flux.utils.BioNetworkUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import fr.inrae.toulouse.metexplore.metexplorejava.utils.GenericUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioMetabolite;

/**
 *
 * @author lcottret
 *
 */
public class Orphan extends MetExploreGuiFunction {

    /**
     * The label of this {@link MetExploreGuiFunction}
     */
    public String label = "Orphan metabolites";
    /**
     * The description of the {@link MetExploreGuiFunction}
     */
    public String description = "Detects the orphan metabolites (i.e internal metabolites used by less than two reactions"
            + " and that are not produced and/or not consumed). The identification does not depend on the objective function.";
    /**
     * The 'requireLogin' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when user is not connected
     */
    public Boolean requireLogin = false;
    /**
     * The 'requireNetwork' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when no Network is
     * selected
     */
    public Boolean requireNetwork = true;
    /**
     * The 'sendMail' attributes of the {@link MetExploreGuiFunction}. Setting
     * this to true enables the method to send an email when the job is done
     */
    public Boolean sendMail = false;

    /**
     * Mysql id of the Network
     */
    @Option(name = "-id_network", usage = "Sql id of the Network", metaVar = "id_network", required = true)
    public int id_network = -1;

    /**
     * Constructor
     */
    public Orphan() {
        super();
        this.setLongJob(true);
    }

    /**
     * Main
     * @param args
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static void main(String[] args) throws Exception {

        Orphan f = new Orphan();

        CmdLineParser parser = new CmdLineParser(f);

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            if (f.getPrintJson()) {
                f.printJson();
                System.exit(1);
            } else {
                e.printStackTrace();
                GenericUtils
                        .MetExploreError("Problem in the arguments of the function: "
                                + f.getLabel()
                                + ". . Please contact contact-metexplore@inrae.fr ");
                System.exit(0);
            }
        }


        JSONObject jsonObject = f.run();

        String json = jsonObject.toJSONString();

        FileWriter fw = new FileWriter(new File(f.jsonFile));
        fw.write(json);
        fw.close();
    }

    @SuppressWarnings("unchecked")
    public JSONObject run() throws Exception {
        if (this.getPrintJson()) {
            this.printJson();
            System.exit(1);
        }

        this.initNetwork(this.getid_network());

        BioNetwork network = this.getNetwork();

        BioCollection<BioMetabolite> orphans = BioNetworkUtils.getOrphanMetabolites(network);

        ArrayList<HashMap<String, String>> results = new ArrayList<HashMap<String, String>>();

        for (String cpdId : network.getMetabolitesView().getIds()) {

            HashMap<String, String> map = new HashMap<String, String>();
            map.put("db_identifier", cpdId);
            if (orphans.containsId(cpdId)) {
                map.put("orphan", "1");
            } else {
                map.put("orphan", "0");
            }

            results.add(map);

        }


        JSONArray json_results = new JSONArray();
        json_results.addAll(results);
        JSONObject jsonObject = new JSONObject();
        jsonObject
                .put("message",
                        new String(orphans.size() + " orphan metabolites have been identified." +
                                "Results of orphan metabolite identification have been added in the grid of metabolites"));

        jsonObject.put("success", "true");
        jsonObject.put("metabolites", json_results);
        jsonObject.put("nb_orphans", orphans.size());

        return jsonObject;

    }


    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getLabel()
     */
    @Override
    public String getLabel() {
        return this.label;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getDescription()
     */
    @Override
    public String getDescription() {
        return this.description;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getRequireLogin()
     */
    @Override
    public Boolean getRequireLogin() {
        return this.requireLogin;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getRequireNetwork()
     */
    @Override
    public Boolean getRequireNetwork() {
        return this.requireNetwork;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getSendMail()
     */
    @Override
    public Boolean getSendMail() {
        return this.sendMail;
    }

    /**
     * @return the id_network
     */
    public int getid_network() {
        return id_network;
    }


}
