package fr.inrae.toulouse.metexplore.metexplorejava.apps.gui;

import java.util.HashMap;
import java.util.Map;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioEntity;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioReaction;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.collection.BioCollection;
import fr.inrae.toulouse.metexplore.met4j_flux.general.Bind;
import fr.inrae.toulouse.metexplore.met4j_flux.general.Constraint;
import fr.inrae.toulouse.metexplore.met4j_flux.general.Vars;
import fr.inrae.toulouse.metexplore.met4j_flux.objective.Objective;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.GenericUtils;

/**
 * @author lcottret
 */
public class FluxUtils {
    public static void addKoConstraints(String list, Bind bind) {
        if (list != null && !list.equals("")) {
            String[] kos = list.split(",");

            for (String ko : kos) {

                Map<BioEntity, Double> constMap = new HashMap<BioEntity, Double>();

                constMap.put(bind.getInteractionNetwork().getEntity(ko),
                        1.0);

                Constraint c = new Constraint(constMap, 0.0, 0.0);
                bind.getSimpleConstraints().put(bind.getInteractionNetwork().getEntity(ko), c);
                bind.getConstraints().add(c);

            }
        }
    }

    ;

    /**
     * Adds an objective function result as a constraint for the following one
     *
     * @param objectiveReactionsStr
     * @param objectiveSense
     * @param bind
     */
    public static void addNestedObjectiveFunction(String objectiveReactionsStr,
                                                  Senses objectiveSense, Bind bind) {
        String[] objectiveReactions = objectiveReactionsStr.split(",");

        BioEntity[] entities = new BioEntity[objectiveReactions.length];
        double[] coeffs = new double[objectiveReactions.length];
        int i = 0;
        for (String objectiveReaction : objectiveReactions) {

            BioReaction reaction = bind.getBioNetwork()
                    .getReaction(objectiveReaction);

            if (reaction == null) {

                // check if reaction is a key word
                if (objectiveReaction.equals(Vars.FluxSumKeyWord)) {
                    entities[i] = bind.createFluxesSummation();
                    coeffs[i] = 1;
                } else {

                    GenericUtils.MetExploreError("The reaction " + objectiveReaction
                            + " does not exist");
                    System.exit(0);
                }
            } else {

                // Sets the coefficient of the reaction in the objective
                // function:
                entities[i] = reaction;
                coeffs[i] = 1;
                i++;
            }
        }

        Boolean max = true;
        if (objectiveSense.equals(Senses.MIN)) {
            max = false;
        }

//		bind.setObjective(new Objective(entities,coeffs,"",max));


        bind.constraintObjectives.add(new Objective(entities, coeffs, "", max));
    }

    /**
     * Adds an objective function
     *
     * @param objectiveReactionsStr
     * @param objectiveSense
     * @param bind
     */
    public static void addObjectiveFunction(String objectiveReactionsStr,
                                            Senses objectiveSense, Bind bind) {
        String[] objectiveReactions = objectiveReactionsStr.split(",");

        BioEntity[] entities = new BioEntity[objectiveReactions.length];
        double[] coeffs = new double[objectiveReactions.length];
        int i = 0;
        for (String objectiveReaction : objectiveReactions) {


            BioNetwork net = bind.getBioNetwork();

            BioCollection<BioReaction> list = net.getReactionsView();

            BioReaction reaction = list.get(objectiveReaction);

            if (reaction == null) {

                // check if reaction is a key word
                if (objectiveReaction.equals(Vars.FluxSumKeyWord)) {
                    entities[i] = bind.createFluxesSummation();
                    coeffs[i] = 1;
                } else {

                    GenericUtils.MetExploreError("The reaction " + objectiveReaction + " is a blocked reaction and cannot be set in any of the objective functions");
                    System.exit(0);
                }
            } else {

                // Sets the coefficient of the reaction in the objective
                // function:
                entities[i] = reaction;
                coeffs[i] = 1;
                i++;
            }
        }

        Boolean max = true;
        if (objectiveSense.equals(Senses.MIN)) {
            max = false;
        }

        bind.setObjective(new Objective(entities, coeffs, "", max));


    }

    /**
     * @author lcottret
     */
    public enum Senses {
        MIN, MAX
    }

}
