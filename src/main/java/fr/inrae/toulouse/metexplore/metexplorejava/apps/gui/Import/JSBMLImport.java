/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Import;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.GenericAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.metabolite.MetaboliteAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.reaction.ReactionAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.reader.JsbmlReader;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.reader.plugin.*;
import fr.inrae.toulouse.metexplore.met4j_io.utils.StringUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.BioNetworkToMetExploreDataBase;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.NetworkQueries;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.GenericUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.database.MetExploreConnection;
import fr.inrae.toulouse.metexplore.metexplorejava.webServices.PubMebWebservice;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import static fr.inrae.toulouse.metexplore.met4j_core.utils.StringUtils.isVoid;

/**
 * This class is used to import a SBML file into our database. This is used in
 * the metexplore UI. This class, however does not use the automatic UI
 * generation.
 * <p>
 * See MetExplore.controller.C_LaunchJavaApplication
 *
 * @author bmerlet
 */
public class JSBMLImport extends MetExploreGuiFunction {

    /**
     * The label of this {@link MetExploreGuiFunction}
     */
    public String label = "Import SBML";
    /**
     * The description of the {@link MetExploreGuiFunction}
     */
    public String description = "Creates a Network from a SBML file. ";
    /**
     * The 'requireLogin' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when user is not connected
     */
    public Boolean requireLogin = true;
    /**
     * The 'requireNetwork' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when no Network is
     * selected
     */
    public Boolean requireNetwork = false;
    /**
     * The 'sendMail' attributes of the {@link MetExploreGuiFunction}. Setting
     * this to true enables the method to send an email when the job is done
     */
    public Boolean sendMail = false;
    /**
     * The type of result expected by the interface
     *
     * @see MetExploreGuiFunction#resultType
     */
    public String resultType = "Network";


    /**
     * A notification e-mail will be sent to this address when the job will end.
     * the default value is only used when this is used in the admin interface
     */
    @Option(name = "-mail", usage = "Mail to send the result", metaVar = "mail", required = false)
    public String mail = "";

    /**
     * The SBML file name
     */
    @Option(name = "-networkFile", usage = "Network file", metaVar = "file", required = true)
    public String networkFile = "";

    @Option(name = "-name", usage = "Network name", metaVar = "networkName", required = true)
    public String networkName = "";

    @Option(name = "-identifier", usage = "Network identifier (must be short and not containing special characters", metaVar = "networkName", required = true)
    public String networkIdentifier = "";

    /**
     * The Mysql id of the collection
     */
    @Option(name = "-dbId", usage = "collection", metaVar = "dbId", required = true)
    public int dbId = 0; // unclassified

    /**
     * The NCBI id of the organism this network refers to
     */
    @Option(name = "-ncbiId", usage = "Id of the organism in the NCBI taxonomy database", metaVar = "ncbiId", required = true)
    public int ncbiId = 12908; // unclassified

    /**
     * The type of tissue this network refers to
     */
    @Option(name = "-tissue", usage = "Tissue (ex: liver tissue)")
    public String tissue = "";

    /**
     * The type of cells this network refers to
     */
    @Option(name = "-cellType", usage = "Cell Type (ex: Hepatocytes)")
    public String cellType = "";

    /**
     * Strain or cell line of the organism
     */
    @Option(name = "-strain", usage = "Strain(ex: K12) or cell line for eukaryotes (ex: HepaRG)")
    public String strain = "";

    /**
     * A PMID of an article referencing this network
     */
    @Option(name = "-pmid", usage = "PMID of the reference article")
    public String pmid = "";


    /**
     * Version number of the network in the source database
     */
    @Option(name = "-version", usage = "version of the source database")
    public String version = "";
    /**
     * Url of the database
     */
    @Option(name = "-url", usage = "url of the source database")
    public String url = "";

    /**
     * Origin of the identifiers in the network (e.g. KEGG, MetaCyc, etc.)
     */
    @Option(name = "-identifierOrigin", usage = "url of the source database")
    public String identifierOrigin = "";

    /**
     * Origin of the identifiers in the network (e.g. KEGG, MetaCyc, etc.)
     */
    @Option(name = "-source", usage = "Original database")
    public String source = "";

    /**
     * Description of the network
     */
    @Option(name = "-descriptionNetwork", usage = "Description of the network")
    public String descriptionNetwork = "";

    /**
     * Mysql ID of the user importing the network
     */
    @Option(name = "-idUser", usage = "Mysql MetExplore user id", metaVar = "id_user", required = true)
    public int idUser = -1;

    /**
     * True to use the default parameters for this class and all it's plugin
     */
    @Option(name = "-useDefaultPluginParams", usage = "Check to use the default values for all the parsing Plugins. "
            + "If you enable this option, no other changes in the advanced Parameters will be taken into account.")
    public boolean useDefaultPluginParams = false;
    /**
     * true to import Annotation element
     */
    @Option(name = "-useAnnotPlugin", usage = "Enable the Annotation Parser Plugin", metaVar = "advanced")
    public boolean useAnnotPlugin = false;
    /**
     * True to use the default parameters for the annotation parser
     */
    @Option(name = "-useDefaultAnnotPluginParams", usage = "Use the default Parameter for the Annotation Parser plugin", metaVar = "advanced")
    public boolean useDefaultAnnotPluginParams = false;
    /**
     * Describes how the annotation url/uri is written. In the UI, there are
     * only two possible syntax but here, any syntax is possible.
     */
    @Option(name = "-annotationPattern", usage = "Pattern of the annotation in the model", metaVar = "advanced")
    public String annotationPattern = "https://identifiers.org/";
    /**
     * Describes how are separated database names and identifiers in the
     * annotation
     */
    @Option(name = "-annotationSeparator", usage = "Separator used in the annotation to separate Database from identifier", metaVar = "advanced")
    public String annotationSeparator = "/";
    /**
     * true to parse and import Notes element. SBML notes are syntax free
     * elements that can be linked to any SBML elements. However most SBML use
     * notes to enrich the network with human readable data. This data a simple
     * list of key/value pairs embedded in html:
     *
     * </br>
     * Example: &lt;p&gt;key : value &lt;/p&gt;
     */
    @Option(name = "-useNotesPlugin", usage = "Enable the Notes Parser Plugin", metaVar = "advanced")
    public boolean useNotesPlugin = false;
    /**
     * True to use the default parameters for the Notes parser
     */
    @Option(name = "-useDefaultNotesPluginParams", usage = "Use the default Parameter for the Notes Parser plugin", metaVar = "advanced")
    public boolean useDefaultNotesPluginParams = false;
    /**
     * key for pathways value
     *
     * @see #useNotesPlugin
     */
    @Option(name = "-pathwayKey", usage = "Key for the Pathway values in reaction Notes", metaVar = "advanced")
    public String pathwayKey = ReactionAttributes.SUBSYSTEM;
    /**
     * In case of multiple values for pathway, they can be grouped in a single
     * key/value pair. This is to know how each pathways are separated. This
     * separator is different then the global note separator. This is because
     * the default value for the global separator is ',' but this character is
     * frequently used in pathway names
     *
     * @see #pathwayKey
     * @see #separator
     */
    @Option(name = "-pathwaySep", usage = "Pathway value separator in reaction Notes", metaVar = "advanced")
    public String pathwaySep = "||";
    /**
     * key for EC number value
     *
     * @see #useNotesPlugin
     */
    @Option(name = "-ECKey", usage = "Key for the EC Number value in reaction Notes", metaVar = "advanced")
    public String ECKey = ReactionAttributes.EC_NUMBER;
    /**
     * key for GPR value
     *
     * @see #useNotesPlugin
     */
    @Option(name = "-GPRKey", usage = "Key for the Gene Association value in reaction Notes", metaVar = "advanced")
    public String GPRKey = ReactionAttributes.GENE_ASSOCIATION;
    /**
     * key for score value, this uses the Palson score scheme
     *
     * @see #useNotesPlugin
     */
    @Option(name = "-scoreKey", usage = "Key for the Score value in reaction Notes (this uses the Palson score scheme)", metaVar = "advanced")
    public String scoreKey = ReactionAttributes.SCORE;
    /**
     * key for status value, this uses the Palson score scheme
     *
     * @see #useNotesPlugin
     */
    @Option(name = "-statusKey", usage = "Key for the Status value in reaction Notes (this uses the Palson score scheme)", metaVar = "advanced")
    public String statusKey = ReactionAttributes.STATUS;
    /**
     * key for user comments value
     *
     * @see #useNotesPlugin
     */
    @Option(name = "-commentKey", usage = "Key for the Comments value in reaction Notes", metaVar = "advanced")
    public String commentKey = GenericAttributes.COMMENT;
    /**
     * key for publications value
     *
     * @see #useNotesPlugin
     */
    @Option(name = "-authorKey", usage = "Key for the Authors value in reaction Notes", metaVar = "advanced")
    public String authorKey = GenericAttributes.AUTHORS;
    /**
     * key for metabolitess' charge value
     *
     * @see #useNotesPlugin
     */
    @Option(name = "-chargeKey", usage = "Key for the Charge value in metabolites Notes", metaVar = "advanced")
    public String chargeKey = MetaboliteAttributes.CHARGE;
    /**
     * key for metabolites' chemical formula value
     *
     * @see #useNotesPlugin
     */
    @Option(name = "-formulaKey", usage = "Key for the Formula value in metabolites Notes", metaVar = "advanced")
    public String formulaKey = MetaboliteAttributes.FORMULA;
    /**
     * Separator in notes
     */
    @Option(name = "-separator", usage = "Value separator in all Notes", metaVar = "advanced")
    public String separator = ",";
    /**
     * Set this to true, to use other key/values in metabolite notes as external references
     */
    @Option(name = "-otherKeysAsRefs", usage = "Use other remaining keys in metabolites Notes as external metabolite identifiers. This also impacts import of InChIs, InChIKeys and SMILES.", metaVar = "advanced")
    public boolean otherKeysAsRefs = false;
    /**
     * true to import FBC lvl 2 elements
     */
    @Option(name = "-useFBC2Plugin", usage = "Enable the FBC2 Parser Plugin", metaVar = "advanced")
    public boolean useFBC2Plugin = false;

    @Option(name = "-useGroupPlugin", usage = "Enable the Group Parser Plugin for ", metaVar = "advanced")
    public boolean useGroupPlugin = true;
    public  BioNetworkToMetExploreDataBase bntm = null;


    /**
     * We use {@link #setLongJob(Boolean)} in the constructor to override the
     * default value set in {@link MetExploreGuiFunction}</br>
     * The same is done for {@link #setAutoUI(Boolean)}
     */
    public JSBMLImport() {
        super();
        this.setLongJob(true);
        this.setAutoUI(false);
    }

    /**
     * Main
     *
     * @param args
     * @throws IllegalAccessException
     */
    public static void main(String[] args) throws IllegalAccessException {

        JSBMLImport importSBML = new JSBMLImport();
        /*
         * Get the command line parameters
         */
        CmdLineParser parser = new CmdLineParser(importSBML);

        String mailBody = "";
        String mailSubject = "";

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            if (importSBML.getPrintJson()) {
                importSBML.printJson();
                System.exit(1);
            } else {
                System.err.println("Problem in the arguments of the function: " + importSBML.getLabel()
                        + ". Please contact contact-metexplore@inrae.fr ");
                System.exit(0);
            }
        }

        if (importSBML.getPrintJson()) {
            importSBML.printJson();
            System.exit(1);
        }

        Boolean flag = importSBML.compute();

        if (!flag) {
            System.exit(0);
        } else {
            System.exit(1);
        }
    }

    public Boolean compute() {
        String mailSubject;
        String mailBody;
        String sbmlPath = this.getNetworkFile();

        if (!new File(sbmlPath).exists()) {
            this.sendError("The file " + sbmlPath + " does not exist");
            return false;
        }

        /*
         * convert file to bionetwork using parsebionet.io.LibSBMLToBionetworkV2
         */
        String mail = this.getMail();
        // JSBMLToBionetwork reader=new JSBMLToBionetwork(sbmlPath);

        JsbmlReader reader = new JsbmlReader(sbmlPath);

        ArrayList<PackageParser> pkgs;

        if (this.isUseDefaultPluginParams()) {

            System.err.println("Using Default plugins and parameters");

            pkgs = new ArrayList<>(Arrays.asList(
                    new NotesParser(true), new FBCParser(), new GroupPathwayParser(), new AnnotationParser(
                            true)));
        } else {
            pkgs = new ArrayList<PackageParser>();

            if (this.isUseAnnotPlugin()) {
                pkgs.add(this.createAnnotPlugin());
            }
            if (this.isUseNotesPlugin()) {
                pkgs.add(this.createNotesPlugin());
            }
            if (this.isUseFBC2Plugin()) {
                pkgs.add(this.createFBC2Plugin());
            }
            if (this.isUseGroupPlugin()) {
                pkgs.add(new GroupPathwayParser());
            }
        }

        BioNetwork network = null;
        try {
            network = reader.read(pkgs);
        } catch (Exception e) {
            this.sendError("Problem while parsing the network file. " +
                    "Please contact contact-metexplore@inrae.fr for support", reader);
        }

        if (network == null) {
            this.sendError("Problem while parsing the network file. " +
                    "Please contact contact-metexplore@inrae.fr  for support", reader);
        }

        /*
         * Adding the bionetwork into the database
         */
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd_HH:mm:ss");

        Date date = new Date();

        MetExploreConnection connection = null;

        FileWriter fwJson = null;

        try {

            System.err.println("Connecting to database...");

            try {
                connection = new MetExploreConnection(this.getDbConf());
                System.err.println("Connected");
            } catch (Exception e) {
                this.sendError("Unable to connect to the database.");
                return false;
            }


            System.err.println("Loading data... ");

            String description = dateFormat.format(date) + ": created with " + this.getLabel()+"\n"+this.getDescriptionNetwork();

            String name = isVoid(this.networkName) ? network.getName() : this.networkName;
            String identifier = isVoid(this.networkIdentifier) ? network.getId() : this.networkIdentifier;

            bntm = new BioNetworkToMetExploreDataBase(network,
                    connection.getConnection(), this.dbId, name, identifier,
                    this.ncbiId, this.getStrain(), this.getTissue(),
                    this.getCellType(),
                    this.getUrl(), this.getVersion(),this.getIdUser(), description, this.getIdentifierOrigin(), this.getSource());

            bntm.toMetExplore();

            MetExploreResult result = bntm.getResult();

            if (result.getIntError() == 1) {
                System.err.println(result.getMessage());
                connection.close();
                this.sendError("Problem while importing the Network in the database. Please contact contact-metexplore@inrae.fr  </br></br>");
                return false;
            }

            int id_network = bntm.id_network;

            /*
             * add the Article if pmid is present
             */
            PubMebWebservice PMws = new PubMebWebservice("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi");

            if (PMws.testConnection()) {

                if (!this.getPmid().isEmpty()) {

                    String barePMID = this.getPmid().replaceAll("PMID[ :]+", "");

                    if (barePMID.contains(",")) {

                        String[] ids = barePMID.split(",\\s?");

                        for (String s : ids) {
                            PMws = new PubMebWebservice(s);

                            HashMap<String, ArrayList<String>> databiblio = PMws.getData();

                            result = NetworkQueries.addBiblio(databiblio, id_network, connection.getConnection());

                            if (result.getIntError() == 1) {
                                reader.errorsAndWarnings
                                        .add("Error While trying to add the article to your Network, the given PMID ("
                                                + s + ") is invalid");
                            }
                        }

                    } else {
                        PMws = new PubMebWebservice(barePMID);

                        HashMap<String, ArrayList<String>> databiblio = PMws.getData();

                        result = NetworkQueries.addBiblio(databiblio, id_network, connection.getConnection());

                        if (result.getIntError() == 1) {
                            reader.errorsAndWarnings
                                    .add("Error While trying to add the article to your Network, the given PMID ("
                                            + this.getPmid() + ") is invalid");
                        }
                    }
                }
            }
            mailBody = "Network successfully imported in MetExplore. id_network : " + id_network + "</br></br>";

            if (!result.message.isEmpty()) {
                reader.errorsAndWarnings.add(result.message);
            }

            if (!reader.errorsAndWarnings.isEmpty()) {

                mailSubject = "Warnings raised during upload";

                mailBody = mailBody + "However, some minor errors occured during the Import process :</br>";
                for (String warn : reader.errorsAndWarnings) {
                    mailBody = mailBody + warn.replaceAll("\n", "</br>") + "</br>";
                }
            } else {
                mailSubject = "Successful upload";
            }

            if(this.sendMail) {
                this.sendMail(mail, mailSubject, mailBody);
            }
            fwJson = new FileWriter(new File(this.jsonFile));
            fwJson.write("{\"success\":\"true\", \"id_network\":\"" + id_network + "\"}");

        } catch (Exception e) {
            e.printStackTrace();
            if(this.sendMail) {
                mailBody = "Problem while while connecting to the database. Please contact contact-metexplore@inrae.fr </br></br>";
                mailBody += "Please join the following error message: " + e.getStackTrace()[0].toString();
                mailSubject = "Upload error";
                this.sendMail(mail, mailSubject, mailBody);
            }
            GenericUtils.MetExploreError(e.getStackTrace().toString());

            return false;
        } finally {
            if (connection != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    GenericUtils.MetExploreError(
                            "Problem while closing the connection to the database. Please contact contact-metexplore@inrae.fr ");
                    return false;
                }
            }
            if (fwJson != null) {
                try {
                    fwJson.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return true;

    }



    /**
     * Send an email and write the json with an error message
     *
     * @param msg The message
     * @throws IOException
     */
    private void sendError(String msg, JsbmlReader reader) {
        String mailBody = msg + "</br></br>";

        if (reader != null && reader.errorsAndWarnings.size() > 0) {
            mailBody += "Errors raised during import: </br>";
            for (String warn : reader.errorsAndWarnings) {
                mailBody += warn.replaceAll("\n", "</br>") + "</br>";
            }
        }
        String mailSubject = "Network Upload error";

        if(this.sendMail) {
            this.sendMail(mail, mailSubject, mailBody);
        }

        try {
            System.err.println("Write error in "+this.jsonFile);
            FileWriter fw = new FileWriter(new File(this.jsonFile));
            fw.write("{\"success\":\"false\", \"message\":\""+msg+"\"}");
            fw.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.exit(0);
    }

    private void sendError(String msg) {
        this.sendError(msg, null);
    }


    /**
     * Instantiate a new Flux parser
     *
     * @return a {@link FBCParser}
     */
    private FBCParser createFBC2Plugin() {
        FBCParser fbcplugin = new FBCParser();
        return fbcplugin;
    }

    /**
     * Instantiate a new annotation parser with the correct parameters
     *
     * @return a {@link AnnotationParser}
     */
    private AnnotationParser createAnnotPlugin() {
        AnnotationParser annotPlugin;

        if (this.useDefaultAnnotPluginParams) {
            annotPlugin = new AnnotationParser(true);
        } else {
            String basePattern = StringUtils.escapeSpecialRegexChars(this.annotationPattern);
            String sep = StringUtils.escapeSpecialRegexChars(this.annotationSeparator);

            String finalPattern = basePattern + "([^" + sep + "]+)" + sep + "(.*)";

            annotPlugin = new AnnotationParser(finalPattern);
        }

        return annotPlugin;
    }

    /**
     * Instantiate a new note parser with the correct parameters
     *
     * @return a {@link NotesParser}
     */
    private NotesParser createNotesPlugin() {
        NotesParser nPlugin = new NotesParser(true);

        if (!this.useDefaultNotesPluginParams) {
            nPlugin.setPathwayPattern(".*[> ]+" + StringUtils.escapeSpecialRegexChars(pathwayKey) + ":\\s*([^<]+)<.*");
            nPlugin.setPathwaySep(StringUtils.escapeSpecialRegexChars(pathwaySep));
            nPlugin.setECPattern(".*[> ]+" + StringUtils.escapeSpecialRegexChars(ECKey) + ":\\s*([^<]+)<.*");
            nPlugin.setGPRPattern(".*[> ]+" + StringUtils.escapeSpecialRegexChars(GPRKey) + ":\\s*([^<]+)<.*");
            nPlugin.setScorePattern(".*[> ]+" + StringUtils.escapeSpecialRegexChars(scoreKey) + ":\\s*([^<]+)<.*");
            nPlugin.setStatusPattern(".*[> ]+" + StringUtils.escapeSpecialRegexChars(statusKey) + ":\\s*([^<]+)<.*");
            nPlugin.setCommentPattern(".*[> ]+" + StringUtils.escapeSpecialRegexChars(commentKey) + ":\\s*([^<]+)<.*");
            nPlugin.setPmidPattern(".*[> ]+" + StringUtils.escapeSpecialRegexChars(authorKey) + ":\\s*([^<]+)<.*");
            nPlugin.setChargePattern(".*[> ]+" + StringUtils.escapeSpecialRegexChars(chargeKey) + ":\\s*([^<]+)<.*");
            nPlugin.setFormulaPattern(".*[> ]+" + StringUtils.escapeSpecialRegexChars(formulaKey) + ":\\s*([^<]+)<.*");
            nPlugin.setSeparator(StringUtils.escapeSpecialRegexChars(separator));

            nPlugin.setOthersAsRefs(this.isOtherKeysAsRefs());
        }

        return nPlugin;
    }

    /**
     * @return the label
     */
    @Override
    public String getLabel() {
        return label;
    }

    /**
     * @return the description
     */
    @Override
    public String getDescription() {
        return description;
    }


    /**
     * @return the network pmid
     */
    public String getPmid() {
        return pmid;
    }


    /**
     * @return the version
     */
    public String getVersion() {
        return version;
    }

    /**
     * @return the url
     */
    public String getUrl() {
        return url;
    }

    /**
     * @return the sbml filename
     */
    public String getPathFile() {
        return networkFile;
    }

    /**
     * @return the strain
     */
    public String getStrain() {
        return strain;
    }

    /**
     * @return the strain
     */
    public String getTissue() {
        return tissue;
    }

    /**
     * @return the strain
     */
    public String getCellType() {
        return cellType;
    }

    /**
     * @return requireLogin
     */
    @Override
    public Boolean getRequireLogin() {
        return requireLogin;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getRequireNetwork()
     */
    @Override
    public Boolean getRequireNetwork() {
        return requireNetwork;
    }

    /**
     * @return the sbmlFile
     */
    public String getNetworkFile() {
        return networkFile;
    }

    /**
     * @return the id_user
     */
    public int getIdUser() {
        return idUser;
    }

    /**
     * @return the mail
     */
    public String getMail() {
        return mail;
    }

    /**
     * @see fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction#getSendMail()
     */
    @Override
    public Boolean getSendMail() {
        return sendMail;
    }

    /**
     * @return useDefaultPluginParams
     */
    public boolean isUseDefaultPluginParams() {
        return useDefaultPluginParams;
    }

    /**
     * @param useDefaultPluginParams
     */
    public void setUseDefaultPluginParams(boolean useDefaultPluginParams) {
        this.useDefaultPluginParams = useDefaultPluginParams;
    }

    /**
     * @return useAnnotPlugin value
     */
    public boolean isUseAnnotPlugin() {
        return useAnnotPlugin;
    }

    /**
     * @param useAnnotPlugin
     */
    public void setUseAnnotPlugin(boolean useAnnotPlugin) {
        this.useAnnotPlugin = useAnnotPlugin;
    }

    /**
     * @return useDefaultAnnotPluginParams value
     */
    public boolean isDefaultAnnotParams() {
        return useDefaultAnnotPluginParams;
    }

    /**
     * @param defaultAnnot
     */
    public void setDefaultAnnotParams(boolean defaultAnnot) {
        this.useDefaultAnnotPluginParams = defaultAnnot;
    }

    /**
     * @return annotationPattern value
     */
    public String getAnnotationPattern() {
        return annotationPattern;
    }

    /**
     * @param annotationPattern
     */
    public void setAnnotationPattern(String annotationPattern) {
        this.annotationPattern = annotationPattern;
    }

    /**
     * @return annotationSeparator value
     */
    public String getAnnotationSeparator() {
        return annotationSeparator;
    }

    /**
     * @param annotationSeparator
     */
    public void setAnnotationSeparator(String annotationSeparator) {
        this.annotationSeparator = annotationSeparator;
    }

    /**
     * @return useNotesPlugin value
     */
    public boolean isUseNotesPlugin() {
        return useNotesPlugin;
    }

    /**
     * @param useNotesPlugin
     */
    public void setUseNotesPlugin(boolean useNotesPlugin) {
        this.useNotesPlugin = useNotesPlugin;
    }

    /**
     * @return pathwayKey value
     */
    public String getPathwayKey() {
        return pathwayKey;
    }

    /**
     * @param pathwayKey
     */
    public void setPathwayKey(String pathwayKey) {
        this.pathwayKey = pathwayKey;
    }

    /**
     * @return pathwaySep value
     */
    public String getPathwaySep() {
        return pathwaySep;
    }

    /**
     * @param pathwaySep
     */
    public void setPathwaySep(String pathwaySep) {
        this.pathwaySep = pathwaySep;
    }

    /**
     * @return ECKey  value
     */
    public String getECKey() {
        return ECKey;
    }

    /**
     * @param eCKey
     */
    public void setECKey(String eCKey) {
        ECKey = eCKey;
    }

    /**
     * @return GPRKey value
     */
    public String getGPRKey() {
        return GPRKey;
    }

    /**
     * @param gPRKey
     */
    public void setGPRKey(String gPRKey) {
        GPRKey = gPRKey;
    }

    /**
     * @return scoreKey value
     */
    public String getScoreKey() {
        return scoreKey;
    }

    /**
     * @param scoreKey
     */
    public void setScoreKey(String scoreKey) {
        this.scoreKey = scoreKey;
    }

    /**
     * @return statusKey value
     */
    public String getStatusKey() {
        return statusKey;
    }

    /**
     * @param statusKey
     */
    public void setStatusKey(String statusKey) {
        this.statusKey = statusKey;
    }

    /**
     * @return commentKey value
     */
    public String getCommentKey() {
        return commentKey;
    }

    /**
     * @param commentKey
     */
    public void setCommentKey(String commentKey) {
        this.commentKey = commentKey;
    }

    /**
     * @return authorKey value
     */
    public String getAuthorKey() {
        return authorKey;
    }

    /**
     * @param authorKey
     */
    public void setAuthorKey(String authorKey) {
        this.authorKey = authorKey;
    }

    /**
     * @return chargeKey value
     */
    public String getChargeKey() {
        return chargeKey;
    }

    /**
     * @param chargeKey
     */
    public void setChargeKey(String chargeKey) {
        this.chargeKey = chargeKey;
    }

    /**
     * @return formulaKey value
     */
    public String getFormulaKey() {
        return formulaKey;
    }

    /**
     * @param formulaKey
     */
    public void setFormulaKey(String formulaKey) {
        this.formulaKey = formulaKey;
    }

    /**
     * @return separator value
     */
    public String getSeparator() {
        return separator;
    }

    /**
     * @param separator
     */
    public void setSeparator(String separator) {
        this.separator = separator;
    }

    /**
     * @return otherKeysAsRefs value
     */
    public boolean isOtherKeysAsRefs() {
        return otherKeysAsRefs;
    }

    /**
     * @param otherKeysAsRefs
     */
    public void setOtherKeysAsRefs(boolean otherKeysAsRefs) {
        this.otherKeysAsRefs = otherKeysAsRefs;
    }

    /**
     * @return useFBC2Plugin value
     */
    public boolean isUseFBC2Plugin() {
        return useFBC2Plugin;
    }

    /**
     * @param useFBC2Plugin
     */
    public void setUseFBC2Plugin(boolean useFBC2Plugin) {
        this.useFBC2Plugin = useFBC2Plugin;
    }

    public Boolean isUseGroupPlugin() {
        return this.useGroupPlugin;
    }

    public String getDescriptionNetwork() {
        return descriptionNetwork;
    }

    public String getIdentifierOrigin() {
        return identifierOrigin;
    }

    public String getSource() {
        return source;
    }

}
