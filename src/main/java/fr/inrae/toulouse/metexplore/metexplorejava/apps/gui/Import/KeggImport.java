package fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Import;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioMetabolite;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioRef;
import fr.inrae.toulouse.metexplore.met4j_core.utils.StringUtils;
import fr.inrae.toulouse.metexplore.met4j_io.kegg.Kegg2BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_io.kegg.KeggServices;
import fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.MetExploreGuiFunction;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.BioNetworkToMetExploreDataBase;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.NetworkQueries;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.GenericUtils;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.database.MetExploreConnection;
import fr.inrae.toulouse.metexplore.metexplorejava.webServices.ChEBIWebService;
import fr.inrae.toulouse.metexplore.metexplorejava.webServices.PubMebWebservice;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

import static fr.inrae.toulouse.metexplore.met4j_core.utils.StringUtils.isDouble;
import static fr.inrae.toulouse.metexplore.met4j_core.utils.StringUtils.isInteger;


/**
 * This class is used to import a network referenced in the KEGG database into our database.
 * It is used in metexplore UI.
 *
 * </br>See MetExplore.controller.C_LaunchJavaApplication
 *
 * @author bmerlet
 */
public class KeggImport extends MetExploreGuiFunction {
    /**
     * The label of this {@link MetExploreGuiFunction}
     */
    public String label = "Import a Network from Kegg";
    /**
     * The description of the {@link MetExploreGuiFunction}
     */
    public String description = "Creates a Network from a Kegg organism";
    /**
     * The 'requireLogin' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when user is not connected
     */
    public Boolean requireLogin = true;
    /**
     * The 'requireNetwork' attributes of the {@link MetExploreGuiFunction}.
     * Setting this to true enables to lock function when no Network is
     * selected
     */
    public Boolean requireNetwork = false;
    /**
     * The 'sendMail' attributes of the {@link MetExploreGuiFunction}. Setting
     * this to true enables the method to send an email when the job is done
     */
    public Boolean sendMail = false;
    /**
     * The type of result expected by the interface
     *
     * @see MetExploreGuiFunction#resultType
     */
    public String resultType = "Network";

    /**
     * Mysql id of the collection where copy the new Network
     */
    @Option(name = "-id_collection", usage = "Mysql id of the collection where copy the new Network", required = true)
    public int id_collection;

    /**
     * A notification e-mail will be sent to this address when the job will end.
     * the default value is only used when this is used in the admin interface
     */
    @Option(name = "-mail", usage = "Mail to send the result", metaVar = "mail", required = true)
    public String mail = "";

    /**
     * The kegg identifier of this network
     */
    @Option(name = "-keggOrgId", usage = "the three or four letters id of the organism in Kegg", required = true)
    public String keggOrgId = "";


    /**
     * The MySQL id of the organism this network refers to
     */
    @Option(name = "-idOrg", usage = "ncbi taxon id", metaVar = "organism", required = true)
    public int idOrg = -1;
    /**
     * The type of tissue this network refers to
     */
    @Option(name = "-tissue", usage = "Tissue (ex: liver tissue)")
    public String tissue = "";
    /**
     * The type of cells this network refers to
     */
    @Option(name = "-celltype", usage = "Cell Type (ex: Hepatocytes)")
    public String celltype = "";
    /**
     * Strain or cell line of the organism
     */
    @Option(name = "-strain", usage = "Strain(ex: K12) or cell line for eukaryotes (ex: HepaRG)")
    public String strain = "";

    /**
     * A PMID of an article referencing this network
     */
    @Option(name = "-pmid", usage = "PMID of the reference article")
    public String pmid = "";
    /**
     * Version number of the network in the source database
     */
    @Option(name = "-version", usage = "version of the database source")
    public String version = "";

    /**
     * Mysql ID of the user importing the network
     */
    @Option(name = "-id_user", usage = "Mysql MetExplore user id", metaVar = "id_user", required = true)
    public int id_user = -1;

    /**
     * set to true to use ChEBI identifiers to enrich the networks with other Identifiers (such as InChI and InChIKeys)
     */
    @Option(name = "-enrich", usage = "Enrich networks with identifiers from the ChEBI database. The import will be much longer.")
    public Boolean enrich = false;
    /**
     *
     */
    @Option(name = "-KeggSource", usage = "Specifies which KEGG source to use to generate reactions equations. 'Map' will add to reactions only the main compounds. 'Reactions' will add side compound metabolites"
            + " but the import will be much longer", required = true)
    public Types KeggSource = Types.Map;


    /**
     * We use {@link #setLongJob(Boolean)} in the constructor to override the
     * default value set in {@link MetExploreGuiFunction}</br>
     */
    public KeggImport() {
        super();
        this.setLongJob(true);
    }

    /**
     * Main method. Called in the interface
     *
     * @param args
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    public static void main(String[] args) throws Exception {

        KeggImport importKegg = new KeggImport();

        CmdLineParser parser = new CmdLineParser(importKegg);

        String mailBody = "";
        String mailSubject = "";

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            if (importKegg.getPrintJson()) {
                importKegg.printJson();
                System.exit(1);
            } else {
                e.printStackTrace();
                GenericUtils.MetExploreError("Problem in the arguments of the function: " + importKegg.getLabel() + ". Please contact contact-metexplore@inrae.fr ");
                System.exit(0);
            }
        }

        if (importKegg.getPrintJson()) {
            importKegg.printJson();
            System.exit(1);
        }

        System.err.println("Connecting to Kegg API...");

        KeggServices ks = new KeggServices();

        if (ks.checkKeggOrgId(importKegg.keggOrgId)) {
            Kegg2BioNetwork ktbn = new Kegg2BioNetwork(importKegg.keggOrgId, importKegg.getKeggSourceString().toLowerCase());

            try {
                ktbn.createBionetworkFromKegg();
            } catch (Exception e) {
                GenericUtils.MetExploreError("Unable to connect to the KEGG API. Please contact contact-metexplore@inrae.fr ");

                System.exit(1);
            }

            if (ktbn.getNetwork() == null) {
                GenericUtils.MetExploreError("Unable to connect to the KEGG API. Please contact contact-metexplore@inrae.fr ");

                System.exit(1);
            }

            System.err.println("Network created.");

            BioNetwork network = ktbn.getNetwork();

            if (importKegg.getEnrich()) {

                System.err.println("Enriching Network...");
                ChEBIWebService ChEBIWS = new ChEBIWebService("http://www.ebi.ac.uk/webservices/chebi/2.0/test/");

                for (BioMetabolite entity : network.getMetabolitesView()) {
                    importKegg.enrichEntity(entity, ChEBIWS);
                }

                System.err.println("Network Enriched");
            }
            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd_HH:mm:ss");

            Date date = new Date();

            MetExploreConnection connection = null;

            try {
                System.err.println("Loading data... ");
                try {
                    connection = new MetExploreConnection(importKegg.getDbConf());
                } catch (Exception e) {
                    e.printStackTrace();
                }


                BioNetworkToMetExploreDataBase bioNetToMet = new BioNetworkToMetExploreDataBase(network,
                        connection.getConnection(),
                        importKegg.id_collection,
                        network.getName(),
                        network.getId(),
                        importKegg.idOrg,
                        importKegg.getStrain(),
                        importKegg.getTissue(),
                        importKegg.getCelltype(),
                        "https://www.genome.jp/kegg/",
                        importKegg.getVersion(),
                        importKegg.getIdUser(),
                        dateFormat.format(date) + ": created with " + importKegg.getLabel()+" with kegg organism id "+importKegg.keggOrgId,
                        "kegg", "kegg"
                );

                bioNetToMet.toMetExplore();

                MetExploreResult result = bioNetToMet.getResult();

                if (result.getIntError() == 1) {
                    System.err.println(result.getMessage());
                    mailBody = "Error while importing the Network in the database. Please contact contact-metexplore@inrae.fr ";
                    mailSubject = "Upload error";
                    importKegg.sendMail(importKegg.getMail(), mailSubject, mailBody);

                    GenericUtils.MetExploreError("Database Error: Error while writing data in MetExplore.");

                    connection.close();
                    System.exit(0);
                }

                int id_network = bioNetToMet.id_network;


                ArrayList<String> pmidList = new ArrayList<String>();
                if (!StringUtils.isVoid(importKegg.getPmid())) {
                    pmidList.add(importKegg.getPmid());
                }
                pmidList.add("24214961");
                pmidList.add("10592173");

                String errorOnPmid = "";
                for (String pmid : pmidList) {
                    try {
                        if (pmid.equalsIgnoreCase("0")) {
                            continue;
                        }
                        PubMebWebservice PMws = new PubMebWebservice(pmid);

                        HashMap<String, ArrayList<String>> databiblio = PMws.getData();

                        result = NetworkQueries.addBiblio(databiblio, id_network, connection.getConnection());

                        if (result.getIntError() == 1) {
                            errorOnPmid = "\n\nError While trying to add the article to your Network, the given PMID is invalid";
                        }
                    } catch (SQLException e) {
                        System.err.println("Problem while loading pubmed references");
                    }
                }
                FileWriter fw = new FileWriter(new File(importKegg.jsonFile));
                fw.write("{\"success\":\"true\", \"id_network\":\"" + id_network + "\"}");
                fw.close();

                connection.close();

                mailBody = "Network successfully imported from the Kegg database in MetExplore."
                        + "id_network : " + id_network + errorOnPmid;

                mailSubject = "Successful Kegg upload";

                importKegg.sendMail(importKegg.getMail(), mailSubject, mailBody);

                System.exit(1);


            } catch (Exception e) {
                e.printStackTrace();
                mailBody = "Problem while while connecting to the database. Please contact contact-metexplore@inrae.fr ";
                mailSubject = "Upload error";
                importKegg.sendMail(importKegg.getMail(), mailSubject, mailBody);
                System.exit(0);
            } finally {
                if (connection != null) {
                    try {
                        connection.close();
                    } catch (SQLException e) {
                        e.printStackTrace();
                        GenericUtils.MetExploreError("Problem while closing the connection to the database. Please contact contact-metexplore@inrae.fr ");
                        System.exit(0);
                    }
                }
            }

        } else {
            System.err.println("Invalid organism identifier. Please check the kegg organism identifier you entered or contact contact-metexplore@inrae.fr ");

            GenericUtils.MetExploreError("Wrong kegg organism identifier: " + importKegg.keggOrgId + " doesn't exist in Kegg Organism.");

            System.exit(0);
        }

    }

    /**
     * Use the {@link ChEBIWebService} class to enrich metabolite with additional identifiers
     *
     * @param entity
     * @param chEBIWS
     */
    public void enrichEntity(BioMetabolite entity, ChEBIWebService chEBIWS) {

        Set<BioRef> refs = entity.getRefs("ChEBI");

        if (refs != null) {
            for (BioRef ref : refs) {

                chEBIWS.setChebi(ref.id);
                HashMap<String, ArrayList<String>> data = chEBIWS.getData();

                if (data.isEmpty()) {
                    continue;
                }

                if (StringUtils.isVoid(entity.getName()) && !data.get("chebiAsciiName").isEmpty()) {
                    entity.setName(data.get("chebiAsciiName").get(0));
                }
                if (!data.get("charge").isEmpty() && isInteger(data.get("charge").get(0))) {
                    entity.setCharge(Integer.parseInt(data.get("charge").get(0)));
                }
                if (entity.getMolecularWeight() == null && !data.get("mass").isEmpty() && isDouble(data.get("mass").get(0))) {
                    entity.setMolecularWeight(Double.parseDouble(data.get("mass").get(0)));
                }
                if (StringUtils.isVoid(entity.getChemicalFormula()) && !data.get("Formulae").isEmpty()) {
                    entity.setChemicalFormula(data.get("Formulae").get(0));
                }

                if (entity.getRefs("inchi") == null && !data.get("inchi").isEmpty()) {
                    entity.addRef("inchi", data.get("inchi").get(0), 1, "is", "Import");
                }

                if (entity.getRefs("inchiKey") == null && !data.get("inchiKey").isEmpty()) {
                    entity.addRef("inchiKey", data.get("inchiKey").get(0), 1, "is", "Import");
                }

            }
        }

    }

    @Override
    public String getLabel() {
        return label;
    }

    /**
     * @param label
     */
    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String getDescription() {
        return description;
    }

    /**
     * @param description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public Boolean getRequireLogin() {
        return requireLogin;
    }

    /**
     * @param requireLogin
     */
    public void setRequireLogin(Boolean requireLogin) {
        this.requireLogin = requireLogin;
    }

    @Override
    public Boolean getRequireNetwork() {
        return requireNetwork;
    }

    /**
     * @param requireNetwork
     */
    public void setRequireNetwork(Boolean requireNetwork) {
        this.requireNetwork = requireNetwork;
    }

    @Override
    public Boolean getSendMail() {
        return sendMail;
    }

    /**
     * @param sendMail
     */
    public void setSendMail(Boolean sendMail) {
        this.sendMail = sendMail;
    }

    /**
     * @return mail value
     */
    public String getMail() {
        return mail;
    }

    /**
     * @param mail
     */
    public void setMail(String mail) {
        this.mail = mail;
    }

    /**
     * @return keggOrgId value
     */
    public String getKeggOrgId() {
        return keggOrgId;
    }

    /**
     * @param keggOrgId
     */
    public void setKeggOrgId(String keggOrgId) {
        this.keggOrgId = keggOrgId;
    }

    /**
     * @return idOrg value
     */
    public int getIdOrg() {
        return idOrg;
    }

    /**
     * @param idOrg
     */
    public void setIdOrg(int idOrg) {
        this.idOrg = idOrg;
    }

    /**
     * @return strain value
     */
    public String getStrain() {
        return strain;
    }

    /**
     * @param strain
     */
    public void setStrain(String strain) {
        this.strain = strain;
    }

    /**
     * @return version value
     */
    public String getVersion() {
        return version;
    }

    /**
     * @param version
     */
    public void setVersion(String version) {
        this.version = version;
    }

    /**
     * @return id_user value
     */
    public int getIdUser() {
        return id_user;
    }

    /**
     * @param id_user
     */
    public void setIdUser(int id_user) {
        this.id_user = id_user;
    }

    /**
     * @return pmid value
     */
    public String getPmid() {
        return pmid;
    }

    /**
     * @param pmid
     */
    public void setPmid(String pmid) {
        this.pmid = pmid;
    }

    /**
     * @return tissue value
     */
    public String getTissue() {
        return tissue;
    }

    /**
     * @param tissue
     */
    public void setTissue(String tissue) {
        this.tissue = tissue;
    }

    /**
     * @return celltype value
     */
    public String getCelltype() {
        return celltype;
    }

    /**
     * @param celltype
     */
    public void setCelltype(String celltype) {
        this.celltype = celltype;
    }

    /**
     * @return enrich value
     */
    public Boolean getEnrich() {
        return enrich;
    }

    /**
     * @param enrich
     */
    public void setEnrich(Boolean enrich) {
        this.enrich = enrich;
    }

    @Override
    public String getResultType() {
        return resultType;
    }

    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

    /**
     * @return KeggSource value
     */
    public Types getKeggSource() {
        return KeggSource;
    }

    /**
     * @param databaseType
     */
    public void setKeggSource(Types databaseType) {
        this.KeggSource = databaseType;
    }

    /**
     * @return KeggSource Stringvalue
     */
    public String getKeggSourceString() {
        return KeggSource.toString();
    }

    /**
     * Enumeration to list possible KEGG source to create reaction topology.
     *
     * @author bmerlet
     */
    public enum Types {
        /**
         * This will use the KEGG's pathway map to create the reaction's topology. It will be faster but will miss side compound metabolites.
         */
        Map,
        /**
         * This will use the KEGG Reaction cards to create the reaction's topology. Will be longer but reaction topology will be more accurate
         */
        Reaction
    }


}
