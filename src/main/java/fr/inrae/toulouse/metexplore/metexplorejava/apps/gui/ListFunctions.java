/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.apps.gui;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.net.URL;

import fr.inrae.toulouse.metexplore.metexplorejava.utils.resources.ResourcesMetExploreJava;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.resources.ResourceURLFilter;

/**
 * @author lcottret
 */
public class ListFunctions {

    /**
     * @param args
     * @throws ClassNotFoundException
     * @throws IOException
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     * @throws URISyntaxException
     * @throws NoSuchMethodException
     * @throws SecurityException
     * @throws InstantiationException
     * @throws InvocationTargetException
     */
    public static void main(String[] args) throws ClassNotFoundException,
            IOException, IllegalArgumentException, IllegalAccessException,
            URISyntaxException, NoSuchMethodException, SecurityException, InstantiationException, InvocationTargetException {


        try {


            ResourceURLFilter filter = new ResourceURLFilter() {

                public @Override
                boolean accept(URL u) {

                    String path = ListFunctions.class.getPackage().getName()
                            .replace(".", "/");

                    String s = u.getFile();
                    return s.endsWith(".class") && !s.contains("$")
                            && s.contains(path);
                }
            };

            String jsonStr = "{\"status\":\"true\",\"functions\":[";

            String path = ListFunctions.class.getPackage().getName()
                    .replace(".", "/");


            int n = 0;

            for (URL u : ResourcesMetExploreJava.getResourceURLs(ResourcesMetExploreJava.class, filter)) {

                String entry = u.getFile();
                int idx = entry.indexOf(path);

                entry = entry.substring(idx, entry.length() - ".class".length());

                Class<?> myClass = Class.forName(entry.replace('/', '.'));

                if (myClass.getSuperclass().equals(MetExploreGuiFunction.class)) {

                    if (n > 0) {
                        jsonStr += ",";
                    }

                    n++;

                    Constructor<?> ctor = myClass.getConstructor();

                    Object obj = ctor.newInstance();

                    Method method = obj.getClass().getMethod("json");

                    jsonStr += method.invoke(obj);

                }
            }

            jsonStr += "]}";

            System.out.println(jsonStr);

        } catch (Exception e) {
            System.out.println("{\"status\":\"false\"}");
            e.printStackTrace();
        }

    }


}
