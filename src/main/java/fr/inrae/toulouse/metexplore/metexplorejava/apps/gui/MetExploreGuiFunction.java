/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.apps.gui;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.mail.MetExploreMail;
import org.hibernate.validator.constraints.Range;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.kohsuke.args4j.Option;

import fr.inrae.toulouse.metexplore.metexplorejava.convert.MetExploreDataBaseToBioNetwork;

/**
 * @author lcottret
 */
public abstract class MetExploreGuiFunction {

    /**
     * Setting this to true tells the UI that the function needs to be added as
     * a Metexplore job
     */
    public Boolean longJob = false;
    /**
     * Set to true to to enable automatic UI generation in ExtJS. this is set to
     * false for {@link fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Import.JSBMLImport} and
     * {@link fr.inrae.toulouse.metexplore.metexplorejava.apps.gui.Export.JSBMLExport}
     */
    public Boolean autoUI = true;
    /**
     * the type of result expected by the interface. Values understood by the UI
     * are:
     * <ul>
     * <li><b>Network</b> : A new Network was added in the database.
     * Clicking the result button will load that Network
     * <li><b>file</b> : A file was created on the server's file system.
     * Clicking the result button launch it's download.
     * <li><b><em>null</em></b> : Leaving this to <em>null</em> will trigger a
     * mapping on the associated Network (can be another Network than the
     * loaded one)
     * </ul>
     */
    public String resultType = null;
    // Mail parameters
//    public String mailFrom = "contact-metexplore@inrae.fr ";
    public String mailFrom = "contact-metexplore@inrae.fr";
    public String smtp = "smtp.inrae.fr";
    @Option(name = "-printJson", usage = "[Deactivated] Prints the description of the application in json format")
    public Boolean printJson = false;
    @Option(name = "-jsonFile", usage = "Output json file", metaVar = "file", required = true)
    public String jsonFile = "";
    @Option(name = "-dbConf", usage = "Database Config file")
    String dbConf = ".env";
    @Option(name = "-tmpPath", usage = "[/tmp] Temporary path to save the files")
    String tmpPath = "/tmp/";
    @Option(name = "-urlTmpPath", usage = "[/tmp] Temporary path to save the files")
    String urlTmpPath = "http://localhost/metexplore_test/tmp";
    private ArrayList<HashMap<String, String>> options = new ArrayList<HashMap<String, String>>();
    private BioNetwork network = null;
    private Boolean requireAdmin = false;

    /**
     * Inits the options from the field annotations
     *
     * @throws IllegalArgumentException
     * @throws IllegalAccessException
     */
    private void initOptions() throws IllegalArgumentException, IllegalAccessException {
        // Browse each class field
        for (Field f : this.getClass().getDeclaredFields()) {
            // Browse each annotation of the field
            HashMap<String, String> map = new HashMap<String, String>();

            Boolean flag = false;

            for (Annotation a : f.getDeclaredAnnotations()) {
                if (a instanceof Option) {
                    flag = true;
                    Option option = (Option) a;

                    map.put("name", f.getName());

                    map.put("description", option.usage());
                    if (!option.metaVar().equals("")) {
                        map.put("metaVar", option.metaVar());
                    } else {
                        map.put("metaVar", "");
                    }

                    if (f.getType().isEnum()) {
                        map.put("type", "String");
                    } else {
                        map.put("type", f.getType().getSimpleName());
                    }

                    String required = "false";
                    if (option.required()) {
                        required = "true";
                    }
                    map.put("required", required);

                    String defaultValue = "";
                    if (f.get(this) != null) {
                        defaultValue = f.get(this).toString();
                    }
                    map.put("default", defaultValue);

                    if (f.getType().isEnum()) {

                        Enum<?> enumValue = (Enum<?>) f.get(this);

                        Object[] possibleValues = enumValue.getDeclaringClass().getEnumConstants();

                        String choices = "";

                        int n = 0;

                        for (Object object : possibleValues) {

                            String choice = object.toString();

                            if (n > 0) {
                                choices += ",";
                            }

                            choices += choice;

                            n++;
                        }

                        map.put("choices", choices);
                    }

                } else if (a instanceof Range) {
                    Range option = (Range) a;

                    map.put("min", Double.toString(option.min()));
                    map.put("max", Double.toString(option.max()));

                }
            }
            if (flag) {
                options.add(map);
            }

        }
    }

    /**
     * Prints the description of the application in json format
     *
     * @return a json representing the detailed description of the class and all
     * the parameters available for it's main method
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    @SuppressWarnings("unchecked")
    public String json() throws IllegalArgumentException, IllegalAccessException {

        String json = "";

        this.initOptions();

        JSONArray parameters = new JSONArray();
        parameters.addAll(options);

        JSONObject jsonObject = new JSONObject();
        jsonObject.put("name", new String(this.getLabel()));
        jsonObject.put("description", new String(this.getDescription()));
        jsonObject.put("java_class", this.getClass().getName());
        jsonObject.put("require_login", this.getRequireLogin());
        jsonObject.put("require_Network", this.getRequireNetwork());
        jsonObject.put("send_mail", this.getSendMail());
        jsonObject.put("require_admin", this.getRequireAdmin());
        jsonObject.put("long_job", this.getLongJob());
        jsonObject.put("resultType", this.getResultType());

        String packageName = this.getClass().getPackage().getName();

        String tab[] = packageName.split("\\.");
        String simplePackageName = tab[tab.length - 1];

        jsonObject.put("package", simplePackageName);

        jsonObject.put("parameters", parameters);

        json = jsonObject.toJSONString();

        return json;
    }

    /**
     * Creates the BioNetwork from the MetExplore database
     *
     * @param id_network
     */
    public void initNetwork(int id_network) throws Exception {

        MetExploreDataBaseToBioNetwork mtb = new MetExploreDataBaseToBioNetwork();


        mtb.Connection(this.getDbConf());


        mtb.setid_network(id_network);

        mtb.convert();

        this.network = mtb.getNetwork();

    }

    /**
     * print the json corresponding to the function
     *
     * @throws IllegalAccessException
     * @throws IllegalArgumentException
     */
    public void printJson() throws IllegalArgumentException, IllegalAccessException {

        System.out.println(this.json());

    }

    /**
     * @param nanoStartTime
     * @return the time elapsed since nanoStartTime
     */
    public String getElapsedTime(long nanoStartTime, String jobName) {

        long elapsedTime = System.nanoTime() - nanoStartTime;

        if (elapsedTime / 1000000000 == 0) {
            return (jobName + " completed in :" + elapsedTime / 1000000 + " miliseconds.");
        } else {
            return (jobName + " completed in :" + elapsedTime / 1000000000 + " seconds.");
        }
    }

    /**
     * Send an email
     *
     * @param to
     * @param subject
     * @param body
     * @return true if ok
     */
    public Boolean sendMail(String to, String subject, String body) {

        return MetExploreMail.sendMail(this.mailFrom, to, "[MetExplore] " + subject, body, this.smtp);

    }

    /**
     * @return the options
     */
    public ArrayList<HashMap<String, String>> getOptions() {
        return options;
    }

    /**
     * @return the network
     */
    public BioNetwork getNetwork() {
        return network;
    }

    public void setNetwork(BioNetwork network) {
        this.network = network;
    }

    /**
     * @return the printJson
     */
    public Boolean getPrintJson() {
        return printJson;
    }

    public String getDbConf() {
        return dbConf;
    }

    public void setDbConf(String dbConf) {
        this.dbConf = dbConf;
    }

    /**
     * @return the tmpPath
     */
    public String getTmpPath() {
        return tmpPath;
    }

    public void setTmpPath(String path) {
        tmpPath = path;
    }

    /**
     * @return the urlTmpPath
     */
    public String getUrlTmpPath() {
        return urlTmpPath;
    }

    public void setUrlTmpPath(String url) {
        urlTmpPath = url;
    }

    /**
     * @return the label
     */
    public abstract String getLabel();

    /**
     * @return the description
     */
    public abstract String getDescription();

    /**
     * @return true if the user must be logged to use this function
     */
    public abstract Boolean getRequireLogin();

    /**
     * @return true if a Network must be loaded to use this function
     */
    public abstract Boolean getRequireNetwork();

    /**
     * @return true if the process is sent in the server and that the result is
     * sent by mail
     */
    public abstract Boolean getSendMail();

    public Boolean getRequireAdmin() {
        return requireAdmin;
    }

    public void setRequireAdmin(Boolean requireAdmin) {
        this.requireAdmin = requireAdmin;
    }

    public Boolean getLongJob() {
        return longJob;
    }

    public void setLongJob(Boolean longJob) {
        this.longJob = longJob;
    }

    public Boolean getAutoUI() {
        return autoUI;
    }

    public void setAutoUI(Boolean autoUI) {
        this.autoUI = autoUI;
    }

    public String getResultType() {
        return resultType;
    }

    /**
     * @param resultType
     */
    public void setResultType(String resultType) {
        this.resultType = resultType;
    }

}
