/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/

package fr.inrae.toulouse.metexplore.metexplorejava.convert;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.*;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.collection.BioCollection;
import fr.inrae.toulouse.metexplore.met4j_core.utils.StringUtils;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.AnnotatorComment;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.GenericAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.compartment.CompartmentAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.network.NetworkAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.reaction.Flux;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.reaction.ReactionAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.attributes.Notes;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.attributes.SbmlAnnotation;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.BioUnitDefinition;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.BioUnitDefinitionCollection;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.UnitSbml;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.*;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Set;

/**
 * Export a bioNetwork in the MetExplore database. this class will only be used
 * for all import types
 *
 * @author bmerlet
 */
public class BioNetworkToMetExploreDataBase {

    private final HashMap<String, Integer> cacheMetabolites;
    /**
     * MySql id of the Network that will be created
     */
    public int id_network = -1;
    /**
     * Object used to handle mysql errors
     */
    public MetExploreResult result;
    /**
     * Id of the user
     */
    public int id_user = -1;
    /**
     * MySQl Id of the collection
     */
    public int dbId = -1;
    /**
     * Mysql ID of the organism this Network comes from.
     */
    public int orgId = -1;
    /**
     * @see #setRefScores()
     */
    public String referentialReactionScore = "metexplore_rscores";
    /**
     * @see #setRefScores()
     */
    public int idReferentialReactionScore = -1;
    /**
     * @see #setRefStatus()
     */
    public String referentialReactionStatus = "metexplore_rstatus";
    /**
     * @see #setRefStatus()
     */
    public int idReferentialReactionStatus = -1;

    protected BioNetwork network;
    protected Connection con;


    /**
     * Constructor for BioNetworkToMetExploreDataBase.
     *
     * @param network           the BioNetwork to be exported
     * @param metConnection     the database connection
     * @param collectionId      the ID of the collection
     * @param networkName       the name of the network
     * @param dbIdentifier    the name of the collection
     * @param ncbiId            the NCBI ID of the organism
     * @param strain     the strain of the network
     * @param tissue     the tissue of the network
     * @param cellType   the cell type of the network
     * @param url               the URL of the network
     * @param version           the version of the network
     * @param userId            the ID of the user
     * @param description       the description of the network
     * @param identifierOrigin  the origin of the identifier
     * @param source            the database source of the network
     * @throws SQLException if a database access error occurs
     */
    public BioNetworkToMetExploreDataBase(BioNetwork network, Connection metConnection, int collectionId, String networkName,
                                          String dbIdentifier, int ncbiId, String strain,
                                          String tissue, String cellType, String url, String version, int userId,
                                          String description, String identifierOrigin, String source) throws SQLException {

        this.network = network;

        cacheMetabolites = new HashMap<>();

        con = metConnection;

        con.setAutoCommit(false);

        this.dbId = collectionId;
        this.orgId = ncbiId;

        MetExploreIntegerArrayResult err = NetworkQueries.createNetwork(dbIdentifier, networkName, this.orgId, strain, tissue,
                cellType, description, version, url, identifierOrigin, source, userId , this.dbId, con);

        if (err.getIntError() == 1) {
            System.err.println(err.message);
            System.err.println("[ERROR] MetExplore import failed");
            System.exit(1);
        }
        this.id_network = err.get(0);

        Notes notes = NetworkAttributes.getNotes(network);

        if (notes != null && notes.getXHTMLasString() != null && !notes.getXHTMLasString().isEmpty()) {
            err = NetworkQueries.addmetadata(notes.getXHTMLasString(), id_network, con);
        }
        if (err.getIntError() == 1) {
            System.err.println(err.message);
            System.err.println("[ERROR] MetExplore import failed");
            System.exit(1);
        }

        SbmlAnnotation annot = NetworkAttributes.getAnnotation(network);

        if (annot != null && annot.getXMLasString() != null && !annot.getXMLasString().isEmpty()) {
            err = NetworkQueries.addmetadata(annot.getXMLasString(), id_network, con);
        }

        if (err.getIntError() == 1) {
            System.err.println(err.message);
            System.err.println("[ERROR] MetExplore import failed");
            System.exit(1);
        }


        this.id_user = userId;

        this.result = new MetExploreResult(0);

    }

    /**
     * Fills the MetExplore database with BioNetwork data
     *
     * @throws InterruptedException
     * @throws SQLException
     */
    public void toMetExplore() throws InterruptedException, SQLException {

        MetExploreResult resSetRefStatus = this.setRefStatus();

        if (resSetRefStatus.getIntError() == 1) {
            result = resSetRefStatus;
            NetworkQueries.removeNetwork(this.id_network, con);
            return;
        }

        MetExploreResult resSetRefScores = this.setRefScores();

        if (resSetRefScores.getIntError() == 1) {
            result = resSetRefScores;
            NetworkQueries.removeNetwork(this.id_network, con);
            return;
        }

        // System.err.println("Load Unit definitions");
        MetExploreResult resLoadUnitDefinitions = this.loadUnitDefinitions();

        // System.err.println(resLoadUnitDefinitions.message);

        if (resLoadUnitDefinitions.getIntError() == 1) {
            result = resLoadUnitDefinitions;
            NetworkQueries.removeNetwork(this.id_network, con);
            return;
        }

        MetExploreResult resComp = this.loadCompartments();

        if (resComp.getIntError() == 1) {
            result = resComp;
            NetworkQueries.removeNetwork(this.id_network, con);
            return;
        }

        MetExploreResult resMetabolites = this.loadMetabolites();

        if (resMetabolites.getIntError() == 1) {
            result = resMetabolites;
            NetworkQueries.removeNetwork(this.id_network, con);
            return;
        }

        MetExploreResult resGenes = this.loadGenes();

        if (resGenes.getIntError() == 1) {
            result = resGenes;
            NetworkQueries.removeNetwork(this.id_network, con);
            return;
        }

        MetExploreResult resProteins = this.loadProteins();

        if (resProteins.getIntError() == 1) {
            result = resProteins;
            NetworkQueries.removeNetwork(this.id_network, con);
            return;
        }

        // System.err.println("Load enzymes");
        MetExploreResult resEnzymes = this.loadEnzymes();

        if (resEnzymes.getIntError() == 1) {
            result = resEnzymes;
            NetworkQueries.removeNetwork(this.id_network, con);
            return;
        }


        // System.err.println("Load pathways");
        MetExploreResult resPathway = this.loadPathways();

        if (resPathway.getIntError() == 1) {
            result = resPathway;
            NetworkQueries.removeNetwork(this.id_network, con);
            return;
        }

        // Load the reactions in MetExplore
        // System.err.println("Load Reactions");
        MetExploreResult resReactions = this.loadReactions();

        result = resReactions;
        if (resReactions.getIntError() == 1) {
            NetworkQueries.removeNetwork(this.id_network, con);
            return;
        }

        System.err.println("Network Import finished at : " + new Date());

    }

    /**
     * Load Unit definitions of the BioNetwork
     *
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public MetExploreResult loadUnitDefinitions() throws SQLException, InterruptedException {

        MetExploreResult result = new MetExploreResult(0);

        BioUnitDefinitionCollection unitDefinitions = NetworkAttributes.getUnitDefinitions(this.getNetwork());

        this.con.setAutoCommit(false);

        if (unitDefinitions != null) {
            int i = 0;
            for (BioUnitDefinition unitDefinition : unitDefinitions) {
                i++;
                boolean deadlock;
                int retries = 0;
                int maxRetries = 3600; // Corresponds to one hour

                do {
                    deadlock = false;
                    retries++;
                    try {
                        String name = unitDefinition.getName();
                        String id = Globals.formatId(unitDefinition.getId());

                        MetExploreIntegerArrayResult errUd = UnitDefinitionQueries.createUnitDefinition(id, name, dbId, con);

                        if (errUd.getIntError() == 1) {
                            result = errUd;
                            this.rollback();
                            return result;
                        } else {

                            int idUd = errUd.get(0);

                            // Load units of each unit Definition

                            HashMap<String, UnitSbml> units = unitDefinition.getUnits();
                            for (UnitSbml unit : units.values()) {

                                String kind = Globals.formatId(unit.getKind());

                                int scale = unit.getScale();
                                double exponent = unit.getExponent();
                                double multiplier = unit.getMultiplier();

                                // Create the unit
                                MetExploreIntegerArrayResult errUnit = UnitQueries.createUnit(kind, scale, exponent, multiplier,
                                        con);

                                if (errUnit.getIntError() == 1) {
                                    result = errUnit;
                                    this.rollback();
                                    return result;
                                } else {
                                    int id_unit = errUnit.get(0);
                                    // Create the link between unit and unit
                                    // definition
                                    MetExploreResult errLinkUnit = UnitQueries.addUnit(id_unit, idUd, kind, id, con);

                                    if (errLinkUnit.getIntError() == 1) {
                                        result = errLinkUnit;
                                        this.rollback();
                                        return result;
                                    }
                                }
                            }

                            // Add the unit definition in the Network
                            MetExploreResult errUdBs = UnitDefinitionQueries.addUnitDefinition(idUd, id_network, con);

                            if (errUdBs.getIntError() == 1) {
                                result = errUdBs;
                                this.rollback();
                                return result;
                            }
                        }
                    } catch (SQLException e) {

                        // System.err.println("State : " + e.getSQLState());

                        if (e.getSQLState().equals("40001") || e.getSQLState().equals("41000")) {
                            // deadlock ("40001") or lock-wait time-out ("41000")
                            // occured
                            // System.err.println("Database lock found. Retry");
                            deadlock = true;
                            i--;
                            Thread.sleep(5000); // short delay before retry
                        } else {
                            result = new MetExploreResult(1);
                            result.message = "Database error.";
                            e.printStackTrace();
                            this.rollback();
                            return result;
                        }
                    }
                }
                while (deadlock && retries < maxRetries);

                if (deadlock) {
                    result = new MetExploreResult(1);
                    result.message = "Database blocked by other processes";
                    this.rollback();
                    return result;
                }
                if (i % 1000 == 0) {
                    this.con.commit();
                }
            }

            this.con.commit();
            this.con.setAutoCommit(true);
        }

        return result;
    }

    /**
     * Load compartments in the database
     *
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public MetExploreResult loadCompartments() throws SQLException, InterruptedException {

        this.con.setAutoCommit(false);

        MetExploreResult result = new MetExploreResult(0);

        // Load the compartments
        BioCollection<BioCompartment> listOfCompartments = this.getNetwork().getCompartmentsView();

        int i = 0;
        for (BioCompartment compartment : listOfCompartments) {
            i++;
            boolean deadlock;
            int retries = 0;
            int maxRetries = 3600; // Corresponds to one hour

            do {
                deadlock = false;
                retries++;
                try {

                    MetExploreIntegerArrayResult errCpt = CompartmentQueries.createCompartment(compartment, id_network, con);

                    if (errCpt.getIntError() == 1) {
                        result = errCpt;
                        this.rollback();
                        return result;
                    } else {

                        int idCptBs = errCpt.get(0);

                        BioCompartment outsideCompartment = CompartmentAttributes.getOutsideCompartment(compartment);

                        if (outsideCompartment != null) {

                            if (!outsideCompartment.getId().isEmpty()) {

                                MetExploreIntegerArrayResult errOutside = CompartmentQueries
                                        .createCompartment(outsideCompartment, id_network, con);

                                if (errOutside.getIntError() == 1) {
                                    result = errOutside;
                                    this.rollback();
                                    return result;
                                } else {

                                    int idOutsideBs = errOutside.get(0);
                                    MetExploreResult errLinkOutside = CompartmentQueries.linkCompartments(idCptBs,
                                            idOutsideBs, con);

                                    if (errLinkOutside.getIntError() == 1) {
                                        result = errLinkOutside;
                                        this.rollback();
                                        return result;
                                    }
                                }
                            }
                        }
                    }
                } catch (SQLException e) {

                    // System.err.println("State : " + e.getSQLState());

                    if (e.getSQLState().equals("40001") || e.getSQLState().equals("41000")) {
                        // deadlock ("40001") or lock-wait time-out ("41000")
                        // occured
                        // System.err.println("Database lock found. Retry");
                        deadlock = true;
                        i--;
                        Thread.sleep(5000); // short delay before retry
                    } else {
                        result = new MetExploreResult(1);
                        result.message = "Database error.";
                        e.printStackTrace();
                        this.rollback();
                        return result;
                    }
                }
            }
            while (deadlock && retries < maxRetries);

            if (deadlock) {
                result = new MetExploreResult(1);
                result.message = "Database blocked by other processes";
                this.rollback();
                return result;
            }
            if (i % 1000 == 0) {
                this.con.commit();
            }
        }

        this.con.commit();
        this.con.setAutoCommit(true);

        return result;

    }

    /**
     * Load metabolites
     *
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public MetExploreResult loadMetabolites() throws SQLException, InterruptedException {

        MetExploreResult result = new MetExploreResult(0);

        if (this.getNetwork().getCompartmentsView().size() == 0) {
            result.setIntError(1);
            result.setMessage("Compartments must be loaded before metabolites");
        }

        BioCollection<BioMetabolite> metabolites = this.getNetwork().getMetabolitesView();

        int i = 0;
        this.con.setAutoCommit(false);

        for (BioMetabolite metabolite : metabolites) {
            i++;
            boolean deadlock;
            int retries = 0;
            int maxRetries = 3600; // Corresponds to one hour

            do {
                deadlock = false;
                retries++;
                try {
                    // Create the entry in the Metabolite table
                    MetExploreIntegerArrayResult errMetabolite = CollectionQueries.createMetabolite(metabolite, dbId, con);

                    if (errMetabolite.getIntError() == 1) {
                        result = errMetabolite;
                        this.rollback();
                        return result;
                    } else {

                        int id_metabolite = errMetabolite.get(0);

                        cacheMetabolites.put(Globals.formatId(metabolite.getId()), id_metabolite);

                        // Add the metabolite in the Network
                        MetExploreIntegerResult errMetaboliteBs = NetworkQueries.addMetabolite(metabolite, id_metabolite, id_network,
                                con);

                        if (errMetaboliteBs.getIntError() == 1) {
                            result = errMetaboliteBs;
                            this.rollback();
                            return result;
                        } else {

                            int id_metabolite_in_network = errMetaboliteBs.get();

                            BioCollection<BioCompartment> cpts = this.getNetwork().getCompartmentsOf(metabolite);

                            for (BioCompartment cpt : cpts) {

                                // Link the compartment
                                MetExploreIntegerArrayResult errCpt = CompartmentQueries
                                        .getCompartmentByIdentifier(cpt.getId(), id_network, con);

                                if (errCpt.getIntError() == 1) {
                                    result = errCpt;
                                    this.rollback();
                                    return result;
                                } else {

                                    int id_compartment = errCpt.get(0);

                                    MetExploreResult errCpdCpt = CompartmentQueries.addMetaboliteInCompartment(id_metabolite_in_network,
                                            id_compartment, con);

                                    if (errCpdCpt.getIntError() == 1) {
                                        result = errCpdCpt;
                                        this.rollback();
                                        return result;
                                    }
                                }
                            }
                        }
                    }
                } catch (SQLException e) {

                    // System.err.println("State : " + e.getSQLState());

                    if (e.getSQLState().equals("40001") || e.getSQLState().equals("41000")) {
                        // deadlock ("40001") or lock-wait time-out ("41000")
                        // occured
                        // System.err.println("Database lock found. Retry");
                        deadlock = true;
                        i--;
                        Thread.sleep(5000); // short delay before retry
                    } else {
                        result = new MetExploreResult(1);
                        result.message = "Database error.";
                        e.printStackTrace();
                        this.rollback();
                        return result;
                    }
                }
            }
            while (deadlock && retries < maxRetries);

            if (deadlock) {
                result = new MetExploreResult(1);
                result.message = "Database blocked by other processes";
                this.rollback();
                return result;
            }
            if (i % 1000 == 0) {
                this.con.commit();
            }
        }

        this.con.commit();
        this.con.setAutoCommit(true);

        return result;

    }

    /**
     * Load genes of the Network
     *
     * @return {@link MetExploreResult}
     * @throws SQLException
     */
    public MetExploreResult loadGenes() throws SQLException, InterruptedException {

        this.con.setAutoCommit(false);

        MetExploreResult result = new MetExploreResult(0);


        BioCollection<BioGene> genes = this.getNetwork().getGenesView();

        int i = 0;
        for (BioGene gene : genes) {
            i++;
            boolean deadlock ;
            int retries = 0;
            int maxRetries = 3600; // Corresponds to one hour

            do {
                deadlock = false;
                retries++;
                try {

                    // Create a gene
                    MetExploreIntegerArrayResult errGene = NetworkQueries.createGene(gene, id_network, con);

                    if (errGene.getIntError() == 1) {
                        result = errGene;
                        this.rollback();
                        return result;
                    }
                } catch (SQLException e) {

                    // System.err.println("State : " + e.getSQLState());

                    if (e.getSQLState().equals("40001") || e.getSQLState().equals("41000")) {
                        // deadlock ("40001") or lock-wait time-out ("41000")
                        // occured
                        // System.err.println("Database lock found. Retry");
                        deadlock = true;
                        i--;
                        Thread.sleep(5000); // short delay before retry
                    } else {
                        result = new MetExploreResult(1);
                        result.message = "Database error.";
                        e.printStackTrace();
                        this.rollback();
                        return result;
                    }
                }
            }
            while (deadlock && retries < maxRetries);

            if (deadlock) {
                result = new MetExploreResult(1);
                result.message = "Database blocked by other processes";
                this.rollback();
                return result;
            }
            if (i % 1000 == 0) {
                this.con.commit();
            }
        }

        this.con.commit();
        this.con.setAutoCommit(true);

        return result;

    }

    /**
     * Load proteins of the Network
     *
     * @return {@link MetExploreResult}
     * @throws SQLException
     */
    public MetExploreResult loadProteins() throws SQLException, InterruptedException {

        this.con.setAutoCommit(false);

        MetExploreResult result = new MetExploreResult(0);


        BioCollection<BioProtein> proteins = this.getNetwork().getProteinsView();

        int i = 0;
        for (BioProtein protein : proteins) {

            i++;
            boolean deadlock;
            int retries = 0;
            int maxRetries = 3600; // Corresponds to one hour

            do {
                deadlock = false;
                retries++;
                try {

                    // Create a protein
                    MetExploreIntegerArrayResult errProtein = NetworkQueries.createProtein(protein, id_network, con);

                    if (errProtein.getIntError() == 1) {
                        result = errProtein;
                        this.rollback();
                        return result;
                    }

                    int id_protein = errProtein.get(0);

                    // Link proteins to genes

                    BioGene gene = protein.getGene();

                    if (gene != null) {

                        String geneId = gene.getId();

                        // Get Gene id in Network
                        MetExploreIntegerArrayResult resGetGene = GeneQueries.getid_geneFromIdentifier(Globals.formatId(geneId),
                                id_network, con);

                        if (resGetGene.getIntError() == 1) {
                            result = resGetGene;
                            this.rollback();
                            return result;
                        }

                        int id_gene = resGetGene.get(0);

                        // Link gene and protein
                        MetExploreResult errGeneProtein = GeneQueries.linkGene(id_gene, id_protein, con);

                        if (errGeneProtein.getIntError() == 1) {
                            result = errGeneProtein;
                            this.rollback();
                            return result;
                        }
                    }
                } catch (SQLException e) {

                    if (e.getSQLState().equals("40001") || e.getSQLState().equals("41000")) {
                        // deadlock ("40001") or lock-wait time-out ("41000")
                        // occured
                        deadlock = true;
                        i--;
                        Thread.sleep(5000); // short delay before retry
                    } else {
                        result = new MetExploreResult(1);
                        result.message = "Database error.";
                        e.printStackTrace();
                        this.rollback();
                        return result;
                    }
                }
            } while (deadlock && retries < maxRetries);

            if (deadlock) {
                result = new MetExploreResult(1);
                result.message = "Database blocked by other processes";
                this.rollback();
                return result;
            }
            if (i % 1000 == 0) {
                this.con.commit();
            }
        }

        this.con.commit();
        this.con.setAutoCommit(true);

        return result;
    }

    /**
     * Load Enzymes of the Network
     *
     * @return {@link MetExploreResult}
     * @throws SQLException
     */
    public MetExploreResult loadEnzymes() throws SQLException, InterruptedException {

        HashMap<String, Integer> cache = new HashMap<>();

        this.con.setAutoCommit(false);

        MetExploreResult result = new MetExploreResult(0);


        BioCollection<BioEnzyme> enzymes = this.getNetwork().getEnzymesView();

        int i = 0;
        for (BioEnzyme enzyme : enzymes) {
            i++;
            boolean deadlock;
            int retries = 0;
            int maxRetries = 3600; // Corresponds to one hour

            do {
                deadlock = false;
                retries++;
                try {
                    // Create the enzyme
                    MetExploreIntegerArrayResult errEnz = NetworkQueries.createEnzyme(enzyme, id_network, con);

                    if (errEnz.getIntError() == 1) {
                        result = errEnz;
                        this.rollback();
                        return result;
                    }

                    int id_enzyme = errEnz.get(0);

                    // Link Proteins to enzymes

                    // if the enzyme is a simple protein

                    BioCollection<BioEnzymeParticipant> participants = enzyme.getParticipantsView();

                    for (BioEnzymeParticipant p : participants) {

                        BioPhysicalEntity ent = p.getPhysicalEntity();

                        if (ent.getClass().equals(BioProtein.class)) {

                            int id_protein;

                            // Get id_protein
                            if (!cache.containsKey(ent.getId())) {
                                MetExploreIntegerArrayResult resGetProtId = ProteinQueries
                                        .getid_proteinFromIdentifier(Globals.formatId(ent.getId()), id_network, con);

                                if (resGetProtId.getIntError() == 1) {
                                    result = resGetProtId;
                                    this.rollback();
                                    return result;
                                }

                                id_protein = resGetProtId.get(0);
                                cache.put(Globals.formatId(ent.getId()), id_protein);
                            } else {
                                id_protein = cache.get(ent.getId());
                            }

                            // Link protein and enzyme
                            MetExploreResult errProtEnz = ProteinQueries.linkProtein(id_protein, id_enzyme, con);

                            if (errProtEnz.getIntError() == 1) {
                                result = errProtEnz;
                                this.rollback();
                                return result;
                            }

                            if (result.getIntError() == 1) {
                                this.rollback();
                                return result;
                            }

                        }
                        if (result.getIntError() == 1) {
                            this.rollback();
                            return result;
                        }
                    }
                } catch (SQLException e) {

                    // System.err.println("State : " + e.getSQLState());

                    if (e.getSQLState().equals("40001") || e.getSQLState().equals("41000")) {
                        // deadlock ("40001") or lock-wait time-out ("41000")
                        // occured
                        // System.err.println("Database lock found. Retry");
                        deadlock = true;
                        i--;
                        Thread.sleep(5000); // short delay before retry
                    } else {
                        result = new MetExploreResult(1);
                        result.message = "Database error.";
                        e.printStackTrace();
                        this.rollback();
                        return result;
                    }
                }
            }
            while (deadlock && retries < maxRetries);

            if (deadlock) {
                result = new MetExploreResult(1);
                result.message = "Database blocked by other processes";
                this.rollback();
                return result;
            }
            if (i % 1000 == 0) {
                this.con.commit();
            }
        }
        this.con.commit();
        this.con.setAutoCommit(true);

        return result;
    }

    /**
     * Loads in the database the pathways
     *
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public MetExploreResult loadPathways() throws SQLException, InterruptedException {

        MetExploreResult result = new MetExploreResult(0);

        BioCollection<BioPathway> pathways = this.getNetwork().getPathwaysView();

        this.con.setAutoCommit(false);

        int i = 0;
        for (BioPathway pathway : pathways) {
            i++;
            boolean deadlock;
            int retries = 0;
            int maxRetries = 3600; // Corresponds to one hour

            do {
                deadlock = false;
                retries++;
                try {

                    // Create a Pathway
                    MetExploreIntegerArrayResult errPathway = NetworkQueries.createPathway(pathway, id_network, con);

                    if (errPathway.getIntError() == 1) {
                        result = errPathway;
                        this.rollback();
                        return result;
                    }

                } catch (SQLException e) {

                    // System.err.println("State : " + e.getSQLState());

                    if (e.getSQLState().equals("40001") || e.getSQLState().equals("41000")) {
                        // deadlock ("40001") or lock-wait time-out ("41000")
                        // occured
                        // System.err.println("Database lock found. Retry");
                        deadlock = true;
                        i--;
                        Thread.sleep(5000); // short delay before retry
                    } else {
                        result = new MetExploreResult(1);
                        result.message = "Database error.";
                        e.printStackTrace();
                        this.rollback();
                        return result;
                    }
                }
            }
            while (deadlock && retries < maxRetries);

            if (deadlock) {
                result = new MetExploreResult(1);
                result.message = "Database blocked by other processes";
                this.rollback();
                return result;
            }
            if (i % 1000 == 0) {
                this.con.commit();
            }
        }
        this.con.commit();
        this.con.setAutoCommit(true);

        return result;
    }

    /**
     * Load reactions
     *
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public MetExploreResult loadReactions() throws SQLException, InterruptedException {

        MetExploreResult result = new MetExploreResult(0);

        BioCollection<BioReaction> reactions = this.getNetwork().getReactionsView();

        this.con.setAutoCommit(false);

        int i = 0;
        for (BioReaction reaction : reactions) {
            i++;
            boolean deadlock;
            int retries = 0;
            int maxRetries = 3600; // Corresponds to one hour

            do {
                deadlock = false;
                retries++;
                try {
                    MetExploreIntegerArrayResult errReaction = CollectionQueries.createReaction(reaction, dbId, con);

                    if (errReaction.getIntError() == 1) {
                        result = errReaction;
                        this.rollback();
                        return result;
                    }

                    int id_reaction = errReaction.get(0);

                    // Get the different scores and set in a single SQL statement
                    HashMap<String, Integer> scores = new HashMap<>();

                    Double score = ReactionAttributes.getScore(reaction);
                    if (score != null) {

                        MetExploreIntegerArrayResult errGetScoreId = StatusQueries.getStatusId(score.toString(),
                                this.getIdReferentialReactionScore(), this.getCon());

                        if (errGetScoreId.getIntError() == 1) {
                            result.message += "</br>" + errGetScoreId.message;

                        } else {
                            scores.put("score", errGetScoreId.get(0));
                        }
                    }

                    String status = ReactionAttributes.getStatus(reaction);
                    if (!StringUtils.isVoid(status)) {
                        MetExploreIntegerArrayResult errGetStatusId = StatusQueries.getStatusId(status,
                                this.getIdReferentialReactionStatus(), this.getCon());

                        if (errGetStatusId.getIntError() == 1) {
                            result.message += "</br>" + errGetStatusId.message;

                        } else {
                            scores.put("Status", errGetStatusId.get(0));
                        }
                    }
                    if (!scores.isEmpty()) {
                        MetExploreResult errStatus = ReactionQueries.setReactionStatus(id_reaction, scores, this.getCon());

                        if (errStatus.getIntError() == 1) {
                            result = errStatus;
                            this.rollback();
                            return result;
                        }
                    }
                    // Add comments

                    Set<AnnotatorComment> comments = GenericAttributes.getAnnotatorComments(reaction);

                    if (comments != null) {
                        for (AnnotatorComment comment : comments) {
                            MetExploreIntegerArrayResult errComment = CommentQueries.addComment(comment, this.getCon());

                            if (errComment.getIntError() == 1) {
                                result = errComment;
                                this.rollback();
                                return result;
                            }

                            int id_comment = errComment.get(0);

                            MetExploreResult errLinkComment = ReactionQueries.linkCommentAndReaction(id_reaction, id_comment,
                                    this.getCon());

                            if (errLinkComment.getIntError() == 1) {
                                result = errLinkComment;
                                this.rollback();
                                return result;
                            }

                        }
                    }

                    // add the reaction in the Network
                    MetExploreIntegerArrayResult errReactionBs = NetworkQueries.addReaction(reaction, id_reaction, id_network,
                            con);

                    if (errReactionBs.getIntError() == 1) {
                        result = errReactionBs;
                        this.rollback();
                        return result;
                    }

                    int id_reaction_in_network = errReactionBs.get(0);

                    // Load pathways
                    // System.err.println("Load pathways");

                    BioCollection<BioPathway> pathways = network.getPathwaysFromReaction(reaction);

                    if (!pathways.isEmpty()) {
                        MetExploreIntegerArrayResult errPathway = PathwayQueries
                                .batchInsertReactionInPathways(id_reaction_in_network, pathways, id_network, con);

                        if (errPathway.getIntError() == 1) {
                            result = errPathway;
                            this.rollback();
                            return result;
                        }
                    }

                    // Load enzymes
                    BioCollection<BioEnzyme> enzymes = reaction.getEnzymesView();

                    if (!enzymes.isEmpty()) {
                        MetExploreIntegerArrayResult resGetEnzId = EnzymeQueries
                                .batchInsertEnzymeInReaction(id_reaction_in_network, enzymes, id_network, con);

                        if (resGetEnzId.getIntError() == 1) {
                            result = resGetEnzId;
                            this.rollback();
                            return result;
                        }
                    }



                    // Load left and right compounds
                    BioCollection<BioReactant> leftParticipants = network.getLeftReactants(reaction);

                    if (!leftParticipants.isEmpty()) {
                        MetExploreIntegerArrayResult errLMetReac = ReactionQueries.batchAddLeft(leftParticipants, reaction,
                                id_reaction, cacheMetabolites, con);

                        if (errLMetReac.getIntError() == 1) {
                            result = errLMetReac;
                            this.rollback();
                            return result;
                        }
                    }

                    BioCollection<BioReactant> rightParticipants = network.getRightReactants(reaction);

                    if (!rightParticipants.isEmpty()) {
                        MetExploreIntegerArrayResult errRMetReac = ReactionQueries.batchAddRight(rightParticipants, reaction,
                                id_reaction, cacheMetabolites, con);

                        if (errRMetReac.getIntError() == 1) {
                            result = errRMetReac;
                            this.rollback();
                            return result;
                        }
                    }


                    // Add the parameters to the ReactionInNetwork
                    BioCollection<Flux> parameters = ReactionAttributes.getAdditionalFluxParams(reaction);

                    if (parameters != null) {
                        for (Flux parameter : parameters) {

                            MetExploreResult errMetParam = ReactionQueries.addParameters(id_reaction_in_network, parameter, con);

                            if (errMetParam.getIntError() == 1) {
                                result = errMetParam;
                                this.rollback();
                                return result;
                            }
                        }
                    }
                    // try {
                    // PubMebWebservice PMws = new PubMebWebservice();

                    // if (reaction.getPmids().size() > 0) {

                    // if (PMws.testConnection()) {
                    // // Add the Biblio. using labeled "for" loop to use
                    // // nested
                    // // loop and continue statement
                    // add: for (String pmid : reaction.getPmids()) {

                    // if (pmid.equalsIgnoreCase("0")) {
                    // continue;
                    // }

                    // PMws.setPMID(pmid);

                    // HashMap<String, ArrayList<String>> databiblio = PMws.getData();

                    // for (Entry<String, ArrayList<String>> ent : databiblio.entrySet()) {

                    // if (ent.getValue().size() == 0) {
                    // result.message += "</br>Error While trying to add the article to the Reaction
                    // "
                    // + reaction.getId() + ", the given PMID (" + pmid + ") is invalid";
                    // continue add;
                    // }
                    // }

                    // MetExploreResult biblioResult = ReactionQueries.addBiblio(databiblio,
                    // id_reaction, con);

                    // if (biblioResult.getIntError() == 1) {
                    // result.message += "</br><Error While trying to add the article to the
                    // Reaction "
                    // + reaction.getId() + ", the given PMID (" + pmid + ") is invalid";
                    // }

                    // }

                    // } else {
                    // result.message += "</br>PubMed Service Unavailable (status not equal 200) for
                    // reaction "
                    // + reaction.getId();
                    // }
                    // }
                    // } catch (Exception e) {
                    // result.message += "</br>Error While trying to add the article to the Reaction
                    // " + reaction.getId()
                    // + ", the PubMed WebService crashed.";
                    // }
                } catch (SQLException e) {

                    // System.err.println("State : " + e.getSQLState());

                    if (e.getSQLState().equals("40001") || e.getSQLState().equals("41000")) {
                        // deadlock ("40001") or lock-wait time-out ("41000")
                        // occured
                        // System.err.println("Database lock found. Retry");
                        deadlock = true;
                        i--;
                        Thread.sleep(5000); // short delay before retry
                    } else {
                        result = new MetExploreResult(1);
                        result.message = "Database error.";
                        e.printStackTrace();
                        this.rollback();
                        return result;
                    }
                }
            } while (deadlock && retries < maxRetries);

            if (deadlock) {
                result = new MetExploreResult(1);
                result.message = "Database blocked by other processes";
                this.rollback();
                return result;
            }
            if (i % 1000 == 0) {
                this.con.commit();
            }
        }

        this.con.commit();
        this.con.setAutoCommit(true);

        return result;

    }

    /**
     * Close the connection to the database
     *
     * @throws SQLException
     */
    public void closeConnection() throws SQLException {
        this.getCon().close();
    }

    /**
     * Set the referential of the reaction status
     *
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public MetExploreResult setRefStatus() throws SQLException {

        MetExploreResult res = new MetExploreResult(0);

        MetExploreIntegerArrayResult resRefStatus = StatusTypeQueries
                .getStatusTypeIdByName(this.getReferentialReactionStatus(), this.getCon());

        if (resRefStatus.getIntError() == 1) {
            res = resRefStatus;
            return res;
        }

        this.setIdReferentialReactionStatus(resRefStatus.get(0));

        return res;

    }

    /**
     * Set the referential of the reaction scores
     *
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public MetExploreResult setRefScores() throws SQLException {

        MetExploreResult res = new MetExploreResult(0);

        MetExploreIntegerArrayResult resRefScore = StatusTypeQueries
                .getStatusTypeIdByName(this.getReferentialReactionScore(), this.getCon());

        if (resRefScore.getIntError() == 1) {
            res = resRefScore;
            return res;
        }

        this.setIdReferentialReactionScore(resRefScore.get(0));

        return res;

    }

    /**
     * Cancel a query
     */
    public void rollback() {
        try {
            con.setAutoCommit(false);
            con.rollback();
            con.setAutoCommit(true);
        } catch (SQLException eRollBack) {
            eRollBack.printStackTrace();
        }
    }

    /**
     * @return the input network
     */
    public BioNetwork getNetwork() {
        return network;
    }

    /**
     * @return the database connection
     */
    public Connection getCon() {
        return con;
    }


    /**
     * @return the result of the sql queries
     */
    public MetExploreResult getResult() {
        return result;
    }

    /**
     * @return the referentialReactionScore
     */
    public String getReferentialReactionScore() {
        return referentialReactionScore;
    }

    /**
     * @return the referentialReactionStatus
     */
    public String getReferentialReactionStatus() {
        return referentialReactionStatus;
    }

    /**
     * @return the idReferentialReactionScore
     */
    public int getIdReferentialReactionScore() {
        return idReferentialReactionScore;
    }

    /**
     * @param idReferentialReactionScore the idReferentialReactionScore to set
     */
    public void setIdReferentialReactionScore(int idReferentialReactionScore) {
        this.idReferentialReactionScore = idReferentialReactionScore;
    }

    /**
     * @return the idReferentialReactionStatus
     */
    public int getIdReferentialReactionStatus() {
        return idReferentialReactionStatus;
    }

    /**
     * @param idReferentialReactionStatus the idReferentialReactionStatus to set
     */
    public void setIdReferentialReactionStatus(int idReferentialReactionStatus) {
        this.idReferentialReactionStatus = idReferentialReactionStatus;
    }


}
