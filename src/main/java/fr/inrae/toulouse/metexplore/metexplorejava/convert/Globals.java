package fr.inrae.toulouse.metexplore.metexplorejava.convert;

public class Globals {

    public static String dbType = "unknown";

    /**
     * FormatId considering database type
     * @param id id to format
     */
    public static String formatId(String id) {

        if(id==null)
            return null;

        String newId = id;
        if(dbType.equalsIgnoreCase("biocyc")) {
            newId = fr.inrae.toulouse.metexplore.met4j_io.utils.StringUtils.sbmlDecode(id);
        }
        return newId;
    }

    /**
     * FormatId considering database type
     * @param id id to format
     */
    public static String unformatId(String id) {

        if(id==null)
            return null;

        String newId = id;
        if(dbType.equalsIgnoreCase("biocyc")) {
            newId = fr.inrae.toulouse.metexplore.met4j_io.utils.StringUtils.sbmlEncode(id);
        }
        return newId;
    }

}
