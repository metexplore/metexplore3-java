/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.convert;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioCompartment;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.network.NetworkAttributes;
import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioNetwork;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.BioUnitDefinition;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.BioUnitDefinitionCollection;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.NetworkQueries;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.CompartmentQueries;
import fr.inrae.toulouse.metexplore.metexplorejava.sql.UnitDefinitionQueries;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.database.MetExploreConnection;

/**
 * This class converts a Network stored in an instance of the metexplore
 * Database to a {@link BioNetwork} java object. </br>
 * It is used in the web Interface during the exports and the fluxes analysis
 *
 * @author bmerlet
 */
public class MetExploreDataBaseToBioNetwork {

    /**
     * The MySQL id of the Network that is being converted
     */
    @Option(name = "-id_network", usage = "[] Mysql id of the Network")
    public int id_network = -1;
    /**
     * In a private version of the Metexplore Database, the actual name of the
     * MySQL database
     */
    @Option(name = "-d", usage = "[metexplore] Mysql database")
    public String database = "metexplore";
    /**
     * The host where the database is held
     */
    @Option(name = "-host", usage = "[localhost] url of the mysql server")
    public String host = "localhost";
    /**
     * In a private version of the Metexplore Database, the name of the MySQL
     * user accessing the database
     */
    @Option(name = "-u", usage = "[metexplore] Mysql user")
    public String user = "metexplore";
    /**
     * In a private version of the Metexplore Database, the password of the
     * MySQL user accessing the database
     */
    @Option(name = "-passwd", usage = "[] Mysql password")
    public String passwd = "";

    @Option(name = "-dbConf", usage = "Metexplore conf path (e.g. .env)")
    public String dbConf = ".env";
    /**
     * The network created from the database
     */
    private BioNetwork network;
    /**
     * The application message
     */
    private final String message = "Application to create a SBML file from a Network in MetExplore.\n";
    /**
     * Java object holding the connection to the database
     */
    private Connection con;
    @Option(name = "-h", usage = "Prints this help")
    public Boolean h = false;

    /**
     * From a Network id and a list of parameters, outputs the size of the
     * converted Bionetwork </br>
     * This is mainly used for testing
     *
     * @param args
     * @throws IOException
     */

    public static void main(String[] args) throws Exception {
        MetExploreDataBaseToBioNetwork mtb = new MetExploreDataBaseToBioNetwork();

        CmdLineParser parser = new CmdLineParser(mtb);

        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            System.err.println(mtb.getMessage());
            parser.printUsage(System.err);
            System.exit(1);
        }

        try {
            mtb.Connection(mtb.dbConf);
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error while accessing the database");
            System.exit(1);
        }

        if (mtb.getH()) {
            System.err.println(mtb.getMessage());
            parser.printUsage(System.err);
            System.exit(0);
        }

        if (mtb.getid_network() == -1) {
            System.err.println("The Network sql id must be filled in");
            System.err.println(mtb.getMessage());
            parser.printUsage(System.err);
            System.exit(1);
        }

        MetExploreResult err;

        System.err.println("Convert MetExplore network into parseBioNet network");

        err = mtb.convert();

        if (err.getIntError() == 1) {
            System.err.println(err.message);
            System.exit(1);
        }
    }

    /**
     * Database connection through dbconf file.
     * This file has the same format as the one used in the Web Interface PhPs
     *
     * @param dbConf A .ini file containing connection parameters for an instance
     *               of the MetExplore Database. see {@link MetExploreConnection}
     * @throws IOException
     * @throws SQLException
     */
    public void Connection(String dbConf) throws IOException, SQLException, ClassNotFoundException {
        MetExploreConnection connection = new MetExploreConnection(dbConf);
        this.con = connection.getConnection();
    }

    /**
     * Convert a MetExplore Network Network in a met4j bioNetwork. </br>
     * This is the main function used in this class when converting a Network
     *
     * @return a {@link MetExploreResult}
     */
    public MetExploreResult convert() throws Exception {

        MetExploreResult err;
        this.setNetwork(new BioNetwork());

        try {

            MetExploreIntegerArrayResult resNetwork = NetworkQueries.getNetwork(this.getNetwork(), id_network,
                    con);

            if (resNetwork.getIntError() == 1) {
                System.err.println(resNetwork.message);
                return resNetwork;
            }
            this.getUnitDef();

            System.out.println("Number of unit definitions : "+NetworkAttributes.getUnitDefinitions(this.getNetwork()).size());

            this.getCompartments();

            System.out.println("Number of compartments : "+this.getNetwork().getCompartmentsView().size());

            this.getMetabolites();

            System.out.println("Number of metabolites : "+this.getNetwork().getMetabolitesView().size());

            this.getPathways();

            System.out.println("Number of pathways : "+this.getNetwork().getPathwaysView().size());

            this.getProteins();

            System.out.println("Number of proteins : "+this.getNetwork().getProteinsView().size());

            this.getEnzymesAndComplexes();

            System.out.println("Number of enzymes : "+this.getNetwork().getEnzymesView().size());

            this.getReactions();

            System.out.println("Number of reactions : "+this.getNetwork().getReactionsView().size());


        } catch (SQLException e) {
            err = new MetExploreResult(1);
            e.printStackTrace();
            err.message = e.getMessage();

            return err;
        }

        err = new MetExploreResult(0);

        return err;

    }

    /**
     * This private method retrieves the unit definitions of the Network,
     * along with the simple units, and adds them to the Bionetwork
     *
     * @throws SQLException
     * @see UnitDefinitionQueries#getUnitDefinitions(int, Connection)
     */
    private void getUnitDef() throws SQLException {

        BioUnitDefinitionCollection uDList = UnitDefinitionQueries.getUnitDefinitions(id_network, con);

        if (uDList.isEmpty()) {
            // Unit definition by default
            BioUnitDefinition UD = new BioUnitDefinition();

            NetworkAttributes.addUnitDefinition(this.getNetwork(), UD);

        } else {

            for (BioUnitDefinition ud : uDList) {
                NetworkAttributes.addUnitDefinition(this.getNetwork(), ud);
            }
        }
    }

    /**
     * Private method that retrieves all the compartments of the Network and
     * adds them to the bionetwork
     *
     * @throws SQLException
     */
    private void getCompartments() throws SQLException {

        HashMap<Integer, BioCompartment> compartList = NetworkQueries.getCompartments(NetworkAttributes.getUnitDefinitions(this.getNetwork()),
                id_network, con);

        CompartmentQueries.setOutsides(compartList, id_network, con);

        for (BioCompartment compartment : compartList.values()) {
            if (!compartment.getId().equalsIgnoreCase("fake_compartment")
                    && ! this.getNetwork().containsCompartment(compartment.getId()))
                this.getNetwork().add(compartment);
        }
    }

    /**
     * Private method that retrieves all the metabolites of the Network and
     * add them to the bionetwork
     *
     * @throws SQLException
     * @see NetworkQueries#getMetabolites(BioNetwork, int, Connection)
     */
    private void getMetabolites() throws SQLException {
        NetworkQueries.getMetabolites(network, id_network, con);
    }

    /**
     * Private method that retrieves all the pathways of the Network and add
     * them to the bionetwork
     *
     * @throws SQLException
     * @see NetworkQueries#getPathways(BioNetwork, int, Connection)
     */
    private void getPathways() throws SQLException {
        NetworkQueries.getPathways(network, id_network, con);
    }

    /**
     * Private method that retrieves all the Proteins of the Network and add
     * them to the bionetwork. </br>
     * In {@link NetworkQueries#getProteins(BioNetwork, int, Connection)},
     * there is also a query that retrieves the genes associated to each protein
     * and add them to the bionetwork
     *
     * @throws SQLException
     * @see NetworkQueries#getProteins(BioNetwork, int, Connection)
     */
    private void getProteins() throws SQLException {
        NetworkQueries.getProteins(network, id_network, con);
    }

    /**
     * Private method that retrieves all the Enzymes And the Complexes of the Network and add
     * them to the bionetwork. it also creates the links between proteins and complexes
     *
     * @throws SQLException
     * @see NetworkQueries#getEnzymesAndComplexes(BioNetwork, int, Connection)
     */
    private void getEnzymesAndComplexes() throws SQLException {
        NetworkQueries.getEnzymesAndComplexes(network, id_network, con);
    }

    /**
     * Private method that retrieves all the reactions of the Network and add
     * them to the bionetwork. it also creates all the links between the reactions and the other entities of the Bionetwork
     *
     * @throws SQLException
     * @see NetworkQueries#getReactions(BioNetwork, int, Connection)
     */
    private void getReactions() throws Exception {
        NetworkQueries.getReactions(network, id_network, con);
    }

    /*
     *
     *
     * private void convertIds() {
     *
     * for (BioMetabolite met:
     * this.network.getPhysicalEntityList().values()){
     * this.SetIdToSBMLStandard(met,"M"); } for (BioProtein prot:
     * this.network.getProteinList().values()){
     * this.SetIdToSBMLStandard(prot,"P"); } for(BioComplex
     * cplx:this.network.getComplexList().values()){
     * this.SetIdToSBMLStandard(cplx,"C"); } for (BioReaction react:
     * this.network.getReactionsView().values()){
     * this.SetIdToSBMLStandard(react,"R"); }
     *
     * }
     */

    /*
     * Set id to SBML SID format (M_xx, R_aaa,... )
     *
     * @param entity
     *
     * @param letter
     *
     * private void SetIdToSBMLStandard(BioEntity entity, String letter) {
     *
     * String id=entity.getId();
     *
     * Matcher m= Pattern.compile("^\\w?_.*").matcher(id);
     *
     * if (!m.matches()){ entity.setId(letter+"_"+id); }
     *
     * }
     */

    /**
     * @return the database connection
     */
    public Connection getCon() {
        return con;
    }

    /**
     * @param con
     */
    public void setCon(Connection con) {
        this.con = con;
    }

    /**
     * @return the input Network Mysql Id
     */
    public int getid_network() {
        return id_network;
    }

    /**
     * @param id_network
     */
    public void setid_network(int id_network) {
        this.id_network = id_network;
    }

    /**
     * @return the created bionetwork
     */
    public BioNetwork getNetwork() {
        return network;
    }

    /**
     * @param network
     */
    public void setNetwork(BioNetwork network) {
        this.network = network;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @return the h (help) value
     */
    public Boolean getH() {
        return h;
    }

    /**
     * @return the user
     */
    public String getUser() {
        return user;
    }

    /**
     * @return the database
     */
    public String getDatabase() {
        return database;
    }

}
