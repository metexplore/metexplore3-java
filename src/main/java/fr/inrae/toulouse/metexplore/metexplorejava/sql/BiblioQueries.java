/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;

/**
 * @author bmerlet
 */
public class BiblioQueries {

    /**
     * Add a bibliographic reference
     *
     * @param title
     * @param authors
     * @param journal
     * @param year
     * @param con
     * @return {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult addBiblio(String title,
                                                         String authors, String journal, Integer year, Connection con)
            throws SQLException {
        MetExploreIntegerArrayResult result;

        String reqPrep = "SELECT * FROM Biblio WHERE title LIKE ? AND authors LIKE ? AND journal LIKE ? AND year LIKE ?";

        PreparedStatement prepSt;

        int rowCount = 0;

        int generatedId = -1;

        prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, title);
        prepSt.setString(2, authors);
        prepSt.setString(3, journal);
        prepSt.setString(4, Integer.toString(year));

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            rowCount++;
        }

        if (rowCount == 0) {
            reqPrep = "INSERT INTO Biblio (title, authors, journal, year) VALUES (?, ?, ?, ?)";
            prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
            prepSt.setString(1, title);
            prepSt.setString(2, authors);
            prepSt.setString(3, journal);
            prepSt.setString(4, Integer.toString(year));

            prepSt.executeUpdate();

            rs = prepSt.getGeneratedKeys();

            if (rs.next()) {
                generatedId = rs.getInt(1);
            }
        }

        if (generatedId != -1) {
            result = new MetExploreIntegerArrayResult(0);

            result.add(generatedId);
            result.message = "Insert new Biblio " + title;
        } else {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "Error in setting Biblio " + title;
        }

        return result;

    }

    /**
     * Add a bibliographic reference
     *
     * @param pubmed_id
     * @param con
     * @return {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult addBiblio(Integer pubmed_id,
                                                         Connection con) throws SQLException {
        MetExploreIntegerArrayResult result = new MetExploreIntegerArrayResult(0);

        String reqPrep = "SELECT * FROM Biblio WHERE pubmed_id LIKE ?";

        PreparedStatement prepSt;

        int rowCount = 0;

        int generatedId = -1;

        prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, pubmed_id);
        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            rowCount++;
            generatedId = rs.getInt(1);
        }

        if (rowCount == 0) {
            reqPrep = "INSERT INTO Biblio (pubmed_id) VALUES (?)";
            prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
            prepSt.setInt(1, pubmed_id);
            prepSt.executeUpdate();

            rs = prepSt.getGeneratedKeys();

            if (rs.next()) {
                generatedId = rs.getInt(1);
            }
            result = new MetExploreIntegerArrayResult(0);
            result.add(generatedId);
            result.message = "Insert new Biblio " + pubmed_id;
        } else if (rowCount == 1) {
            result = new MetExploreIntegerArrayResult(0);
            result.add(generatedId);
            result.message = "Biblio already present " + pubmed_id;
        } else {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "Error in setting Biblio " + pubmed_id;
        }

        return result;

    }

}
