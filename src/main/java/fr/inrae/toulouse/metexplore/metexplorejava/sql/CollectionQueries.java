/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Map.Entry;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.*;
import fr.inrae.toulouse.metexplore.met4j_core.utils.StringUtils;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.GenericAttributes;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.Globals;
import fr.inrae.toulouse.metexplore.metexplorejava.refs.IdentifiersOrg;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.*;

import java.util.Set;

import static com.hp.hpl.jena.vocabulary.RSS.url;

/**
 * @author bmerlet
 */
public class CollectionQueries {


    /**
     * Creates a new collection in the database.
     *
     * @param name          the name of the collection
     * @param description   the description of the collection
     * @param id_user       the ID of the user creating the collection
     * @param forceCreation if true, forces the creation of the collection even if it already exists
     * @param con           the database connection
     * @return a MetExploreIntegerResult containing the result of the operation
     * @throws SQLException if a database access error occurs
     */
    public static MetExploreIntegerResult createCollection(String name, String description,
                                                                 int id_user, Boolean forceCreation,  Connection con)
            throws SQLException {

        MetExploreIntegerResult result = new MetExploreIntegerResult(0);

        ResultSet rs;
        int generatedId = -1;

        int rowCount = 0;

        MetExploreIntegerArrayResult resReq;

        if (!forceCreation) {
            resReq = getCollectionId(name, id_user, con);
            if (resReq.getIntError() == 1) {
                result.setMessage(resReq.getMessage());
                result.setIntError(1);
                return result;
            }
            rowCount = resReq.count();
            for (int res : resReq.results) {
                result.add(res);
            }
        }

        if (rowCount == 0) {
            String reqPrep = "insert into Collection (name, id_user, description) values(?, ?, ?)";

            PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
            prepSt.setString(1, name);
            prepSt.setInt(2, id_user);
            prepSt.setString(3, description);

            prepSt.executeUpdate();

            rs = prepSt.getGeneratedKeys();

            if (rs.next()) {
                generatedId = rs.getInt(1);
            }

            if (generatedId == -1) {
                result.setIntError(1);
                result.message = "Error in the creation of the collection " + name + ".";
            } else {
                result.message = "Creation of the collection " + name + " for the user " + id_user;
                result.add(generatedId);
            }
        } else {
            result.message = "collection " + name + " already exists";
            System.err.println(result.message);
        }
        return result;
    }

    /**
     * Get the mysql id(s) of collection
     *
     * @param collectionId      : String : name of the collection
     * @param id_user             : mysql id of the user
     * @param con                : {@link Connection}
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getCollectionId(String collectionId,
                                                                int id_user, Connection con) throws SQLException {

        MetExploreIntegerArrayResult result = new MetExploreIntegerArrayResult(0);

        String req = "SELECT id FROM Collection WHERE BINARY name='" + collectionId + "' AND id_user='" + id_user
                + "' ;";

        Statement st;
        ResultSet rs;
        int generatedId = -1;

        st = con.createStatement();
        rs = st.executeQuery(req);

        while (rs.next()) {
            generatedId = rs.getInt(1);
            result.add(generatedId);
        }

        return result;

    }

    /**
     * Generic function to update a String attribute of a collection
     * <p>
     *
     * @param id_collection : mysql id of the collectionerence
     * @param attribute
     * @param value
     * @param oldValue
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updatecollectionStringAttribute(int id_collection, String attribute, String value,
                                                                    String oldValue, Connection con) throws SQLException {

        MetExploreResult err;

        String reqPrep = "UPDATE Collection SET " + attribute + "=? WHERE id=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
        prepSt.setString(1, value);
        prepSt.setInt(2, id_collection);
        prepSt.executeUpdate();

        err = new MetExploreResult(0);

        err.message = "Update the " + attribute + " of the collection " + id_collection + " from " + oldValue + " to "
                + value;

        return err;

    }

    /**
     * Remove a collection instance
     *
     * @param dbId
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult removecollection(int dbId, Connection con) throws SQLException {

        return GenericQueries.removeRecordsByIntegerAttribute("Collection", "id", dbId, con);

    }

    /**
     * @param metabolite
     * @param dbId
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult createMetabolite(BioMetabolite metabolite, int dbId, Connection con)
            throws SQLException {

        MetExploreIntegerArrayResult result;

        String req = "SELECT id FROM Metabolite WHERE BINARY db_identifier=? AND id_collection=?;";

        PreparedStatement st;

        st = con.prepareStatement(req);
        String id = Globals.formatId(metabolite.getId());
        st.setString(1, Globals.formatId(id));
        st.setInt(2, dbId);

        ResultSet rs = st.executeQuery();

        int rowCount = 0;

        int generatedId = -1;

        while (rs.next()) {
            rowCount++;
            generatedId = rs.getInt(1);

        }

        if (rowCount == 0) {

            int genericInt = 0;
            if (GenericAttributes.getGeneric(metabolite)) {
                genericInt = 1;
            }

            String reqPrep = "insert into Metabolite (name, chemical_formula, weight, generic, id_collection, db_identifier, charge, inchi) "
                    + "values(?, ?, ?, ?, ?, ?, ?, ?);";

            PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);

            prepSt.setString(1, metabolite.getName());
            prepSt.setString(2, metabolite.getChemicalFormula());
            if (metabolite.getMolecularWeight() != null) {
                prepSt.setString(3, metabolite.getMolecularWeight().toString());
            } else {
                prepSt.setString(3, null);
            }
            prepSt.setInt(4, genericInt);
            prepSt.setInt(5, dbId);
            prepSt.setString(6, id);
            prepSt.setString(7, metabolite.getCharge().toString());

            if (metabolite.getInchi() != null) {
                String inchi = metabolite.getInchi().replaceAll("(?i)InChI\\=", "");
                prepSt.setString(8, inchi);
            } else {
                prepSt.setString(8, null);
            }

            prepSt.executeUpdate();

            rs = prepSt.getGeneratedKeys();

            if (rs.next()) {
                generatedId = rs.getInt(1);
            }

            if (generatedId == -1) {
                result = new MetExploreIntegerArrayResult(1);
                result.message = "Error in the creation of the metabolite " + id;
            } else {

                // Creation of the external db references
                MetExploreIntegerArrayResult errRef = createExtDBrefs(metabolite, generatedId, con);
                if (errRef.getIntError() == 1) {
                    result = new MetExploreIntegerArrayResult(1);
                    result.message = errRef.message;
                } else {
                    result = new MetExploreIntegerArrayResult(0);
                    result.message = "Creation of the metabolite " + id;
                    result.add(generatedId);
                }
            }
        } else {
            result = new MetExploreIntegerArrayResult(0);
            result.message = "Metabolite " + metabolite + " already exists";
            result.add(generatedId);
        }

        return result;
    }

    /**
     * adds the external DB id of the metabolite in the Metexplore DB
     *
     * @param e
     * @param mysqlId
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult createExtDBrefs(BioEntity e, int mysqlId,
                                                                Connection con) throws SQLException {

        MetExploreIntegerArrayResult result;

        String dbName, dbId, origin, tableName, columnName;
        int score;

        if (e instanceof BioMetabolite) {
            tableName = "MetaboliteIdentifiers";
            columnName = "id_metabolite";
        } else if (e instanceof BioReaction) {
            tableName = "ReactionIdentifiers";
            columnName = "id_reaction";
        } else if (e instanceof BioProtein) {
            tableName = "ProteinIdentifiers";
            columnName = "id_protein";
        }
        else if (e instanceof BioGene) {
            tableName = "GeneIdentifiers";
            columnName = "id_gene";
        } else {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "Not posssible to create references for " + e.getClass().getSimpleName();
            return result;
        }


        for (Entry<String, Set<BioRef>> entry : e.getRefs().entrySet()) {

            dbName = entry.getKey().toLowerCase();

            // we don't import ec-code ref to avoid duplicate with the EX attribute of the the Reaction Table
            if (IdentifiersOrg.validIdentifiers.contains(dbName) && ! dbName.equals("ec-code")) {

                for (BioRef idset : entry.getValue()) {

                    dbId = idset.getId();
                    score = idset.getConfidenceLevel();
                    origin = idset.getOrigin();

                    String reqPrep = "INSERT INTO " + tableName + " (" + columnName + ", ext_db_name, ext_id, origin, score) "
                            + "values(?, ?, ?, ?, ?);";

                    PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
                    prepSt.setInt(1, mysqlId);
                    prepSt.setString(2, dbName);
                    prepSt.setString(3, dbId);
                    prepSt.setString(4, origin);
                    prepSt.setInt(5, score);

                    try {
                        prepSt.executeUpdate();
                    } catch (Exception exc) {
                        result = new MetExploreIntegerArrayResult(1);
                        result.message = "Error in the creation of the " + e.getClass().getSimpleName() + "'s reference " + e.getId()+ "("+ exc.getMessage()+")";
                        return result;
                    }

                    ResultSet rs = prepSt.getGeneratedKeys();

                    int generatedId = -1;
                    if (rs.next()) {
                        generatedId = rs.getInt(1);
                    }
                    if (generatedId == -1) {
                        result = new MetExploreIntegerArrayResult(1);
                        result.message = "Error in the creation of the " + e.getClass().getSimpleName() + "'s reference " + e.getId();
                        return result;
                    }
                }
            }

        }
        result = new MetExploreIntegerArrayResult(0);
        result.message = "Creation of the " + e.getClass().getSimpleName() + " references " + e.getId();
        return result;

    }


    /**
     * Creates a reaction in the database after checking if it already exists.
     *
     * @param reaction
     * @param dbId
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult createReaction(BioReaction reaction, int dbId, Connection con)
            throws SQLException {

        MetExploreIntegerArrayResult result;

        String reqPrep = "SELECT id FROM Reaction WHERE BINARY db_identifier=? AND id_collection=?;";

        PreparedStatement prepSt;

        prepSt = con.prepareStatement(reqPrep);
        String id = Globals.formatId(reaction.getId());
        prepSt.setString(1, id);
        prepSt.setInt(2, dbId);

        ResultSet rs = prepSt.executeQuery();

        int rowCount = 0;
        int generatedId = -1;

        while (rs.next()) {
            rowCount++;
            generatedId = rs.getInt(1);
        }

        if (rowCount == 0) {
            int genericInt = 0;
            if (GenericAttributes.getGeneric(reaction)) {
                genericInt = 1;
            }

            reqPrep = "INSERT INTO Reaction (name, generic, type, ec, id_collection, db_identifier, sbo) VALUES (?, ?, ?, ?, ?, ?, ?)";

            prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
            prepSt.setString(1, reaction.getName());
            prepSt.setInt(2, genericInt);
            prepSt.setString(3, GenericAttributes.getType(reaction));

            String ecNumber = reaction.getEcNumber();
            if(StringUtils.isVoid(ecNumber)){
                ecNumber = "";
            }
            if(ecNumber.length() > 1000) {
                System.err.println("EC number fo reaction "+ id + " too long (truncated to 1000 characters)");
                ecNumber = ecNumber.substring(0,999);
            }
            prepSt.setString(4, ecNumber);
            prepSt.setInt(5, dbId);
            prepSt.setString(6, id);
            prepSt.setString(7, GenericAttributes.getSboTerm(reaction));

            prepSt.executeUpdate();

            rs = prepSt.getGeneratedKeys();

            if (rs.next()) {
                generatedId = rs.getInt(1);
            }

            if (generatedId == -1) {
                result = new MetExploreIntegerArrayResult(1);
                result.message = "Error in the creation of the reaction " + id + ".";
            } else {
                // Creation of the external db references
                MetExploreIntegerArrayResult errRef = createExtDBrefs(reaction, generatedId, con);
                if (errRef.getIntError() == 1) {
                    result = new MetExploreIntegerArrayResult(1);
                    result.message = errRef.message;
                } else {
                    result = new MetExploreIntegerArrayResult(0);
                    result.message = "Creation of the reaction " + id;
                    result.add(generatedId);
                }
            }
        } else {
            result = new MetExploreIntegerArrayResult(0);
            result.message = "Reaction " + id + " already exist";
            result.add(generatedId);
        }
        return result;
    }

    /**
     * Update attributes of a metabolite entry.
     *
     * @param metabolite
     * @param dbId
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult updateMetabolite(BioMetabolite metabolite, int dbId, Connection con)
            throws SQLException {

        MetExploreIntegerArrayResult result;

        String req = "SELECT id AS MysqlId FROM Metabolite WHERE BINARY db_identifier=? AND id_collection=?;";

        PreparedStatement st;

        st = con.prepareStatement(req);
        st.setString(1, metabolite.getId());
        st.setInt(2, dbId);

        ResultSet rs = st.executeQuery();

        int rowCount = 0;

        int existingId = -1;

        while (rs.next()) {
            rowCount++;
            existingId = rs.getInt("MysqlId");

        }

        if (rowCount == 1) {

            int genericInt = 0;
            if (GenericAttributes.getGeneric(metabolite)) {
                genericInt = 1;
            }

            String reqPrep = "UPDATE `Metabolite` SET `name`=?,`chemical_formula`=?,"
                    + "`smiles`=?,`weight`=?,`generic`=?," + "`id_collection`=?, `charge`=? WHERE  `id`=?";

            PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);

            prepSt.setString(1, metabolite.getName());
            prepSt.setString(2, metabolite.getChemicalFormula());
            prepSt.setString(3, metabolite.getSmiles());

            if (metabolite.getMolecularWeight() != null) {
                prepSt.setString(4, metabolite.getMolecularWeight().toString());
            } else {
                prepSt.setString(4, "0.0");
            }
            prepSt.setInt(5, genericInt);
            prepSt.setInt(6, dbId);
            prepSt.setString(7, metabolite.getCharge().toString());

            prepSt.setInt(10, existingId);

            int updated = prepSt.executeUpdate();

            if (updated == 0) {
                result = new MetExploreIntegerArrayResult(1);
                result.message = "Error in update of metabolite " + metabolite.getId();
            } else {

                MetExploreResult errDelRef = deleteExtDBrefs(existingId, con);
                if (errDelRef.getIntError() == 1) {
                    result = new MetExploreIntegerArrayResult(1);
                    result.message = "Error on metabolite update : " + metabolite.getId();
                }

                MetExploreIntegerArrayResult errRef = createExtDBrefs(metabolite, existingId, con);
                if (errRef.getIntError() == 1) {
                    result = new MetExploreIntegerArrayResult(1);
                    result.message = "Error on metabolite update : " + metabolite.getId();
                } else {
                    result = new MetExploreIntegerArrayResult(0);
                    result.message = "Update of metabolite " + metabolite.getId() + " completed";
                    result.add(existingId);
                }
            }
        } else {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "Error on metabolite update, metabolite " + metabolite.getId()
                    + " doesn't exist in collection " + dbId;

        }

        return result;

    }

    /**
     * Delete all external identifiers associated to a metabolite
     *
     * @param metID
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    private static MetExploreResult deleteExtDBrefs(int metID, Connection con) throws SQLException {

        MetExploreResult err = new MetExploreResult(0);

        String reqPrep = "DELETE FROM `MetaboliteIdentifiers` WHERE `id_metabolite`=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
        prepSt.setInt(1, metID);

        int deletedRows = prepSt.executeUpdate();

        if (deletedRows == -1) {
            err = new MetExploreResult(1);
            err.message = "Error on ref deletion";
        } else {
            err.message = "All ref deleted for metabolite " + metID;
        }

        return err;
    }

    /**
     * Update attributes of a Reaction entry.
     *
     * @param reaction
     * @param dbId
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult updateReaction(BioReaction reaction, int dbId, Connection con)
            throws SQLException {

        MetExploreIntegerArrayResult result = new MetExploreIntegerArrayResult(1);

        String reqPrep = "SELECT id AS MysqlId FROM Reaction WHERE BINARY db_identifier=? AND id_collection=?;";

        PreparedStatement prepSt;

        prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, reaction.getId());
        prepSt.setInt(2, dbId);

        ResultSet rs = prepSt.executeQuery();

        int rowCount = 0;

        int existingId = -1;

        while (rs.next()) {
            rowCount++;
            existingId = rs.getInt("MysqlId");
        }

        if (rowCount == 1) {
            int genericInt = 0;
            if (GenericAttributes.getGeneric(reaction)) {
                genericInt = 1;
            }

            reqPrep = "UPDATE `Reaction` SET `name`=?,`generic`=?,`type`=?,`ec`=?,`sbo`=? "
                    + "WHERE `id`=?";

            prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
            prepSt.setString(1, reaction.getName());
            prepSt.setInt(2, genericInt);
            prepSt.setString(3, GenericAttributes.getType(reaction));
            prepSt.setString(4, reaction.getEcNumber());
            prepSt.setString(5, GenericAttributes.getSboTerm(reaction));
            prepSt.setInt(6, existingId);

            int updated = prepSt.executeUpdate();

            if (updated == 0) {
                result = new MetExploreIntegerArrayResult(1);
                result.message = "Error on reaction update: " + reaction.getId() + ".";
            } else {
                result = new MetExploreIntegerArrayResult(0);
                result.message = "Update of reaction " + reaction.getId();
                result.add(existingId);
            }

        }

        return result;
    }

    /**
     * get a string attribute of a collection
     *
     * @param idDb
     * @param con
     * @return a {@link MetExploreResult} whose idString is filled
     * @throws SQLException
     */
    private static MetExploreStringResult getcollectionStringAttribute(int idDb, String attribute,
                                                                        Connection con) throws SQLException {

        MetExploreStringResult result;

        String req = "SELECT " + attribute + " FROM Collection WHERE id='" + idDb + "'";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        String value = "";

        int n = 0;

        while (rs.next()) {
            n++;
            value = rs.getString(1);
        }

        if (n == 0) {
            result = new MetExploreStringResult(1);
            result.message = "The collection " + idDb + " does not exist";
            return result;
        }

        result = new MetExploreStringResult(0);
        result.add(value);

        return result;

    }

    /**
     * get a int attribute of a collection
     *
     * @param idDb
     * @param con
     * @return a {@link MetExploreResult} whose idString is filled
     * @throws SQLException
     */
    private static MetExploreIntegerResult getcollectionIntegerAttribute(int idDb, String attribute,
                                                                        Connection con) throws SQLException {

        MetExploreIntegerResult result;

        String req = "SELECT " + attribute + " FROM Collection WHERE id='" + idDb + "'";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        Integer value = null;

        int n = 0;

        while (rs.next()) {
            n++;
            value = rs.getInt(1);
        }

        if (n == 0) {
            result = new MetExploreIntegerResult(1);
            result.message = "The collection " + idDb + " does not exist";
            return result;
        }

        result = new MetExploreIntegerResult(0);
        result.add(value);

        return result;

    }



    /**
     * Get name of the collection
     * @param id_collection
     * @param con
     * @return an integer
     * @throws SQLException
     */
    public static MetExploreStringResult getDbName(int id_collection, Connection con) throws SQLException {
        return getcollectionStringAttribute(id_collection, "name", con);
    }


    /**
     * Get id User of the collection
     * @param id_collection
     * @param con
     * @return an integer
     * @throws SQLException
     */
    public static MetExploreIntegerResult getDbIdUser(int id_collection, Connection con) throws SQLException {
        return getcollectionIntegerAttribute(id_collection, "id_user", con);
    }
}
