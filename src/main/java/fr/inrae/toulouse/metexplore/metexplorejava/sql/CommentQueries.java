/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import fr.inrae.toulouse.metexplore.met4j_core.utils.StringUtils;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.AnnotatorComment;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;

/**
 * @author lcottret
 */
public class CommentQueries {

    /**
     * Add a comment created by a user
     *
     * @param id_user  : mysql id of the user
     * @param comment : String
     * @param con
     * @return {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult addComment(String id_user, String comment, Connection con)
            throws SQLException {

        MetExploreIntegerArrayResult result;

        String reqPrep = "INSERT INTO Comment (id_user, text) VALUES (?, ?)";

        int generatedId = -1;

        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
        prepSt.setString(1, id_user);
        prepSt.setString(2, comment);
        prepSt.executeUpdate();

        ResultSet rs = prepSt.getGeneratedKeys();

        if (rs.next()) {
            generatedId = rs.getInt(1);
        }

        result = new MetExploreIntegerArrayResult(0);
        result.message = "Creation of a comment for the user " + id_user;
        result.add(generatedId);

        return result;

    }

    /**
     * @param comment
     * @param con
     * @return {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult addComment(AnnotatorComment comment, Connection con)
            throws SQLException {

        MetExploreIntegerArrayResult result;

        if (StringUtils.isVoid(comment.annotator)) {

            String reqPrep = "INSERT INTO Comment (text) VALUES (?)";

            int generatedId = -1;

            PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
            prepSt.setString(1, comment.comment);
            prepSt.executeUpdate();

            ResultSet rs = prepSt.getGeneratedKeys();

            if (rs.next()) {
                generatedId = rs.getInt(1);
            }

            result = new MetExploreIntegerArrayResult(0);
            result.message = "Creation of a comment ";
            result.add(generatedId);

        } else {
            result = addComment(comment.annotator, comment.comment, con);

        }

        return result;
    }

}
