/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioCompartment;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.compartment.CompartmentAttributes;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.Globals;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreStringArrayResult;

/**
 * @author lcottret
 */
public class CompartmentQueries {

    /**
     * Link a compartment with its outside compartment
     * <p>
     * TODO : save the Network Id in the history
     *
     * @param id_compartment_in : Mysql id of the link between compartment
     *                                 and Network
     * @param idOutsideInNetwork     : Mysql id of the link between the outside
     *                                 compartment and the Network
     * @param con                      : Sql Connection
     * @return {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult linkCompartments(int id_compartment_in, int idOutsideInNetwork,
                                                    Connection con) throws SQLException {

        MetExploreResult err;

        Statement st;

        st = con.createStatement();

        String req = "REPLACE INTO CompartmentInCompartment (id_compartment_in, id_compartment_outside) VALUES ('"
                + id_compartment_in + "', '" + idOutsideInNetwork + "');";
        st.executeUpdate(req);

        err = new MetExploreResult(0);
        err.message = "Creation of the link between the compartment " + id_compartment_in
                + " and its outside compartment " + idOutsideInNetwork;

        return err;

    }

    /**
     * Return the mysql id of a compartment from its identifier and its reference
     * database
     *
     * @param compartmentId : Identifier of the compartment
     * @param id_network          : mysql id of the Network
     * @param con           : sql Connection
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getCompartmentByIdentifier(String compartmentId, int id_network,
                                                                          Connection con) throws SQLException {

        MetExploreIntegerArrayResult result;

        String reqPrep = "select id from Compartment where BINARY db_identifier=? AND id_network=?";

        PreparedStatement prepSt;

        prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, Globals.formatId(compartmentId));
        prepSt.setInt(2, id_network);

        ResultSet rs = prepSt.executeQuery();
        int rowCount = 0;

        int id_compartment = -1;

        while (rs.next()) {
            rowCount++;
            id_compartment = rs.getInt(1);
        }

        if (rowCount == 0) {
            result = new MetExploreIntegerArrayResult(1);
            result.add(-1);
            result.message = "The compartment " + compartmentId + " does not exist in the Network " + id_network;
            return result;
        }

        if (rowCount > 1) {
            result = new MetExploreIntegerArrayResult(1);
            result.add(-1);
            result.message = "There are several instances of the compartment " + compartmentId
                    + " in the Network " + id_network;
            return result;
        }

        result = new MetExploreIntegerArrayResult(0);
        result.add(id_compartment);
        result.message = "Get the mysql id of the compartment " + compartmentId + " in the Network " + id_network;

        return result;

    }

    /**
     * Gets the identifiers of the compartments from their id_compartment_in
     *
     * @param list : a {@link ArrayList}
     * @param con  : a {@link Connection}
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getIdentifiersFromid_compartment_inArray(ArrayList<Integer> list,
                                                                                              Connection con) throws SQLException {
        return GenericQueries.getStringColumnsFromIntegerArray(list, "Compartment", "db_identifier",
                "id", con);
    }

    /**
     * Add a metabolite in a compartment of a Network
     *
     * @param id_metabolite_in_network  : mysql id of the metabolite in Network
     * @param id_compartment : mysql id of the compartment
     * @param con : sql connection
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult addMetaboliteInCompartment(int id_metabolite_in_network, int id_compartment,
                                                              Connection con) throws SQLException {

        MetExploreResult err;

        Statement st;

        st = con.createStatement();

        String req = "insert ignore into MetaboliteInCompartment (id_metabolite_in_network, id_compartment) values('" + id_metabolite_in_network
                + "', '" + id_compartment + "')";
        st.executeUpdate(req);

        err = new MetExploreResult(0);
        err.message = "Add the metaboliteInNetwork " + id_metabolite_in_network + " in the compartmentInNetwork "
                + id_compartment;


        return err;
    }

    /**
     * Creates a compartment in the database from a {@link BioCompartment} instance.
     *
     * @param compartment
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult createCompartment(BioCompartment compartment, int id_network, Connection con)
            throws SQLException {

        MetExploreIntegerArrayResult result;

        String compartmentId = Globals.formatId(compartment.getId());

        String compartmentName = compartment.getName();

        if (compartmentName == null || compartmentName.equals("NIL") || compartmentName.isEmpty()) {
            compartmentName = compartmentId;
        }

        String req = "SELECT id FROM Compartment WHERE BINARY db_identifier='" + compartmentId + "' AND id_network='" + id_network +"';";

        Statement st;

        st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        int rowCount = 0;

        int generatedId = -1;

        while (rs.next()) {
            rowCount++;
            generatedId = rs.getInt(1);
        }



        if (rowCount == 0) {

            String reqPrep = "INSERT INTO `Compartment` (`db_identifier`, `name`,`sbo`, `id_network`) VALUES (?,?,?,?);";
            PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
            prepSt.setString(1, compartmentId);
            prepSt.setString(2, compartmentName);
            prepSt.setString(3, CompartmentAttributes.getSboTerm(compartment));
            prepSt.setInt(4, id_network);

            prepSt.executeUpdate();

            rs = prepSt.getGeneratedKeys();

            if (rs.next()) {
                generatedId = rs.getInt(1);
            }

            if (generatedId == -1) {
                result = new MetExploreIntegerArrayResult(1);
                result.message = "Error in the creation of the compartment " + compartmentName + ".";
            } else {
                result = new MetExploreIntegerArrayResult(0);
                result.message = "Creation of the compartment " + compartmentName;
                result.add(generatedId);
            }
        } else {
            result = new MetExploreIntegerArrayResult(0);
            result.message = "Compartment " + compartmentName + " already exists";
            result.add(generatedId);
        }

        return result;
    }

    /**
     * Link all {@link BioCompartment} instances in a selected Network by setting
     * the 'outsideCompartment' attribute. This information is collected from the
     * database
     *
     * @param compartList
     * @param id_network
     * @param con
     * @throws SQLException
     */
    public static void setOutsides(HashMap<Integer, BioCompartment> compartList, int id_network, Connection con)
            throws SQLException {

        String reqPrep = "SELECT * FROM CompartmentInCompartment WHERE id_compartment_in IN "
                + "(SELECT id FROM Compartment WHERE id_network=?);";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_network);

        ResultSet rs = prepSt.executeQuery();
        while (rs.next()) {

            BioCompartment outside = compartList.get(rs.getInt("id_compartment_outside"));
            if (outside != null) {
                CompartmentAttributes.setOutsideCompartment(compartList.get(rs.getInt("id_compartment_in")),
                        outside);
            }
        }
    }
}
