/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioEnzyme;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.collection.BioCollection;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.Globals;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreStringArrayResult;

/**
 * @author bmerlet
 */
public class EnzymeQueries {

    /**
     * Update the name of an enzyme Fills the history table
     * <p>
     * TODO : Add the id_network in the history
     *
     * @param id_enzyme : mysql id of the enzyme
     * @param name     : new name of the Enzyme
     * @param oldName  : old name of the Enzyme
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateEnzymeName(int id_enzyme, String name, String oldName, Connection con)
            throws SQLException {

        MetExploreResult err;

        String reqPrep = "UPDATE Enzyme SET name=? WHERE id=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
        prepSt.setString(1, name);
        prepSt.setInt(2, id_enzyme);
        prepSt.executeUpdate();

        err = new MetExploreResult(0);

        err.message = "Update the name of the enzyme " + id_enzyme + " from " + oldName + " to " + name;

        return err;

    }

    /**
     * returns an array of db_identifiers elements filtered by a list of integers
     * contained in another column
     *
     * @param list
     * @param columnRef
     * @param con
     * @return a {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getdb_identifiersFromIntegerArray(ArrayList<Integer> list,
                                                                               String columnRef, Connection con) throws SQLException {
        return GenericQueries.getStringColumnsFromIntegerArray(list, "Enzyme", "db_identifier", columnRef, con);
    }

    /**
     * returns an array of db_identifiers elements filtered by a list of sql ids
     *
     * @param list
     * @param con
     * @return a {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getdb_identifiersFromIdArray(ArrayList<Integer> list, Connection con)
            throws SQLException {
        return getdb_identifiersFromIntegerArray(list, "id", con);
    }

    /**
     * Get Db identifier of an enzyme by its sql id
     *
     * @param id_enzyme : sql id of the enzyme
     * @param con      : {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getdb_identifier(int id_enzyme, Connection con) throws SQLException {

        return GenericQueries.getStringColumnsFilteredByIntegerAttribute("Enzyme", "id", id_enzyme, "db_identifier", con);

    }

    /**
     * Get Name of an enzyme by its sql id
     *
     * @param id_enzyme : sql id of the enzyme
     * @param con      : {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getName(int id_enzyme, Connection con) throws SQLException {

        return GenericQueries.getStringColumnsFilteredByIntegerAttribute("Enzyme", "id", id_enzyme, "name", con);

    }

    /**
     * Get the protein identifiers of a enzyme in a Network
     *
     * @param id_enzyme    : sql id of the enzyme
     * @param id_network : sql id of the Network
     * @param con         : {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getProteindb_identifiers(int id_enzyme, int id_network, Connection con)
            throws SQLException {
        MetExploreStringArrayResult res = new MetExploreStringArrayResult(0);

        String reqPrep = "SELECT p.db_identifier FROM Protein as p, ProteinInEnzyme as pe, "
                + "Enzyme as e "
                + "WHERE pe.id_protein=p.id AND pe.id_enzyme=e.id "
                + "AND e.id=? AND e.id_network=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_enzyme);
        prepSt.setInt(2, id_network);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            if (!res.results.contains(rs.getString(1))) {
                res.add(rs.getString(1));
            }
        }

        return res;

    }

    /**
     * Insert a batch of enzymes as catalysing a given reaction
     *
     * @param id_reaction_in_network
     * @param enzymes
     * @param id_network
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult batchInsertEnzymeInReaction(int id_reaction_in_network,
                                                                           BioCollection<BioEnzyme> enzymes, int id_network, Connection con) throws SQLException {

        MetExploreIntegerArrayResult result;
        int nEnzymes = enzymes.size();

        StringBuilder sb = new StringBuilder();
        int i = 0;
        for (BioEnzyme enz : enzymes) {
            i++;
            sb.append("'" + Globals.formatId(enz.getId()) + "'");
            if (i != nEnzymes) {
                sb.append(",");
            }
        }

        String inStatement = sb.toString();

        String sql = "SELECT e.id as id FROM Enzyme AS e WHERE e.id_network=? AND "
                + "e.db_identifier IN (" + inStatement + ");";

        PreparedStatement prepSt = con.prepareStatement(sql);
        prepSt.setInt(1, id_network);
        ResultSet rs = null;
        try {
            rs = prepSt.executeQuery();
        } catch (SQLException s) {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "Error while trying to retrieve the reaction's enzymes MySQL identifiers. (id id_reaction_in_network="
                    + id_reaction_in_network + ") ("+s.getMessage()+")";

            return result;
        }
        sb = new StringBuilder();

        while (rs.next()) {

            sb.append("(" + rs.getInt("id") + "," + id_reaction_in_network + " ),");

        }

        sb.deleteCharAt(sb.length() - 1);

        String newValues = sb.toString();
        String sql2 = "INSERT IGNORE INTO Catalyses (id_enzyme, id_reaction_in_network) VALUES " + newValues + ";";

        PreparedStatement prepSt2 = con.prepareStatement(sql2);
        int res = prepSt2.executeUpdate();

        if (res == 0) {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "Error while trying to add the enzymes to the reaction.(id id_reaction_in_network="
                    + id_reaction_in_network + ")";

            return result;
        }

        result = new MetExploreIntegerArrayResult(0);
        return result;
    }

}
