/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import fr.inrae.toulouse.metexplore.metexplorejava.convert.Globals;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreStringArrayResult;

/**
 * @author lcottret
 */
public class GeneQueries {

    /**
     * Update the name of a gene Fills the history table
     * <p>
     * TODO : Add the id_network in the history ?
     *
     * @param id_gene  : mysql id of the gene
     * @param name    : new name of the gene
     * @param oldName : old name of the gene
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateGeneName(int id_gene, String name, String oldName, Connection con)
            throws SQLException {

        MetExploreResult err;

        String reqPrep = "UPDATE Gene SET name=? WHERE id=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
        prepSt.setString(1, name);
        prepSt.setInt(2, id_gene);
        prepSt.executeUpdate();

        err = new MetExploreResult(0);

        err.message = "Update the name of the gene " + id_gene + " from " + oldName + " to " + name;

        return err;

    }

    /**
     * Link a gene and a protein in a Network
     *
     * @param id_gene    : mysql id of the gene
     * @param id_protein : mysql id of the protein
     * @param con                  : sql connection
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult linkGene(int id_gene, int id_protein,
                                            Connection con) throws SQLException {

        MetExploreResult err;

        Statement st;

        st = con.createStatement();

        String req = "insert ignore into GeneCodesForProtein (id_gene, id_protein) values('"
                + id_gene + "', '" + id_protein + "')";
        st.executeUpdate(req);

        err = new MetExploreResult(0);
        err.message = "Link the gene " + id_gene + " to the protein "
                + id_protein;

        return err;

    }

    /**
     * returns an array of db_identifiers elements filtered by a list of integers
     * contained in another column
     *
     * @param list
     * @param columnRef
     * @param con
     * @return a {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getdb_identifiersFromIntegerArray(ArrayList<Integer> list,
                                                                               String columnRef, Connection con) throws SQLException {
        return GenericQueries.getStringColumnsFromIntegerArray(list, "Gene", "db_identifier", columnRef, con);
    }

    /**
     * returns an array of db_identifiers elements filtered by a list of sql ids
     *
     * @param list
     * @param con
     * @return a {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getdb_identifiersFromIdArray(ArrayList<Integer> list, Connection con)
            throws SQLException {
        return getdb_identifiersFromIntegerArray(list, "id", con);
    }

    /**
     * Get Db identifier of a gene by its sql id
     *
     * @param id_gene : sql id of the reaction
     * @param con    : {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getdb_identifier(int id_gene, Connection con) throws SQLException {

        return GenericQueries.getStringColumnsFilteredByIntegerAttribute("Gene", "id", id_gene, "db_identifier", con);

    }

    /**
     * Get Name of a gene by its sql id
     *
     * @param id_gene : sql id of the gene
     * @param con    : {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getName(int id_gene, Connection con) throws SQLException {

        return GenericQueries.getStringColumnsFilteredByIntegerAttribute("Gene", "id", id_gene, "name", con);

    }

    /**
     * Returns the value of the id in the GeneInNetwork table from a
     * db_identifier nad a id_network
     *
     * @param db_identifier : String : the db_identifier of the gene
     * @param id_network  : the mysql id of the Network
     * @param con          : {@link Connection}
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getid_geneFromIdentifier(String db_identifier, int id_network,
                                                                       Connection con) throws SQLException {

        MetExploreIntegerArrayResult result;

        String reqPrep = "select g.id from Gene as g where binary g.db_identifier = ? AND g.id_network=?";

        PreparedStatement prepSt;
        prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, Globals.formatId(db_identifier));
        prepSt.setInt(2, id_network);
        ResultSet rs = prepSt.executeQuery();

        int rowCount = 0;

        int id_geneInNetwork = -1;

        while (rs.next()) {
            rowCount++;
            id_geneInNetwork = rs.getInt(1);
        }

        if (rowCount == 0) {
            result = new MetExploreIntegerArrayResult(1);
            result.add(-1);
            result.message = "The gene " + db_identifier + " does not exist in the Network " + id_network;
            return result;
        }

        result = new MetExploreIntegerArrayResult(0);
        result.add(id_geneInNetwork);
        result.message = "Get the mysql id of the geneInNetwork of the gene " + db_identifier + " in the Network "
                + id_network;

        return result;

    }

}
