/**
 * Copyright INRA
 * <p>
 * Contact: ludovic.cottret@toulouse.inra.fr
 * <p>
 * <p>
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 * <p>
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 * In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 * The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 * <p>
 * 10 déc. 2013
 */

package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreStringArrayResult;

/**
 * @author lcottret 10 déc. 2013
 *
 */
public class GenericQueries {

    /**
     *
     * Lock the tables specified in the string tables. See the LOCK syntaxe in
     * the Mysql reference manual
     *
     * @param tables
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult lockTables(String tables, Connection con) throws SQLException {

        MetExploreResult err;

        con.setAutoCommit(false);

        Statement st;

        String req = "LOCK TABLES " + tables;

        st = con.createStatement();
        st.executeUpdate(req);

        err = new MetExploreResult(0);

        return err;

    }

    /**
     * Unlock tables
     *
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult unlockTables(Connection con) throws SQLException {

        MetExploreResult err;

        Statement st;

        String req = "UNLOCK TABLES";

        st = con.createStatement();
        st.executeUpdate(req);

        err = new MetExploreResult(0);

        con.setAutoCommit(true);

        return err;

    }

    /**
     *
     * Start a translation
     *
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult begin(Connection con) throws SQLException {

        MetExploreResult err;

        Statement st;

        String req = "BEGIN";

        st = con.createStatement();
        st.executeUpdate(req);

        err = new MetExploreResult(0);

        return err;

    }

    /**
     *
     * commit a translation
     *
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult commit(Connection con) throws SQLException {

        MetExploreResult err;

        Statement st;

        String req = "COMMIT";

        st = con.createStatement();
        st.executeUpdate(req);

        err = new MetExploreResult(0);

        return err;

    }

    /**
     * Generic function to remove records from a table that corresponds to a
     * value of an attribute that is an integer
     *
     * @param table
     * @param attribute
     * @param value
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult removeRecordsByIntegerAttribute(String table, String attribute, int value,
                                                                   Connection con) throws SQLException {
        MetExploreResult err;

        String reqPrep = "DELETE FROM " + table + " WHERE " + attribute + "=?";
        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, value);
        prepSt.executeUpdate();

        err = new MetExploreResult(0);

        err.message = "Remove from " + table + " records whose " + attribute + " equals to " + value;

        return err;

    }

    /**
     * Generic function to remove records from a table that corresponds to a
     * value of an attribute that is a String
     *
     * @param table
     * @param attribute
     * @param value
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult removeRecordsByStringAttribute(String table, String attribute, String value,
                                                                  Connection con) throws SQLException {
        MetExploreResult err;

        String reqPrep = "DELETE FROM " + table + " WHERE BINARY " + attribute + "=?";
        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, value);
        prepSt.executeUpdate();

        err = new MetExploreResult(0);

        err.message = "Remove from " + table + " records whose " + attribute + " equals to " + value;

        return err;

    }

    /**
     * Get Records by String attribute
     *
     * @param table
     *            : the name of the MetExplore table
     * @param attribute
     *            : the name of the attribute
     * @param value
     *            : the value to test
     * @param con
     *            : the sql connection
     * @return a MetExploreIntegerArrayResult
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getIntegerColumnsFilteredByStringAttribute(String table,
                                                                                          String attribute, String value, Connection con) throws SQLException {

        MetExploreIntegerArrayResult result;

        String reqPrep = "SELECT id FROM " + table + " WHERE BINARY " + attribute + "=?";
        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, value);

        ResultSet rs = prepSt.executeQuery();

        result = new MetExploreIntegerArrayResult(0);

        result.message = "Select from " + table + " records whose " + attribute + " equals to " + value;

        while (rs.next()) {
            result.add(rs.getInt(1));
        }

        return result;

    }

    /**
     * Get Records by Integer attribute : the column is id by default
     *
     * @param table
     *            : the name of the MetExplore table
     * @param attribute
     *            : the name of the attribute
     * @param value
     *            : the value to test
     * @param con
     *            : the sql connection
     * @return a MetExploreIntegerArrayResult
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getIdsFilteredByIntegerAttribute(String table, String attribute,
                                                                                int value, Connection con) throws SQLException {

        return getIntegerColumnsFilteredByIntegerAttribute(table, attribute, value, "id", con);

    }

    /**
     * Get Records by Integer attribute
     *
     * @param table
     *            : the name of the MetExplore table
     * @param attribute
     *            : the name of the attribute
     * @param value
     *            : the value to test
     * @param column
     * @param con
     *            : the sql connection
     * @return a MetExploreIntegerArrayResult
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getIntegerColumnsFilteredByIntegerAttribute(String table,
                                                                                           String attribute, int value, String column, Connection con) throws SQLException {

        MetExploreIntegerArrayResult result = new MetExploreIntegerArrayResult(0);

        String reqPrep = "SELECT " + column + " FROM " + table + " WHERE " + attribute + "=?";
        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, value);

        ResultSet rs = prepSt.executeQuery();

        result = new MetExploreIntegerArrayResult(0);

        result.message = "Select from " + table + " records whose " + attribute + " equals to " + value;

        while (rs.next()) {
            result.add(rs.getInt(1));
        }

        return result;

    }

    /**
     *
     * @param table
     * @param attribute
     * @param value
     * @param column
     * @param con
     * @return a {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getStringColumnsFilteredByIntegerAttribute(String table, String attribute,
                                                                                         int value, String column, Connection con) throws SQLException {

        MetExploreStringArrayResult result = new MetExploreStringArrayResult(0);

        String reqPrep = "SELECT " + column + " FROM " + table + " WHERE " + attribute + "=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, value);

        ResultSet rs = prepSt.executeQuery();

        result = new MetExploreStringArrayResult(0);

        result.message = "Select from " + table + " records whose " + attribute + " equals to " + value;

        while (rs.next()) {
            result.add(rs.getString(1));
        }

        return result;

    }

    /**
     * returns an array of string elements contained in a column filtered by a
     * list of integers contained in another column Be careful, the size of the
     * array obtained is not necessarly not the same than the one of the
     * original array !!!
     *
     * @param list
     *            : a {@link ArrayList} containing the integer values
     * @param table
     *            : the name of the sql table
     * @param columnToGet
     *            : the name of the string column to get
     * @param columnRef
     *            : the name of the integer column to test
     * @param con
     *            : {@link Connection}
     * @return a {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getStringColumnsFromIntegerArray(ArrayList<Integer> list, String table,
                                                                               String columnToGet, String columnRef, Connection con) throws SQLException {
        MetExploreStringArrayResult result = new MetExploreStringArrayResult(0);

        for (Integer valueToTest : list) {
            MetExploreStringArrayResult res = getStringColumnsFilteredByIntegerAttribute(table, columnRef, valueToTest,
                    columnToGet, con);

            if (res.getIntError() == 1) {
                return res;
            }

            result.results.addAll(res.results);
        }

        return result;
    }

}
