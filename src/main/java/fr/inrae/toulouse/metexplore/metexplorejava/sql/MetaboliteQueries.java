/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import fr.inrae.toulouse.metexplore.met4j_chemUtils.chemicalStructures.InChI;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioMetabolite;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioRef;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreStringArrayResult;

/**
 * @author lcottret
 */
public class MetaboliteQueries {

    /**
     * Update the boundary condition of a metabolite in a Network
     *
     * @param id_metabolite      : mysql id of the metabolite
     * @param sourceId          : myslq id of the Network
     * @param boundary_condition
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateboundary_condition(int id_metabolite, int sourceId, int boundary_condition,
                                                           Connection con) throws SQLException {

        return updateMetaboliteInNetworkIntegerAttribute(id_metabolite, sourceId, "boundary_condition",
                boundary_condition, con);

    }

    /**
     * Update the name of a metabolite Fills the history table
     *
     * @param id_metabolite : mysql id of the metabolite
     * @param name         : new name of the metabolite
     * @param oldName      : old name of the metabolite
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateMetaboliteName(int id_metabolite, String name, String oldName, Connection con)
            throws SQLException {

        return updateMetaboliteStringAttribute(id_metabolite, "name", name, oldName, con);

    }

    /**
     * Update the name of a metabolite Fills the history table
     *
     * @param id_metabolite : mysql id of the metabolite
     * @param name         : new name of the metabolite
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateMetaboliteName(int id_metabolite, String name, Connection con)
            throws SQLException {

        return updateMetaboliteStringAttribute(id_metabolite, "name", name, con);

    }

    /**
     * Update the formula of a metabolite Fills the history table
     *
     * @param id_metabolite : mysql id of the metabolite
     * @param formula      : new formula of the metabolite
     * @param oldFormula   : old formula of the metabolite
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateMetaboliteFormula(int id_metabolite, String formula, String oldFormula,
                                                           Connection con) throws SQLException {

        return updateMetaboliteStringAttribute(id_metabolite, "chemical_formula", formula, oldFormula,
                con);

    }

    /**
     * Update the formula of a metabolite Fills the history table
     *
     * @param id_metabolite : mysql id of the metabolite
     * @param formula      : new formula of the metabolite
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateMetaboliteFormula(int id_metabolite, String formula, Connection con)
            throws SQLException {

        return updateMetaboliteStringAttribute(id_metabolite, "chemical_formula", formula, con);

    }

    /**
     * Update the mass of a metabolite Fills the history table
     *
     * @param id_metabolite : mysql id of the metabolite
     * @param mass         : new mass of the metabolite
     * @param oldMass      : old mass of the metabolite
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateMetaboliteMass(int id_metabolite, String mass, String oldMass, Connection con)
            throws SQLException {

        return updateMetaboliteStringAttribute(id_metabolite, "weight", mass, oldMass, con);

    }

    /**
     * Update the mass of a metabolite Fills the history table
     *
     * @param id_metabolite : mysql id of the metabolite
     * @param mass         : new mass of the metabolite
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateMetaboliteMass(int id_metabolite, String mass, Connection con)
            throws SQLException {

        return updateMetaboliteStringAttribute(id_metabolite, "weight", mass, con);

    }

    /**
     * Update the exact_neutral_mass of a metabolite
     *
     * @param id_metabolite
     * @param neutralMass
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateMetaboliteNeutralMass(int id_metabolite, String neutralMass, Connection con)
            throws SQLException {

        return updateMetaboliteStringAttribute(id_metabolite, "exact_neutral_mass", neutralMass, con);
    }

    /**
     * Update the average_mass of a metabolite
     *
     * @param id_metabolite
     * @param avMass
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateMetaboliteAverageMass(int id_metabolite, String avMass, Connection con)
            throws SQLException {

        return updateMetaboliteStringAttribute(id_metabolite, "average_mass", avMass, con);

    }

    /**
     * Update the Smiles of a metabolite Fills the history table
     *
     * @param id_metabolite : mysql id of the metabolite
     * @param smiles       : new smiles of the metabolite
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateMetaboliteSmiles(int id_metabolite, String smiles, Connection con)
            throws SQLException {

        return updateMetaboliteStringAttribute(id_metabolite, "smiles", smiles, con);

    }

    /**
     * Update the Smiles of a metabolite Fills the history table
     *
     * @param id_metabolite : mysql id of the metabolite
     * @param caas         : new caas of the metabolite
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateMetaboliteCaas(int id_metabolite, String caas, Connection con)
            throws SQLException {

        return updateMetaboliteStringAttribute(id_metabolite, "caas", caas, con);

    }

    /**
     * Update the charge of a metabolite Fills the history table
     *
     * @param id_metabolite : mysql id of the metabolite
     * @param charge       : new charge of the metabolite
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateMetaboliteCharge(int id_metabolite, String charge, Connection con)
            throws SQLException {

        return updateMetaboliteStringAttribute(id_metabolite, "charge", charge, con);

    }

    /**
     * Update the generic attribute of a metabolite Fills the history table
     *
     * @param id_metabolite : mysql id of the metabolite
     * @param generic      : new generic type of the metabolite (0 : not generic, 1
     *                     : generic)
     * @param oldGeneric   : old generic type of the metabolite
     * @param con
     * @return a {@link MetExploreResult}. err.getIntError() = 1 if sql problem or
     * if generic is different from 0 or 1
     * @throws SQLException
     */
    public static MetExploreResult updateMetaboliteGeneric(int id_metabolite, int generic, int oldGeneric,
                                                           Connection con) throws SQLException {

        MetExploreResult err;

        if (generic != 0 && generic != 1) {
            err = new MetExploreResult(1);
            err.message = "The generic attribute must be equal to 0 or 1";
            return err;
        }

        err = updateMetaboliteIntegerAttribute(id_metabolite, "generic", generic, oldGeneric, con);

        return err;

    }

    /**
     * Update the generic attribute of a metabolite Fills the history table
     *
     * @param id_metabolite : mysql id of the metabolite
     * @param generic      : new generic type of the metabolite (0 : not generic, 1
     *                     : generic)
     * @param con
     * @return a {@link MetExploreResult}. err.getIntError() = 1 if sql problem or
     * if generic is different from 0 or 1
     * @throws SQLException
     */
    public static MetExploreResult updateMetaboliteGeneric(int id_metabolite, int generic, Connection con)
            throws SQLException {

        MetExploreResult err;

        if (generic != 0 && generic != 1) {
            err = new MetExploreResult(1);
            err.message = "The generic attribute must be equal to 0 or 1";
            return err;
        }

        err = updateMetaboliteIntegerAttribute(id_metabolite, "generic", generic, con);

        return err;

    }

    /**
     * Return the mysql id of a metabolite from its identifier and its reference
     * database
     *
     * @param metaboliteId : Identifier of the metabolite
     * @param dbId         : mysql id of the reference database
     * @param con          : sql Connection
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getMetaboliteByIdentifier(String metaboliteId, int dbId, Connection con)
            throws SQLException {

        MetExploreIntegerArrayResult result;

        String reqPrep = "select id from Metabolite where BINARY db_identifier=? AND id_collection=?";

        PreparedStatement prepSt;

        prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, metaboliteId);
        prepSt.setInt(2, dbId);

        ResultSet rs = prepSt.executeQuery();

        int rowCount = 0;

        int id_metabolite = -1;

        while (rs.next()) {
            rowCount++;
            id_metabolite = rs.getInt(1);
        }

        if (rowCount == 0) {
            result = new MetExploreIntegerArrayResult(1);
            result.add(-1);
            result.message = "The metabolite " + metaboliteId + " does not exist in the reference database " + dbId;
            return result;
        }

        if (rowCount > 1) {
            result = new MetExploreIntegerArrayResult(1);
            result.add(-1);
            result.message = "There are several instances of the metabolite " + metaboliteId
                    + " in the reference database " + dbId;
            return result;
        }

        result = new MetExploreIntegerArrayResult(0);
        result.add(id_metabolite);
        result.message = "Get the mysql id of the metabolite " + metaboliteId + " in the reference database " + dbId;

        return result;

    }

    /**
	 * @deprecated : use old table
	 * 
     * Update the stoichiometric coefficient of a metabolite in a reaction Fills the
     * history table
     * <p>
     * TODO : Add the id_network in the history
     *
     * @param id_metabolite : mysql id of the metabolite
     * @param id_reaction   : mysql id of the reaction
     * @param coeff        : new coefficient
     * @param oldCoeff     : old coefficient
     * @param side         : Left or Right
     * @param con          : sql Connection
     * @return a {@link MetExploreResult}. err.getIntError() = 1 if sql problem
     * @throws SQLException
     */
    // public static MetExploreResult updateCoeff(int id_metabolite, int id_reaction, String coeff, String oldCoeff,
    //                                            String side, Connection con) throws SQLException {

    //     MetExploreResult err;

    //     if (!(side.contentEquals("LEFT") || side.contentEquals("RIGHT"))) {
    //         err = new MetExploreResult(1);
    //         err.message = "The side parameter must be equal to LEFT or RIGHT";
    //         return err;
    //     }

    //     String table = "RightParticipant";

    //     if (side.contentEquals("LEFT")) {
    //         table = "LeftParticipant";
    //     }

    //     String reqPrep = "UPDATE " + table + " SET coeff=? WHERE id_metabolite=? AND id_reaction=?";

    //     PreparedStatement prepSt = con.prepareStatement(reqPrep);
    //     prepSt.setString(1, coeff);
    //     prepSt.setInt(2, id_metabolite);
    //     prepSt.setInt(3, id_reaction);
    //     prepSt.executeUpdate();

    //     err = new MetExploreResult(0);

    //     err.message = "Update the coeff of the " + table + " " + id_metabolite + " in the reaction " + id_reaction
    //             + " from " + oldCoeff + " to " + coeff;

    //     return err;

    // }

    /**
	 * 
	 * @deprecated: use the old table
	 * 
     * Update the cofactor attribute of a metabolite in a reaction Fills the history
     * table
     * <p>
     * TODO : Add the id_network in the history ?
     *
     * @param id_metabolite : mysql id of the metabolite
     * @param id_reaction   : mysql id of the reaction
     * @param cof          : new cofactor attribute (0 or 1)
     * @param oldCof
     * @param side         : Left or Right
     * @param con          : sql Connection
     * @return a {@link MetExploreResult}. err.getIntError() = 1 if sql problem
     * @throws SQLException
     */
    // public static MetExploreResult updateCof(int id_metabolite, int id_reaction, int cof, int oldCof, String side,
    //                                          Connection con) throws SQLException {

    //     MetExploreResult err;

    //     if (!(side.contentEquals("LEFT") || side.contentEquals("RIGHT"))) {
    //         err = new MetExploreResult(1);
    //         err.message = "The side parameter must be equal to LEFT or RIGHT";
    //         return err;
    //     }

    //     if (cof != 0 && cof != 1) {
    //         err = new MetExploreResult(1);
    //         err.message = "The cofactor parameter must be equal to 0 or 1";
    //         return err;
    //     }

    //     String table = "RightParticipant";

    //     if (side.contentEquals("LEFT")) {
    //         table = "LeftParticipant";
    //     }

    //     Statement st;

    //     String req = "UPDATE " + table + " SET cofactor='" + cof + "' WHERE id_metabolite='" + id_metabolite
    //             + "' AND id_reaction='" + id_reaction + "'";

    //     st = con.createStatement();
    //     st.executeUpdate(req);

    //     err = new MetExploreResult(0);

    //     err.message = "Update the cofactor attribute of the " + table + " " + id_metabolite + " in the reaction "
    //             + id_reaction + " from " + oldCof + " to " + cof;

    //     return err;

    // }

    /**
	 * @deprecated: use the old table
	 * 
     * Update the side attribute of a metabolite in a reaction Fills the history
     * table
     * <p>
     * TODO : Add the id_network in the history
     *
     * @param id_metabolite : mysql id of the metabolite
     * @param id_reaction   : mysql id of the reaction
     * @param isSide       : new side attribute (0 or 1)
     * @param oldIsSide    : old side attribute
     * @param side         : Left or Right
     * @param con          : sql Connection
     * @return a {@link MetExploreResult}. err.getIntError() = 1 if sql problem
     * @throws SQLException
     */
    // public static MetExploreResult updateSide(int id_metabolite, int id_reaction, int isSide, int oldIsSide, String side,
    //                                           Connection con) throws SQLException {

    //     MetExploreResult err;

    //     if (!(side.contentEquals("LEFT") || side.contentEquals("RIGHT"))) {
    //         err = new MetExploreResult(1);
    //         err.message = "The side parameter must be equal to LEFT or RIGHT";
    //         return err;
    //     }

    //     if (isSide != 0 && isSide != 1) {
    //         err = new MetExploreResult(1);
    //         err.message = "The side attribute must be equal to 0 or 1";
    //         return err;
    //     }

    //     String table = "RightParticipant";

    //     if (side.contentEquals("LEFT")) {
    //         table = "LeftParticipant";
    //     }

    //     Statement st;

    //     String req = "UPDATE " + table + " SET side='" + isSide + "' WHERE id_metabolite='" + id_metabolite
    //             + "' AND id_reaction='" + id_reaction + "'";

    //     st = con.createStatement();
    //     st.executeUpdate(req);

    //     err = new MetExploreResult(0);

    //     err.message = "Update the side attribute of the " + table + " " + id_metabolite + " in the reaction "
    //             + id_reaction + " from " + oldIsSide + " to " + isSide;

    //     return err;

    // }

    /**
     * Generic function to update a string attribute of a metabolite
     * <p>
     * TODO : Add the id_network in the history ?
     *
     * @param id_metabolite
     * @param attribute    : attribute in the Metabolite Table
     * @param value
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    private static MetExploreResult updateMetaboliteStringAttribute(int id_metabolite, String attribute, String value,
                                                                    Connection con) throws SQLException {

        MetExploreResult err;

        Statement st;

        String req = "SELECT " + attribute + " FROM Metabolite WHERE id='" + id_metabolite + "'";

        st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        String oldValue = "";

        int n = 0;

        while (rs.next()) {
            n++;
            oldValue = rs.getString(1);
        }

        if (n == 0) {
            err = new MetExploreResult(1);
            err.message = "The metabolite with the mysql id " + id_metabolite + " does not exist ";
            return err;
        }

        String reqPrep = "UPDATE Metabolite SET " + attribute + "=? WHERE id=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, value);
        prepSt.setInt(2, id_metabolite);
        prepSt.executeUpdate();

        err = new MetExploreResult(0);

        err.message = "Update the attribute " + attribute + " of the metabolite " + id_metabolite + " from " + oldValue
                + " to " + value;

        return err;

    }

    /**
     * Generic function to update a string attribute of a metabolite
     * <p>
     * TODO : Add the id_network in the history
     *
     * @param id_metabolite
     * @param attribute    : attribute in the Metabolite Table
     * @param value
     * @param oldValue
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateMetaboliteStringAttribute(int id_metabolite, String attribute, String value,
                                                                   String oldValue, Connection con) throws SQLException {

        MetExploreResult err;

        String reqPrep = "UPDATE Metabolite SET " + attribute + "=? WHERE id=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, value);
        prepSt.setInt(2, id_metabolite);
        prepSt.executeUpdate();
        err = new MetExploreResult(0);

        err.message = "Update the " + attribute + " of the metabolite " + id_metabolite + " from " + oldValue + " to "
                + value;

        return err;

    }

    /**
     * Generic function to update a integer attribute of a MetaboliteInNetwork
     * <p>
     * TODO : Add the id_network in the history
     *
     * @param id_metabolite
     * @param sourceId
     * @param attribute
     * @param value
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateMetaboliteInNetworkIntegerAttribute(int id_metabolite, int sourceId,
                                                                               String attribute, int value, Connection con) throws SQLException {

        MetExploreResult err;

        // Get the old value first

        int oldValue = 1;

        Statement st;
        String req = "SELECT " + attribute + " FROM MetaboliteInNetwork WHERE id_network='" + sourceId
                + "' AND id_metabolite='" + id_metabolite + "'";

        st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        while (rs.next()) {
            oldValue = rs.getInt(1);
        }

        req = "UPDATE MetaboliteInNetwork SET " + attribute + "='" + value + "' WHERE id_network='" + sourceId
                + "' AND id_metabolite='" + id_metabolite + "'";

        st = con.createStatement();
        st.executeUpdate(req);

        err = new MetExploreResult(0);

        err.message = "Update the attribute " + attribute + " of the metabolite " + id_metabolite + " from " + oldValue
                + " to " + value;

        return err;

    }

    /**
     * Generic function to update a integer attribute of a metabolite
     * <p>
     * TODO : Add the id_network in the history
     *
     * @param id_metabolite
     * @param attribute    : attribute in the Metabolite Table
     * @param value
     * @param oldValue
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */

    public static MetExploreResult updateMetaboliteIntegerAttribute(int id_metabolite, String attribute, int value,
                                                                    int oldValue, Connection con) throws SQLException {

        MetExploreResult err;

        Statement st;

        String req = "UPDATE Metabolite SET " + attribute + "='" + value + "' WHERE id='" + id_metabolite + "'";

        st = con.createStatement();
        st.executeUpdate(req);

        err = new MetExploreResult(0);

        err.message = "Update the attribute " + attribute + " of the metabolite " + id_metabolite + " from " + oldValue
                + " to " + value;

        return err;

    }

    /**
     * Generic function to update a integer attribute of a metabolite
     * <p>
     * TODO : Add the id_network in the history
     *
     * @param id_metabolite
     * @param attribute    : attribute in the Metabolite Table
     * @param value
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */

    public static MetExploreResult updateMetaboliteIntegerAttribute(int id_metabolite, String attribute, int value,
                                                                    Connection con) throws SQLException {

        MetExploreResult err;

        Statement st;

        String req = "SELECT " + attribute + " FROM Metabolite WHERE id='" + id_metabolite + "'";

        st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        String oldValue = "";

        while (rs.next()) {
            oldValue = rs.getString(1);
        }

        req = "UPDATE Metabolite SET " + attribute + "='" + value + "' WHERE id='" + id_metabolite + "'";

        st = con.createStatement();
        st.executeUpdate(req);

        err = new MetExploreResult(0);

        err.message = "Update the attribute " + attribute + " of the metabolite " + id_metabolite + " from " + oldValue
                + " to " + value;

        return err;

    }

    /**
     * returns an array of db_identifiers elements filtered by a list of integers
     * contained in another column
     *
     * @param list      : a {@link ArrayList} containing the integer values
     * @param columnRef : the name of the string column to get
     * @param con       : {@link Connection}
     * @return a {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getdb_identifiersFromIntegerArray(ArrayList<Integer> list,
                                                                               String columnRef, Connection con) throws SQLException {
        return GenericQueries.getStringColumnsFromIntegerArray(list, "Metabolite", "db_identifier", columnRef, con);
    }

    /**
     * returns an array of db_identifiers elements filtered by a list of sql ids
     *
     * @param list
     * @param con
     * @return a {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getdb_identifiersFromIdArray(ArrayList<Integer> list, Connection con)
            throws SQLException {
        return getdb_identifiersFromIntegerArray(list, "id", con);
    }

    /**
     * Get the db_identifier of a metabolite by its sql id
     *
     * @param id_metabolite : sql id of the metabolite
     * @param con          {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getdb_identifier(int id_metabolite, Connection con) throws SQLException {
        return GenericQueries.getStringColumnsFilteredByIntegerAttribute("Metabolite", "id", id_metabolite,
                "db_identifier", con);
    }

    /**
     * Get the name of a metabolite by its sql id
     *
     * @param id_metabolite : sql id of the metabolite
     * @param con          {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getName(int id_metabolite, Connection con) throws SQLException {
        return GenericQueries.getStringColumnsFilteredByIntegerAttribute("Metabolite", "id", id_metabolite, "name", con);
    }

    /**
     * Get the formula of a metabolite by its sql id
     *
     * @param id_metabolite : sql id of the metabolite
     * @param con          {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getFormula(int id_metabolite, Connection con) throws SQLException {
        return GenericQueries.getStringColumnsFilteredByIntegerAttribute("Metabolite", "id", id_metabolite,
                "chemical_formula", con);
    }

    /**
     * Get the mass of a metabolite by its sql id
     *
     * @param id_metabolite : sql id of the metabolite
     * @param con          {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getMolecularWeight(int id_metabolite, Connection con) throws SQLException {
        return GenericQueries.getStringColumnsFilteredByIntegerAttribute("Metabolite", "id", id_metabolite, "weight",
                con);
    }

    /**
     * Get the charge of a metabolite by its sql id
     *
     * @param id_metabolite : sql id of the metabolite
     * @param con          {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getCharge(int id_metabolite, Connection con) throws SQLException {
        return GenericQueries.getIntegerColumnsFilteredByIntegerAttribute("Metabolite", "id", id_metabolite, "charge",
                con);
    }

    /**
     * Retrieves the inchi of a metabolite as a {@link InChI} instance
     *
     * @param MySQLiD
     * @param con
     * @return an {@link InChI} if the value exists, null otherwise
     * @throws SQLException
     */
    public static InChI getInchi(int MySQLiD, Connection con) throws SQLException {

        String req = "SELECT MetaboliteIdentifiers.ext_id AS Inchi FROM MetaboliteIdentifiers "
                + "WHERE id_metabolite=? AND ext_db_name='inchi";

        PreparedStatement prepSt = con.prepareStatement(req);
        prepSt.setInt(1, MySQLiD);
        ResultSet rs = prepSt.executeQuery();

        String inchiString = null;
        int count = 0;

        while (rs.next()) {
            count++;
            inchiString = rs.getString("Inchi");
        }

        if (count == 1) {
            return new InChI(inchiString);
        } else {
            return null;
        }

    }


    /**
     * Retrieves the svg file name of the metabolite and add it as a {@link BioRef}
     * to the {@link BioMetabolite}
     *
     * @param metabolite
     * @param con
     * @throws SQLException
     */
    @Deprecated
    public static void getSVGInRef(BioMetabolite metabolite, Connection con) throws SQLException {
        String req = "SELECT Mid.svg_file AS svg " + "FROM MetaboliteIdentifiers Mid " + "WHERE Mid.svg_file IS NOT NULL "
                + "AND Mid.id_metabolite =?;";

        String mysqlId = "";

        for (BioRef ref : metabolite.getRefs("mysqlId")) {
            mysqlId = ref.id;
        }

        PreparedStatement prepSt = con.prepareStatement(req);
        prepSt.setInt(1, Integer.parseInt(mysqlId));
        ResultSet rs = prepSt.executeQuery();

        int count = 0;
        String svg = null;
        while (rs.next()) {
            count++;
            svg = rs.getString("svg");
        }

        if (count == 1) {
            BioRef ref = new BioRef("database", "svg_file", svg, 1);
            metabolite.addRef(ref);
        }
    }


    /**
     * Get compartment identifier. Assumption : a metabolite is only in one compartment
     *
     * @param id
     * @param id_network
     * @param con
     * @return
     * @throws SQLException
     */
    public static String getCompartmentIdentifier(int id, int id_network, Connection con) throws SQLException {

        String cptId = null;

        String reqPrep = "SELECT C.db_identifier as id "
                + "FROM Compartment C, "
                + "MetaboliteInCompartment MIC, "
                + "MetaboliteInNetwork MIB "
                + "WHERE MIC.id_metabolite_in_network=MIB.id"
                + " AND C.id_network=? AND MIC.id_compartment=C.id AND MIB.id_metabolite=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);

        prepSt.setInt(2, id);
        prepSt.setInt(1, id_network);

        ResultSet rs = prepSt.executeQuery();

        if (rs.next()) {
            cptId = rs.getString("id");
        }

        return cptId;


    }


}
