/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.*;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.collection.BioCollection;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.GenericAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.compartment.CompartmentAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.gene.GeneAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.metabolite.MetaboliteAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.network.NetworkAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.protein.ProteinAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.reaction.Flux;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.reaction.ReactionAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.attributes.Notes;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.attributes.SbmlAnnotation;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.BioUnitDefinition;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.Globals;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreStringResult;
import org.apache.commons.lang3.StringUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import javax.net.ssl.*;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.cert.X509Certificate;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static fr.inrae.toulouse.metexplore.met4j_core.utils.StringUtils.isDouble;
import static fr.inrae.toulouse.metexplore.met4j_core.utils.StringUtils.isVoid;
import static fr.inrae.toulouse.metexplore.met4j_io.utils.StringUtils.isValidSboTerm;

/**
 * @author lcottret
 */
public class NetworkQueries {


    /**
     * Creates a new network in the database.
     *
     * @param dbIdentifier     the database identifier of the network
     * @param networkName      the name of the network
     * @param idOrg            the NCBI ID of the organism
     * @param strain           the strain of the network
     * @param tissue           the tissue of the network
     * @param cellType         the cell type of the network
     * @param description      the description of the network
     * @param version          the version of the network
     * @param url              the URL of the network
     * @param identifierOrigin the origin of the identifier
     * @param userId           the ID of the user creating the network
     * @param collectionId     the ID of the collection
     * @param con              the database connection
     * @return a MetExploreIntegerArrayResult containing the result of the operation
     * @throws SQLException if a database access error occurs
     */
    public static MetExploreIntegerArrayResult createNetwork(String dbIdentifier,
                                                             String networkName,
                                                             int idOrg,
                                                             String strain,
                                                             String tissue,
                                                             String cellType,
                                                             String description,
                                                             String version,
                                                             String url,
                                                             String identifierOrigin,
                                                             String source,
                                                             int userId,
                                                             int collectionId,
                                                             Connection con) throws SQLException {

        MetExploreIntegerArrayResult result = new MetExploreIntegerArrayResult(1);

        // Get the species name from the NCBI API
        String speciesName = getNcbiSpeciesName(idOrg, con);

        String reqPrep = "insert into Network (name, db_identifier, strain, tissue, cell_type, ncbi_id, id_collection, description, version, url, identifier_origin, source, organism_name) values(?, ?,  ?,  ?,  ?,  ?, ?, ?, ?, ?, ?, ?, ?)";

        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);

        if (isVoid(networkName)) {
            networkName = dbIdentifier;
        }

        prepSt.setString(1, networkName);
        prepSt.setString(2, dbIdentifier);
        prepSt.setString(3, strain);
        prepSt.setString(4, tissue);
        prepSt.setString(5, cellType);
        prepSt.setInt(6, idOrg);
        prepSt.setInt(7, collectionId);
        prepSt.setString(8, description);
        prepSt.setString(9, version);
        prepSt.setString(10, url);
        prepSt.setString(11, identifierOrigin);
        prepSt.setString(12, source);
        prepSt.setString(13, speciesName);
        prepSt.executeUpdate();

        ResultSet rs = prepSt.getGeneratedKeys();

        int generatedId = -1;
        if (rs.next()) {
            generatedId = rs.getInt(1);
        }

        if (generatedId == -1) {
            result.message = "Error in the creation of the Network " + dbIdentifier + ".";
        } else {
            result.setIntError(0);
            result.message = "Creation of the Network " + dbIdentifier + " by the user " + userId;
            result.add(generatedId);

        }
        return result;
    }

    /**
     * Retrieves the species name from the NCBI API based on the given organism ID.
     *
     * @param idOrg the NCBI ID of the organism
     * @param con   the database connection
     * @return the species name retrieved from the API, or "Not found by api" if not found
     * @throws SQLException if a database access error occurs
     */
    public static String getNcbiSpeciesName(int idOrg, Connection con) throws SQLException {

        String apiUrl = "https://metexplore.toulouse.inrae.fr/mth-ncbi-taxonomy-api/taxon_name?id=" + idOrg;
        String speciesName = "Not found by api";

        try {
            // Disable SSL certificate validation
            TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        public X509Certificate[] getAcceptedIssuers() {
                            return null;
                        }
                        public void checkClientTrusted(X509Certificate[] certs, String authType) {
                        }
                        public void checkServerTrusted(X509Certificate[] certs, String authType) {
                        }
                    }
            };

            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new java.security.SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

            // Create all-trusting host name verifier
            HostnameVerifier allHostsValid = new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            };

            // Install the all-trusting host verifier
            HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

            URL url = new URL(apiUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");

            if (conn.getResponseCode() != 200) {
                System.err.println("Failed : HTTP error code : " + conn.getResponseCode());
                return speciesName;
            }

            BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

            String output;
            StringBuilder response = new StringBuilder();
            while ((output = br.readLine()) != null) {
                response.append(output);
            }

            conn.disconnect();


            JSONParser parser = new JSONParser();
            JSONObject jsonResponse = (JSONObject) parser.parse(response.toString());

            if(! jsonResponse.containsKey("success") || jsonResponse.get("success").equals("false")) {
                String message = jsonResponse.containsKey("message") ? (String) jsonResponse.get("message") : "Error while querying the species name from the API";
                System.err.println(message);
                return speciesName;
            }
            if(! jsonResponse.containsKey("name")) {
                System.err.println("Error while querying the species name from the API");
                return speciesName;
            }

            speciesName = (String) jsonResponse.get("name");

        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Error while querying the species name from the API");
            return speciesName;
        }

        return speciesName;
    }


    /**
     * Get the sql id of a Network in a collection
     *
     * @param sourceId   : String : id_collection column
     * @param sourceName : String : Network name
     * @param strain
     * @param tissue
     * @param cellType
     * @param dbId
     * @param con        : {@link Connection}
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getNetwork(String sourceId, String sourceName, String strain, String tissue, String cellType, int dbId, Connection con) throws SQLException {

        MetExploreIntegerArrayResult result = new MetExploreIntegerArrayResult(0);

        String req = "SELECT id FROM Network WHERE BINARY id_collection='" + sourceId + "' AND id_collection='" + dbId + "' AND strain='" + strain + "' AND tissue='" + tissue + "' AND cell_type='" + cellType + "';";

        Statement st;

        st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        while (rs.next()) {
            result.add(rs.getInt(1));
        }

        return result;

    }


    /**
     * Update the name of a Network
     *
     * @param sourceId   : mysql id of the Network
     * @param sourceName : new name of the Network
     * @param oldName    : old name of the Network
     * @param con        : sql con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateNetworkName(int sourceId, String sourceName, String oldName, Connection con) throws SQLException {

        MetExploreResult result = updateNetworkStringAttribute(sourceId, "name", sourceName, oldName, con);

        return result;

    }

    /**
     * Update the tissue of a Network
     *
     * @param sourceId  : mysql id of the Network
     * @param tissue    : new tissue of the Network
     * @param oldTissue : old name of the Network
     * @param con       : sql con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateNetworkTissue(int sourceId, String tissue, String oldTissue, Connection con) throws SQLException {

        MetExploreResult result = updateNetworkStringAttribute(sourceId, "tissue", tissue, oldTissue, con);

        return result;

    }

    /**
     * Update the strain of a Network
     *
     * @param sourceId  : mysql id of the Network
     * @param strain    : new strain of the Network
     * @param oldStrain : old strain of the Network
     * @param con       : sql con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateNetworkStrain(int sourceId, String strain, String oldStrain, Connection con) throws SQLException {

        MetExploreResult result = updateNetworkStringAttribute(sourceId, "strain", strain, oldStrain, con);

        return result;

    }

    /**
     * Update the cell type of a Network
     *
     * @param sourceId    : mysql id of the Network
     * @param cellType    : new cell type of the Network
     * @param oldCellType : old cell type of the Network
     * @param con         : sql con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateNetworkCellType(int sourceId, String cellType, String oldCellType, Connection con) throws SQLException {

        MetExploreResult result = updateNetworkStringAttribute(sourceId, "cell_type", cellType, oldCellType, con);

        return result;

    }

    /**
     * Returns the mysql id of the collectionerence of a Network
     *
     * @param sourceId : mysql id of the Network
     * @param con      : sql con
     * @return a {@link MetExploreResult}
     * @throws SQLException when sql error
     */
    public static MetExploreIntegerArrayResult getNetworkcollection(int sourceId, Connection con) throws SQLException {

        MetExploreIntegerArrayResult result;

        String req = "select id_collection from Network where id='" + sourceId + "'";

        Statement st;

        st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        int rowCount = 0;

        int id_collection = -1;

        while (rs.next()) {
            rowCount++;
            id_collection = rs.getInt(1);
        }

        if (rowCount == 0) {
            result = new MetExploreIntegerArrayResult(1);
            result.add(-1);
            result.message = "The Network " + sourceId + " does not exist ?";
            return result;
        }
        result = new MetExploreIntegerArrayResult(0);
        result.add(id_collection);
        result.message = "Get the mysql id of the collectionerence of the Network " + sourceId;

        return result;

    }

    /**
     * get the name of a Network
     *
     * @param id_network
     * @param con
     * @return a {@link MetExploreResult} whose idString is filled
     * @throws SQLException
     */
    public static MetExploreStringResult getNetworkName(int id_network, Connection con) throws SQLException {

        MetExploreStringResult err = getNetworkStringAttribute(id_network, "name", con);

        return err;

    }

    /**
     * get the identifier origin of a Network
     *
     * @param id_network
     * @param con
     * @return a {@link MetExploreResult} whose idString is filled
     * @throws SQLException
     */
    public static MetExploreStringResult getNetworkIdentifierOrigin(int id_network, Connection con) throws SQLException {

        MetExploreStringResult err = getNetworkStringAttribute(id_network, "identifier_origin", con);

        return err;
    }

    /**
     * get the version of a Network
     *
     * @param id_network
     * @param con
     * @return a {@link MetExploreResult} whose idString is filled
     * @throws SQLException
     */
    public static MetExploreStringResult getNetworkVersion(int id_network, Connection con) throws SQLException {

        MetExploreStringResult err = getNetworkStringAttribute(id_network, "version", con);

        return err;
    }


    /**
     * get the url of the origin database of a Network
     *
     * @param id_network
     * @param con
     * @return a {@link MetExploreResult} whose idString is filled
     * @throws SQLException
     */
    public static MetExploreStringResult getNetworkUrl(int id_network, Connection con) throws SQLException {

        MetExploreStringResult err = getNetworkStringAttribute(id_network, "url", con);

        return err;
    }

    /**
     * get the identifier of a Network
     *
     * @param id_network
     * @param con
     * @return a {@link MetExploreResult} whose idString is filled
     * @throws SQLException
     */
    public static MetExploreStringResult getNetworkIdentifier(int id_network, Connection con) throws SQLException {

        MetExploreStringResult err = getNetworkStringAttribute(id_network, "id_collection", con);

        return err;

    }

    /**
     * Generic function to update a string attribute of a Network
     * <p>
     * TODO : Add the id_network in the history
     *
     * @param sourceId  : mysql id of the Network
     * @param attribute : attribute in the Metabolite Table
     * @param value     : new value
     * @param oldValue
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    private static MetExploreResult updateNetworkStringAttribute(int sourceId, String attribute, String value, String oldValue, Connection con) throws SQLException {

        MetExploreResult err;

        String reqPrep = "UPDATE Network SET " + attribute + "=? WHERE id=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, value);
        prepSt.setInt(2, sourceId);
        prepSt.executeUpdate();

        err = new MetExploreResult(0);

        err.message = "Update the attribute " + attribute + " of the Network " + sourceId + " from " + oldValue + " to " + value;

        return err;

    }

    /**
     * get a string attribute of a Network
     *
     * @param id_network
     * @param con
     * @return a {@link MetExploreResult} whose idString is filled
     * @throws SQLException
     */
    private static MetExploreStringResult getNetworkStringAttribute(int id_network, String attribute, Connection con) throws SQLException {

        MetExploreStringResult result;

        String req = "SELECT " + attribute + " FROM Network WHERE id='" + id_network + "'";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        String value = "";

        int n = 0;

        while (rs.next()) {
            n++;
            value = rs.getString(1);
        }

        if (n == 0) {
            result = new MetExploreStringResult(1);
            result.add(value);
            result.message = "The Network " + id_network + " does not exist ?";
            return result;
        }

        result = new MetExploreStringResult(0);
        result.add(value);

        return result;

    }

    /**
     * get a int attribute of a Network
     *
     * @param id_network
     * @param con
     * @return a {@link MetExploreResult} whose idString is filled
     * @throws SQLException
     */
    private static MetExploreIntegerResult getNetworkIntegerAttribute(int id_network, String attribute, Connection con) throws SQLException {

        MetExploreIntegerResult result;

        String req = "SELECT " + attribute + " FROM Network WHERE id='" + id_network + "'";
        Statement st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        Integer value = null;

        int n = 0;

        while (rs.next()) {
            n++;
            value = rs.getInt(1);
        }

        if (n == 0) {
            result = new MetExploreIntegerResult(1);
            result.add(value);
            result.message = "The Network " + id_network + " does not exist ?";
            return result;
        }

        result = new MetExploreIntegerResult(0);
        result.add(value);

        return result;

    }


    /**
     * Returns the ids of the reactions that are in the Network
     *
     * @param id_network :sql id of the Network
     * @param con        : java sql con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getReactions(int id_network, Connection con) throws SQLException {
        return GenericQueries.getIntegerColumnsFilteredByIntegerAttribute("ReactionInNetwork", "id_network", id_network, "id_reaction", con);
    }

    /**
     * Returns the ids of the genes that are in the Network
     *
     * @param id_network :sql id of the Network
     * @param con        : java sql con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getGenes(int id_network, Connection con) throws SQLException {
        return GenericQueries.getIntegerColumnsFilteredByIntegerAttribute("Gene", "id_network", id_network, "id", con);
    }

    /**
     * Returns the ids of the proteins that are in the Network
     *
     * @param id_network :sql id of the Network
     * @param con        : java sql con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getProteins(int id_network, Connection con) throws SQLException {
        return GenericQueries.getIntegerColumnsFilteredByIntegerAttribute("Protein", "id_network", id_network, "id", con);
    }

    /**
     * Returns the ids of the enzymes that are in the Network
     *
     * @param id_network :sql id of the Network
     * @param con        : java sql con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getEnzymes(int id_network, Connection con) throws SQLException {
        return GenericQueries.getIntegerColumnsFilteredByIntegerAttribute("Enzyme", "id_network", id_network, "id", con);
    }

    /**
     * Returns the ids of the compartments that are in the Network
     *
     * @param id_network :sql id of the Network
     * @param con        : java sql con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getCompartmentSqlIds(int id_network, Connection con) throws SQLException {
        return GenericQueries.getIntegerColumnsFilteredByIntegerAttribute("Compartment", "id_network", id_network, "id", con);
    }

    /**
     * Returns the ids of the metabolites that are in the Network
     *
     * @param id_network :sql id of the Network
     * @param con        : java sql con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getMetaboliteSqlIds(int id_network, Connection con) throws SQLException {
        return GenericQueries.getIntegerColumnsFilteredByIntegerAttribute("MetaboliteInNetwork", "id_network", id_network, "id_metabolite", con);
    }

    /**
     * Returns the ids of the unit definitions that are in the Network
     *
     * @param id_network :sql id of the Network
     * @param con        : java sql con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getUnitDefinitionSqlIds(int id_network, Connection con) throws SQLException {
        return GenericQueries.getIntegerColumnsFilteredByIntegerAttribute("UnitDefinitionInNetwork", "id_network", id_network, "id_unit_definition", con);
    }

    /**
     * Select UnitDefinition mysqlid with the input Identifier present in the
     * Network
     *
     * @param id_network
     * @param identifier
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getUnitDefinitionFromIdentifier(int id_network, String identifier, Connection con) throws SQLException {

        MetExploreIntegerArrayResult result;

        String reqPrep = "SELECT UD.id FROM UnitDefinition UD, UnitDefinitionInNetwork UDinBio WHERE " + "UD.id=UDinBio.id_unit_definition AND UDinBio.id_network=? AND UD.identifier=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_network);
        prepSt.setString(2, identifier);

        ResultSet rs = prepSt.executeQuery();

        result = new MetExploreIntegerArrayResult(0);
        result.message = "Select UnitDefinition mysqlid which has the identifier " + identifier + " and is present in the Network " + id_network;

        if (rs.next()) {
            result.add(rs.getInt(1));
        } else {
            result.add(-1);
        }

        return result;
    }

    /**
     * @param metadata
     * @param id_network
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult addmetadata(String metadata, int id_network, Connection con) throws SQLException {
        MetExploreIntegerArrayResult err;

        String reqPrep = "INSERT INTO `NetworkMetadata`(`id_network`, `metadata`) " + "VALUES (?, ?);";
        PreparedStatement prepSt = con.prepareStatement(reqPrep);

        prepSt.setInt(1, id_network);
        prepSt.setString(2, metadata);

        prepSt.executeUpdate();

        err = new MetExploreIntegerArrayResult(0);
        err.message = "Creation of the Network's metadata ";

        return err;
    }

    /**
     * Adds the metabolite in the Network (table MetaboliteInNetwork). if the
     * metabolite is flagged as "in conflict" also retrieves and add the mysql id of
     * the referenced metabolite
     *
     * @param metabolite
     * @param mySQLMetaboliteId
     * @param mySQLNetworkId
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreIntegerResult addMetabolite(BioMetabolite metabolite, int mySQLMetaboliteId, int mySQLNetworkId, Connection con) throws SQLException {

        MetExploreIntegerResult err;


        int boundary_conditionInt = 0;

        Boolean boundary_condition = MetaboliteAttributes.getBoundaryCondition(metabolite);

        if (boundary_condition != null && boundary_condition) {
            boundary_conditionInt = 1;
        }

        int sideInt = 0;
        Boolean isCofactor = MetaboliteAttributes.getIsCofactor(metabolite);

        if (isCofactor) {
            sideInt = 1;
        }

        int ConstantInt = 0;

        Boolean constantBool = MetaboliteAttributes.getConstant(metabolite);

        if (constantBool) {
            ConstantInt = 1;
        }

        String reqPrep = "REPLACE INTO MetaboliteInNetwork (id_network, id_metabolite, boundary_condition, side_compound, constant, substance_unit, has_only_substance_unit, initial_quantity) " + "VALUES (?,?,?,?,?,?,?,?)";

        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
        prepSt.setInt(1, mySQLNetworkId);
        prepSt.setInt(2, mySQLMetaboliteId);
        prepSt.setInt(3, boundary_conditionInt);
        prepSt.setInt(4, sideInt);
        prepSt.setInt(5, ConstantInt);

        prepSt.setString(6, MetaboliteAttributes.getSubtanceUnits(metabolite));

        Boolean has_only_substance_units = MetaboliteAttributes.getHasOnlySubstanceUnits(metabolite);
        if (has_only_substance_units == null) {
            prepSt.setInt(7, Types.INTEGER);
        } else {
            prepSt.setInt(7, has_only_substance_units ? 1 : 0);
        }

        Double initialAmount = MetaboliteAttributes.getInitialAmount(metabolite);
        if (initialAmount == null) {
            prepSt.setString(8, null);
        } else {
            prepSt.setString(8, initialAmount.toString());
        }

        prepSt.executeUpdate();

        ResultSet rs = prepSt.getGeneratedKeys();

        int generatedId = -1;
        if (rs.next()) {
            generatedId = rs.getInt(1);
        }

        if (generatedId != -1) {
            err = new MetExploreIntegerResult(0);
            err.add(generatedId);
            err.message = "Add the metabolite " + mySQLMetaboliteId + " in the Network " + mySQLNetworkId;

        } else {
            err = new MetExploreIntegerResult(1);
            err.add(generatedId);
            err.message = "Error while adding the metabolite " + metabolite.getId() + " in the Network " + mySQLNetworkId;
        }

        return err;

    }

    /**
     * @param reaction
     * @param id_reaction
     * @param id_network
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult addReaction(BioReaction reaction, int id_reaction, int id_network, Connection con) throws SQLException {

        MetExploreIntegerArrayResult result;
        int generatedId = -1;


        int uDMysqlId = -1;

        Flux lowerFlux = ReactionAttributes.getLowerBound(reaction);
        Flux upperFlux = ReactionAttributes.getUpperBound(reaction);

        if (lowerFlux != null && lowerFlux.unitDefinition != null) {
            String uDIDentifier = lowerFlux.unitDefinition.getId();

            MetExploreIntegerArrayResult rUD = getUnitDefinitionFromIdentifier(id_network, uDIDentifier, con);
            uDMysqlId = rUD.get(0);
        }

        int reversibleInt = 0, fastInt = 0, holeInt = 0;
        if (reaction.isReversible()) {
            reversibleInt = 1;
        }

        if (ReactionAttributes.getSpontaneous(reaction)) {
            fastInt = 1;
        }
        if (ReactionAttributes.getHole(reaction)) {
            holeInt = 1;
        }
        String reqPrep = "REPLACE INTO ReactionInNetwork (id_reaction, id_network, reversible, fast, hole, upper_bound, lower_bound, unit_definition_bounds, kinetic_formula) " + "VALUES( ?, ?, ?, ?, ?, ?, ?, ?, ? );";

        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);

        prepSt.setInt(1, id_reaction);
        prepSt.setInt(2, id_network);
        prepSt.setInt(3, reversibleInt);
        prepSt.setInt(4, fastInt);
        prepSt.setInt(5, holeInt);


        if (upperFlux != null) {
            prepSt.setString(6, upperFlux.value.toString());
        } else {
            prepSt.setString(6, Flux.FLUXMAX.toString());
        }

        if (lowerFlux != null) {
            prepSt.setString(7, lowerFlux.value.toString());
        } else {
            prepSt.setString(7, reaction.isReversible() ? Flux.FLUXMIN.toString() : "0.0");
        }

        if (uDMysqlId != -1) {
            prepSt.setInt(8, uDMysqlId);
        } else {
            prepSt.setNull(8, Types.INTEGER);
        }

        String formula = ReactionAttributes.getKineticFormula(reaction);

        if (formula != null) {
            prepSt.setString(9, formula);
        } else {
            prepSt.setNull(9, Types.VARCHAR);
        }

        prepSt.executeUpdate();

        ResultSet rs = prepSt.getGeneratedKeys();

        if (rs.next()) {
            generatedId = rs.getInt(1);
        }

        if (generatedId != -1) {

            result = new MetExploreIntegerArrayResult(0);
            result.message = "Add the reaction " + reaction.getId() + " in the Network " + id_network;
            result.add(generatedId);

        } else {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "Error while trying to add the reaction " + reaction.getId() + " in the Network " + id_network;
        }

        return result;

    }

    /**
     * @param network
     * @param id_network
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getNetwork(BioNetwork network, int id_network, Connection con) throws SQLException {

        MetExploreIntegerArrayResult result;

        String reqPrep = "SELECT BS.name AS name FROM Network BS WHERE BS.id =? ";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_network);

        ResultSet rs = prepSt.executeQuery();

        int rowCount = 0;
        String nameBS = "";
        while (rs.next()) {
            rowCount++;
            nameBS = rs.getString("name");
        }

        if (rowCount != 0) {
            result = new MetExploreIntegerArrayResult(0);
            network.setName(nameBS);
        } else {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "The Network " + id_network + " does not exist in the Database ";
        }
        return result;
    }

    /**
     * @param network
     * @param id_network
     * @param con
     * @throws SQLException
     */
    public static void getNetworkMeta(BioNetwork network, int id_network, Connection con) throws SQLException {

        String reqPrep = "SELECT BSM.metadata AS meta FROM NetworkMetadata BSM WHERE BSM.id_network=? ";
        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_network);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            String metadata = rs.getString("meta");

            Matcher m;
            if (metadata.startsWith("<annotation>")) {
                m = Pattern.compile(".*?rdf:about=\"#([^\"]+)\".*").matcher(metadata.replaceAll(">\\s+<", "><"));
                if (m.matches()) {
                    String value = m.group(1);

                    SbmlAnnotation annot = new SbmlAnnotation(value, metadata);

                    NetworkAttributes.setAnnotation(network, annot);

                }
            } else if (metadata.startsWith("<notes>")) {

                NetworkAttributes.setNotes(network, new Notes(metadata));

            }

        }

    }

    /**
     * @param bioUnitList
     * @param id_network
     * @param con
     * @return {@link HashMap}: Integer, {@link BioCompartment}
     * @throws SQLException
     */
    public static HashMap<Integer, BioCompartment> getCompartments(BioCollection<BioUnitDefinition> bioUnitList, int id_network, Connection con) throws SQLException {

        HashMap<Integer, BioCompartment> compartList = new HashMap<>();

        String reqPrep = "SELECT C.id AS mySQLid , C.spatial_dimensions AS dim, C.units AS unit, C.constant AS cst, "
                + "C.size AS size, C.db_identifier AS dbID, C.name AS name, C.sbo AS sbo FROM Compartment C "
                + "WHERE C.id_network=? ORDER BY C.name ASC;";
        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_network);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {

            String id = Globals.unformatId(rs.getString("dbID"));

            BioCompartment compart = new BioCompartment(id, rs.getString("name"));

            if (rs.getString("sbo") != null) {
                CompartmentAttributes.setSboTerm(compart, rs.getString("sbo"));
            }

            if (rs.getString("cst") != null) {
                CompartmentAttributes.setConstant(compart, rs.getBoolean("cst"));
            }

            if (rs.getString("dim") != null) {

                CompartmentAttributes.setSpatialDimensions(compart, rs.getInt("dim"));

                if (rs.getString("size") != null) {

                    CompartmentAttributes.setSize(compart, rs.getDouble("size"));

                }
            }

            if (rs.getString("unit") != null) {
                CompartmentAttributes.setUnitDefinition(compart, bioUnitList.get(rs.getString("unit")));
            }

            compartList.put(rs.getInt("mySQLid"), compart);
        }

        return compartList;

    }

    /**
     * Gets all the metabolites of a given Network from the database, creates the
     * corresponding {@link BioMetabolite} and add them to the {@link BioNetwork}
     *
     * @param network    : the network representing the Network
     * @param id_network : the id of the network
     * @param con        : the database con
     * @throws SQLException
     */
    public static void getMetabolites(BioNetwork network, int id_network, Connection con) throws SQLException {

        String reqPrep = "SELECT MBS.id AS MBSmySQLid, MBS.boundary_condition, MBS.side_compound, "
                + "MBS.constant, MBS.substance_unit, MBS.has_only_substance_unit, MBS.initial_quantity, M.id AS MetabID, M.name, M.chemical_formula, " +
                "M.weight, M.generic, M.db_identifier, M.charge, C.db_identifier AS ComDBId "
                + "FROM MetaboliteInNetwork MBS, Metabolite M, MetaboliteInCompartment MiC, Compartment C "
                + "WHERE M.id = MBS.id_metabolite " + "AND MBS.id = MiC.id_metabolite_in_network "
                + "AND MiC.id_compartment = C.id "
                + "AND MBS.id_network =?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_network);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {

            String id = Globals.unformatId(rs.getString("db_identifier"));

            if (!network.containsMetabolite(id)) {
                BioMetabolite metabolite;
                if (!rs.getString("name").isEmpty()) {
                    metabolite = new BioMetabolite(id, rs.getString("name"));
                } else {
                    metabolite = new BioMetabolite(id, id);
                }

                MetaboliteAttributes.setBoundaryCondition(metabolite, rs.getBoolean("boundary_condition"));

                MetaboliteAttributes.setIsCofactor(metabolite, rs.getBoolean("side_compound"));
                MetaboliteAttributes.setConstant(metabolite, rs.getBoolean("constant"));
                MetaboliteAttributes.setSubstanceUnits(metabolite, rs.getString("substance_unit"));
                MetaboliteAttributes.setHasOnlySubstanceUnits(metabolite, rs.getBoolean("has_only_substance_unit"));

                metabolite.setChemicalFormula(rs.getString("chemical_formula"));

                String weight = rs.getString("weight");
                if (weight != null) weight = weight.replaceAll("d0", "");

                if (isDouble(weight)) {
                    metabolite.setMolecularWeight(Double.parseDouble(weight));
                }

                MetaboliteAttributes.setGeneric(metabolite, rs.getBoolean("generic"));

                metabolite.setCharge(rs.getInt("charge"));

                String initial_quantity = rs.getString("initial_quantity");
                int ind = -1;
                if (initial_quantity != null) {
                    ind = initial_quantity.indexOf('-');
                }
                if (ind != -1) {
                    initial_quantity = initial_quantity.substring(0, initial_quantity.indexOf('-'));
                } else {
                    initial_quantity = "1.0";
                }

                if (isDouble(initial_quantity)) {
                    MetaboliteAttributes.setInitialAmount(metabolite, Double.parseDouble(initial_quantity));
                }

                BioCompartment comp;

                String cptId = Globals.unformatId(rs.getString("ComDBId"));

                if (!cptId.equalsIgnoreCase("fake_compartment")) {
                    if (!network.containsCompartment(cptId)) {
                        System.err.println("Error: compartment " + cptId + " not created");
                        System.exit(1);
                    }

                    comp = network.getCompartment(cptId);

                    network.add(metabolite);
                    network.affectToCompartment(comp, metabolite);
                }

                getEntityRef(metabolite, rs.getInt("MetabID"), con);
            }

        }
    }

    /**
     * Get all external bd ids from the database (including SBO, InChI => those
     * attribute are not set in the BioEntity)
     *
     * @param e
     * @param entityId
     * @param con
     * @throws SQLException
     */
    public static MetExploreResult getEntityRef(BioEntity e, int entityId, Connection con) throws SQLException {

        String tableName, columnName;
        MetExploreResult result;

        if (e instanceof BioMetabolite) {
            tableName = "MetaboliteIdentifiers";
            columnName = "id_metabolite";
        } else if (e instanceof BioReaction) {
            tableName = "ReactionIdentifiers";
            columnName = "id_reaction";
        } else if (e instanceof BioProtein) {
            tableName = "ProteinIdentifiers";
            columnName = "id_protein";
        } else if (e instanceof BioGene) {
            tableName = "GeneIdentifiers";
            columnName = "id_gene";
        } else {
            result = new MetExploreResult(1);
            result.message = "Not posssible to create references for " + e.getClass().getSimpleName();
            return result;
        }

        String reqPrep = "SELECT ext_db_name, group_concat(ext_id, \" | \", origin , \" | \", score SEPARATOR \" ; \") AS idsandScores" + " FROM " + tableName + " " + "WHERE " + columnName + "=? GROUP BY ext_db_name";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, entityId);

        ResultSet rs = prepSt.executeQuery();
        HashMap<String, Set<BioRef>> ref = new HashMap<>();
        while (rs.next()) {
            String DBName = rs.getString("ext_db_name");

            String[] idsandScores = rs.getString("idsandScores").split(" ; ");

            Set<BioRef> DBref = new HashSet<>();

            for (String value : idsandScores) {

                String[] array = value.split(" \\| ");
                String DBid = array[0];

                if (array.length < 3) {
                    continue;
                }

                int score = Integer.parseInt(array[2].trim());
                String origin = array[1];

                DBref.add(new BioRef(origin, DBName, DBid, score));
            }

            ref.put(DBName, DBref);
        }

        e.setRefs(ref);

        return new MetExploreResult(0);
    }

    /**
     * Gets all the pathways of a given Network from the database, creates the
     * corresponding {@link BioPathway} and add them to the {@link BioNetwork}
     *
     * @param network    : the network representing the Network
     * @param id_network : the id of the network
     * @param con        : the database con
     * @throws SQLException
     */
    public static void getPathways(BioNetwork network, int id_network, Connection con) throws SQLException {

        String reqPrep = "SELECT DISTINCT(Pa.id), Pa.name, Pa.db_identifier FROM Pathway Pa, ReactionInPathway RiP, ReactionInNetwork RBS " + "WHERE Pa.id=RiP.id_pathway AND RiP.id_reaction_in_network=RBS.id AND RBS.id_network=? ";
        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_network);

        ResultSet rs = prepSt.executeQuery();
        while (rs.next()) {

            String id = Globals.unformatId(rs.getString("db_identifier"));

            if (!isVoid(id)) {
                BioPathway path = new BioPathway(id, rs.getString("name"));
                network.add(path);
            }
        }
    }

    /**
     * Adds all the proteins and the corresponding genes of a given Network from
     * the database, creates the {@link BioProtein} and {@link BioGene} and add them
     * to the {@link BioNetwork}
     *
     * @param network    : the network representing the Network
     * @param id_network : the id of the network
     * @param con        : the database con
     * @throws SQLException
     */
    public static void getProteins(BioNetwork network, int id_network, Connection con) throws SQLException {

        String reqPrep = "SELECT P.id as PMysqlId, P.name AS PName, P.db_identifier AS PdbId, P.sbo AS PSbo "
                + "FROM Protein P WHERE P.id_network =?";
        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_network);

        ResultSet rs = prepSt.executeQuery();

        int rowCount = 0;

        while (rs.next()) {
            rowCount = rowCount + 1;

            int proteinMySqlId = rs.getInt("PMysqlId");

            String reqGenes = "SELECT G.id AS Gid, G.name AS GName, G.db_identifier AS GdbId, G.sbo AS Gsbo "
                    + "FROM Gene G, GeneCodesForProtein GCFP "
                    + "WHERE GCFP.id_protein=? " + " AND G.id=GCFP.id_gene";

            PreparedStatement prepStGenes = con.prepareStatement(reqGenes, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
            prepStGenes.setInt(1, proteinMySqlId);

            ResultSet rsGenes = prepStGenes.executeQuery();

            int rowcount = 0;
            if (rsGenes.last()) {
                rowcount = rsGenes.getRow();
                rsGenes.beforeFirst(); // not rs.first() because the rs.next()
                // below will move on, missing the first
                // element
            }

            if (rowcount <= 1) {

                String protId = Globals.unformatId(rs.getString("PdbId"));

                BioProtein prot;

                if (network.containsProtein(protId)) {
                    prot = network.getProtein(protId);
                } else {
                    prot = new BioProtein(protId, rs.getString("PName"));
                    network.add(prot);
                }


                getEntityRef(prot, proteinMySqlId, con);

                if (rs.getString("PSbo") != null && isValidSboTerm(rs.getString("PSbo"))) {
                    ProteinAttributes.setSboTerm(prot, rs.getString("PSbo"));
                }

                while (rsGenes.next()) {

                    String geneId = Globals.unformatId(rsGenes.getString("GdbId"));

                    if (!network.containsGene(geneId)) {
                        BioGene gene = new BioGene(geneId, rsGenes.getString("GName"));

                        if (rsGenes.getString("Gsbo") != null && isValidSboTerm(rsGenes.getString("Gsbo"))) {

                            GeneAttributes.setSboTerm(gene, rsGenes.getString("Gsbo"));
                        }

                        network.add(gene);

                        network.affectGeneProduct(prot, gene);

                        getEntityRef(gene, rsGenes.getInt("Gid"), con);

                    } else {

                        network.affectGeneProduct(prot, network.getGene(geneId));

                    }
                }

            } else {

                String proteindb_identifier = rs.getString("PdbId");
                // The protein has several genes, what is impossible in the new
                // Parsebionet/met4j scheme -> Transforms
                // protein in enzyme and set protein ids from gene ids

                Set<BioProtein> proteins = new HashSet<>();

                while (rsGenes.next()) {

                    String geneId = Globals.unformatId(rsGenes.getString("GdbId"));
                    String name = rsGenes.getString("GName");

                    BioProtein prot;

                    if (network.containsProtein(geneId)) {
                        prot = network.getProtein(geneId);
                    } else {
                        prot = new BioProtein(geneId, name);
                        network.add(prot);
                    }

                    BioGene gene;

                    if (!network.containsGene(geneId)) {

                        gene = new BioGene(geneId, rsGenes.getString("GName"));

                        getEntityRef(gene, rsGenes.getInt("Gid"), con);

                        network.add(gene);

                        if (rsGenes.getString("Gsbo") != null && isValidSboTerm(rsGenes.getString("Gsbo"))) {
                            GeneAttributes.setSboTerm(gene, rsGenes.getString("Gsbo"));
                        }

                    } else {
                        gene = network.getGene(geneId);
                    }

                    network.affectGeneProduct(prot, gene);

                    proteins.add(prot);

                }

                BioEnzyme enz;

                if (network.containsEnzyme(proteindb_identifier)) {
                    enz = network.getEnzyme(proteindb_identifier);
                } else {
                    enz = new BioEnzyme(proteindb_identifier, proteindb_identifier);
                    network.add(enz);
                }

                for (BioProtein p : proteins) {
                    network.affectSubUnit(enz, 1.0, p);
                }
            }
        }

    }

    /**
     * Gets all the Enzymes and the corresponding proteic complexes of a given
     * Network from the database, creates the {@link BioEnzyme} and add them to
     * the {@link BioNetwork}
     *
     * @param network    : the network representing the Network
     * @param id_network : the id of the network
     * @param con        : the database con
     * @throws SQLException
     */
    public static void getEnzymesAndComplexes(BioNetwork network, int id_network, Connection con) throws SQLException {

        String reqPrep = "SELECT GROUP_CONCAT(DISTINCT R.db_identifier SEPARATOR ' | ') AS reactionIds, E.id AS mySQLId, E.name AS name, E.db_identifier, E.sbo "
                + "FROM Enzyme E "
                + "LEFT OUTER JOIN Catalyses Cat ON E.id=Cat.id_enzyme "
                + "LEFT OUTER JOIN ReactionInNetwork RBS ON Cat.id_reaction_in_network=RBS.id "
                + "LEFT OUTER JOIN Reaction R ON RBS.`id_reaction` = R.id "
                + "WHERE E.id_network =? GROUP BY mySQLId;";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_network);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {

            String enzId = Globals.unformatId(rs.getString("db_identifier"));
            String enzName = rs.getString("name");

            if (!network.containsEnzyme(enzId)) {

                BioEnzyme enzyme = new BioEnzyme(enzId, enzName);

                network.add(enzyme);

                int id_enzyme = rs.getInt("mySQLId");

                // Get proteins in the enzyme, this determines if the enzyme is a
                // protein or a complex
                String req = "SELECT p.id as PNetworkMysqlId,  p.db_identifier as proteinId, p.name as proteinName " +
                        "FROM Protein as p JOIN (ProteinInEnzyme as pie) ON (p.id=pie.id_protein) WHERE pie.id_enzyme= ? ;";

                prepSt = con.prepareStatement(req);
                prepSt.setInt(1, id_enzyme);

                ResultSet rsProt = prepSt.executeQuery();

                Set<BioProtein> prots = new HashSet<>();

                while (rsProt.next()) {

                    String proteinId = Globals.unformatId(rsProt.getString("proteinId"));
                    String proteinName = rsProt.getString("proteinName");

                    BioProtein protein;

                    if (network.containsProtein(proteinId)) {
                        protein = network.getProtein(proteinId);
                    } else {
                        protein = new BioProtein(proteinId, proteinName);

                        network.add(protein);
                    }

                    prots.add(protein);

                }

                for (BioProtein prot : prots) {
                    network.affectSubUnit(enzyme, 1.0, prot);
                }
            }
        }
    }

    /**
     * Gets all the Reactions of a given Network from the database, creates the
     * {@link BioReaction} and add them to the {@link BioNetwork}
     *
     * @param network    : the network representing the Network
     * @param id_network : the id of the network
     * @param con        : the database con
     * @throws Exception
     */
    public static void getReactions(BioNetwork network, int id_network, Connection con) throws Exception {
        String reqPrep = "SELECT RBS.id AS RBSMySQLid, RBS.reversible AS Rev, RBS.fast AS fast, RBS.hole AS hole, "
                + "RBS.kinetic_formula AS kine, "
                + "R.id AS RMySQLid, R.name AS name, R.generic AS generic, R.type AS type, R.ec AS EC, "
                + "R.db_identifier AS Rid_collection, R.go AS GO, R.sbo AS sbo, "
                + "GROUP_CONCAT( DISTINCT BINARY P.db_identifier SEPARATOR ' | ' ) AS Paths, "
                + "GROUP_CONCAT( DISTINCT BINARY M.id, '|', M.db_identifier, '|', IFNULL(Reactant.coeff,1.0), '|', Reactant.side SEPARATOR '),(' ) AS Reactants "
                + "FROM ReactionInNetwork RBS "
                + "LEFT OUTER JOIN ReactionInPathway RiP ON RiP.id_reaction_in_network=RBS.id "
                + "LEFT OUTER JOIN Pathway P ON RiP.id_pathway=P.id, "
                + "Reaction R "
                + "LEFT OUTER JOIN Reactant ON Reactant.id_reaction = R.id "
                + "LEFT OUTER JOIN Metabolite M ON Reactant.id_metabolite = M.id "
                + "WHERE RBS.id_reaction = R.id " + "AND RBS.id_network =? " + "GROUP BY R.id";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_network);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {

            String id = Globals.unformatId(rs.getString("Rid_collection"));

            if (!network.containsReaction(id)) {

                BioReaction reaction = new BioReaction(id, rs.getString("name"));

                network.add(reaction);

                getEntityRef(reaction, rs.getInt("RMySQLid"), con);

                reaction.setReversible(rs.getBoolean("Rev"));

                ReactionAttributes.setSpontaneous(reaction, rs.getBoolean("fast"));
                ReactionAttributes.setHole(reaction, rs.getBoolean("hole"));

                if (!isVoid(rs.getString("kine"))) {
                    ReactionAttributes.setKineticFormula(reaction, rs.getString("kine"));
                }

                ReactionAttributes.setGeneric(reaction, rs.getBoolean("generic"));

                ReactionAttributes.setType(reaction, rs.getString("type"));

                reaction.setEcNumber(rs.getString("EC"));

                if (rs.getString("sbo") != null && isValidSboTerm(rs.getString("sbo"))) {
                    ReactionAttributes.setSboTerm(reaction, rs.getString("sbo"));
                }

                ReactionQueries.getBounds(reaction, rs.getInt("RBSMySQLid"), network, con);

                if (rs.getString("Paths") != null) {
                    String[] pathways = rs.getString("Paths").split("( \\| )");
                    for (String pathwayId : pathways) {
                        pathwayId = Globals.unformatId(pathwayId);
                        BioPathway p = network.getPathway(pathwayId);
                        if (p != null) network.affectToPathway(network.getPathway(pathwayId), reaction);
                    }
                }
                // set the left and right participant.
                if (rs.getString("Reactants") != null) {
                    String[] lParts = rs.getString("Reactants").split("\\),\\(");

                    addReactantsFromSqlResult(lParts, network, reaction, id_network, con);
                }

                ReactionQueries.getReactionParameters(reaction, rs.getInt("RBSMySQLid"), network, con);
                ReactionQueries.getReactionComment(reaction, rs.getInt("RMySQLid"), con);
                ReactionQueries.getReactionBiblio(reaction, rs.getInt("RMySQLid"), con);
                ReactionQueries.getEnzymes(reaction, rs.getInt("RBSMySQLid"), network, con);
            }

        }

    }

    /**
     * @param parts
     * @param network
     * @param reaction
     * @param id_network
     * @param con
     * @throws Exception
     */
    private static void addReactantsFromSqlResult(String[] parts, BioNetwork network, BioReaction reaction, int id_network, Connection con) throws Exception {

        for (String part : parts) {
            String[] attributes = part.split("\\|");

            String side = attributes[3];
            Boolean rightSide = side.equals("RIGHT");

            Integer mysqlMetaboliteId;
            try {
                mysqlMetaboliteId = Integer.parseInt(attributes[0]);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                throw new Exception("MetExploreJava : error while getting mysql metabolite id");
            }

            String db_identifier = Globals.unformatId(attributes[1]);

            Double coeff = 1.0;

            try {
                coeff = Double.parseDouble(attributes[2]);
            } catch (NumberFormatException e) {
                System.err.println("[MetExplore-Java] WARNING : the coefficient " + attributes[2] + " for the metabolite " + db_identifier + " is not a number, it will be replaced by 1.0");
                coeff = 1.0;
            }

            if (coeff < 0) {
                System.err.println("[MetExplore-Java] WARNING : the coefficient " + attributes[2] + " for the metabolite " + db_identifier + " is negative, it will be replaced by the positive number");
                coeff = -coeff;
            }

            if (coeff == 0) {
                System.err.println("[MetExplore-Java] WARNING : the coefficient " + attributes[2] + " for the metabolite " + db_identifier + " is null, the reactant won't be added");
                coeff = -coeff;
            } else {

                String cptId = Globals.unformatId(MetaboliteQueries.getCompartmentIdentifier(mysqlMetaboliteId, id_network, con));

                BioCompartment cpt = null;

                if (cptId != null) {
                    cpt = network.getCompartment(cptId);
                }

                if (cpt == null) {
                    throw new Exception("Metabolite " + mysqlMetaboliteId + " without compartment");
                }

                BioMetabolite m = network.getMetabolite(db_identifier);
                if (m == null) {
                    throw new Exception("MetExploreJava : error while getting metabolite " + db_identifier);
                }

                if (rightSide) {
                    network.affectRight(reaction, coeff, cpt, network.getMetabolite(db_identifier));
                } else {
                    network.affectLeft(reaction, coeff, cpt, network.getMetabolite(db_identifier));
                }

            }
        }
    }

    /**
     * Add some bibliographic reference to a given Network
     *
     * @param databiblio
     * @param id_network
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult addBiblio(HashMap<String, ArrayList<String>> databiblio, int id_network, Connection con) throws SQLException {

        MetExploreIntegerArrayResult result = new MetExploreIntegerArrayResult(0);

        if (databiblio == null) {
            result.setIntError(1);
            return result;
        }

        int pmid = 0;

        if (databiblio.get("PMID") != null && !databiblio.get("PMID").isEmpty()) {
            pmid = Integer.parseInt(databiblio.get("PMID").get(0));
        }

        String journal = "";
        if (databiblio.get("FulljournalName") != null && !databiblio.get("FulljournalName").isEmpty()) {
            journal = databiblio.get("FulljournalName").get(0);
        }

        String year = "";
        if (databiblio.get("PubDate") != null && !databiblio.get("PubDate").isEmpty()) {

            year = databiblio.get("PubDate").get(0);

        }


        String title = "";

        if (databiblio.get("title") != null && !databiblio.get("title").isEmpty()) {
            title = databiblio.get("Title").get(0);
        }

        String authors = "";
        if (databiblio.get("authors") != null && !databiblio.get("authors").isEmpty()) {
            authors = StringUtils.join(databiblio.get("Author"), ", ");
        }

        String shortRef = "";

        if (databiblio.get("Author") != null) {
            if (databiblio.get("Author").size() > 1) {
                shortRef = databiblio.get("Author").get(0) + " et al., " + year;
            } else if (databiblio.get("Author").size() == 1) {
                shortRef = databiblio.get("Author").get(0) + ", " + year;
            }
        }

        String reqPrep = "INSERT INTO Biblio (pubmed_id, title, authors, journal, year, short_ref) " + "VALUES (?, ?, ?, ?, ?, ?);";

        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);

        prepSt.setInt(1, pmid);
        prepSt.setString(2, title);
        prepSt.setString(3, authors);
        prepSt.setString(4, journal);
        prepSt.setString(5, year);
        prepSt.setString(6, shortRef);
        prepSt.executeUpdate();

        ResultSet rs = prepSt.getGeneratedKeys();
        int newBiblioID = -1;

        if (rs.next()) {
            newBiblioID = rs.getInt(1);
        }

        if (newBiblioID == -1) {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "Error while adding ref to Bilbio to Network  " + id_network + ".";
            return result;
        }

        reqPrep = "INSERT INTO NetworkHasReference (id_network, id_biblio) VALUES (?,?);";
        prepSt = con.prepareStatement(reqPrep);

        prepSt.setInt(1, id_network);
        prepSt.setInt(2, newBiblioID);

        prepSt.executeUpdate();

        return result;

    }

    /**
     * Get id of the organism of a Network
     *
     * @param id_network
     * @param con
     * @return an integer
     * @throws SQLException
     */
    public static MetExploreIntegerResult getOrgId(int id_network, Connection con) throws SQLException {
        return getNetworkIntegerAttribute(id_network, "ncbi_id", con);
    }

    /**
     * Get id of the collection of a Network
     *
     * @param id_network
     * @param con
     * @return an integer
     * @throws SQLException
     */
    public static MetExploreIntegerResult getDbId(int id_network, Connection con) throws SQLException {
        return getNetworkIntegerAttribute(id_network, "id_collection", con);
    }

    /**
     * Get tissue of a Network
     *
     * @param id_network
     * @param con
     * @return an integer
     * @throws SQLException
     */
    public static MetExploreStringResult getNetworkTissue(int id_network, Connection con) throws SQLException {
        return getNetworkStringAttribute(id_network, "tissue", con);
    }

    /**
     * Get strain of a Network
     *
     * @param id_network
     * @param con
     * @return an integer
     * @throws SQLException
     */
    public static MetExploreStringResult getNetworkStrain(int id_network, Connection con) throws SQLException {
        return getNetworkStringAttribute(id_network, "strain", con);
    }

    /**
     * Get cell type of a Network
     *
     * @param id_network
     * @param con
     * @return an integer
     * @throws SQLException
     */
    public static MetExploreStringResult getNetworkCellType(int id_network, Connection con) throws SQLException {
        return getNetworkStringAttribute(id_network, "cell_type", con);
    }

    /**
     * Get description of a Network
     *
     * @param id_network
     * @param con
     * @return an integer
     * @throws SQLException
     */
    public static MetExploreStringResult getNetworkDescription(int id_network, Connection con) throws SQLException {
        return getNetworkStringAttribute(id_network, "description", con);
    }

    public static MetExploreStringResult getNetworkSource(int id_network, Connection con) throws SQLException {
        return getNetworkStringAttribute(id_network, "source", con);
    }

    /**
     * Remove a Network
     *
     * @param id_network the mysql id of the Network
     * @param con        a {@link Connection}
     * @throws SQLException a sql exception
     */
    public static void removeNetwork(int id_network, Connection con) throws SQLException {
        String req = "DELETE FROM Network WHERE id_network=?";

        PreparedStatement prepSt = con.prepareStatement(req);
        prepSt.setInt(1, id_network);

        ResultSet rs = prepSt.executeQuery();
    }

    /**
     * Creates a gene in the database without checking if it already exists. (Done
     * upstream)
     *
     * @param gene
     * @param id_network
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult createGene(BioGene gene, int id_network, Connection con)
            throws SQLException {

        MetExploreIntegerArrayResult result;

        String reqPrep = "insert into Gene (name, id_network, db_identifier, sbo) values(?, ?, ?, ?)";

        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
        prepSt.setString(1, gene.getName());

        prepSt.setInt(2, id_network);

        String id = Globals.formatId(gene.getId());

        prepSt.setString(3, id);
        prepSt.setString(4, GenericAttributes.getSboTerm(gene));
        prepSt.executeUpdate();

        int generatedId = -1;

        ResultSet rs = prepSt.getGeneratedKeys();

        if (rs.next()) {
            generatedId = rs.getInt(1);
        }

        if (generatedId == -1) {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "Error in the creation of the gene " + id + ".";

        } else {

            MetExploreIntegerArrayResult errRef = CollectionQueries.createExtDBrefs(gene, generatedId, con);
            if (errRef.getIntError() == 1) {
                result = new MetExploreIntegerArrayResult(1);
                result.message = errRef.message;
            } else {
                result = new MetExploreIntegerArrayResult(0);
                result.message = "Creation of the gene " + id;
                result.add(generatedId);
            }
        }

        return result;

    }

    /**
     * Creates a protein in the database without checking if it already exists.
     * (Done upstream)
     *
     * @param protein    : {@link BioProtein}
     * @param id_network
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult createProtein(BioProtein protein, int id_network, Connection con)
            throws SQLException {

        MetExploreIntegerArrayResult result;

        PreparedStatement prepSt;

        int generatedId = -1;

        String reqPrep = "INSERT INTO Protein (name, id_network, db_identifier, sbo) VALUES (?, ?, ?, ?)";

        prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
        prepSt.setString(1, protein.getName());
        prepSt.setInt(2, id_network);
        String id = Globals.formatId(protein.getId());
        prepSt.setString(3, id);
        prepSt.setString(4, GenericAttributes.getSboTerm(protein));
        prepSt.executeUpdate();

        ResultSet rs = prepSt.getGeneratedKeys();

        if (rs.next()) {
            generatedId = rs.getInt(1);
        }

        if (generatedId == -1) {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "Error in the creation of the protein " + id + ".";
        } else {
            MetExploreIntegerArrayResult errRef = CollectionQueries.createExtDBrefs(protein, generatedId, con);
            if (errRef.getIntError() == 1) {
                result = new MetExploreIntegerArrayResult(1);
                result.message = errRef.message;
            } else {
                result = new MetExploreIntegerArrayResult(0);
                result.message = "Creation of the protein " + id;
                result.add(generatedId);
            }
        }
        return result;
    }

    /**
     * Creates an enzyme in the database without checking if it already exists.
     * (Done upstream)
     *
     * @param enzyme     : {@link BioMetabolite}
     * @param id_network
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult createEnzyme(BioEnzyme enzyme, int id_network, Connection con)
            throws SQLException {

        MetExploreIntegerArrayResult result;

        int generatedId = -1;

        String reqPrep = "INSERT INTO Enzyme (name, id_network, db_identifier, sbo) VALUES (?, ?, ?, ?)";

        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
        String id = Globals.formatId(enzyme.getId());
        if (enzyme.getName() != null) {
            prepSt.setString(1, enzyme.getName());
        } else {
            prepSt.setString(1, id);
        }
        prepSt.setInt(2, id_network);
        prepSt.setString(3, id);
        prepSt.setString(4, GenericAttributes.getSboTerm(enzyme));
        prepSt.executeUpdate();

        ResultSet rs = prepSt.getGeneratedKeys();

        if (rs.next()) {
            generatedId = rs.getInt(1);
        }

        if (generatedId == -1) {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "Error in the creation of the enzyme " + id + ".";
        } else {
            result = new MetExploreIntegerArrayResult(0);
            result.message = "Creation of the enzyme " + id;
            result.add(generatedId);
        }
        return result;
    }

    /**
     * Creates a pathway in the database after checking if it already exists.
     *
     * @param pathway
     * @param id_network
     * @param con
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult createPathway(BioPathway pathway, int id_network, Connection con)
            throws SQLException {

        MetExploreIntegerArrayResult result;

        String reqPrep = "SELECT id  FROM Pathway WHERE BINARY db_identifier=? AND id_network=?;";

        PreparedStatement prepSt;

        prepSt = con.prepareStatement(reqPrep);
        String id = Globals.formatId(pathway.getId());
        prepSt.setString(1, id);
        prepSt.setInt(2, id_network);

        ResultSet rs = prepSt.executeQuery();

        int rowCount = 0;
        int generatedId = -1;

        while (rs.next()) {
            rowCount++;
            generatedId = rs.getInt(1);
        }

        if (rowCount == 0) {
            reqPrep = "insert into Pathway (name, id_network, db_identifier) values(?, ?, ?)";

            prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
            prepSt.setString(1, pathway.getName());
            prepSt.setInt(2, id_network);
            prepSt.setString(3, id);

            prepSt.executeUpdate();

            rs = prepSt.getGeneratedKeys();

            if (rs.next()) {
                generatedId = rs.getInt(1);
            }

            if (generatedId == -1) {
                result = new MetExploreIntegerArrayResult(1);
                result.message = "Error in the creation of the pathway " + id + ".";

            } else {
                result = new MetExploreIntegerArrayResult(0);
                result.message = "Creation of the pathway " + id;
                result.add(generatedId);

            }
        } else {

            result = new MetExploreIntegerArrayResult(0);
            result.message = "Pathway " + id + " already exist";
            result.add(generatedId);
        }
        return result;
    }


}
