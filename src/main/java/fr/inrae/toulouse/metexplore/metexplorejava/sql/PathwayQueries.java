/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.BioPathway;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.collection.BioCollection;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.Globals;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;

/**
 * @author lcottret
 */
public class PathwayQueries {


    /**
     * Update the name of a pathway Fills the history table
     * <p>
     * TODO : add the id_network in the history ?
     *
     * @param id_pathway : mysql id of the pathway
     * @param name      : new name of the pathway
     * @param oldName   : old name of the pathway
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updatePathwayName(int id_pathway, String name, String oldName, Connection con)
            throws SQLException {

        MetExploreResult err;

        String reqPrep = "UPDATE Pathway SET name=? WHERE id=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, name);
        prepSt.setInt(2, id_pathway);
        prepSt.executeUpdate();

        err = new MetExploreResult(0);

        err.message = "Update the name of the pathway " + id_pathway + " from " + oldName + " to " + name;

        return err;

    }

    /**
     * Insert a reaction in a batch of pathways
     *
     * @param id_reaction_in_network
     * @param pathways
     * @param id_network
     * @param connection
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult batchInsertReactionInPathways(int id_reaction_in_network,
                                                                             BioCollection<BioPathway> pathways, int id_network , Connection connection) throws SQLException {

        MetExploreIntegerArrayResult result;

        Object[] array = pathways.getIds().stream().map(Globals::formatId).distinct().toArray();

        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < array.length; i++) {
            sb.append("'" + array[i] + "'");
            if (i != array.length - 1) {
                sb.append(",");
            }
        }
        String inStatement = sb.toString();
        String sql = "SELECT id FROM Pathway WHERE id_network=? AND db_identifier IN (" + inStatement + ");";

        PreparedStatement prepSt = connection.prepareStatement(sql);
        prepSt.setInt(1, id_network);

        ResultSet rs;
        try {
            rs = prepSt.executeQuery();
        } catch (SQLException s) {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "Error while trying to retrieve the reaction's pathways MySQL identifiers. (id id_reaction_in_network="
                    + id_reaction_in_network + ")";

            return result;
        }

        sb = new StringBuilder();

        while (rs.next()) {

            sb.append("(" + id_reaction_in_network + ", " + rs.getInt("id") + "),");

        }

        if (sb.length() > 0) {
            sb.deleteCharAt(sb.length() - 1);

            String newValues = sb.toString();
            String sql2 = "INSERT IGNORE INTO ReactionInPathway (id_reaction_in_network, id_pathway) VALUES " + newValues + ";";

            PreparedStatement prepSt2 = connection.prepareStatement(sql2);
            int res = prepSt2.executeUpdate();

            if (res == 0) {
                result = new MetExploreIntegerArrayResult(1);
                result.message = "Error while trying to add the pathways to the reaction. (id id_reaction_in_network="
                        + id_reaction_in_network + ")";

                return result;
            }
        }

        result = new MetExploreIntegerArrayResult(0);
        return result;

    }

}
