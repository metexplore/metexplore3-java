package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang.WordUtils;

import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;

/**
 * @author lcottret
 */
public class ProjectQueries {

    private static final String[] actionPatterns = {
            // patternUpdate
            "^Update\\s(compartment|pathway|reaction|metabolite|enzyme|protein|gene)\\s(\\S+).*$",
            // patternAdd
            "^Add\\s(compartment|pathway|reaction|metabolite|enzyme|protein|gene)\\s(\\S+).*$",
            // patternRemove
            "^(Remove|Delete)\\s(compartment|pathway|reaction|metabolite|enzyme|protein|gene)\\s(\\S+).*$",
            // patternUpdateOn
            "^Update\\s(\\d+)\\s(compartment|pathway|reaction|metabolite|enzyme|protein|gene)s\\son\\s(compartment|pathway|reaction|metabolite|enzyme|protein|gene)\\s(.+)$",
            // patternAddIn
            "^Add\\s(\\d+)\\s(compartment|pathway|reaction|metabolite|enzyme|protein|gene)s\\sto\\s(compartment|pathway|reaction|metabolite|enzyme|protein|gene)\\s(.+)$",
            // patternRemoveFrom
            "^(Remove|Delete)\\s(\\d+)\\s(compartment|pathway|reaction|metabolite|enzyme|protein|gene)s\\sfrom\\s(compartment|pathway|reaction|metabolite|enzyme|protein|gene)\\s(.+)$"};

    private static final String[] entities = {"compartment", "pathway", "reaction", "metabolite", "enzyme", "protein",
            "genes"};

    /**
     * @param id_project : mysql id of the project
     * @param from      : String Date (format YYYY-MM-DD)
     * @param to        : StringDate (format YYYY-MM-DD)
     * @param html      : Boolean : if true, creates a html table
     * @param con       : the mysql connection
     * @return a html or a text table containing the list of actions done
     * between two dates in a project
     * @throws SQLException
     */
    public static MetExploreResult getHistory(int id_project, String from, String to, Boolean html, Connection con)
            throws SQLException {
        MetExploreIntegerArrayResult result = new MetExploreIntegerArrayResult(1);

        String reqPrep = "SELECT H.action, BS.name, U.login, H.date FROM History as H, Network as BS, NetworkInProject as BSP, UserMetExplore3Explore3 as U "
                + " WHERE BSP.id_project=? AND BSP.id_network=BS.id AND H.id_network=BS.id AND U.id=H.id_user AND H.date BETWEEN ? AND ? ORDER BY H.date DESC";

        PreparedStatement prepSt;
        prepSt = con.prepareStatement(reqPrep);

        prepSt.setInt(1, id_project);
        prepSt.setString(2, from);
        prepSt.setString(3, to);

        ResultSet rs = prepSt.executeQuery();

        String table = "";

        if (html) {
            table += "<h2>Detail history</h2><table border='1'><thead><tr><th>Network</th><th>Action</th><th>dateA</th><th>User</th></tr></thead>\n";
        }

        HashMap<String, HashMap<String, Integer>> actionNumber = new HashMap<>();

        while (rs.next()) {
            String action = rs.getString(1);

            String typeAction[] = getTypeOfAction(action);

            if (typeAction == null) {
                System.err.println("action " + action + " not analysed");
            } else {

                String entity = typeAction[0];
                String genericAction = typeAction[1];

                if (!actionNumber.containsKey(entity)) {
                    actionNumber.put(entity, new HashMap<>());
                }
                if (!actionNumber.get(entity).containsKey(genericAction)) {
                    actionNumber.get(entity).put(genericAction, 1);
                } else {
                    Integer newValue = actionNumber.get(entity).get(genericAction) + 1;
                    actionNumber.get(entity).put(genericAction, newValue);
                }
            }

            String NetworkName = rs.getString(2);
            String userName = rs.getString(3);
            String date = rs.getString(4);
            if (!html) {
                table += NetworkName + "\t" + action + "\t" + date + "\t" + userName + "\n";
            } else {
                table += "<tr><td>" + NetworkName + "</td><td>" + action + "</td><td>" + date + "</td><td>" + userName
                        + "</td></tr>\n";
            }
        }

        if (html) {
            table += "</table>\n";
        }

        String resume = "<h2>History by biological entity</h2>";

        for (int i = 0; i < entities.length; i++) {

            if (actionNumber.containsKey(entities[i])) {

                String title = WordUtils.capitalizeFully(entities[i]) + "s";

                if (html) {
                    resume += "<h3>" + title + "</h3>\n";
                    resume += "<table><thead><tr><th>Action</th><th>Number</th></tr></thead><tbody>\n";
                } else {
                    resume += title + "\n";
                }

                for (String genericAction : actionNumber.get(entities[i]).keySet()) {

                    Integer nb = actionNumber.get(entities[i]).get(genericAction);

                    if (html) {
                        resume += "<tr><td>" + genericAction + "</td><td>" + nb + "</td></tr>\n";
                    } else {
                        resume += genericAction + "\t" + nb + "\n";
                    }
                }

                if (html) {
                    resume += "</tbody></table><br />";
                }

            }
        }
        if (html) {
            resume += "<br /><br />";
        } else {
            resume += "\n\n";
        }

        String message = resume + table;

        result.setMessage(message);
        result.setIntError(0);

        return result;
    }

    /**
     * @param action
     * @return String[]
     */
    private static String[] getTypeOfAction(String action) {

        String[] type = {null, null};

        int iter = actionPatterns.length - 1;

        boolean found = false;

        Matcher match = null;

        // Parse from the end to the begin as some first patterns are in some
        // last patterns
        while (!found && iter >= 0) {
            Pattern pattern = Pattern.compile(actionPatterns[iter]);
            match = pattern.matcher(action);
            if (match.find()) {
                found = true;
            }
            iter--;
        }
        iter++; // Go back to the iter of the match if any

        if (found) {

            switch (iter) {
                case 0:
                    // The action is an update
                    type[0] = match.group(1);
                    type[1] = match.group(1) + "(s) updated.";
                    break;
                case 1:
                    // The action is an add
                    type[0] = match.group(1);
                    type[1] = match.group(1) + "(s) added.";
                    break;
                case 2:
                    // The action is a remove
                    type[0] = match.group(2);
                    type[1] = match.group(2) + "(s) removed.";
                    break;
                case 3:
                    type[0] = match.group(3);
                    // The action is an update of an object in an other object
                    type[1] = match.group(2) + "(s) updated in " + match.group(3) + "(s).";
                    break;
                case 4:
                    type[0] = match.group(3);
                    // The action is an add of an object in an other object
                    type[1] = match.group(2) + "(s) added in " + match.group(3) + "(s).";
                    break;
                case 5:
                    type[0] = match.group(4);
                    // The action is a remove of an object in an other object
                    type[1] = match.group(3) + "(s) removed from " + match.group(4) + "(s).";
                    break;

                default:
                    break;
            }
        }

        if (type[0] == null) {
            type = null;
        }

        return type;

    }

    /**
     * @param id_project : mysql id of the project
     * @param from      : String Date (format YYYY-MM-DD)
     * @param to        : StringDate (format YYYY-MM-DD)
     * @param html      : Boolean : if true, creates a html table
     * @param con       : the mysql connection
     * @return a html or a text table containing the number of actions done by
     * each member of a project
     * @throws SQLException
     */
    public static MetExploreResult getResumeHistoryByUser(int id_project, String from, String to, Boolean html,
                                                          Connection con) throws SQLException {

        MetExploreIntegerArrayResult result = new MetExploreIntegerArrayResult(1);

        String reqPrep = "SELECT U.name, COUNT(*) as c FROM History as H, Network as BS, NetworkInProject as BSP, UserMetExplore3 as U "
                + " WHERE BSP.id_project=? AND BSP.id_network=BS.id AND H.id_network=BS.id AND U.id=H.id_user AND H.date BETWEEN ? AND ? GROUP By H.id_user ORDER BY c DESC";

        PreparedStatement prepSt;
        prepSt = con.prepareStatement(reqPrep);

        prepSt.setInt(1, id_project);
        prepSt.setString(2, from);
        prepSt.setString(3, to);

        ResultSet rs = prepSt.executeQuery();

        String message = "";

        if (html) {
            message += "<table><thead><tr><th>User</th><th>Number of actions</th></tr></thead>\n";
        }

        int nbResults = 0;
        while (rs.next()) {
            nbResults++;
            String userName = rs.getString(1);
            int nb = rs.getInt(2);
            if (!html) {
                message += userName + "\t" + nb + "\n";
            } else {
                message += "<tr><td>" + userName + "</td><td>" + nb + "</td></tr>\n";
            }
        }

        if (nbResults == 0) {
            message = "";
            result.setMessage(message);
            result.setIntError(0);

            return result;
        }


        if (html) {
            message += "</table>\n";
        }

        result.setMessage(message);
        result.setIntError(0);

        return result;
    }

    /**
     * @param id_project
     * @param con
     * @return the name of the project
     * @throws SQLException
     */
    public static MetExploreResult getProjectName(int id_project, Connection con) throws SQLException {
        MetExploreIntegerArrayResult result = new MetExploreIntegerArrayResult(1);

        String reqPrep = "SELECT p.name FROM Project as p" + " WHERE p.id=?";

        PreparedStatement prepSt;
        prepSt = con.prepareStatement(reqPrep);

        prepSt.setInt(1, id_project);

        ResultSet rs = prepSt.executeQuery();

        if (rs.next()) {
            String projectName = rs.getString(1);

            result.setIntError(0);
            result.setMessage(projectName);

        }

        return result;
    }

    /**
     * @param id_project
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult getProjectMails(int id_project, Connection con) throws SQLException {
        MetExploreIntegerArrayResult result = new MetExploreIntegerArrayResult(1);

        String reqPrep = "SELECT U.email FROM UserMetExplore3 as U, UserInProject as uip "
                + " WHERE uip.id_project=? AND uip.id_user=U.id";


        PreparedStatement prepSt;
        prepSt = con.prepareStatement(reqPrep);

        prepSt.setInt(1, id_project);

        ResultSet rs = prepSt.executeQuery();

        String mails = "";

        int i = 0;

        while (rs.next()) {
            String mail = rs.getString(1);
            mails += ((i > 0) ? "," : "") + mail;
            i++;
        }

        result.setMessage(mails);
        result.setIntError(0);

        return result;
    }

}
