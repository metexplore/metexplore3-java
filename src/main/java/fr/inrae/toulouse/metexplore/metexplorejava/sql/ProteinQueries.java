/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreStringArrayResult;

/**
 * @author lcottret
 */
public class ProteinQueries {

    /**
     * Update the name of a protein Fills the history table
     * <p>
     * TODO : Add the id_network in the history
     *
     * @param id_protein    : mysql id of the protein
     * @param name         : new name of the protein
     * @param oldName      : old name of the protein
     * @param db_identifier : identifier of the protein
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateProteinName(int id_protein, String name, String oldName, String db_identifier,
                                                     Connection con) throws SQLException {

        MetExploreResult err;

        String reqPrep = "UPDATE Protein SET name=? WHERE id=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, name);
        prepSt.setInt(2, id_protein);
        prepSt.executeUpdate();

        err = new MetExploreResult(0);

        err.message = "Update the name of the protein " + db_identifier + " from " + oldName + " to " + name;

        return err;

    }

    /**
     * Link a protein and an enzyme in a Network
     *
     * @param id_protein : mysql id of the protein in Network
     * @param id_enzyme  : mysql id of the enzyme in Network
     * @param con                  : sql connection
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult linkProtein(int id_protein, int id_enzyme, Connection con) throws SQLException {

        MetExploreResult err;

        String req = "insert ignore into ProteinInEnzyme (id_enzyme, id_protein) values('"
                + id_enzyme + "', '" + id_protein + "')";

        Statement st;

        st = con.createStatement();
        st.executeUpdate(req);

        err = new MetExploreResult(0);
        err.message = "Link the protein " + id_protein + " to the enzyme "
                + id_enzyme;


        return err;

    }

    /**
     * Returns the value of the id in the ProteinInNetwork table from a
     * db_identifier and a id_network
     *
     * @param db_identifier : String : the db_identifier of the protein
     * @param id_network  : the mysql id of the Network
     * @param con          : {@link Connection}
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getid_proteinFromIdentifier(String db_identifier, int id_network, Connection con) throws SQLException {

        MetExploreIntegerArrayResult result;

        String reqPrep = "select p.id from Protein as p where BINARY p.db_identifier=? AND p.id_network=?";

        PreparedStatement prepSt;
        prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, db_identifier);
        prepSt.setInt(2, id_network);
        ResultSet rs = prepSt.executeQuery();

        int rowCount = 0;

        int id_gene = -1;

        while (rs.next()) {
            rowCount++;
            id_gene = rs.getInt(1);
        }

        if (rowCount == 0) {
            result = new MetExploreIntegerArrayResult(1);
            result.add(-1);
            result.message = "The protein " + db_identifier + " does not exist in the Network " + id_network;
            return result;
        }

        if (rowCount > 1) {
            result = new MetExploreIntegerArrayResult(1);
            result.add(-1);
            result.message = "There are several instances of the protein " + db_identifier + " in the Network "
                    + id_network;
            return result;
        }

        result = new MetExploreIntegerArrayResult(0);
        result.add(id_gene);
        result.message = "Get the mysql id of the Protein  " + db_identifier
                + " in the Network " + id_network;

        return result;

    }

    /**
     * returns an array of db_identifiers elements filtered by a list of integers
     * contained in another column
     *
     * @param list
     * @param columnRef
     * @param con
     * @return a {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getdb_identifiersFromIntegerArray(ArrayList<Integer> list,
                                                                               String columnRef, Connection con) throws SQLException {
        return GenericQueries.getStringColumnsFromIntegerArray(list, "Protein", "db_identifier", columnRef, con);
    }

    /**
     * returns an array of db_identifiers elements filtered by a list of sql ids
     *
     * @param list
     * @param con
     * @return a {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getdb_identifiersFromIdArray(ArrayList<Integer> list, Connection con)
            throws SQLException {
        return getdb_identifiersFromIntegerArray(list, "id", con);
    }

    /**
     * Get Db identifier of a protein by its sql id
     *
     * @param id_protein : sql id of the protein
     * @param con       : {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getdb_identifier(int id_protein, Connection con) throws SQLException {

        return GenericQueries.getStringColumnsFilteredByIntegerAttribute("Protein", "id", id_protein, "db_identifier",
                con);

    }

    /**
     * Get Name of a protein by its sql id
     *
     * @param id_protein : sql id of the protein
     * @param con       : {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getName(int id_protein, Connection con) throws SQLException {

        return GenericQueries.getStringColumnsFilteredByIntegerAttribute("Protein", "id", id_protein, "name", con);

    }

    /**
     * Get the gene identifiers of a protein in a Network
     *
     * @param id_protein   : sql id of the protein
     * @param id_network : sql id of the Network
     * @param con         : {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getGenedb_identifiers(int id_protein, int id_network, Connection con)
            throws SQLException {

        MetExploreStringArrayResult res = new MetExploreStringArrayResult(0);

        String reqPrep = "SELECT g.db_identifier FROM Gene as g, GeneCodesForProtein as gp, "
                + "Protein as p "
                + "WHERE g.id=gp.id_gene AND p.id=gp.id_protein "
                + "AND p.id=? AND p.id_network=? AND g.id_network=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_protein);
        prepSt.setInt(2, id_network);
        prepSt.setInt(3, id_network);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            if (!res.results.contains(rs.getString(1))) {
                res.add(rs.getString(1));
            }
        }

        return res;

    }

}
