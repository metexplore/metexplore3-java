/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.*;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.BioUnitDefinitionCollection;
import fr.inrae.toulouse.metexplore.metexplorejava.convert.Globals;
import org.apache.commons.lang.StringUtils;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.collection.BioCollection;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.AnnotatorComment;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.GenericAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.network.NetworkAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.reaction.Flux;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.reaction.ReactionAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.attributes.Notes;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.attributes.SbmlAnnotation;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.BioUnitDefinition;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreStringArrayResult;

/**
 * @author lcottret
 */
public class ReactionQueries {

    /**
     * Update the name of a reaction Fills the history table
     *
     * @param id_reaction : mysql id of the reaction
     * @param name       : new name of the reaction
     * @param oldName    : old name of the reaction
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateReactionName(int id_reaction, String name, String oldName, Connection con)
            throws SQLException {

        return updateReactionStringAttribute(id_reaction, "name", name, oldName, con);

    }

    /**
     * Update the name of a reaction Fills the history table
     *
     * @param id_reaction : mysql id of the reaction
     * @param name       : new name of the reaction
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateReactionName(int id_reaction, String name, Connection con) throws SQLException {

        return updateReactionStringAttribute(id_reaction, "name", name, con);

    }

    /**
     * Update the identifier of a reaction Fills the history table
     *
     * @param id_reaction : mysql id of the reaction
     * @param identifier : new identifier of the reaction
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateReactionIdentifier(int id_reaction, String identifier, Connection con)
            throws SQLException {

        return updateReactionStringAttribute(id_reaction, "db_identifier", identifier, con);

    }

    /**
     * Update the type of a reaction Fills the history table
     *
     * @param id_reaction : mysql id of the reaction
     * @param type       : new type of the reaction
     * @param oldType    : old type of the reaction
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateReactionType(int id_reaction, String type, String oldType, Connection con)
            throws SQLException {

        return updateReactionStringAttribute(id_reaction, "type", type, oldType, con);

    }

    /**
     * Update the generic attribute of a reaction Fills the history table
     *
     * @param id_reaction : mysql id of the reaction
     * @param generic    : new generic attribute of the reaction
     * @param oldGeneric : old generic attribute of the reaction
     * @param con
     * @return a {@link MetExploreResult}. err.getIntError() = 1 if sql problem or
     * if generic is different from 0 or 1
     * @throws SQLException
     */
    public static MetExploreResult updateReactionGeneric(int id_reaction, int generic, int oldGeneric, Connection con)
            throws SQLException {

        MetExploreResult err;

        if (generic != 0 && generic != 1) {
            err = new MetExploreResult(1);
            err.message = "The generic attribute must be equal to 0 or 1";
            return err;
        }

        err = updateReactionIntegerAttribute(id_reaction, "generic", generic, oldGeneric, con);

        return err;

    }

    /**
     * Update the generic attribute of a reaction Fills the history table
     *
     * @param id_reaction : mysql id of the reaction
     * @param generic    : new generic attribute of the reaction
     * @param con
     * @return a {@link MetExploreResult}. err.getIntError() = 1 if sql problem or
     * if generic is different from 0 or 1
     * @throws SQLException
     */
    public static MetExploreResult updateReactionGeneric(int id_reaction, int generic, Connection con)
            throws SQLException {

        MetExploreResult err;

        if (generic != 0 && generic != 1) {
            err = new MetExploreResult(1);
            err.message = "The generic attribute must be equal to 0 or 1";
            return err;
        }

        err = updateReactionIntegerAttribute(id_reaction, "generic", generic, con);

        return err;

    }

    /**
     * Update the EC number sof a reaction Fills the history table
     *
     * @param id_reaction : mysql id of the reaction
     * @param ec         : new ec of the reaction
     * @param oldEc      : old ec of the reaction
     * @param con
     * @return a {@link MetExploreResult}. err.getIntError() = 1 if sql problem
     * @throws SQLException
     */
    public static MetExploreResult updateReactionEc(int id_reaction, String ec, String oldEc, Connection con)
            throws SQLException {

        return updateReactionStringAttribute(id_reaction, "ec", ec, oldEc, con);

    }

    /**
     * Update the EC number sof a reaction Fills the history table
     *
     * @param id_reaction : mysql id of the reaction
     * @param ec         : new ec of the reaction
     * @param con
     * @return a {@link MetExploreResult}. err.getIntError() = 1 if sql problem
     * @throws SQLException
     */
    public static MetExploreResult updateReactionEc(int id_reaction, String ec, Connection con) throws SQLException {

        return updateReactionStringAttribute(id_reaction, "ec", ec, con);

    }

    /**
     * Update the hole attribute of a reaction Fills the history table
     *
     * @param id_reaction_in_network : mysql id of the reaction
     * @param hole                  : new hole attribute of the reaction
     * @param oldHole               : old hole of the reaction
     * @param con
     * @return a {@link MetExploreResult}. err.getIntError() = 1 if sql problem
     * @throws SQLException
     */
    public static MetExploreResult updateReactionInNetworkHole(int id_reaction_in_network, int hole, int oldHole,
                                                                 Connection con) throws SQLException {

        MetExploreResult err;

        if (hole != 0 && hole != 1) {
            err = new MetExploreResult(1);
            err.message = "The generic attribute must be equal to 0 or 1";
            return err;
        }

        err = updateReactionInNetworkIntegerAttribute(id_reaction_in_network, "hole", hole, oldHole, con);

        return err;

    }

    /**
     * Update the hole attribute of a reaction Fills the history table
     *
     * @param sourceId   : mysql id of the Network
     * @param id_reaction : mysql id of the reaction
     * @param hole       : integer (0 or 1)
     * @param con        : mysql connection
     * @return a {@link MetExploreResult}. err.getIntError() = 1 if sql problem
     * @throws SQLException
     */
    public static MetExploreResult updateReactionHole(int sourceId, int id_reaction, int hole, Connection con)
            throws SQLException {

        MetExploreResult err;

        if (hole != 0 && hole != 1) {
            err = new MetExploreResult(1);
            err.message = "The generic attribute must be equal to 0 or 1";
            return err;
        }

        String req = "select id, hole from ReactionInNetwork where id_network='" + sourceId + "' AND id_reaction='"
                + id_reaction + "'";

        Statement st;

        int rowCount = 0;

        st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        int id_reaction_in_network = -1;
        int oldHole = -1;

        while (rs.next()) {
            rowCount++;
            id_reaction_in_network = rs.getInt(1);
            oldHole = rs.getInt(2);
        }

        if (rowCount == 0) {
            err = new MetExploreResult(1);
            err.message = "No Reaction " + id_reaction + " in the Network " + sourceId;
        } else {
            err = updateReactionInNetworkIntegerAttribute(id_reaction_in_network, "hole", hole, oldHole, con);
        }

        return err;

    }

    /**
     * Update the flux bounds of a reaction in a Network Fills the history table
     *
     * @param id_reaction : mysql id of the reaction
     * @param sourceId   : mysql id of the Network
     * @param lower_bound
     * @param upper_bound : identifier of the reaction
     * @param con        : sql connection
     * @return a {@link MetExploreResult}. err.getIntError() = 1 if sql problem
     * @throws SQLException
     */
    public static MetExploreResult updateReactionBounds(int id_reaction, int sourceId, String lower_bound,
                                                        String upper_bound, Connection con) throws SQLException {

        MetExploreResult err;

        Statement st;

        String req = "SELECT lower_bound, upper_bound FROM ReactionInNetwork WHERE id_reaction='" + id_reaction
                + "' AND id_network='" + sourceId + "'";

        st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        String oldlower_bound = "";
        String oldUpperBound = "";

        while (rs.next()) {
            oldlower_bound = rs.getString(1);
            oldUpperBound = rs.getString(2);
        }

        if (lower_bound.equals("NA") || lower_bound.isEmpty()) {
            lower_bound = oldlower_bound;
        }

        if (upper_bound.equals("NA") || upper_bound.isEmpty()) {
            upper_bound = oldUpperBound;
        }

        req = "UPDATE ReactionInNetwork SET lower_bound='" + lower_bound + "', upper_bound='" + upper_bound
                + "' WHERE id_reaction='" + id_reaction + "' AND id_network='" + sourceId + "'";

        st = con.createStatement();
        st.executeUpdate(req);

        err = new MetExploreResult(0);

        err.message = "Update the lower and upper bounds of the reaction " + id_reaction + "from " + oldlower_bound
                + " to " + lower_bound + " and from " + oldUpperBound + " to " + upper_bound;

        return err;

    }

    /**
     * Update the lower bound of a reaction in a Network Fills the history table
     *
     * @param id_reaction : mysql id of the reaction
     * @param sourceId   : mysql id of the Network
     * @param lower_bound
     * @param con        : sql connection
     * @return a {@link MetExploreResult}. err.getIntError() = 1 if sql problem
     * @throws SQLException
     */
    public static MetExploreResult updateReactionlower_bound(int id_reaction, int sourceId, String lower_bound,
                                                            Connection con) throws SQLException {

        MetExploreResult err;

        err = updateReactionInNetworkStringAttribute(id_reaction, sourceId, "lower_bound", lower_bound, con);

        return err;
    }

    /**
     * Update the upper bound of a reaction in a Network Fills the history table
     *
     * @param id_reaction : mysql id of the reaction
     * @param sourceId   : mysql id of the Network
     * @param upper_bound
     * @param con        : sql connection
     * @return a {@link MetExploreResult}. err.getIntError() = 1 if sql problem
     * @throws SQLException
     */
    public static MetExploreResult updateReactionUpperBound(int id_reaction, int sourceId, String upper_bound,
                                                            Connection con) throws SQLException {

        MetExploreResult err;

        err = updateReactionInNetworkStringAttribute(id_reaction, sourceId, "upper_bound", upper_bound, con);

        return err;
    }

    /**
     * Update the reversibility of a reaction in a Network Fills the history table
     *
     * @param id_reaction : mysql id of the reaction
     * @param sourceId   : mysql id of the Network
     * @param rev
     * @param con        : sql connection
     * @return a {@link MetExploreResult}. err.getIntError() = 1 if sql problem or
     * if rev != 1 and rev!=0
     * @throws SQLException
     */
    public static MetExploreResult updateReactionReversibility(int id_reaction, int sourceId, int rev, Connection con)
            throws SQLException {

        MetExploreResult err;

        if (rev != 0 && rev != 1) {
            err = new MetExploreResult(1);
            err.message = "Reversibility attribute must be equal to 0 or 1";
            return err;
        }

        err = updateReactionInNetworkIntegerAttribute(id_reaction, sourceId, "reversible", rev, con);
        return err;

    }

    /**
     * Return the mysql id of a reaction from its identifier and its reference
     * database
     *
     * @param reactionId : Identifier of the metabolite
     * @param dbId       : mysql id of the reference database
     * @param con        : sql Connection
     * @return a {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getReactionByIdentifier(String reactionId, int dbId, Connection con)
            throws SQLException {

        MetExploreIntegerArrayResult result;

        String reqPrep = "select id from Reaction where BINARY db_identifier=? AND id_collection=?";

        PreparedStatement prepSt;

        prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, reactionId);
        prepSt.setInt(2, dbId);

        ResultSet rs = prepSt.executeQuery();

        int rowCount = 0;

        int id_reaction = -1;

        while (rs.next()) {
            rowCount++;
            id_reaction = rs.getInt(1);
        }

        if (rowCount == 0) {
            result = new MetExploreIntegerArrayResult(1);
            result.add(-1);
            result.message = "The reaction " + reactionId + " does not exist in the reference database " + dbId;
            return result;
        }

        if (rowCount > 1) {
            result = new MetExploreIntegerArrayResult(1);
            result.add(-1);
            result.message = "There are several instances of the reaction " + reactionId + " in the reference database "
                    + dbId;
            return result;
        }

        result = new MetExploreIntegerArrayResult(0);
        result.add(id_reaction);
        result.message = "Get the mysql id of the reaction " + reactionId + " in the reference database " + dbId;

        return result;

    }

    /**
     * Generic function to update a string attribute of a reaction
     * <p>
     * TODO : Add the id_network in the history
     *
     * @param id_reaction
     * @param attribute  : attribute in the Metabolite Table
     * @param value
     * @param oldValue
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    private static MetExploreResult updateReactionStringAttribute(int id_reaction, String attribute, String value,
                                                                  String oldValue, Connection con) throws SQLException {

        MetExploreResult err;

        String reqPrep = "UPDATE Reaction SET " + attribute + "=? WHERE id=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, value);
        prepSt.setInt(2, id_reaction);
        prepSt.executeUpdate();

        err = new MetExploreResult(0);

        err.message = "Update the attribute " + attribute + " of the reaction " + id_reaction + "from " + oldValue
                + " to " + value;

        return err;

    }

    /**
     * Generic function to update a string attribute of a reaction
     * <p>
     * TODO : Add the id_network in the history
     *
     * @param id_reaction
     * @param attribute  : attribute in the Metabolite Table
     * @param value
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateReactionStringAttribute(int id_reaction, String attribute, String value,
                                                                 Connection con) throws SQLException {

        MetExploreResult err;

        Statement st;

        String req = "SELECT " + attribute + " FROM Reaction WHERE id='" + id_reaction + "'";

        st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        String oldValue = "";

        while (rs.next()) {
            oldValue = rs.getString(1);
        }

        String reqPrep = "UPDATE Reaction SET " + attribute + "=? WHERE id=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, value);
        prepSt.setInt(2, id_reaction);
        prepSt.executeUpdate();

        err = new MetExploreResult(0);

        err.message = "Update the attribute " + attribute + " of the reaction " + id_reaction + "from " + oldValue
                + " to " + value;

        return err;

    }

    /**
     * Generic function to update a integer attribute of a reaction
     * <p>
     * TODO : Add the id_network in the history
     *
     * @param id_reaction
     * @param attribute  : attribute in the Metabolite Table
     * @param value
     * @param oldValue
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    private static MetExploreResult updateReactionIntegerAttribute(int id_reaction, String attribute, int value,
                                                                   int oldValue, Connection con) throws SQLException {

        MetExploreResult err;

        Statement st;

        String req = "UPDATE Reaction SET " + attribute + "='" + value + "' WHERE id='" + id_reaction + "'";

        st = con.createStatement();
        st.executeUpdate(req);

        err = new MetExploreResult(0);

        err.message = "Update the attribute " + attribute + " of the reaction " + id_reaction + "from " + oldValue
                + " to " + value;

        return err;

    }

    /**
     * Generic function to update a integer attribute of a reaction Get the old
     * value
     *
     * @param id_reaction
     * @param attribute
     * @param value
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    private static MetExploreResult updateReactionIntegerAttribute(int id_reaction, String attribute, int value,
                                                                   Connection con) throws SQLException {

        MetExploreResult err;

        String reqPrep = "SELECT " + attribute + " FROM Reaction WHERE id=?;";

        PreparedStatement prepSt;

        prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_reaction);

        ResultSet rs = prepSt.executeQuery();

        int rowCount = 0;

        int oldValue = 0;

        while (rs.next()) {
            rowCount++;
            oldValue = rs.getInt(1);
        }

        if (rowCount == 0) {
            err = new MetExploreResult(1);
            err.message = "No Reaction " + id_reaction;
        } else {
            err = updateReactionIntegerAttribute(id_reaction, attribute, value, oldValue, con);
        }

        return err;

    }

    /**
     * Generic function to update a integer attribute of a reactionInNetwork
     * <p>
     * TODO : Add the id_network in the history
     *
     * @param id_reaction_in_network
     * @param attribute             : attribute in the Metabolite Table
     * @param value
     * @param oldValue
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    private static MetExploreResult updateReactionInNetworkIntegerAttribute(int id_reaction_in_network,
                                                                              String attribute, int value, int oldValue, Connection con) throws SQLException {

        MetExploreResult err;

        Statement st;

        String req = "UPDATE ReactionInNetwork SET " + attribute + "='" + value + "' WHERE id='"
                + id_reaction_in_network + "'";

        st = con.createStatement();
        st.executeUpdate(req);

        err = new MetExploreResult(0);

        err.message = "Update the attribute " + attribute + " of the ReactionInNetwork " + id_reaction_in_network
                + "from " + oldValue + " to " + value;

        return err;

    }

    /**
     * Generic function to update a integer attribute of a reactionInNetwork
     * <p>
     * TODO : Add the id_network in the history
     *
     * @param id_reaction
     * @param sourceId
     * @param attribute  : attribute in the Metabolite Table
     * @param value
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateReactionInNetworkIntegerAttribute(int id_reaction, int sourceId,
                                                                             String attribute, int value, Connection con) throws SQLException {

        MetExploreResult err;

        Statement st;

        String req = "SELECT " + attribute + " FROM ReactionInNetwork WHERE id_reaction='" + id_reaction
                + "' AND id_network='" + sourceId + "'";

        st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        int oldValue = -1;

        while (rs.next()) {
            oldValue = rs.getInt(1);
        }

        req = "UPDATE ReactionInNetwork SET " + attribute + "='" + value + "' WHERE id_reaction='" + id_reaction
                + "' AND id_network='" + sourceId + "'";

        st = con.createStatement();
        st.executeUpdate(req);

        err = new MetExploreResult(0);

        err.message = "Update the attribute " + attribute + " of the reaction " + id_reaction + "from " + oldValue
                + " to " + value;

        return err;

    }

    /**
     * Generic function to update a String attribute of a reactionInNetwork
     * <p>
     * TODO : Add the id_network in the history
     *
     * @param id_reaction
     * @param sourceId
     * @param attribute  : attribute in the Metabolite Table
     * @param value
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateReactionInNetworkStringAttribute(int id_reaction, int sourceId,
                                                                            String attribute, String value, Connection con) throws SQLException {

        MetExploreResult err;

        Statement st;

        String req = "SELECT " + attribute + " FROM ReactionInNetwork WHERE id_reaction='" + id_reaction
                + "' AND id_network='" + sourceId + "'";

        st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        String oldValue = "";

        while (rs.next()) {
            oldValue = rs.getString(1);
        }

        String reqPrep = "UPDATE ReactionInNetwork SET " + attribute + "=? WHERE id=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, value);
        prepSt.setInt(2, id_reaction);
        prepSt.executeUpdate();

        err = new MetExploreResult(0);

        err.message = "Update the attribute " + attribute + " of the reaction " + id_reaction + "from " + oldValue
                + " to " + value;

        return err;

    }

    /**
     * Update the status of a reaction
     *
     * @param id_reaction : mysql id of the reaction
     * @param id_status
     * @param con        : Mysql connection
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult updateReactionStatus(int id_reaction, int id_status, Connection con)
            throws SQLException {

        MetExploreResult err;

        String reqPrep = "INSERT INTO ReactionHasStatus (id_reaction, id_status) VALUES (?, ?)";

        PreparedStatement prepSt;

        prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_reaction);
        prepSt.setInt(2, id_status);
        prepSt.executeUpdate();

        err = new MetExploreResult(0);
        err.message = "Set the status of the reaction " + id_reaction + " to " + id_status;

        return err;
    }

    /**
     * Gets the status of a reaction given a reference status
     *
     * @param id_reaction
     * @param statusRef
     * @param con
     * @return a {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getReactionStatus(int id_reaction, String statusRef, Connection con)
            throws SQLException {

        MetExploreStringArrayResult result;

        String reqPrep = "SELECT s.status FROM Status as s JOIN (StatusType as st, ReactionHasStatus as rhs) ON (s.id_status_type=st.id AND rhs.id_status=s.id) WHERE rhs.id_reaction=? AND st.name=?";
        PreparedStatement prepSt;

        String status = "NA";

        prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_reaction);
        prepSt.setString(2, statusRef);
        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            // We get the last value
            status = rs.getString(1);
        }

        if (status.equalsIgnoreCase("NULL")) {
            status = "NA";
        }

        result = new MetExploreStringArrayResult(0);

        result.add(status);

        return result;

    }


    /**
     * Link a comment and a reaction
     *
     * @param id_reaction : mysql id of a reaction
     * @param id_comment  : mysql id of a comment
     * @param con        : Mysql Connection
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult linkCommentAndReaction(int id_reaction, int id_comment, Connection con)
            throws SQLException {

        MetExploreResult err;

        String reqPrep = "SELECT * FROM ReactionHasComment WHERE id_reaction=? AND id_comment=?";
        PreparedStatement prepSt;

        boolean flag = false;

        prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_reaction);
        prepSt.setInt(2, id_comment);
        prepSt.executeQuery();

        ResultSet rs = prepSt.executeQuery();

        if (rs.next()) {
            flag = true;
        }

        if (!flag) {
            reqPrep = "INSERT INTO ReactionHasComment (id_reaction, id_comment) VALUES (?, ?)";

            prepSt = con.prepareStatement(reqPrep);
            prepSt.setInt(1, id_reaction);
            prepSt.setInt(2, id_comment);
            prepSt.executeUpdate();

        }

        err = new MetExploreResult(0);
        err.message = "Add a comment for the reaction " + id_reaction;

        return err;

    }

    /**
     * Link biblio and reaction
     *
     * @param id_reaction
     * @param id_biblio
     * @param con
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult linkBiblioAndReaction(int id_reaction, int id_biblio, Connection con)
            throws SQLException {

        MetExploreResult err;

        String reqPrep = "SELECT * FROM ReactionHasReference WHERE id_reaction=? AND id_biblio=?";

        PreparedStatement prepSt;

        int rowCount = 0;

        prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_reaction);
        prepSt.setInt(2, id_biblio);
        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            rowCount++;
        }

        if (rowCount == 0) {
            reqPrep = "INSERT INTO ReactionHasReference (id_reaction, id_biblio) VALUES (?, ?)";
            prepSt = con.prepareStatement(reqPrep);
            prepSt.setInt(1, id_reaction);
            prepSt.setInt(2, id_biblio);
            prepSt.executeUpdate();

        }

        err = new MetExploreResult(0);
        err.message = "Link reaction " + id_reaction + " and biblio " + id_biblio;

        return err;

    }

    /**
     * returns an array of db_identifiers elements filtered by a list of integers
     * contained in another column
     *
     * @param list      : a {@link ArrayList} containing the integer values
     * @param columnRef : the name of the string column to get
     * @param con       : {@link Connection}
     * @return a {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getdb_identifiersFromIntegerArray(ArrayList<Integer> list,
                                                                               String columnRef, Connection con) throws SQLException {
        return GenericQueries.getStringColumnsFromIntegerArray(list, "Reaction", "db_identifier", columnRef, con);
    }

    /**
     * returns an array of db_identifiers elements filtered by a list of sql ids
     *
     * @param list
     * @param con
     * @return a {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getdb_identifiersFromIdArray(ArrayList<Integer> list, Connection con)
            throws SQLException {
        return getdb_identifiersFromIntegerArray(list, "id", con);
    }

    /**
     * Get the Db identifiers of the left and right metabolites marked as "cofactor"
     * (side=1)
     *
     * @param id_reaction
     * @param con
     * @return a {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getSidedb_identifiers(int id_reaction, Connection con) throws SQLException {

        MetExploreStringArrayResult res = new MetExploreStringArrayResult(0);

        String reqPrep = "SELECT m.db_identifier FROM Metabolite as m, Reactant as r WHERE m.id=r.id_metabolite AND "
                + "r.id_reaction=? AND r.cofactor=1";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_reaction);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            if (!res.results.contains(rs.getString(1))) {
                res.add(rs.getString(1));
            }
        }

        return res;

    }


    /**
     * Get db_identifiers of the left metabolites of a reaction
     *
     * @param id_reaction : sql id of the reaction
     * @param con        : {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getLeftdb_identifiers(int id_reaction, Connection con) throws SQLException {

        MetExploreStringArrayResult res = new MetExploreStringArrayResult(0);

        String reqPrep = "SELECT m.db_identifier FROM Metabolite as m, Reactant as lp WHERE m.id=lp.id_metabolite AND "
                + "lp.id_reaction=? AND lp.side='LEFT'";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_reaction);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            if (!res.results.contains(rs.getString(1))) {
                res.add(rs.getString(1));
            }
        }

        return res;
    }

    /**
     * Get db_identifiers of the right metabolites of a reaction
     *
     * @param id_reaction : sql id of the reaction
     * @param con        : {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getRightdb_identifiers(int id_reaction, Connection con)
            throws SQLException {

        MetExploreStringArrayResult res = new MetExploreStringArrayResult(0);

        String reqPrep = "SELECT m.db_identifier FROM Metabolite as m, Reactant as rp WHERE m.id=rp.id_metabolite AND "
                + "rp.id_reaction=? AND rp.side='RIGHT'";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_reaction);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            if (!res.results.contains(rs.getString(1))) {
                res.add(rs.getString(1));
            }
        }

        return res;
    }

    /**
     * Get Db identifier of a reaction by its sql id
     *
     * @param id_reaction : sql id of the reaction
     * @param con        : {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getdb_identifier(int id_reaction, Connection con) throws SQLException {

        return GenericQueries.getStringColumnsFilteredByIntegerAttribute("Reaction", "id", id_reaction, "db_identifier",
                con);

    }

    /**
     * Get Name of a reaction by its sql id
     *
     * @param id_reaction : sql id of the reaction
     * @param con        : {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getName(int id_reaction, Connection con) throws SQLException {

        return GenericQueries.getStringColumnsFilteredByIntegerAttribute("Reaction", "id", id_reaction, "name", con);

    }

    /**
     * Get Type of a reaction by its sql id
     *
     * @param id_reaction : sql id of the reaction
     * @param con        : {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getType(int id_reaction, Connection con) throws SQLException {

        return GenericQueries.getStringColumnsFilteredByIntegerAttribute("Reaction", "id", id_reaction, "type", con);

    }

    /**
     * Get EC of a reaction by its sql id
     *
     * @param id_reaction : sql id of the reaction
     * @param con        : {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getEc(int id_reaction, Connection con) throws SQLException {

        return GenericQueries.getStringColumnsFilteredByIntegerAttribute("Reaction", "id", id_reaction, "ec", con);

    }

    /**
     * Get generic flag of a reaction by its sql id
     *
     * @param id_reaction : sql id of the reaction
     * @param con        : {@link Connection}
     * @return {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getGeneric(int id_reaction, Connection con) throws SQLException {

        return GenericQueries.getIntegerColumnsFilteredByIntegerAttribute("Reaction", "id", id_reaction, "generic", con);

    }

    /**
     * Set the status (multiple value possible) of a given reaction
     *
     * @param id_reaction
     * @param scores
     * @param con
     * @return {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult setReactionStatus(int id_reaction, HashMap<String, Integer> scores, Connection con)
            throws SQLException {

        MetExploreResult err;

        String reqPrep = "INSERT INTO ReactionHasStatus (id_reaction, id_status) VALUES ";

        for (int i = 0; i < scores.size(); i++) {
            reqPrep += "(?,?), ";
        }
        reqPrep = (reqPrep.substring(0, reqPrep.length() - 2)) + ";";

        PreparedStatement prepSt;

        prepSt = con.prepareStatement(reqPrep);
        int i = 1;
        for (int score : scores.values()) {
            prepSt.setInt(i, id_reaction);
            prepSt.setInt(i + 1, score);
            i = i + 2;
        }
        prepSt.executeUpdate();

        err = new MetExploreResult(0);
        err.message = "Set the status of the reaction " + id_reaction;

        return err;
    }

    /**
     * Add a new flux parameter to a given reaction
     *
     * @param id_reaction
     * @param con
     * @return {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult addParameters(int id_reaction, Flux flux, Connection con) throws SQLException {
        MetExploreIntegerArrayResult res = new MetExploreIntegerArrayResult(0);

        String reqPrep = "INSERT INTO ParametersInReaction (id_reaction_in_network, name, unit, value) VALUES (?,?,?,?)";
        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);

        prepSt.setInt(1, id_reaction);
        prepSt.setString(2, flux.getId());
        if (flux.unitDefinition != null) {
            prepSt.setString(3, flux.unitDefinition.getId());
        } else {
            prepSt.setNull(4, java.sql.Types.VARCHAR);
        }
        prepSt.setString(4, flux.value.toString());

        prepSt.executeUpdate();
        ResultSet rs = prepSt.getGeneratedKeys();
        rs.next();

        if (rs.getInt(1) == -1) {
            res = new MetExploreIntegerArrayResult(1);
            res.message = "Error while creating the reaction's " + id_reaction + " parameters";
        } else {
            res.message = "Parameter added to the reaction " + id_reaction;

        }

        return res;
    }

    /**
     * Get the reaction's parameters from the reactionInNetwork id. Adds them to
     * the {@link BioReaction}
     *
     * @param reaction
     * @param RBSMySQLid
     * @param network
     * @param con
     * @throws SQLException
     */
    public static void getReactionParameters(BioReaction reaction, int RBSMySQLid, BioNetwork network, Connection con)
            throws SQLException {

        String reqPrep = "SELECT name, unit, value FROM ParametersInReaction WHERE id_reaction_in_network=?";
        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, RBSMySQLid);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {

            double value;
            try {
                value = Double.parseDouble(rs.getString("value"));
            } catch (NumberFormatException e) {
                value = reaction.isReversible() ? Flux.FLUXMIN : 0.0;
            }

            Flux param = new Flux(value);

            if (rs.getString("unit").equalsIgnoreCase("dimensionless")) {
                param.unitDefinition = new BioUnitDefinition("dimensionless", "dimensionless");
            } else if (rs.getString("unit") != null) {
                param.unitDefinition = NetworkAttributes.getUnitDefinitions(network)
                        .get(rs.getString("unit"));
            }

            reaction.getAttributes().put(rs.getString("name"), param);

        }

    }

    /**
     * Get the reaction's metadata from the reactionInNetwork id and add it to the
     * {@link BioReaction}
     *
     * @param reaction
     * @param RBSMySQLid
     * @param con
     * @throws SQLException
     */
    @Deprecated
    public static void getReactionmetadata(BioReaction reaction, int RBSMySQLid, Connection con) throws SQLException {

        String reqPrep = "SELECT RM.metadata AS meta FROM ReactionInNetworkMetadata RM WHERE RM.id_reaction_in_network=? ";
        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, RBSMySQLid);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            String metadata = rs.getString("meta");

            Matcher m;
            if (metadata.startsWith("<annotation>")) {
                m = Pattern.compile(".*rdf:about=\"#([^\"]+)\".*").matcher(metadata.replaceAll(">\\s+<", "><"));
                if (m.matches()) {
                    String value = m.group(1);
                    GenericAttributes.setAnnotation(reaction, new SbmlAnnotation(value, metadata));
                }
            } else if (metadata.startsWith("<notes>")) {
                GenericAttributes.setNotes(reaction, new Notes(metadata));
            }
        }

    }

    /**
     * Get the reaction's comment from the reaction id and add it to the
     * {@link BioReaction}
     *
     * @param reaction
     * @param RMySQLid
     * @param con
     * @throws SQLException
     */
    public static void getReactionComment(BioReaction reaction, int RMySQLid, Connection con) throws SQLException {

        String reqPrep = "SELECT C.id_user, C.text FROM ReactionHasComment RhC, Comment C "
                + "WHERE RhC.id_comment=C.id AND RhC.id_reaction=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, RMySQLid);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            AnnotatorComment com = new AnnotatorComment(rs.getString("text"), rs.getString("id_user"));

            ReactionAttributes.addAnnotatorComment(reaction, com);
        }
    }

    /**
     * Get the reaction's Biblio from the reaction id and add it to the
     * {@link BioReaction}
     *
     * @param reaction
     * @param RMySQLid
     * @param con
     * @throws SQLException
     */
    public static void getReactionBiblio(BioReaction reaction, int RMySQLid, Connection con) throws SQLException {

        String reqPrep = "SELECT B.pubmed_id FROM  Biblio B,  ReactionHasReference RhR "
                + "WHERE RhR.id_biblio=B.id AND RhR.id_reaction=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, RMySQLid);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {

            int pubmed;

            try {
                pubmed = Integer.parseInt(rs.getString("pubmed_id"));
                ReactionAttributes.addPmid(reaction, pubmed);
            } catch (NumberFormatException ignored) {
            }

        }
    }

    /**
     * Get the reaction's Enzyme from the reaction id and add them to the
     * {@link BioReaction}
     *
     * @param reaction
     * @param reactionMySqlId
     * @param network
     * @param con
     * @throws SQLException
     */
    public static void getEnzymes(BioReaction reaction, int reactionMySqlId, BioNetwork network, Connection con)
            throws SQLException {
        String reqPrep = "SELECT E.db_identifier AS enzID FROM Enzyme E "
                + "LEFT OUTER JOIN Catalyses C ON E.id=C.id_enzyme " + "WHERE C.id_reaction_in_network=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, reactionMySqlId);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            network.affectEnzyme(reaction, network.getEnzyme(Globals.unformatId(rs.getString("enzID"))));
        }

    }

    /**
     * Adds one (or more) bibliographic reference to the reaction
     *
     * @param databiblio
     * @param id_reaction
     * @param con
     * @return {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult addBiblio(HashMap<String, ArrayList<String>> databiblio, int id_reaction,
                                             Connection con) throws SQLException {

        MetExploreIntegerArrayResult result = new MetExploreIntegerArrayResult(0);

        int pmid = Integer.parseInt(databiblio.get("PMID").get(0));
        String journal = databiblio.get("FulljournalName").get(0);
        String year = databiblio.get("PubDate").get(0);
        String title = databiblio.get("Title").get(0);
        String authors = StringUtils.join(databiblio.get("Author"), ", ");

        String shortRef;

        if (databiblio.get("Author").size() > 1) {
            shortRef = databiblio.get("Author").get(0) + " et al., " + year;
        } else {
            shortRef = databiblio.get("Author").get(0) + ", " + year;
        }

        String reqPrep = "INSERT INTO Biblio (pubmed_id, title, authors, journal, year, short_ref) "
                + "VALUES (?, ?, ?, ?, ?, ?);";

        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);

        prepSt.setInt(1, pmid);
        prepSt.setString(2, title);
        prepSt.setString(3, authors);
        prepSt.setString(4, journal);
        prepSt.setString(5, year);
        prepSt.setString(6, shortRef);
        prepSt.executeUpdate();

        ResultSet rs = prepSt.getGeneratedKeys();
        int newBiblioID = -1;

        if (rs.next()) {
            newBiblioID = rs.getInt(1);
        }

        if (newBiblioID == -1) {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "Error while adding ref to Bilbio to Network  " + id_reaction + ".";
            return result;
        }

        reqPrep = "INSERT INTO ReactionHasReference (id_reaction, id_biblio) VALUES (?,?);";
        prepSt = con.prepareStatement(reqPrep);

        prepSt.setInt(1, id_reaction);
        prepSt.setInt(2, newBiblioID);

        prepSt.executeUpdate();

        return result;

    }

    /**
     * removes all status attached to the reaction
     *
     * @param id_reaction
     * @param con
     * @return {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult removeStatusFromReaction(int id_reaction, Connection con) throws SQLException {

        MetExploreResult err = new MetExploreResult(0);

        String reqPrep = "DELETE FROM`ReactionHasStatus` WHERE  `id_reaction`=?";
        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);

        prepSt.setInt(1, id_reaction);

        prepSt.executeUpdate();

        return err;
    }

    /**
     * Updates score and status
     *
     * @param id_reaction
     * @param scores
     * @param con
     * @return {@link MetExploreResult}
     * @throws SQLException
     * @see ReactionQueries#updateReactionStatus(int, int, Connection)
     */
    public static MetExploreResult updateReactionStatus(int id_reaction, HashMap<String, Integer> scores, Connection con)
            throws SQLException {

        MetExploreResult err = new MetExploreResult(0);
        String reqPrep;
        PreparedStatement prepSt;

        if (scores.containsKey("score")) {

            reqPrep = "DELETE FROM`ReactionHasStatus` WHERE  `id_reaction`=? AND `id_status` IN "
                    + "(SELECT `id` FROM `Status` WHERE `id_status_type`=1) ;";

            prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
            prepSt.setInt(1, id_reaction);

            prepSt.executeUpdate();
            err = updateReactionStatus(id_reaction, scores.get("score"), con);

        }

        if (scores.containsKey("Status")) {

            reqPrep = "DELETE FROM`ReactionHasStatus` WHERE  `id_reaction`=? AND `id_status` IN "
                    + "(SELECT `id` FROM `Status` WHERE `id_status_type`=2) ;";

            prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
            prepSt.setInt(1, id_reaction);

            prepSt.executeUpdate();

            err = updateReactionStatus(id_reaction, scores.get("Status"), con);

        }

        return err;
    }

    /**
     * Get the reaction's flux upper and lower bound along with the associated unit
     * definition
     *
     * @param reaction
     * @param reactionInBS
     * @param network
     * @param con
     * @throws SQLException
     */
    public static void getBounds(BioReaction reaction, int reactionInBS, BioNetwork network, Connection con)
            throws SQLException {

        String reqPrep = "SELECT RBS.upper_bound AS UBound, RBS.lower_bound AS LBound, UD.identifier AS idUD "
                + "FROM ReactionInNetwork RBS LEFT OUTER JOIN UnitDefinition UD ON RBS.unit_definition_bounds=UD.id "
                + "WHERE RBS.id=?;";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, reactionInBS);

        ResultSet rs = prepSt.executeQuery();

        if (rs.next()) {

            BioUnitDefinition UD = null;

            BioUnitDefinitionCollection unitDefinitions = NetworkAttributes.getUnitDefinitions(network);
            if (rs.getString("idUD") != null) {
                UD = unitDefinitions.get(rs.getString("idUD"));
            }


            if (UD == null) {
                // default unit definition
                UD = new BioUnitDefinition();
                if(! unitDefinitions.containsId(UD.getId()))
                    unitDefinitions.add(UD);
            }

            String lb = formatBound(rs.getString("LBound"), reaction);

            double lbValue;
            String fluxId;

            try {
                lbValue = Double.parseDouble(lb);
                fluxId = lb + "__" + UD.getId();
            } catch (NumberFormatException e) {
                lbValue = reaction.isReversible() ? Flux.FLUXMIN : 0.0;
                fluxId = "fluxMin";
            }

            if (lbValue < 0 && !reaction.isReversible()) {
                lbValue = 0.0;
            }
            ReactionAttributes.setLowerBound(reaction, new Flux(fluxId, lbValue, UD));

            String ub = formatBound(rs.getString("UBound"), reaction);
            Double ubValue;

            try {
                ubValue = Double.parseDouble(ub);
                fluxId = ub + "__" + UD.getId();
            } catch (NumberFormatException e) {
                ubValue = Flux.FLUXMAX;
                fluxId = "fluxMax";
            }
            ReactionAttributes.setUpperBound(reaction, new Flux(fluxId, ubValue, UD));
        }

    }

    /**
     * Format bound
     * <p>
     * If null or not numeric or not starting by -Inf ou inf -> set to "0.0"
     *
     * @param bound
     * @param reaction
     * @return
     */
    private static String formatBound(String bound, BioReaction reaction) {
        String out = bound;

        if (bound != null) {
            if (bound.toLowerCase().startsWith("-inf")) {
                out = reaction.isReversible() ? Flux.FLUXMIN.toString() : "0";
            } else if (bound.toLowerCase().startsWith("+inf") || bound.toLowerCase().startsWith("inf")) {
                out = Flux.FLUXMAX.toString();
            } else {
                try {
                    Double.parseDouble(bound);
                } catch (NumberFormatException e) {
                    out = "0.0";
                }
            }
        } else {
            out = "0.0";
        }

        return out;
    }

    /**
     * Batch insertion of metabolites as left participants
     *
     * @param participants
     * @param id_reaction
     * @param con
     * @return {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult batchAddLeft(BioCollection<BioReactant> participants,
                                                            BioReaction reaction, int id_reaction, HashMap<String, Integer> mapMetabolites, Connection con) throws SQLException {

        return batchAddReactant(participants, reaction, id_reaction, mapMetabolites, true, con);
    }

    public static MetExploreIntegerArrayResult batchAddReactant(BioCollection<BioReactant> participants,
                                                            BioReaction reaction, int id_reaction, HashMap<String, Integer> mapMetabolites, Boolean left, Connection con) throws SQLException {

        MetExploreIntegerArrayResult result;

        BioCollection<BioMetabolite> side_compounds = ReactionAttributes.getSideCompounds(reaction);

        for(BioReactant reactant : participants) {

            String db_identifier = Globals.formatId(reactant.getMetabolite().getId());
            int metaboliteMysqlId = mapMetabolites.get(db_identifier);

            int cofactorInt = 0;
            if (side_compounds != null && side_compounds.containsId(db_identifier)) {
                cofactorInt = 1;
            }

            String side = left ? "LEFT" : "RIGHT";

            String sql2 = "REPLACE INTO Reactant "
                    + "(id_metabolite, id_reaction, coeff, cofactor, side) " +
                    "VALUES (?,?,?,?,?);";

            PreparedStatement prepSt2 = con.prepareStatement(sql2);

            prepSt2.setInt(1, metaboliteMysqlId);
            prepSt2.setInt(2, id_reaction);
            prepSt2.setDouble(3, reactant.getQuantity());
            prepSt2.setInt(4, cofactorInt);
            prepSt2.setString(5, side);

            int res = prepSt2.executeUpdate();

            if (res == 0) {
                result = new MetExploreIntegerArrayResult(1);
                result.message = "Error while trying to add the reactants to the reaction. (id id_reaction=" + id_reaction
                        + ")";

                return result;
            }

        }

        result = new MetExploreIntegerArrayResult(0);

        return result;
    }


    /**
     * Batch insertion of metabolites as right participants
     *
     * @param participants
     * @param id_reaction
     * @param con
     * @return {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult batchAddRight(BioCollection<BioReactant> participants,
                                                             BioReaction reaction, int id_reaction,
                                                             HashMap<String, Integer> mapMetabolites, Connection con) throws SQLException {

        return batchAddReactant(participants, reaction, id_reaction, mapMetabolites, false, con);


    }

}
