/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;

/**
 * @author lcottret
 */
public class StatusTypeQueries {

    /**
     * Get status Type mysql id by giving its name
     *
     * @param statusName
     * @param con
     * @return {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult getStatusTypeIdByName(String statusName, Connection con)
            throws SQLException {

        MetExploreIntegerArrayResult result;

        String reqPrep = "select id from StatusType where BINARY name=?";

        PreparedStatement prepSt;

        int id_status_type = 0;

        int rowCount = 0;

        prepSt = con.prepareStatement(reqPrep);
        prepSt.setString(1, statusName);
        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            rowCount++;
            id_status_type = rs.getInt(1);
        }

        if (rowCount > 0) {
            result = new MetExploreIntegerArrayResult(0);
            result.message = "Get Status id by name " + statusName;
            result.add(id_status_type);
        } else {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "No status with name " + statusName;
        }
        return result;
    }

}
