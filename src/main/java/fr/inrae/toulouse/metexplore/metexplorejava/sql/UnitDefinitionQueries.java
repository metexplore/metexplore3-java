/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashSet;

import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.BioUnitDefinition;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.BioUnitDefinitionCollection;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.UnitSbml;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreStringArrayResult;

/**
 * @author lcottret
 */
public class UnitDefinitionQueries {

    /**
     * Create a unit definition in MetExplore and fills the history table
     *
     * @param id
     * @param name
     * @param con
     * @return {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult createUnitDefinition(String id,
                                                                    String name,
                                                                    int id_collection,
                                                                    Connection con) throws SQLException {

        MetExploreIntegerArrayResult result;

        int generatedId = -1;

        String reqPrep = "INSERT IGNORE INTO UnitDefinition (identifier, name, id_collection) VALUES (?, ?, ?)";

        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
        prepSt.setString(1, id);
        prepSt.setString(2, name);
        prepSt.setInt(3, id_collection);

        prepSt.executeUpdate();

        ResultSet rs = prepSt.getGeneratedKeys();

        if (rs.next()) {
            generatedId = rs.getInt(1);
        }

        if (generatedId == -1) {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "Error in the creation of the unit Definition "
                    + id + ".";
        } else {
            result = new MetExploreIntegerArrayResult(0);
            result.message = "The unit definition " + id
                    + " has been successfully created";
            result.add(generatedId);

        }

        return result;

    }

    /**
     * Add a unit definition in a Network
     *
     * @param idUd     : Mysql id of the unit definition
     * @param sourceId : Mysql id of the Network
     * @param con      : Mysql connection
     * @return {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult addUnitDefinition(int idUd, int sourceId,
                                                     Connection con) throws SQLException {

        MetExploreResult err;

        String req = "SELECT * FROM UnitDefinitionInNetwork WHERE id_network = '"
                + sourceId + "' AND id_unit_definition='" + idUd + "'";

        Statement st;

        st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        int rowCount = 0;

        while (rs.next()) {
            rowCount++;
        }

        if (rowCount == 0) {
            req = "INSERT INTO UnitDefinitionInNetwork (id_network, id_unit_definition) VALUES ('"
                    + sourceId + "', '" + idUd + "');";
            st.executeUpdate(req);

            err = new MetExploreResult(0);
            err.message = "Creation of the link between the unitDefinition "
                    + idUd + " and the Network " + sourceId;
        } else {
            err = new MetExploreResult(0);
            err.message = "The link between  the unitDefinition " + idUd
                    + " and the Network " + sourceId + " already exists";
        }

        return err;
    }

    /**
     * Returns a list of identifiers from a list of id_unit_definition Be carefil,
     * the resulting array don't have necessarily the same size than the
     * original one !!
     *
     * @param list {@link ArrayList}
     * @param con  {@link Connection}
     * @return a {@link MetExploreStringArrayResult}
     * @throws SQLException
     */
    public static MetExploreStringArrayResult getIdentifiersFromIdArray(
            ArrayList<Integer> list, Connection con) throws SQLException {
        return GenericQueries.getStringColumnsFromIntegerArray(list,
                "UnitDefinition", "identifier", "id", con);
    }

    /**
     * Retrieves all unit defintions associated to a Network
     *
     * @param id_network
     * @param con
     * @return HashMap(String, { @ link BioUnitDefinition })
     * @throws SQLException
     */
    public static BioUnitDefinitionCollection getUnitDefinitions(int id_network, Connection con)
            throws SQLException {

        BioUnitDefinitionCollection UnitDefSet = new BioUnitDefinitionCollection();

        String reqPrep = "SELECT ud.id AS mySQLid, ud.identifier as udId, ud.name as udName "
                + "FROM UnitDefinition as ud JOIN UnitDefinitionInNetwork as udbs "
                + "ON udbs.id_unit_definition=ud.id "
                + "WHERE udbs.id_network=?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, id_network);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            int id = rs.getInt("mySQLid");
            String udId = rs.getString("udId");
            String udName = rs.getString("udName");

            BioUnitDefinition ud = new BioUnitDefinition(udId, udName);

            HashSet<UnitSbml> units = UnitQueries.getUnitsFromUD(id, con);
            for (UnitSbml unit : units) {
                ud.addUnit(unit);
            }
            UnitDefSet.add(ud);
        }

        return UnitDefSet;
    }

}
