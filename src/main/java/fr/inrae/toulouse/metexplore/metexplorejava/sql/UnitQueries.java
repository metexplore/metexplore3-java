/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.UnitSbml;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreIntegerArrayResult;
import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;

/**
 * @author lcottret
 */
public class UnitQueries {


    /**
     * Create the link between a unit and a unit definition Fills the history
     * table
     *
     * @param id_unit   : Mysql id of the unit
     * @param idUd     : Mysql id of the unit definition
     * @param nameUnit
     * @param nameUd
     * @param con      : Mysql connection
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult addUnit(int id_unit, int idUd,
                                           String nameUnit, String nameUd, Connection con) throws SQLException {

        MetExploreResult err;

        String req = "SELECT * FROM ListOfUnits WHERE id_unit_definition='"
                + idUd + "' AND id_unit='" + id_unit + "'";

        Statement st;

        st = con.createStatement();
        ResultSet rs = st.executeQuery(req);

        int rowCount = 0;

        while (rs.next()) {
            rowCount++;
        }

        if (rowCount == 0) {
            req = "INSERT INTO ListOfUnits (id_unit_definition, id_unit) VALUES ('"
                    + idUd + "', '" + id_unit + "')";
            st.executeUpdate(req);

            err = new MetExploreResult(0);
            err.message = "Creation of the link between the unit " + nameUnit
                    + " and the unitDefinition " + nameUd;
        } else {
            err = new MetExploreResult(0);
            err.message = "The link between the unit " + nameUnit
                    + " and the unitDefinition " + nameUd + " already exist";
        }

        return err;

    }

    /**
     * Remove the links between a UnitDefinition and all the Units
     *
     * @param id_unit_definition : mysql id of the unitDefinition
     * @param con              : JDBC Connection
     * @return a {@link MetExploreResult}
     * @throws SQLException
     */
    public static MetExploreResult removeUnitsFromUnitDefinition(
            int id_unit_definition, Connection con) throws SQLException {

        return GenericQueries.removeRecordsByIntegerAttribute("ListOfUnits",
                "id_unit_definition", id_unit_definition, con);

    }

    /**
     * Create a new unit
     *
     * @param kind
     * @param scale
     * @param exponent
     * @param multiplier
     * @param con
     * @return {@link MetExploreIntegerArrayResult}
     * @throws SQLException
     */
    public static MetExploreIntegerArrayResult createUnit(String kind, int scale, double exponent, double multiplier, Connection con)
            throws SQLException {

        MetExploreIntegerArrayResult result;

        String reqPrep = "INSERT INTO Unit (kind, scale, exponent,multiplier) VALUES (?, ?, ?, ?)";
        PreparedStatement prepSt = con.prepareStatement(reqPrep, Statement.RETURN_GENERATED_KEYS);
        prepSt.setString(1, kind);
        prepSt.setInt(2, scale);
        prepSt.setDouble(3, exponent);
        prepSt.setDouble(4, multiplier);

        prepSt.executeUpdate();

        ResultSet rs = prepSt.getGeneratedKeys();
        int generatedId = -1;

        if (rs.next()) {
            generatedId = rs.getInt(1);
        }

        if (generatedId == -1) {
            result = new MetExploreIntegerArrayResult(1);
            result.message = "Error in the creation of the unit " + kind
                    + ".";
        } else {
            result = new MetExploreIntegerArrayResult(0);
            result.message = "The unit " + kind
                    + " has been successfully created";
            result.add(generatedId);
        }


        return result;
    }

    /**
     * Get all units associated to a Unit definition
     *
     * @param mySQLid
     * @param con
     * @return HashSet({ @ link UnitSbml }
     * @throws SQLException
     */
    public static HashSet<UnitSbml> getUnitsFromUD(int mySQLid, Connection con)
            throws SQLException {

        HashSet<UnitSbml> units = new HashSet<>();

        String reqPrep = "SELECT `kind` , `exponent` , `scale` , `multiplier` "
                + "FROM ListOfUnits LoU "
                + "LEFT OUTER JOIN Unit ON LoU.id_unit = Unit.id "
                + "WHERE LoU.id_unit_definition =?";

        PreparedStatement prepSt = con.prepareStatement(reqPrep);
        prepSt.setInt(1, mySQLid);

        ResultSet rs = prepSt.executeQuery();

        while (rs.next()) {
            String kind = rs.getString("kind");
            double exponent;
            try {
                exponent = Double.parseDouble(rs.getString("exponent"));
            } catch (NumberFormatException e) {
                exponent = 1.0;
            }

            int scale;
            try {
                scale = Integer.parseInt(rs.getString("scale"));
            } catch (NumberFormatException e) {
                scale = 1;
            }

            double multiplier;
            try {
                multiplier = Double.parseDouble(rs.getString("multiplier"));
            } catch (NumberFormatException e) {
                multiplier = 1.0;
            }

            UnitSbml unit = new UnitSbml(kind, exponent, scale, multiplier);

            units.add(unit);
        }

        return units;
    }

}
