package fr.inrae.toulouse.metexplore.metexplorejava.utils;

/**
 * @author lcottret
 */
public class GenericUtils {

    /**
     * prints a json with the error message
     *
     * @param message : the error message
     */
    public static void MetExploreError(String message) {

        System.out
                .println("{\"success\":\"false\", \"message\":\"[MetExplore-JAVA error] "
                        + message + "\"}");

    }

}
