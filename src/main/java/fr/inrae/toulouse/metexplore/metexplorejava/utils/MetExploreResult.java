/*
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 */
package fr.inrae.toulouse.metexplore.metexplorejava.utils;

import java.util.ArrayList;

/**
 * Class to handle the results of the operations in the MetExplore database
 *
 * @author ludo
 * 2 mai 2011
 */
public class MetExploreResult {

    public String message;
    public ArrayList<String> warnings;
    private int intError;

    public MetExploreResult(int intErrNo) {
        intError = intErrNo;
        message = "";
        warnings = new ArrayList<>();
    }

    public MetExploreResult(String message) {
        this.message = message;
        warnings = new ArrayList<>();
    }

    public MetExploreResult() {
        intError = 0;
        message = "";
        warnings = new ArrayList<>();
    }

    @Override
    public String toString() {
        return "MetExploreResult[" + intError + "] " + message;
    }

    /**
     * Add a warning in the list of warnings
     *
     * @param warning
     */
    public void addWarning(String warning) {

        warnings.add(warning);

    }

    public int getIntError() {
        return intError;
    }

    public void setIntError(int intError) {
        this.intError = intError;
    }

    /**
     * @return the message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message the message to set
     */
    public void setMessage(String message) {
        this.message = message;
    }


}
