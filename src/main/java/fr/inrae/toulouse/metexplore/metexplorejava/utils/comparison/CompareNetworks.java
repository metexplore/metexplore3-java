package fr.inrae.toulouse.metexplore.metexplorejava.utils.comparison;

import fr.inrae.toulouse.metexplore.met4j_core.biodata.*;
import fr.inrae.toulouse.metexplore.met4j_core.biodata.collection.BioCollection;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.metabolite.MetaboliteAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.network.NetworkAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.annotations.reaction.ReactionAttributes;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.BioUnitDefinition;
import fr.inrae.toulouse.metexplore.met4j_io.jsbml.units.UnitSbml;

import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author lcottret 3 déc. 2013
 */
public class CompareNetworks {

    BioNetwork networkRef;
    BioNetwork networkTest;

    public CompareNetworks(BioNetwork networkRef, BioNetwork networkTest) {

        this.networkRef = networkRef;
        this.networkTest = networkTest;

    }

    public void testAll() {

        this.testNumberUnitDefinitions();
        this.testNumberCompartments();
        this.testNumberReactions();
        this.testNumberMetabolites();
        this.testNumberGenes();
        this.testNumberPathways();
        this.testNumberProteins();
        this.testNumberEnzymes();
        this.testAttributesUnitDefinitions();
        this.testAttributesMetabolites();
        this.testAttributesReactions();
        this.testAttributesGenes();
        this.testAttributesProteins();
        this.testAttributeEnzymes();

    }

    public void testNumberUnitDefinitions() {
        int nUnitDefinitionsRef = NetworkAttributes.getUnitDefinitions(networkRef).size();
        int nUnitDefinitionsTest = NetworkAttributes.getUnitDefinitions(networkTest).size();

        assertEquals("Test number of unit definitions", nUnitDefinitionsRef, nUnitDefinitionsTest, 0);
    }

    public void testNumberCompartments() {
        int nCompartmentsRef = networkRef.getCompartmentsView().size();
        int nCompartmentsTest = networkTest.getCompartmentsView().size();
        assertEquals("Test number of compartments", nCompartmentsRef, nCompartmentsTest, 0);

    }

    public void testNumberReactions() {
        int nReactionsRef = networkRef.getReactionsView().size();
        int nReactionsTest = networkTest.getReactionsView().size();
        assertEquals("Test number of reactions", nReactionsRef, nReactionsTest, 0);

    }

    public void testNumberMetabolites() {
        int nMetabolitesRef = networkRef.getMetabolitesView().size();
        int nMetabolitesTest = networkTest.getMetabolitesView().size();
        assertEquals("Test number of metabolites", nMetabolitesRef, nMetabolitesTest, 0);

    }

    public void testNumberGenes() {
        int nGenesRef = networkRef.getGenesView().size();
        int nGenesTest = networkTest.getGenesView().size();
        assertEquals("Test number of genes", nGenesRef, nGenesTest, 0);

    }

    public void testNumberPathways() {
        int nPathwaysRef = networkRef.getPathwaysView().size();
        int nPathwaysTest = networkTest.getPathwaysView().size();
        assertEquals("Test number of pathways", nPathwaysRef, nPathwaysTest, 0);

    }

    public void testNumberProteins() {
        int nProteinsRef = networkRef.getProteinsView().size();
        int nProteinsTest = networkTest.getProteinsView().size();
        assertEquals("Test number of proteins", nProteinsRef, nProteinsTest, 0);

    }

    public void testNumberEnzymes() {
        int nRef = networkRef.getEnzymesView().size();
        int n = networkTest.getEnzymesView().size();
        assertEquals("Test number of enzymes", nRef, n, 0);

    }

    public void testAttributesUnitDefinitions() {

        Set<String> unitsRef = NetworkAttributes.getUnitDefinitions(networkRef).getIds();
        Set<String> unitsTest = NetworkAttributes.getUnitDefinitions(networkTest).getIds();
        assertEquals("Test ids of unitDefinitions", unitsRef, unitsTest);

        for (String unitId : unitsRef) {
            BioUnitDefinition unitRef = NetworkAttributes.getUnitDefinitions(networkRef).get(unitId);
            BioUnitDefinition unitTest = NetworkAttributes.getUnitDefinitions(networkTest).get(unitId);

            String unitNameRef = unitRef.getName();
            String unitNameTest = unitTest.getName();

            assertEquals("Test name of the unitDefinition " + unitId, unitNameRef, unitNameTest);

            Set<String> unitsSbmlRef = unitRef.getUnits().keySet();
            Set<String> unitsSbmlTest = unitTest.getUnits().keySet();

            assertEquals("Test unitSbmls of unitDefinition " + unitId, unitsSbmlRef, unitsSbmlTest);

            for (String unitSbmlId : unitsSbmlRef) {
                UnitSbml unitSbmlRef = unitRef.getUnits().get(unitSbmlId);
                UnitSbml unitSbmlTest = unitTest.getUnits().get(unitSbmlId);

                String unitSbmlKindRef = unitSbmlRef.getKind();
                String unitSbmlKindTest = unitSbmlTest.getKind();

                assertEquals("Test kind of the unitSbml " + unitSbmlId, unitSbmlKindRef, unitSbmlKindTest);

                Double unitSbmlExpRef = unitSbmlRef.getExponent();
                Double unitSbmlExpTest = unitSbmlTest.getExponent();

                assertEquals("Test exponent of the unitSbml " + unitSbmlId, unitSbmlExpRef, unitSbmlExpTest);

                Double unitSbmlMultRef = unitSbmlRef.getMultiplier();
                Double unitSbmlMultTest = unitSbmlTest.getMultiplier();

                assertEquals("Test multiplier of the unitSbml " + unitSbmlId, unitSbmlMultRef, unitSbmlMultTest);

                Integer unitSbmlScaleRef = unitSbmlRef.getScale();
                Integer unitSbmlScaleTest = unitSbmlTest.getScale();

                assertEquals("Test scale of the unitSbml " + unitSbmlId, unitSbmlScaleRef, unitSbmlScaleTest);

            }
        }
    }

    public void testAttributesMetabolites() {
        Set<String> id_metabolitesRef = networkRef.getMetabolitesView().getIds();
        Set<String> id_metabolitesTest = networkTest.getMetabolitesView().getIds();
        assertEquals("Test ids of metabolites", id_metabolitesRef, id_metabolitesTest);

        for (String cpdId : id_metabolitesRef) {
            BioMetabolite cpdRef = networkRef.getMetabolite(cpdId);
            String cpdNameRef = cpdRef.getName();

            BioMetabolite cpdTest = networkTest.getMetabolite(cpdId);
            String cpdNameTest = cpdTest.getName();
            assertEquals("Test name of the metabolite " + cpdId, cpdNameRef, cpdNameTest);
//             TODO Decomment when the MetExplore Data will be ok
//            String formulaRef = cpdRef.getChemicalFormula();
//            String formulaTest = cpdTest.getChemicalFormula();
//            assertEquals("Test formula of the metabolite " + cpdId, formulaRef, formulaTest);

//            Double weightRef = cpdRef.getMolecularWeight();
//            Double weightTest = cpdTest.getMolecularWeight();
//
//
//            assertEquals("Test weight (float) of the metabolite " + cpdId, weightRef, weightTest);

            Boolean isGenericRef = MetaboliteAttributes.getGeneric(cpdRef);
            Boolean isGenericTest = MetaboliteAttributes.getGeneric(cpdTest);
            assertEquals("Test isGeneric of the metabolite " + cpdId, isGenericRef, isGenericTest);

            BioCollection<BioCompartment> cptsRef = networkRef.getCompartmentsOf(cpdRef);
            BioCollection<BioCompartment> cptsTest = networkTest.getCompartmentsOf(cpdTest);

            assertEquals("Test compartment of the metabolite " + cpdId, cptsRef.getIds(), cptsTest.getIds());

            Integer chargeRef = cpdRef.getCharge();
            Integer chargeTest = cpdTest.getCharge();
            assertEquals("Test charge of the metabolite " + cpdId, chargeRef, chargeTest);

        }

    }

    public void testAttributesReactions() {
        Set<String> id_reactionsRef = networkRef.getReactionsView().getIds();
        Set<String> id_reactionsTest = networkTest.getReactionsView().getIds();
        assertEquals("Test ids of reactions", id_reactionsRef, id_reactionsTest);

        for (String reactionId : id_reactionsRef) {
            BioReaction reactionRef = networkRef.getReaction(reactionId);
            BioReaction reactionTest = networkTest.getReaction(reactionId);

            String reactionNameRef = reactionRef.getName();
            String reactionNameTest = reactionTest.getName();
            assertEquals("Test name of the reaction " + reactionId, reactionNameRef, reactionNameTest);

            Boolean revRef = reactionRef.isReversible();
            Boolean revTest = reactionTest.isReversible();
            assertEquals("Test reversibility of the reaction " + reactionId, revRef, revTest);

            String ecRef = reactionRef.getEcNumber();
            String ecTest = reactionTest.getEcNumber();
            assertEquals("Test EC of the reaction " + reactionId, ecRef, ecTest);

            Boolean isHoleRef = ReactionAttributes.getHole(reactionRef);
            Boolean isHoleTest = ReactionAttributes.getHole(reactionTest);
            assertEquals("Test isHole of the reaction " + reactionId, isHoleRef, isHoleTest);

            Boolean isGenericRef = ReactionAttributes.getGeneric(reactionRef);
            Boolean isGenericTest = ReactionAttributes.getGeneric(reactionTest);
            assertEquals("Test isGeneric of the reaction " + reactionId, isGenericRef, isGenericTest);

            String typeRef = ReactionAttributes.getType(reactionRef);
            String typeTest = ReactionAttributes.getType(reactionTest);
            assertEquals("Test biocyc type of the reaction " + reactionId, typeRef, typeTest);

            Set<String> leftsRef = reactionRef.getLeftsView().getIds();
            Set<String> leftsTest = reactionTest.getLeftsView().getIds();
            assertEquals("Test left participants of the reaction " + reactionId, leftsRef, leftsTest);

            Set<String> rightsRef = reactionRef.getRightsView().getIds();
            Set<String> rightsTest = reactionTest.getRightsView().getIds();
            assertEquals("Test right participants of the reaction " + reactionId, rightsRef, rightsTest);

            BioCollection<BioMetabolite> cofactorsRef = ReactionAttributes.getSideCompounds(reactionRef);
            BioCollection<BioMetabolite> cofactorsTest = ReactionAttributes.getSideCompounds(reactionRef);
            assertEquals("Test cofactors of the reaction " + reactionId, cofactorsRef, cofactorsTest);

            Set<String> pathwaysRef = networkRef.getPathwaysFromReaction(reactionRef).getIds();
            Set<String> pathwaysTest = networkTest.getPathwaysFromReaction(reactionTest).getIds();
            assertEquals("Test pathways of the reaction " + reactionId, pathwaysRef, pathwaysTest);

            Set<String> enzymesRef = reactionRef.getEnzymesView().getIds();
            Set<String> enzymesTest = reactionTest.getEnzymesView().getIds();

            assertEquals("Test enzymes of the reaction " + reactionId, enzymesRef, enzymesTest);

        }
    }

    public void testAttributesGenes() {
        Set<String> genesRef = networkRef.getGenesView().getIds();
        Set<String> genesTest = networkTest.getGenesView().getIds();
        assertEquals("Test ids of genes ", genesRef, genesTest);

        for (String geneId : genesRef) {
            BioGene geneRef = networkRef.getGene(geneId);
            BioGene geneTest = networkTest.getGene(geneId);

            String geneNameRef = geneRef.getName();
            String geneNameTest = geneTest.getName();
            assertEquals("Test name of the gene " + geneId, geneNameRef, geneNameTest);

            Set<String> proteinsRef = networkRef.getProteinsView().getIds();
            Set<String> proteinsTest = networkTest.getProteinsView().getIds();
            assertEquals("Test proteins of the gene " + geneId, proteinsRef, proteinsTest);

        }
    }

    public void testAttributesProteins() {

        Set<String> proteinsRef = networkRef.getProteinsView().getIds();
        Set<String> proteinsTest = networkTest.getProteinsView().getIds();
        assertEquals("Test ids of proteins ", proteinsRef, proteinsTest);

        for (String proteinId : proteinsRef) {
            BioProtein proteinRef = networkRef.getProtein(proteinId);
            BioProtein proteinTest = networkTest.getProtein(proteinId);

            String proteinNameRef = proteinRef.getName();
            String proteinNameTest = proteinTest.getName();
            assertEquals("Test name of the protein " + proteinId, proteinNameRef, proteinNameTest);

            BioGene gene = proteinRef.getGene();
            if (gene != null) {
                assertEquals("Test gene of  " + proteinRef.getId(), proteinRef.getGene().getId(),
                        proteinTest.getGene().getId());
            } else {
                assertNull("Gene of " + proteinRef.getId() + " must be null", proteinTest.getGene());
            }
        }
    }

    public void testAttributeEnzymes() {
        Set<String> enzymesRef = networkRef.getEnzymesView().getIds();
        Set<String> enzymesTest = networkTest.getEnzymesView().getIds();
        assertEquals("Test ids of proteins ", enzymesRef, enzymesTest);

        for (String id : enzymesRef) {
            BioEnzyme enzymeRef = networkRef.getEnzyme(id);
            BioEnzyme enzymeTest = networkTest.getEnzyme(id);

            String nameRef = enzymeRef.getName();
            String nameTest = enzymeTest.getName();
            assertEquals("Test name of the enzyme " + id, nameRef, nameTest);

            Set<String> componentRefs = enzymeRef.getParticipantsView().getIds();
            Set<String> componentTests = enzymeTest.getParticipantsView().getIds();

            assertEquals("Test number of components of the enzyme " + id, componentRefs.size(), componentTests.size());

            assertEquals("Test ids of the components of the enzyme " + id, componentRefs, componentTests);

        }
    }

}
