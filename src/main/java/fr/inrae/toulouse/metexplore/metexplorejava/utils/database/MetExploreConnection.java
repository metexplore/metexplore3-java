/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************
 * @author Ludo Cottret
 * @date May 2, 2011
 * Create the connection to the MetExplore database.
 */
package fr.inrae.toulouse.metexplore.metexplorejava.utils.database;

import java.io.BufferedReader;
import java.io.Console;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Properties;

import fr.inrae.toulouse.metexplore.metexplorejava.utils.MetExploreResult;


public class MetExploreConnection {

    public String host = "";
    private Connection connection;
    private String url = "";
    private String database = "";
    private String user = "";
    private String password = "";

    /**
     * Connect to a database using a config File instead of hard coded credentials,  ini file format:
     * [descriptor]
     * key=value
     *
     * @param configFile
     * @throws IOException  IOException
     * @throws SQLException SQLException
     */
    public MetExploreConnection(String configFile) throws IOException, SQLException, ClassNotFoundException {

        HashMap<String, String> config = this.readConfigFile(configFile);

        this.host = config.get("MYSQL_HOST");
        this.database = config.get("MYSQL_DATABASE");

        this.setUrl("jdbc:mysql://" + this.host + ":3306/" + this.database + "?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");

        this.setUser(config.get("MYSQL_USER"));
        this.setPassword(config.get("MYSQL_PASS"));

        Properties connectionProps = new Properties();
        connectionProps.put("user", this.getUser());
        connectionProps.put("password", this.getPassword());

        try {
            this.connection = DriverManager.getConnection(this.getUrl(), connectionProps);
        }
        catch (SQLException ex) {
            // handle any errors
            System.out.println("SQLException: " + ex.getMessage());
            System.out.println("SQLState: " + ex.getSQLState());
            System.out.println("VendorError: " + ex.getErrorCode());
        }
        //this.setConcatMaxLength(100000000);
    }

    /**
     * Read a config file and fill a HashMap with the items
     * The config file must be formatted in this way:
     * VAR1=VALUE1
     * VAR2=VALUE2
     * @param filePath : the path of a file
     * @return a {@link HashMap}
     */
    private HashMap<String, String> readConfigFile(String filePath) {
        HashMap<String, String> configMap = new HashMap<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            String line;
            while ((line = reader.readLine()) != null) {
                String[] parts = line.split("\\s*=\\s*");
                if (parts.length == 2) {
                    String key = parts[0].trim();
                    String value = parts[1].trim().replaceAll("'","").replaceAll("\"", "");
                    configMap.put(key, value);
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return configMap;
    }

    /**
     * @param host
     * @param database
     * @param user
     * @param password
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public MetExploreConnection(String host, String database, String user, String password) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {

        this.setUrl("jdbc:mysql://" + host + ":3306/" + database);
        this.setUser(user);
        this.setPassword(password);

        Properties connectionProps = new Properties();
        connectionProps.put("user", this.getUser());
        connectionProps.put("password", this.getPassword());

        connection = DriverManager.getConnection(this.getUrl(), connectionProps);
    }

    /**
     * @param host
     * @param database
     * @param user
     * @throws ClassNotFoundException
     * @throws SQLException
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public MetExploreConnection(String host, String database, String user) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        this.setUrl("jdbc:mysql://" + host + ":3306/" + database);
        this.setUser(user);

        Console console = System.console();
        if (console == null) {
            System.out.println("Couldn't get Console instance");
            System.exit(0);
        }

        char passwordArray[] = console.readPassword("Enter your secret password: ");

        this.setPassword(new String(passwordArray));

        Properties connectionProps = new Properties();
        connectionProps.put("user", this.getUser());
        connectionProps.put("password", this.getPassword());

        connection = DriverManager.getConnection(this.getUrl(), connectionProps);
    }

    /**
     * Commit each change
     *
     * @return
     */
    public MetExploreResult commitChanges() {
        MetExploreResult result = new MetExploreResult();
        try {
            this.connection.commit();
        } catch (SQLException e) {
            result.setIntError(1);
            result.message = "Unable to commit the changes in the database. Please contact contact-metexplore@inrae.fr";
            return result;
        }

        result.setIntError(0);
        return result;
    }

    public void setAutoCommit(Boolean bool) throws SQLException {
        this.connection.setAutoCommit(bool);
    }

    public Connection getConnection() {
        return connection;
    }

    /**
     * @param connection the connection to set
     */
    public void setConnection(Connection connection) {
        this.connection = connection;
    }

    public String getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(String user) {
        this.user = user;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) {
        this.password = password;
    }

    public String getDatabase() {
        return database;
    }

    public void setDatabase(String database) {
        this.database = database;
    }

    public void close() throws SQLException {
        connection.close();
    }
}
