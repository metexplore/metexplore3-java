package fr.inrae.toulouse.metexplore.metexplorejava.utils.mail;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.net.ConnectException;
import java.util.Properties;

public class MetExploreMail {
    /* @author lcottret inspired by
     *         http://www.tutorialspoint.com/java/java_sending_email.htm
     *
     */
    public static Boolean sendMail(String from, String to, String subject,
                                   String body, String smtp) {

        Boolean flag = true;

        // Get system properties
        Properties properties = System.getProperties();

        // Setup mail server
        properties.setProperty("mail.smtp.host", smtp);

//        properties.setProperty("mail.smtp.port", "587");
//        properties.setProperty("mail.smtp.starttls.enable", "true");
        properties.setProperty("mail.debug", "true");
        // Get the default Session object.
        Session session = Session.getDefaultInstance(properties);

        try {
            // Create a default MimeMessage object.
            MimeMessage message = new MimeMessage(session);

            // Set From: header field of the header.
            message.setFrom(new InternetAddress(from));

            // Set To: header field of the header.
            message.addRecipient(Message.RecipientType.TO, new InternetAddress(
                    to));

            // Set Subject: header field
            message.setSubject(subject);

            // Now set the actual message
            message.setContent(body, "text/html");

            // Send message
            Transport.send(message);
            System.err.println("Sent message successfully....");
        } catch (Exception mex) {
            mex.printStackTrace();
            flag = false;
        }

        return flag;

    }

}
