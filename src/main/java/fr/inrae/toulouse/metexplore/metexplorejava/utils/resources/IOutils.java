/*******************************************************************************
 * Copyright INRA
 *
 *  Contact: ludovic.cottret@toulouse.inra.fr
 *
 *
 * This software is governed by the CeCILL license under French law and
 * abiding by the rules of distribution of free software.  You can  use,
 * modify and/ or redistribute the software under the terms of the CeCILL
 * license as circulated by CEA, CNRS and INRIA at the following URL
 * "http://www.cecill.info".
 *
 * As a counterpart to the access to the source code and  rights to copy,
 * modify and redistribute granted by the license, users are provided only
 * with a limited warranty  and the software's author,  the holder of the
 * economic rights,  and the successive licensors  have only  limited
 * liability.
 *  In this respect, the user's attention is drawn to the risks associated
 * with loading,  using,  modifying and/or developing or reproducing the
 * software by the user in light of its specific status of free software,
 * that may mean  that it is complicated to manipulate,  and  that  also
 * therefore means  that it is reserved for developers  and  experienced
 * professionals having in-depth computer knowledge. Users are therefore
 * encouraged to load and test the software's suitability as regards their
 * requirements in conditions enabling the security of their systems and/or
 * data to be ensured and,  more generally, to use and operate it in the
 * same conditions as regards security.
 *  The fact that you are presently reading this means that you have had
 * knowledge of the CeCILL license and that you accept its terms.
 ******************************************************************************/
package fr.inrae.toulouse.metexplore.metexplorejava.utils.resources;

import org.junit.rules.TemporaryFolder;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStream;


public class IOutils {

    /**
     * @param is
     * @return
     */
    public static String convertStreamToString(java.io.InputStream is) {

        java.util.Scanner scanner = new java.util.Scanner(is);
        java.util.Scanner s = scanner.useDelimiter("\\A");
        String res = s.hasNext() ? s.next() : "";
        scanner.close();
        return res;
    }


    /**
     * @param host
     */
    public static Boolean getFileFromUrl(String host, String filePath) {
        InputStream input = null;
        FileOutputStream writeFile = null;

        try {
            URL url = new URL(host);
            URLConnection connection = url.openConnection();
            int fileLength = connection.getContentLength();

            if (fileLength == -1) {
                System.err.println("Invalide URL or file.");
                return false;
            }

            input = connection.getInputStream();
            writeFile = new FileOutputStream(filePath);
            byte[] buffer = new byte[1024];
            int read;

            while ((read = input.read(buffer)) > 0)
                writeFile.write(buffer, 0, read);
            writeFile.flush();
        } catch (IOException e) {
            e.printStackTrace();
            System.err.println("Error while trying to download the file.");
            return false;
        } finally {
            try {
                writeFile.close();
                input.close();
            } catch (IOException e) {
                e.printStackTrace();
                return false;
            }
        }

        return true;

    }

    public static String copyProjectResource(String path, TemporaryFolder tempDirectory) throws IOException {
        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();

        InputStream stream = classLoader.getResourceAsStream(path);
        if (stream == null) {
            System.err.println("file not found");
            throw new IOException();
        }

        File fileList = tempDirectory.newFile();

        OutputStream outStream = new BufferedOutputStream(new FileOutputStream(fileList, true));
        byte[] bucket = new byte[32 * 1024];
        int bytesRead = 0;
        while (bytesRead != -1) {
            bytesRead = stream.read(bucket); //-1, 0, or more
            if (bytesRead > 0) {
                outStream.write(bucket, 0, bytesRead);
            }
        }

        outStream.close();
        stream.close();

        return fileList.getAbsolutePath();

    }

    public static String copyProjectResource(String path, File tempDirectory) throws IOException {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream stream = classLoader.getResourceAsStream(path);
        if (stream == null) {
            System.err.println("file not found");
            throw new IOException();
        }

        File file = new File(tempDirectory + File.pathSeparator + "test.tmp");

        OutputStream outStream = new BufferedOutputStream(new FileOutputStream(file, true));
        byte[] bucket = new byte[32 * 1024];
        int bytesRead = 0;
        while (bytesRead != -1) {
            bytesRead = stream.read(bucket); //-1, 0, or more
            if (bytesRead > 0) {
                outStream.write(bucket, 0, bytesRead);
            }
        }

        outStream.close();
        stream.close();

        return file.getAbsolutePath();

    }

    /**
     * Creates a temporary file in the default tmp directory and copy the content of the input file to this
     * tmp file.
     *
     * @param path
     * @return
     * @throws IOException
     */
    public static String copyProjectResourceToTmp(String path) throws IOException {

        ClassLoader classLoader = Thread.currentThread().getContextClassLoader();
        InputStream stream = classLoader.getResourceAsStream(path);
        if (stream == null) {
            System.err.println("file not found");
            throw new IOException();
        }

        File file = File.createTempFile("test_parseBioNet", "test.tmp");

        OutputStream outStream = new BufferedOutputStream(new FileOutputStream(file, true));
        byte[] bucket = new byte[32 * 1024];
        int bytesRead = 0;
        while (bytesRead != -1) {
            bytesRead = stream.read(bucket); //-1, 0, or more
            if (bytesRead > 0) {
                outStream.write(bucket, 0, bytesRead);
            }
        }

        outStream.close();
        stream.close();

        return file.getAbsolutePath();
    }

    public static BufferedReader readRessourcesFromJar(String path) {
        InputStream is = IOutils.class.getResourceAsStream(path);
        InputStreamReader isr = new InputStreamReader(is);
        BufferedReader br = new BufferedReader(isr);
        return br;
    }

}
