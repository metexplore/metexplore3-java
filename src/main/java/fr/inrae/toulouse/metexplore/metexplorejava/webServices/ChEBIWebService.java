package fr.inrae.toulouse.metexplore.metexplorejava.webServices;

import java.util.ArrayList;
import java.util.HashMap;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.ClientResponse;

public class ChEBIWebService extends ExtWebService {

    @Option(name = "-ChEBI", usage = "ChEBI identifier of the Compound", required = true)
    public String chebi;

    public ChEBIWebService(String Url, String id) {
        super(Url);
        this.setChebi(id);
    }


    ////////////////////////////////////////////////

    public ChEBIWebService(String URL) {
        super(URL);
    }

    public ChEBIWebService() {
        super();
    }

    ///////////////////////////////////////////////
    public static void main(String[] args) {

        ChEBIWebService ChEBIWS = new ChEBIWebService("http://www.ebi.ac.uk/webservices/chebi/2.0/test/");

        CmdLineParser parser = new CmdLineParser(ChEBIWS);
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            parser.printUsage(System.err);
            System.exit(0);
        }


        HashMap<String, ArrayList<String>> dataForJSon = ChEBIWS.getData();

        String output = ChEBIWS.toJSON(dataForJSon);

        System.out.println(output);


    }

    ////////////////////////////////////////////////

    private ArrayList<String> getDatafromXml(Document xmldata, String tag) {

        ArrayList<String> output = new ArrayList<>();

        NodeList nodes = xmldata.getElementsByTagName(tag);

        for (int i = 0, c = nodes.getLength(); i < c; i++) {
            Node entry = nodes.item(i);

            while (entry.getChildNodes().item(0).getNodeValue() == null) {
                entry = entry.getChildNodes().item(0);
            }
            output.add(entry.getChildNodes().item(0).getNodeValue());
        }

        return output;
    }


    @Override
    public HashMap<String, ArrayList<String>> getData() {

        HashMap<String, ArrayList<String>> dataForJSon = new HashMap<>();

        if (this.testConnection()) {

            String data = this.getWebservice().path("getCompleteEntity").queryParam("chebiId", this.getChebi()).get(String.class);


            Document xmldata = null;
            try {
                xmldata = this.loadXMLFromString(data);
            } catch (Exception e) {
                e.printStackTrace();
            }


            String[] Elements = new String[]{"chebiAsciiName", "smiles", "inchi", "inchiKey", "charge", "mass", "Formulae"};

            for (String tag : Elements) {
                assert xmldata != null;
                dataForJSon.put(tag, this.getDatafromXml(xmldata, tag));
            }
        }

        return dataForJSon;
    }


    @Override
    public String toJSON(HashMap<String, ArrayList<String>> dataForJSon) {

        JsonObject myObj = new JsonObject();
        Gson gson = new Gson();
        JsonElement jsonData = gson.toJsonTree(dataForJSon);


        if (dataForJSon.isEmpty()) {
            myObj.addProperty("success", false);

            if (this.statusCode == 200) {
                myObj.addProperty("message", "Invalid ChEBI ID");
            } else {
                myObj.addProperty("message", "ChEBi Web Service Unavailable (status not equal 200)");
            }
        } else {
            myObj.addProperty("success", true);
            myObj.add("data", jsonData);
        }

        return myObj.toString();
    }


    @Override
    public boolean testConnection() {

        if (this.chebi != null) {
            this.statusCode = this.getWebservice().path("getCompleteEntity").queryParam("chebiId", "1").get(ClientResponse.class).getStatus();
        } else {
            this.statusCode = this.getWebservice().path("getCompleteEntity").queryParam("chebiId", this.getChebi()).get(ClientResponse.class).getStatus();
        }

        return this.statusCode == 200;

    }


    public String getChebi() {
        return chebi;
    }

    public void setChebi(String chebi) {
        this.chebi = chebi;
    }

}
