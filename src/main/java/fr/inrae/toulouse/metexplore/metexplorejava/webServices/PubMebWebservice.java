package fr.inrae.toulouse.metexplore.metexplorejava.webServices;

import java.util.ArrayList;
import java.util.HashMap;

import org.kohsuke.args4j.CmdLineException;
import org.kohsuke.args4j.CmdLineParser;
import org.kohsuke.args4j.Option;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.sun.jersey.api.client.ClientResponse;

public class PubMebWebservice extends ExtWebService {

    @Option(name = "-PMID", usage = "PMID identifier of the article", required = true)
    public String PMID;


    public PubMebWebservice(String id) {
        super("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi");
        this.setPMID(id);
    }

    public PubMebWebservice() {
        super("https://eutils.ncbi.nlm.nih.gov/entrez/eutils/esummary.fcgi");
    }

    public static void main(String[] args) {

        PubMebWebservice pubMebWS = new PubMebWebservice();

        CmdLineParser parser = new CmdLineParser(pubMebWS);
        try {
            parser.parseArgument(args);
        } catch (CmdLineException e) {
            System.err.println(e.getMessage());
            parser.printUsage(System.err);
            System.exit(0);
        }


        HashMap<String, ArrayList<String>> dataForJSon = pubMebWS.getData();

        String output = pubMebWS.toJSON(dataForJSon);

        System.out.println(output);

    }

    @Override
    public HashMap<String, ArrayList<String>> getData() {


        HashMap<String, ArrayList<String>> dataForJSon = new HashMap<String, ArrayList<String>>();

        if (this.testConnection()) {

            String data = this.getWebservice().queryParam("db", "pubmed").queryParam("id", this.getPMID()).get(String.class);

            ArrayList<String> id = new ArrayList<String>();

            id.add(this.getPMID());
            dataForJSon.put("PMID", id);

            Document xmldata = null;
            try {
                xmldata = this.loadXMLFromString(data);
            } catch (Exception e) {
                e.printStackTrace();
            }

            String[] Elements = new String[]{"Author", "Title", "FulljournalName", "PubDate"};

            for (String name : Elements) {
                dataForJSon.put(name, this.getDatafromXmlNameAttribute(xmldata, name));
            }

        }

        return dataForJSon;
    }

    @Override
    protected String toJSON(HashMap<String, ArrayList<String>> data) {
        JsonObject myObj = new JsonObject();
        Gson gson = new Gson();
        JsonElement jsonData = gson.toJsonTree(data);


        if (data.isEmpty()) {
            myObj.addProperty("success", false);

            if (this.statusCode == 200) {
                myObj.addProperty("message", "Invalid PMID");
            } else {
                myObj.addProperty("message", "PubMed Service Unavailable (status =/= 200)");
            }
        } else {
            myObj.addProperty("success", true);
            myObj.add("data", jsonData);
        }

        return myObj.toString();

    }

    private ArrayList<String> getDatafromXmlNameAttribute(Document xmldata, String name) {

        ArrayList<String> output = new ArrayList<String>();

        NodeList nodes = xmldata.getElementsByTagName("Item");
        int nbNodes = nodes.getLength();

        for (int i = 0; i < nbNodes; i++) {
            Node entry = nodes.item(i);

            if (((Element) entry).getAttribute("Name").equalsIgnoreCase(name)) {
                if (name.equalsIgnoreCase("PubDate")) {
                    output.add(entry.getChildNodes().item(0).getNodeValue().substring(0, 4));
                } else {
                    output.add(entry.getChildNodes().item(0).getNodeValue());
                }
            }
        }

        return output;
    }

    @Override
    public boolean testConnection() {

        if (this.PMID != null) {
            this.statusCode = this.getWebservice().queryParam("db", "pubmed").queryParam("id", this.getPMID()).get(ClientResponse.class).getStatus();
        } else {
            this.statusCode = this.getWebservice().queryParam("db", "pubmed").queryParam("id", "1").get(ClientResponse.class).getStatus();
        }

//		System.err.println(this.statusCode);
//		System.err.println(this.getWebservice().queryParam("db", "pubmed").queryParam("id", "1").get(ClientResponse.class).getLocation());

        if (this.statusCode == 200) {
            return true;
        } else {
            return false;
        }

    }

    public String getPMID() {
        return PMID;
    }


    public void setPMID(String pMID) {
        PMID = pMID;
    }
}
