package fr.inrae.toulouse.metexplore.metexplorejava.refs;

import org.junit.Test;

import java.io.IOException;

import static org.junit.Assert.*;

public class IdentifiersOrgTest {

    @Test
    public void testConstructor() throws IOException {

        IdentifiersOrg ids = new IdentifiersOrg();

        assertTrue(ids.validIdentifiers.contains("kegg.compound"));

        assertTrue(ids.validIdentifiers.contains("bigg.metabolite"));

    }


}